<?php $perusahaanku=$this->m_perusahaan->perusahaanku();
//$marketingku=$this->m_marketing->marketingku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisiutama'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
//$clusterku=$this->m_cluster->clusterku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
//$perumahanku=$this->m_perumahan->perumahanku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
?><div id="boxindex" class="box">
  <div class="box-header">
    <h3 class="box-title">List Pengeluaran</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div id="filter" class="row">
    <h4 class="col-xs-12">Filer</h4>
    <div class="form-group col-xs-6 col-md-3">
      <label>Status</label>
      <select id="status" name="status" class="form-control filterinput">
        <option value="Semua" selected>Semua</option>
        <?php foreach ($model->liststatus() as $key => $status) {
          $dipilih=($status['status']=='Pending')?' selected':'';
          echo '<option value="'.$status['status'].'"'.$dipilih.'>'.$status['label'].'</option>';
        } ?>
      </select>
    </div>
    <div class="form-group col-xs-6 col-md-3">
      <label>Jumlah</label>
      <select id="perpage" name="perpage" class="form-control filterinput">
        <option value="25" selected>25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="300">100</option>
      </select>
    </div>
    <div class="form-group col-xs-6 col-md-3">
      <label>Urutkan</label>
      <select id="orderby" name="orderby" class="form-control filterinput">
        <option value="id_pengeluaran DESC" selected>Terbaru</option>
        <option value="id_pengeluaran ASC">Terlama</option>
      </select>
    </div>
    <!-- Date range -->
    <div class="form-group col-xs-6 col-md-3">
      <label>Tanggal</label>

      <div class="input-group">
        <div class="input-group-addon">
          <i class="fa fa-calendar"></i>
        </div>
        <input type="hidden" class="form-control pull-right" id="inputfiltertanggal">
        <input type="text" class="form-control pull-right" id="filtertanggal">
      </div>
      <!-- /.input group -->
    </div>
    <div class="form-group col-xs-6 col-md-3">
      <label>Nomor pengeluaran</label>
      <input type="text" class="form-control filterinput" name="no_pengeluaran" id="no_pengeluaran">
    </div>

    <div class="form-group col-xs-12">
      <button id="filterbutton" onclick="fitersubmit();" class="btn btn-primary pull-right"> Terapkan </button>
    </div>
  </div>
  <div class="box-table">
    <table id="tabelpengeluaran" class="tableapproval table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_pengeluaran' && $key!='diskon1_rp' && $key!='uang_muka') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      </thead>
      <tbody>
        <tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>
      </tbody>
      <tfoot>
      <tr>
        <th>No.</th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_pengeluaran' && $key!='diskon1_rp' && $key!='uang_muka') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      </tfoot>
    </table>
  </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-left">
      <?php /*<li><?=($this->keamanan->allowedaction['ubah'])?'<button id="btnubah" class="btn btn-success" onclick="ubahdata(false,true);">Ubah</button>':''; ?></li> */ ?>
    </ul>

    <ul class="pagination pagination-sm no-margin pull-right">
      <li><button id="btnprev" class="btn btn-default" onclick="changepage(false);">« Prev</button></li>
      <li><button id="btnnext" class="btn btn-default" onclick="changepage();">Next »</button></li>
    </ul>
  </div>
</div>
<!-- /.box -->

  <div id="boxapproval" class="box" style="display:none">
    <div class="box-header">
      <h3 class="box-title">Detail pengeluaran</h3><button type="button" class="close pull-right" onclick="$('#boxapproval').hide();$('#boxindex').show();dataajax();" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?php $this->load->view('pengeluaran/_form'); ?>
        <button id="btnapprove" type="button" class="btn btn-primary" onclick="approvedataajax('Approve')">Approve</button>
        <button id="btnreject" type="button" class="btn btn-danger" onclick="approvedataajax('Reject')">Reject</button>
  </div>
</div>

<script type="text/javascript">
var perpage = 25;
var page = 1;
<?php /* var listperusahaanku = <?=json_encode($perusahaanku)?>;
var listclusterku = <?=json_encode($clusterku)?>;
var listperumahanku = <?=json_encode($perumahanku)?>; */?>
var listkolom = <?=json_encode($model->kolom())?>;
var liststatus = [{'aktif':1,'id':'Aktif'},{'aktif':0,'id':'Tidak Aktif'}];
var listpengeluaran = [];
var hargajual = 0;
var angkaum=0;

function dataajax() {
  perpage=$("#perpage").val();
  $( "#tabelpengeluaran>tbody" ).html('<tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>');
  var request = $.ajax({
    url: "<?=base_url()?>ajax/datapengeluaran",
    method: "POST",
    data: {
      'status': $('#status').val(),
      'no_pengeluaran': $('#no_pengeluaran').val(),
      'rangetanggal': $('#inputfiltertanggal').val(),
      'lengkap': 1,
      'page': page,
      'perpage': perpage,
      'posisi': '<?=$_SESSION['posisi']?>',
      'orderby': $("#orderby").val(),
      'id_user': <?=$_SESSION['id_user']?>,
      'auth_key': '<?=$_SESSION['auth_key']?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    var hasil = '';
    console.log(datahasil);
    $.each(datahasil, function( index, pengeluaran ) {
      hasil +='<tr onclick="bukaformubah('+pengeluaran["id_pengeluaran"]+')">';<?php
      $hapus='';
      echo 'hasil +=\'<td>\'+(((page-1)*perpage)+index+1)+\'</td>\';';
      foreach ($model->kolom() as $key => $value) {
        if ($key!='id_pengeluaran') {
          if ($key=='id_kwitansi') {
            echo 'pengeluaran["'.$key.'teks"]=\'<a class="popupsingle_image" data-fancybox="listkwitansi" href="'.base_url().'assets/images/kwitansi/\'+pengeluaran["id_perusahaan"]+\'/\'+pengeluaran["slug_kwitansi"]+\'"><img class="img-responsive" style="width:100px;" src="'.base_url().'assets/images/kwitansi/\'+pengeluaran["id_perusahaan"]+\'/\'+pengeluaran["slug_kwitansi"]+\'" /></a>\';';
            echo 'pengeluaran["text_kwitansi"]=pengeluaran["no_kwitansi"];';
            $key = $key.'teks';
          } else if ($key=='tanggal') {
              echo 'pengeluaran["'.$key.'teks"]=formatDate(new Date(pengeluaran["'.$key.'"]));';
              $key = $key.'teks';
          } else if ($key=='harga_satuan'||$key=='total') {
            echo 'pengeluaran["'.$key.'teks"]=Math.trunc(pengeluaran["'.$key.'"]);';
            echo 'pengeluaran["'.$key.'teks"]=\'Rp. \'+pengeluaran["'.$key.'teks"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");';
            $key = $key.'teks';
          } else {
            if ($key=='id_cluster') {echo 'pengeluaran["text_cluster"]=pengeluaran["nama_cluster"];';$key = 'text_cluster';}
            if ($key=='id_perumahan') {echo 'pengeluaran["text_perumahan"]=pengeluaran["nama_perumahan"];';$key = 'text_perumahan';}
            if ($key=='id_user_input') {echo 'pengeluaran["'.$key.'teks"]=pengeluaran["nama_depan_user_input"];';$key = $key.'teks';}
            if ($key=='id_user_approve') {echo 'pengeluaran["'.$key.'teks"]=pengeluaran["nama_depan_user_approve"];';$key = $key.'teks';}
          }
          echo 'hasil +=\'<td>\'+pengeluaran["'.$key.'"]+\'</td>\';';
        }
      }
      ?>
      hasil +='</tr>';
      listpengeluaran[pengeluaran["id_pengeluaran"]]=pengeluaran;
    });
    $( "#tabelpengeluaran>tbody" ).html(hasil);
    if (datahasil.length<perpage) $('#btnnext').attr("disabled", true); else $('#btnnext').attr("disabled", false);
    if (page<=1) $('#btnprev').attr("disabled", true); else $('#btnprev').attr("disabled", false);
    ubahdata(true);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}

<?php if ($this->keamanan->allowedaction['ubah']) { ?>
  function updatedataajax() {
    var request = $.ajax({
      url: "<?=base_url()?>ajax/updatedatapengeluaran",
      method: "POST",
      data: $("#tabelpengeluaran .databerubah").serialize(),
      dataType: "json"
    });

    request.done(function( datahasil ) {
      console.log(datahasil);
      dataajax();
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      dataajax();
    });
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
  function resettambah(datas=listkolom) {
    $.each(datas, function( index, data ) {
      $('#input'+index).val(data["default"]);
    });
  }
  function bukaformtambah(){
    resettambah();
    $('#boxindex').hide();$('#boxapproval').show();$('.boxapproval input.form-control').first().focus();
  }
function resetformtambah(){
  $('.formsatuan').remove();
  $('.formjumlah>.input-group').append('<div class="input-group-addon" style="border: 0;padding: 0;width: 40%;"><input id="inputsatuan" type="text" class="form-control datatambah" name="satuan" value="" required="" placeholder="Satuan"></div>');
  $('#inputtotal').prop('readonly',true);
  $('#inputid_cluster').attr({'name':'text_cluster','id':'inputtext_cluster'});
  $('.formid_cluster .input-group').append('<input type="hidden" name="id_cluster" class="datatambah" id="inputid_cluster">');
  $('#inputid_perumahan').attr({'name':'text_perumahan','id':'inputtext_perumahan'});
  $('.formid_perumahan .input-group').append('<input type="hidden" name="id_perumahan" class="datatambah" id="inputid_perumahan">');

  $('#inputharga_satuan').attr({'name':'harga_satuanteks','id':'inputharga_satuanteks'});
  $('.formharga_satuan .input-group').append('<input type="hidden" name="harga_satuan" class="datatambah" id="inputharga_satuan">');
  $('#inputtotal').attr({'name':'totalteks','id':'inputtotalteks'});
  $('.formtotal .input-group').append('<input type="hidden" name="total" class="datatambah" id="inputtotal">');

  $('#inputid_kwitansi').attr({'name':'text_kwitansi','id':'inputtext_kwitansi'});
  $('#boxapproval .formtanggal').after($('.formid_kwitansi'));
  $('#modaltambahkwitansi #formpengeluaran').prepend('<div class="form-group col-xs-6 formimageFile">'+htmlinputimage('imageFile','')+'</div>');
  $('#boxapproval .formid_kwitansi .input-group').append('<input type="hidden" name="id_kwitansi" class="datatambah" id="inputid_kwitansi">');
  $('#modaltambahkwitansi .formid_kwitansi .input-group').append('<input type="hidden" name="id_kwitansi" class="datatambah" id="inputid_kwitansi">');
  $('#boxapproval .formid_kwitansi').append('<a id="popupsingle_image" data-fancybox="kwitansi" href="<?=base_url()?>assets/images/default/kwitansi.jpg"><img class="img-responsive" style="width:100%;" src="<?=base_url()?>assets/images/default/kwitansi.jpg" /></a>');
  $('#modaltambahkwitansi .formid_kwitansi').append('<a id="popupsingle_imagebaru" data-fancybox="kwitansibaru" href="<?=base_url()?>assets/images/default/kwitansi.jpg"><img class="img-responsive" style="width:100%;" src="<?=base_url()?>assets/images/default/kwitansi.jpg" /></a>');
  $('#modaltambahkwitansi #inputid_kwitansi').attr('name','id_kwitansibaru');
  $('#inputharga_satuan,#inputjumlah').attr({'min':'0','onkeyup':'hitunghargatotal()','onchange': 'hitunghargatotal()'});
  $('#boxapproval input[type="number"]').prop('type','text');

  $('#boxapproval .box-header>.box-title').after('<span class="statuspengeluaran"><h4 style="margin:2px" class="label label-primary"></h4></span>');

  $('#boxapproval .form-group:last').after('<div class="form-group col-xs-4 formuserinput"><label>Diinput Oleh</label><div class="input-group"><div class="input-group-addon"><i class="fa fa-user"></i></div><input id="inputid_user_inputteks" type="text" class="form-control datatambah" name="id_user_inputteks" value="" required="" onkeydown="inputenter(\'tambah\',event)" readonly=""></div></div>');
}
function approvedataajax(status='approve') {
  var request = $.ajax({
    url: "<?=base_url()?>ajax/approvepengeluaran",
    method: "POST",
    data: {
      'id_pengeluaran': $('#inputid_pengeluaran').val(),
      'status': status,
      '_csrf': '<?=$this->keamanan->generatecsrf()?>',
      'posisi': '<?=$_SESSION['posisi']?>',
      'id_user': <?=$_SESSION['id_user']?>,
      'auth_key': '<?=$_SESSION['auth_key']?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    console.log(datahasil);
    $('#boxapproval').hide();$('#boxindex').show();
    dataajax();
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( 'Mohon Cek Kembali Data Anda');
  });
}
window.addEventListener('DOMContentLoaded', (event) => {
  resetformtambah();
    <?php if ($id_cluster!=0) {
      echo 'bukaformtambah();pilihcluster('.$id_cluster.');';
    } ?>
});

<?php } ?>
function hitunghargatotal() {
  $('#inputtotal').val(parseInt($('#inputjumlah').val())*parseInt($('#inputharga_satuan').val()));
}
function bukaformubah(id_pengeluaran) {
  $.each(listpengeluaran[id_pengeluaran], function( index, pengeluaran ) {
    $('#input'+index).val(pengeluaran);
  });
  var linkkwitansi = '<?=base_url()?>assets/images/kwitansi/<?=$_SESSION['id_perusahaan']?>/'+listpengeluaran[id_pengeluaran]["slug_kwitansi"];
  $('.formid_kwitansi #popupsingle_image').attr('href',linkkwitansi);
  $('.formid_kwitansi #popupsingle_image>img').attr('src',linkkwitansi);
  $('#boxapproval .form-control').prop('readonly', true);
  if (listpengeluaran[id_pengeluaran]['status']!='Pending') {
    $('#boxapproval .btn').hide();
    $('#boxapproval .statuspengeluaran').html('<h4 style="margin:2px" class="label label-primary">'+listpengeluaran[id_pengeluaran]['status']+'</h4>');
  } else {
    $('#boxapproval .btn').show();
    $('#boxapproval .statuspengeluaran').html('');
  }
  $('#boxindex').hide();
  $('#boxapproval').show();
}
<?php if ($this->keamanan->allowedaction['tambah'] || $this->keamanan->allowedaction['ubah']) { ?>
function openmodalid_cluster(){
  $('#modalinputid_cluster').modal();
  if (!$.fn.DataTable.isDataTable('#tabellistcluster')){
    $('#tabellistcluster').DataTable();
  }
}
function openmodalid_perumahan(){
  $('#modalinputid_perumahan').modal();
  if (!$.fn.DataTable.isDataTable('#tabellistperumahan')){
    $('#tabellistperumahan').DataTable();
  }
}

function openmodalid_kwitansi(){
  if ($('#inputtext_kwitansi').val()!=''&&$('#inputid_kwitansi').val()==0) {
    $('#modaltambahkwitansi').modal();
  } else {
    $('#modalinputid_kwitansi').modal();
    if (!$.fn.DataTable.isDataTable('#tabellistkwitansi')){
      $('#tabellistkwitansi').DataTable();
    }
  }
}

<?php } ?>
window.addEventListener('DOMContentLoaded', (event) => {
$('#filtertanggal').daterangepicker(
  {
    ranges   : {
      'Hari Ini'       : [moment(), moment()],
      'Kemarin'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      '7 Hari Terakhir' : [moment().subtract(6, 'days'), moment()],
      '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
      'Bulan Ini'  : [moment().startOf('month'), moment().endOf('month')],
      'Bulan Lalu'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    startDate: moment().subtract(29, 'days'),
    endDate  : moment()
  },
  function (start, end) {
    $('#inputfiltertanggal').val('\''+start.format('YYYY-MM-DD') + '\' Hingga \'' + end.format('YYYY-MM-DD')+'\'')
  }
);
$('#inputfiltertanggal').val('\''+moment().subtract(29, 'days').format('YYYY-MM-DD') + '\' Hingga \'' + moment().format('YYYY-MM-DD')+'\'');

});
</script>
