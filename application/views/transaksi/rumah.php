<?php $perusahaanku=$this->m_perusahaan->perusahaanku();
//$marketingku=$this->m_marketing->marketingku(['id_userutama'=>$_SESSION['id_user'],'lengkap'=>true,'posisiutama'=>$_SESSION['posisi'],'auth_keyutama'=>$_SESSION['auth_key']]);
//$unitku=$this->m_unit->unitku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
//$customerku=$this->m_customer->customerku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
?>
<script type="text/javascript">
var textareatransaksi=null;
var datatemplatetransaksi=[];
var id_template_dokumen=0;
</script>
<div id="boxindex" class="box">
  <div class="box-header">
    <h3 class="box-title">List Transaksi</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <?=($this->keamanan->allowedaction['tambah'])?'<button class="btn btn-primary" onclick="bukaformtambah()">Tambah</button>':''; ?>
    <button onclick="$('#filter').toggle(300);" class="btn btn-primary pull-right"><i class="fa fa-gear"></i></button>
    <div id="filter" class="row" style="display:none">
    <h4 class="col-xs-12">Filer</h4>
    <div class="form-group col-xs-6 col-md-3">
      <label>Status</label>
      <select id="status" name="status" class="form-control filterinput">
        <option value="Semua" selected>Semua</option>
        <?php foreach ($model->liststatus() as $key => $status) {
          echo '<option value="'.$status['status'].'"'.$dipilih.'>'.$status['label'].'</option>';
        } ?>
      </select>
    </div>
    <div class="form-group col-xs-6 col-md-3">
      <label>Jumlah</label>
      <select id="perpage" name="perpage" class="form-control filterinput">
        <option value="25" selected>25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="300">100</option>
      </select>
    </div>
    <div class="form-group col-xs-6 col-md-3">
      <label>Urutkan</label>
      <select id="orderby" name="orderby" class="form-control filterinput">
        <option value="id_pesanan DESC" selected>Terbaru</option>
        <option value="id_pesanan ASC">Terlama</option>
      </select>
    </div>
    <!-- Date range -->
    <div class="form-group col-xs-6 col-md-3">
      <label>Tanggal</label>

      <div class="input-group">
        <div class="input-group-addon">
          <i class="fa fa-calendar"></i>
        </div>
        <input type="hidden" class="form-control pull-right" id="inputfiltertanggal">
        <input type="text" class="form-control pull-right" id="filtertanggal">
      </div>
      <!-- /.input group -->
    </div>
    <div class="form-group col-xs-6 col-md-3">
      <label>Nomor Pesanan</label>
      <input type="text" class="form-control filterinput" name="no_pesanan" id="no_pesanan">
    </div>

    <div class="form-group col-xs-12">
      <button id="filterbutton" onclick="$('#filter').toggle(300);fitersubmit();" class="btn btn-primary pull-right"> Terapkan </button>
    </div>
  </div>
  <div class="box-table">
    <table id="tabeltransaksi" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <th></th>
        <?php
        foreach ($model->kolomkecil() as $key => $value) {
          if ($key!='id_pesanan' && $key!='diskon1_rp' && $key!='uang_muka') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      <?php if ($this->keamanan->allowedaction['ubah']) { ?>
      <tr style="display:none"><td><input type="hidden" name="id_userutama" class="databerubah" value="<?=$_SESSION['id_user']?>"></td><td><input type="hidden" name="auth_keyutama" class="databerubah" value="<?=$_SESSION['auth_key']?>">
      <input type="hidden" name="_csrf" class="databerubah" value="<?=$this->keamanan->generatecsrf()?>"></td></tr>
    <?php } ?>
      </thead>
      <tbody>
        <tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>
      </tbody>
      <tfoot>
      <tr>
        <th>No.</th>
        <th></th>
        <?php
        foreach ($model->kolomkecil() as $key => $value) {
          if ($key!='id_pesanan' && $key!='diskon1_rp' && $key!='uang_muka') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      </tfoot>
    </table>
  </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-left">
      <?php /*<li><?=($this->keamanan->allowedaction['ubah'])?'<button id="btnubah" class="btn btn-success" onclick="ubahdata(false,true);">Ubah</button>':''; ?></li> */ ?>
    </ul>

    <ul class="pagination pagination-sm no-margin pull-right">
      <li><button id="btnprev" class="btn btn-default" onclick="changepage(false);">« Prev</button></li>
      <li><button id="btnnext" class="btn btn-default" onclick="changepage();">Next »</button></li>
    </ul>
  </div>
</div>
<!-- /.box -->

<div class="modal fade" id="modalcicilanindex">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Detail Cicilan</h4>
      </div>
      <div class="modal-body">
        <p>Jumlah yang Harus Dibayar <b id="cicilanjmldibayar">0</b></p>
        <div style="clear:both"></div>
        <?php $this->load->view('transaksi/_simulasicicilan'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div id="boxcetaktandaterima" class="box" style="display:none">
  <div id="cetaktandaterima">
      <div style="text-align:center"><h2>Tanda Terima Sementara</h2></div><div style="padding:30px"><table style="width:100%"><tbody><tr><td style="width:150px;">Diterima dari</td><td colspan="4">: <span class="kwitansinama_customer">........................................................................................................................................................................</span></td></tr><tr><td>Alamat</td><td colspan="2">: <span class="kwitansialamat_customer">..............................</span></td><td style="width:50px">Telp</td><td>: <span class="kwitansitelp_customerall">...............</span></td></tr><tr><td>No. Unit/Type</td><td colspan="2">: <span class="kwitansinomor">...</span> Tipe <span class="kwitansinama_tipe">...</span></td><td>SP No.</td><td>: <span class="kwitansino_pesanan">....................</span></td></tr><tr><td>Jumlah</td><td colspan="4">: <span class="kwitansiterbilangum">.........................................................................................................................................................</span></td></tr><tr><td>Jenis Setoran</td><td colspan="4">: a. Transfer</td></tr><tr><td style="width:100px;"></td><td style="width:170px;"> &nbsp; b. Cheque/ BG Bank</td><td style="width:300px;">: ...........</td><td style="width:50px;">No.</td><td>: .................................................</td></tr><tr><td style="width:100px;"></td><td> &nbsp; c. Credit Card</td><td colspan="3">: ........................................................................................................</td></tr><tr><td>Pembayaran</td><td colspan="4">: Uang Pesanan / <span><strike>Uang Muka / Angsuran Ke</strike></span> </td></tr></tbody></table><table style="width: 100%; margin-top:20px"><tbody><tr><td style="width:40%" colspan="2">..............................................., <span ><?=date('d F Y')?></span></td><td style="width:60%;border:2px solid black;vertical-align: top;padding: 10px;" rowspan="2">
      <h4 style="margin:0">PERHATIAN!!</h4>
  <ul type="-" style="padding-left:20px">
      <li>Tanda Terima Sementara Ini bukan merupakan bukti pesanan</li><li>Pesanan baru sah setelah dibuat surat pesanan</li><li>Tanda Terima Sementara ini merupakan bagian yang tak terpisahkan dari syarat-syarat pada reservation Form/Surat Pesanan/PPJB</li><li>Tanda terima ini hanya bersifat sementara dan tidak berlaku sebagai kwitansi resmi</li>
  </ul></td></tr><tr><td style="text-align:center;padding-top:30px">Pemesan<br><br><br><p>( <span class="kwitansinama_customer">.................................</span> )</p></td><td style="text-align:center;padding-top:30px">Penerima<br><br><br><p>( ................................. )</p></td></tr></tbody></table><div style="border:2px solid black;margin-top:20px;text-align:center;padding:5px"><b>Tanda Terima Sementara ini dapat digantikan dengan kwitansi resmi jika sudah di cek oleh Developer Keabsahannya</b></div></div>

  </div>
</div>

<div id="boxcetakkwitansi" class="box" style="display:none">
  <div id="cetakkwitansi">
<div style="float:left"><p style="margin:0"><b>Kwitansi Uang Tanda Jadi</b></p><p style="margin:0"><b>No. <span class="kwitansino_pesanan">NO-PESANAN</span></b></p></div>

<h4 style="text-align:right;margin:10px"><span class="kwitansinama_perumahan">Nama Perumahan</span></h4><div style="width:100%;margin-top:10px;clear:both;"><div style="width:60%;float:left"> <table style="width:100%;border:2px solid black"><tbody><tr><td colspan="2">Sudah Terima Dari Tn/Ny</td></tr><tr><td>Nama</td><td>: <span class="kwitansinama_customer">Mochamad Saiful Umam</span></td></tr><tr><td>Alamat</td><td>: <span class="kwitansialamat_customer">Alamat Customer</span></td></tr><tr><td>No. Telp</td><td>: <span class="kwitansitelp_customerall">Telp Customer</span></td></tr></tbody></table></div><div style="width:38%;float:right"> <table style="width:100%;border:2px solid black"><tbody><tr><td><span class="kwitansinama_perusahaan">Nama Perusahaan</span><br><span class="kwitansialamat_perusahaan">Alamat Perusahaan</span><br>Telp <span class="kwitansitelp_perusahaan">Telp Perusahaan</span></td></tr></tbody></table></div>
<div style="clear:both"></div><div style="border:2px solid black;margin-top:10px;padding:5px;clear:both;font-size:14px;">Banyaknya Uang: <span class="kwitansiterbilangum">Terbilang Jumlah Uang</span></div><div style="border:2px solid black;margin-top:10px;padding:5px;clear:both;font-size:14px;">Untuk Pembayaran: <span class="kwitansi">Uang Tanda Jadi</span> Unit <span class="kwitansinomor">No. Unit</span> Tipe <span class="kwitansinama_tipe">Nama Tipe</span></div><div style="width:30%;float:left;border:2px solid black;margin-top:10px;padding:5px;font-size:14px;"><span class="kwitansiuang_tanda_jadirupiah">Nominal Uang</span></div><div style="width:40%;float:right;font-size:14px;margin-top:10px;"><span class="kwitansikota_perumahan">Kota Perumahan</span>, <span ><?=date('d F Y')?></span><br><br><br><br><br>Accounting</div><div style="width:60%;float:right;font-size:14px;margin-top:10px;"><br><br><br><div style="padding-right:30px">Pembayaran dengan Cheque/Giro Harus ditujukan langsung Atas Nama <span class="kwitansinama_perusahaan">Nama Perusahaan</span> dan baru dianggap sah setelah dapat diuangkan (kliring)</div></div></div>

</div>
</div>
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
  <div id="boxtambah" class="box" style="display:none">
    <div class="box-header">
      <h3 class="box-title">Tambah Transaksi</h3><button type="button" class="close pull-right" onclick="$('#boxtambah').hide();$('#boxindex').show();dataajax();" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <?php $this->load->view('transaksi/_form'); ?>
      <button id="btntambah" type="button" class="btn btn-primary" onclick="tambahdataajax()">Simpan</button>
  </div>
</div>



<div class="modal fade" id="modalsimulasicicilan">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Simulasi Cicilan</h4>
      </div>
      <div class="modal-body">
        <p>Jumlah yang Harus Dibayar <b id="simulasijmldibayar">0</b></p>
        <div style="clear:both"></div>
        <?php $this->load->view('transaksi/_simulasicicilan'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php }
if ($this->keamanan->allowedaction['tambah'] || $this->keamanan->allowedaction['ubah']) { ?>
<div class="modal fade" id="modalinputid_unit">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Unit List <?=$_SESSION['nama_perusahaan']?></h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('unit/_listlengkap'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modalinputid_customer">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Customer List <?=$_SESSION['nama_perusahaan']?></h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('customer/_listlengkap'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modalinputid_marketing">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Marketing List <?=$_SESSION['nama_perusahaan']?></h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('marketing/_listlengkap'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<!-- /.modal -->
<?php } ?>


<div class="modal fade" id="modaltemplatedokumen">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Pilih Template</h4>
      </div>
      <div class="modal-body">
          <?php $this->load->view('template_dokumen/_listlengkap',['jenis_template'=>'transaksi']); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<div id="boxdetail" class="box" style="display:none">
  <div class="box-header">
    <h3 class="box-title">Template SPR</h3><button type="button" class="close pull-right" onclick="$('#boxdetail').hide();$('#boxindex').show();dataajax();" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <textarea id="textareatransaksi" rows="10" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
  </div>
  <div class="box-footer">
    <button type="button" class="btn btn-primary pull-right" onclick="cetakdokumen()">Cetak</button>
  </div>

</div>
<script type="text/javascript">
var perpage = 25;
var page = 1;
<?php /* var listperusahaanku = <?=json_encode($perusahaanku)?>;
var listunitku = <?=json_encode($unitku)?>;
var listcustomerku = <?=json_encode($customerku)?>; */?>
var listkolom = <?=json_encode($model->kolom())?>;
var listtransaksi = [];
var liststatus = [{'aktif':1,'id':'Aktif'},{'aktif':0,'id':'Tidak Aktif'}];
var listjenisbayar = [{'jenis_bayar':'Cash'},{'jenis_bayar':'InHouse'},{'jenis_bayar':'KPR'}];
var htmllistcicilan = '';
var hargajual = 0;
var angkaum=0;

function dataajax() {
  perpage=$("#perpage").val();
  $( "#tabeltransaksi>tbody" ).html('<tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>');
  var request = $.ajax({
    url: "<?=base_url()?>ajax/datatransaksi",
    method: "POST",
    data: {
      'status': $('#status').val(),
      'no_pesanan': $('#no_pesanan').val(),
      'rangetanggal': $('#inputfiltertanggal').val(),
      'lengkap': 1,
      'alldataselect' : true,
      'page': page,
      'perpage': perpage,
      'posisi': '<?=$_SESSION['posisi']?>',
      'orderby': $("#orderby").val(),
      'id_userutama': <?=$_SESSION['id_user']?>,
      'auth_keyutama': '<?=$_SESSION['auth_key']?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    var hasil = '';
    console.log(datahasil);
    $.each(datahasil, function( index, transaksi ) {
      hasil +='<tr><?php
      $hapus='';
      echo '<td>\'+(((page-1)*perpage)+index+1)+\'</td><td style="min-width:0px">\';';
      echo 'if (transaksi["status"]==\'Approve\') hasil +=\'<i class="btn-link fa fa-print" style="cursor:pointer;width:95px" onclick="bukapilihantemplate(\'+transaksi[\'id_pesanan\']+\')"> SPR</i> <i class="btn-link fa fa-print" style="cursor:pointer;width:95px" onclick="cetaktandaterima(\'+transaksi[\'id_pesanan\']+\')"> Tanda Terima</i> <i class="btn-link fa fa-print" style="cursor:pointer;width:95px" onclick="cetakkwitansi(\'+transaksi[\'id_pesanan\']+\')"> Kwitansi</i>\';';
      echo 'hasil +=\'</td>';
      foreach ($model->kolomkecil() as $key => $value) {
        if ($key!='id_pesanan' && $key!='diskon1_rp' && $key!='uang_muka') {
          if ($key=='id_unit') {
            echo '<td>\'+transaksi["nomor"]+\' Tipe \'+transaksi["nama_tipe"]+\'</td>';
          } else if ($key=='no_pesanan') {
            echo '<td><button class="btn-link" onclick="datatransaksidetail(\'+transaksi["id_pesanan"]+\');$( "#modalcicilanindex" ).modal();">\'+transaksi["'.$key.'"]+\'</button></td>';
          } else if ($key=='harga_jual'||$key=='cashback'||$key=='harga_net'||$key=='uang_tanda_jadi'||$key=='uang_muka_rp') {
            echo '\'; transaksi["'.$key.'"]=Math.trunc(transaksi["'.$key.'"]);transaksi["'.$key.'rupiah"]=\'Rp. \'+transaksi["'.$key.'"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");';
            echo ' hasil +=\'<td>\'+transaksi["'.$key.'rupiah"]+\'</td>';
          } else if ($key=='diskon1') {
            echo '\'; transaksi["'.$key.'"]=Math.trunc(transaksi["'.$key.'"]);';
            echo ' hasil +=\'<td>\'+transaksi["'.$key.'"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+\'%</td>';
          } else {
            if ($key=='id_marketing') $key='nama_marketing';
            if ($key=='id_customer') $key='nama_customer';
            if ($key=='id_user_approve') $key='nama_depan_approve';
            echo '<td>\'+transaksi["'.$key.'"]+\'</td>';
          }
        }
      }
      // echo '<td>\'+formdiindex(\'hidden\',transaksi["id_pesanan"],\'id_pesanan\',(((page-1)*perpage)+index+1))+\'</td>';
      // echo '<td>\'+formdiindex(\'text\',transaksi["id_pesanan"],\'no_pesanan\',transaksi["no_pesanan"])+\'</td>';
      // echo '<td>\'+formdiindex(\'pilihanpopup\',transaksi["id_pesanan"],\'id_unit\',transaksi["id_unit"])+\'</td>';
      // echo '<td>\'+formdiindex(\'pilihanpopup\',transaksi["id_pesanan"],\'id_customer\',transaksi["id_customer"])+\'</td>';
      // echo '<td>\'+formdiindex(\'date\',transaksi["id_pesanan"],\'tanggal\',transaksi["tanggal"])+\'</td>';
      // echo '<td>\'+formdiindex(\'text\',transaksi["id_pesanan"],\'alamat_surat_menyurat\',transaksi["alamat_surat_menyurat"])+\'</td>';
      // echo '<td>\'+formdiindex(\'select\',transaksi["id_pesanan"],\'jenis_bayar\',transaksi["jenis_bayar"],listjenisbayar,\'jenis_bayar\')+\'</td>';
      // echo '<td>\'+formdiindex(\'number\',transaksi["id_pesanan"],\'lama_cicilan\',transaksi["lama_cicilan"])+\'</td>';
      // echo '<td>\'+formdiindex(\'number\',transaksi["id_pesanan"],\'harga_jual\',transaksi["harga_jual"])+\'</td>';
      // echo '<td>\'+formdiindex(\'select\',transaksi["id_pesanan"],\'aktif\',transaksi["aktif"],liststatus,\'id\')+\'</td>';
/*
      'id_pesanan'=> ['label'=>'ID transaksi','default'=>0,'type'=>'text'],
      'no_pesanan'=> ['label'=>'No. Pesanan','default'=>'','type'=>'text'],
      'id_unit'=> ['label'=>'Unit','default'=>'','type'=>'pilihanpopup'],
      'id_customer'=> ['label'=>'Customer','default'=>0,'type'=>'select'],
      'tanggal'=> ['label'=>'Tanggal','default'=>date('Y-m-d'),'type'=>'date'],
      'alamat_surat_menyurat'=> ['label'=>'Alamat Surat Menyurat','default'=>'','type'=>'textarea'],
      'cara_bayar'=> ['label'=>'Cara Bayar','default'=>'','type'=>'text'],
      'lama_cicilan'=> ['label'=>'Lama Cicilan','default'=>0,'type'=>'number'],
      'harga_jual'=> ['label'=>'Harga Jual','default'=>0,'type'=>'number'],
      'nama_diskon'=> ['label'=>'Nama Diskon','default'=>'','type'=>'text'],
      'diskon1'=> ['label'=>'Diskon 1','default'=>0,'type'=>'number'],
      'diskon2'=> ['label'=>'Diskon 2','default'=>0,'type'=>'number'],
      'diskon3'=> ['label'=>'Diskon 3','default'=>0,'type'=>'number'],
      'diskon1_rp'=> ['label'=>'Diskon Rupiah 1','default'=>0,'type'=>'number'],
      'diskon2_rp'=> ['label'=>'Diskon Rupiah 1','default'=>0,'type'=>'number'],
      'diskon3_rp'=> ['label'=>'Diskon Rupiah 1','default'=>0,'type'=>'number'],
      'cashback'=> ['label'=>'Cashback','default'=>0,'type'=>'number'],
      'harga_net'=> ['label'=>'Harga NET','default'=>0,'type'=>'number'],
      'uang_tanda_jadi'=> ['label'=>'Uang Tanda Jadi','default'=>0,'type'=>'number'],
      'uang_muka'=> ['label'=>'Uang Muka','default'=>0,'type'=>'number'],
      'uang_muka_rp'=> ['label'=>'Uang Muka','default'=>0,'type'=>'number'],
      'catatan'=> ['label'=>'Catatan','default'=>'','type'=>'text'],
      'id_marketing'=> ['label'=>'Nama Marketing','default'=>'','type'=>'select'],
      'tanggal_approve'=> ['label'=>'Tanggal Approve','default'=>date('Y-m-d'),'type'=>'date'],
      'id_user_approve'=> ['label'=>'Approver','default'=>0,'type'=>'select'],
      'status'=> ['label'=>'Status','default'=>'Pending','type'=>'text']
    /*  echo '<td>';
      if ($this->keamanan->allowedaction['ubah']) {
        echo '<a tabindex="-1" href="'.base_url().'transaksi/edit/\'+transaksi["id_pesanan"]+\'"><i class="fa fa-pencil"></i></a> ';
      }
      if ($this->keamanan->allowedaction['hapus']) {
          echo '<a tabindex="-1" href="'.base_url().'transaksi/hapus/\'+transaksi["id_pesanan"]+\'"><i class="fa fa-trash"></i></a> ';
      }
      echo '</td>'; */
      ?></tr>';
      transaksi['terbilangum'] = terbilang(transaksi['uang_tanda_jadi'].toString())+' Rupiah';
      transaksi['telp_customerall'] = (transaksi['telp_customer']!=''&&transaksi['telp_customer']!=0)?transaksi['telp_customer'].toString():'';
      transaksi['telp_customerall'] = (transaksi['hp1']!=''&&transaksi['hp1']!=0)?transaksi['telp_customerall']+((transaksi['telp_customerall']=='')?'':' / ')+transaksi['hp1'].toString():transaksi['telp_customerall'];
      transaksi['telp_customerall'] = (transaksi['hp2']!=''&&transaksi['hp2']!=0)?transaksi['telp_customerall']+((transaksi['telp_customerall']=='')?'':' / ')+transaksi['hp2'].toString():transaksi['telp_customerall'];
      listtransaksi[transaksi['id_pesanan']]=transaksi;
    });
    $( "#tabeltransaksi>tbody" ).html(hasil);
    if (datahasil.length<perpage) $('#btnnext').attr("disabled", true); else $('#btnnext').attr("disabled", false);
    if (page<=1) $('#btnprev').attr("disabled", true); else $('#btnprev').attr("disabled", false);
    ubahdata(true);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
function datatransaksidetail(id_pesanan) {
  var request = $.ajax({
    url: "<?=base_url()?>ajax/datatransaksidetail",
    method: "POST",
    data: {
      'id_pesanan': id_pesanan,
      'posisi': '<?=$_SESSION['posisi']?>',
      'id_userutama': <?=$_SESSION['id_user']?>,
      'auth_keyutama': '<?=$_SESSION['auth_key']?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    var hasil = '';
    console.log(datahasil);
    $( "#modalcicilanindex #cicilanjmldibayar" ).html(listtransaksi[id_pesanan]['harga_netrupiah']);
    $.each(datahasil, function( index, transaksi ) {
      transaksi["cicilan"]=Math.trunc(transaksi["cicilan"]);
      hasil +='<tr><td>'+transaksi["keterangan"]+'</td><td>Rp. '+transaksi["cicilan"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'</td><td style="text-align:left">'+formatDate(new Date(transaksi["jatuh_tempo"]))+'</td></tr>';
    });
    htmllistcicilan = hasil;
    $( "#tabelsimulasicicilan>tbody" ).html(hasil);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
<?php if ($this->keamanan->allowedaction['ubah']) { ?>
  function updatedataajax() {
    var request = $.ajax({
      url: "<?=base_url()?>ajax/updatedatatransaksi",
      method: "POST",
      data: $("#tabeltransaksi .databerubah").serialize(),
      dataType: "json"
    });

    request.done(function( datahasil ) {
      console.log(datahasil);
      dataajax();
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      dataajax();
    });
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
  function resettambah(datas=listkolom) {
    $.each(datas, function( index, data ) {
      $('#input'+index).val(data["default"]);
    });
  }
  function bukaformtambah(){
    resettambah();
    $('#boxindex').hide();$('#boxtambah').show();$('.boxtambah input.form-control').first().focus();
  }
  function setformtambah() {
    $('.formdiskon1').hide();
    $('.formdiskon2').hide();
    $('.formdiskon3').hide();
    $('.formdiskon1_rp').hide();
    $('.formdiskon2_rp').hide();
    $('.formdiskon3_rp').hide();
    $('.formtanggal_approve').hide();
    $('.formid_user_approve').hide();
    $('.formstatus').hide();
    $('#inputharga_jual').attr({'min':'0','onkeyup':'hitungharganet()','onchange': 'cekhargajual()'});
    // $('.formdiskon1 .input-group .input-group-addon').html('<input type="checkbox" onclick="$(\'#inputdiskon1,#inputdiskon1_rp\').prop(\'disabled\', !$(this).prop(\'checked\'));">');
    $('#inputdiskon1,#inputdiskon1_rp,#inputnama_diskon').prop('disabled', true);
    $('#inputharga_net').prop('readonly', true);
    $('#inputdiskon1,#inputdiskon1_rp').attr({'min':'0','max':'100','onkeyup':'hitungharganet()','onchange': 'hitungharganet()'});
    $('#inputuang_muka,#typeuang_muka').attr({'onkeyup':'hitunguangmuka()','onchange': 'hitunguangmuka()'});
    $('#inputjenis_bayar').attr({'onkeyup':'cekjenisbayar()','onclick':'cekjenisbayar()','onchange': 'cekjenisbayar()'});
    $('#inputlama_cicilan').attr({'onkeyup':'resetsimulasicicilan()','onchange': 'resetsimulasicicilan()'});
    //$('.formdiskon1 .input-group').append('<div class="input-group-addon"><select name="typediskon1" id="inputtypediskon1"><option value="1">%</option><option value="2">Rp</option></select></div>');
    $('.formnama_diskon .input-group .input-group-addon i').attr('class','fa fa-book');
    $('.formnama_diskon').append($('.formdiskon1 .input-group'));
    $('#inputnama_diskon').attr('placeholder','Nama Diskon');
    $('.formnama_diskon>label').html('<input type="checkbox" class="datatambah" name="checkdiskon" onclick="$(\'#inputdiskon1,#inputdiskon1_rp,#inputnama_diskon\').prop(\'disabled\', !$(this).prop(\'checked\'));hitungharganet();"> Diskon');

    //$('.formdiskon2 .input-group .input-group-addon').html('<input type="checkbox" onclick="$(\'#inputdiskon2,#inputdiskon2_rp\').prop(\'disabled\', !$(this).prop(\'checked\'));">');
    //$('#inputdiskon2,#inputdiskon2_rp').prop('disabled', true);
    //$('#inputdiskon2,#inputdiskon2_rp').attr('onkeyup', 'hitungharganet()');
    //$('.formdiskon2 .input-group').append('<div class="input-group-addon"><select name="typediskon2" id="inputtypediskon2"><option value="1">%</option><option value="2">Rp</option></select></div>');
    //$('.formnama_diskon').append($('.formdiskon2 .input-group'));

    //$('.formdiskon3 .input-group .input-group-addon').html('<input type="checkbox" onclick="$(\'#inputdiskon3,#inputdiskon3_rp\').prop(\'disabled\', !$(this).prop(\'checked\'));">');
    //$('#inputdiskon3,#inputdiskon3_rp').prop('disabled', true);
    //$('#inputdiskon3,#inputdiskon3_rp').attr('onkeyup', 'hitungharganet()');
    //$('.formdiskon3 .input-group').append('<div class="input-group-addon"><select name="typediskon3" id="inputtypediskon3"><option value="1">%</option><option value="2">Rp</option></select></div>');
    //$('.formnama_diskon').append($('.formdiskon3 .input-group'));

    $('.formuang_muka .input-group').append('<div class="input-group-addon"><select name="typeuang_muka" class="datatambah" id="typeuang_muka"><option value="1">%</option><option value="2">Rp</option></select></div>');
    $('.formuang_muka_rp').hide();
    $('#inputid_unit').attr({'name':'text_unit','id':'inputtext_unit'});
    $('.formid_unit .input-group').append('<input type="hidden" name="id_unit" class="datatambah" id="inputid_unit">');
    $('#inputid_customer').attr('name','text_customer');
    $('.formid_customer .input-group').append('<input type="hidden" name="id_customer" class="datatambah" id="hidid_customer">');
    $('#inputid_marketing').attr('name','text_marketing');
    $('.formid_marketing .input-group').append('<input type="hidden" name="id_marketing" class="datatambah" id="hidid_marketing">');
    $('.formharga_net>label').after('<label class="pull-right"><sub>Diskon Rp. <b>0</b></sub></label>');
    $('.formuang_muka>label').after('<label class="pull-right"><sub><b>0</b></sub></label>');

    $('.formalamat_surat_menyurat').after('<div class="form-group col-xs-3 formimageFile"><label>Foto Identitas</label><a href="<?=base_url()?>assets/images/default/no_identitas.jpg" class="popupsingle_image linkimgidentitas" ><img class="img-responsive imgidentitas" src="<?=base_url()?>assets/images/default/no_identitas.jpg"></a></div> <div class="form-group col-xs-3 formimageFile"><label>Foto NPWP</label><a href="<?=base_url()?>assets/images/default/npwp.jpg" class="popupsingle_image linkimgnpwp" ><img class="img-responsive imgnpwp" src="<?=base_url()?>assets/images/default/npwp.jpg"></a></div>');

    $('.formlama_cicilan .input-group').append('<div class="input-group-addon" onclick="openmodalsimulasicicilan()" style="cursor:pointer"><i class="fa fa-calculator"></i></div>');
    cekjenisbayar();
  }
  function cekhargajual(){
    var hj=parseInt($('#inputharga_jual').val());
    if (hj<hargajual) {
      $('#inputharga_jual').val(hargajual);
    }
    hitungharganet();
  }
  function hitungharganet() {
    var hj=$('#inputharga_jual').val();
    var dics1=$('#inputdiskon1').val();
    //var dics2=$('#inputdiskon2').val();
    //var dics3=$('#inputdiskon3').val();
    if (dics1<0) $('#inputdiskon1').val(0);
    if (dics1>100) $('#inputdiskon1').val(100);
    dics1=$('#inputdiskon1').val();
    if ($('#inputdiskon1').prop('disabled')) {dics1=0;}
    var hnet = 0;
    var jmldis1=(hj*dics1/100);
    hnet=(hj-jmldis1);
    //hnet=(hnet-(hnet*dics2/100));
    //hnet=(hnet-(hnet*dics3/100));
    $('.formharga_net .pull-right>sub>b').html(jmldis1.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
    $('#inputharga_net').val(hnet);
    hitunguangmuka();
    resetsimulasicicilan();
  }
function hitunguangmuka() {
  var um = $('#inputuang_muka').val();
  var hnet = $('#inputharga_net').val();
  var typeum = $('#typeuang_muka').val();
  var hasilum = '';
  if (typeum==1) {hasilum=um*hnet/100;angkaum=hasilum;hasilum='Rp. '+hasilum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") } else {hasilum=(um*100/hnet);angkaum=um;hasilum=hasilum.toFixed(2)+'%';}
  $('.formuang_muka .pull-right>sub>b').html(hasilum);
}
function cekjenisbayar() {
  if ($('#inputjenis_bayar').val()=='KPR') {
    $('.formuang_muka .datatambah').attr('disabled',false);
  } else{
    $('#typeuang_muka').val(1);
    $('#inputuang_muka').val(0);
    $('.formuang_muka .datatambah').attr('disabled',true);
  }
}
function cetaktandaterima(id) {
  $.each(listtransaksi[id], function( index, transaksi ) {
    $('#cetaktandaterima .kwitansi'+index).html(transaksi);
  });
  var html = $('#cetaktandaterima').html()+'<style type="text/css">body{font-family:\'Arial\';color: black;}td{font-size:14px;}</style>';
  var w = window.open();
  w.document.write(html);
  w.print();
  w.close();
}

function cetakkwitansi(id) {
  $.each(listtransaksi[id], function( index, transaksi ) {
    $('#cetakkwitansi .kwitansi'+index).html(transaksi);
  });
  var html = $('#cetakkwitansi').html()+'<style type="text/css">body{font-family:\'Arial\';color: black;}td{font-size:14px;}</style>';
  var w = window.open();
  w.document.write(html);
  w.print();
  w.close();
}

function tambahdataajax() {
  $('#modal-tambah').modal('toggle');
  var request = $.ajax({
    url: "<?=base_url()?>ajax/tambahdatatransaksi",
    method: "POST",
    data: $("#boxtambah .datatambah, #tabelsimulasicicilan .datatambah").serialize(),
    dataType: "json"
  });

  request.done(function( datahasil ) {
    console.log(datahasil);
    $('#boxtambah').hide();$('#boxindex').show();
    dataajax();
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( 'Mohon Cek Kembali Data Anda');
  });
}
window.addEventListener('DOMContentLoaded', (event) => {
    setformtambah();
    <?php if ($id_unit!=0) {
      echo 'bukaformtambah();pilihunit('.$id_unit.');';
    } ?>
});
function resetsimulasicicilan(){
  var jmlcicilan = $('#inputlama_cicilan').val();
  var hnet = $('#inputharga_net').val();
  var uangtjadi = $('#inputuang_tanda_jadi').val();
  $('#simulasijmldibayar').html('Rp. '+hnet.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
  var jmldibayar = hnet-uangtjadi;
  if ($('#inputjenis_bayar').val()=='KPR') jmldibayar = angkaum;
  var cicilan = jmldibayar/jmlcicilan;
  cicilan=Math.ceil(Math.ceil(cicilan) / 1000) * 1000;
  var tanggal = new Date($('#inputtanggal').val());
  tanggal.setMonth(tanggal.getMonth());
  var rpuangtjadi = 'Rp. '+uangtjadi.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  var texttanggal = ("0" + tanggal.getDate()).slice(-2)+'/'+("0" + (tanggal.getMonth() + 1)).slice(-2)+'/'+tanggal.getFullYear();
  var isitabelcicilan = '<tr><td>Uang Tanda Jadi</td><td>'+rpuangtjadi+'</td><td>'+texttanggal+'</td></tr>';
  var rpcicilan = '';
  rpcicilan='Rp. '+cicilan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  tanggal.setDate(tanggal.getDate()+14);
  for (i = 1; i < jmlcicilan; i++) {
    texttanggal = ("0" + tanggal.getDate()).slice(-2)+'/'+("0" + (tanggal.getMonth() + 1)).slice(-2)+'/'+tanggal.getFullYear();
    valtanggal = tanggal.getFullYear()+'-'+("0" + (tanggal.getMonth() + 1)).slice(-2)+'-'+("0" + tanggal.getDate()).slice(-2);
    isitabelcicilan+='<tr><td><input class="datatambah" name="keteranganke['+i+']" value="Angsuran ke '+i+'" /></td><td><input class="datatambah" name="cicilanke['+i+']" value="'+cicilan+'" /></td><td><input type="date" class="form-control datatambah datepicker" name="jatuhtempoke['+i+']" value="'+valtanggal+'"></td></tr>';
    tanggal.setMonth(tanggal.getMonth()+1);
  }
  cicilan=jmldibayar-(cicilan*(jmlcicilan-1));
  rpcicilan='Rp. '+cicilan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  // tanggal.setMonth(tanggal.getMonth()+1);
  texttanggal = ("0" + tanggal.getDate()).slice(-2)+'/'+("0" + (tanggal.getMonth() + 1)).slice(-2)+'/'+tanggal.getFullYear();
  valtanggal = tanggal.getFullYear()+'-'+("0" + (tanggal.getMonth() + 1)).slice(-2)+'-'+("0" + tanggal.getDate()).slice(-2);
  isitabelcicilan+='<tr><td><input class="datatambah" name="keteranganke['+jmlcicilan+']" value="Angsuran ke '+jmlcicilan+'" /></td><td><input class="datatambah" name="cicilanke['+jmlcicilan+']" value="'+cicilan+'" /></td><td><input type="date" class="form-control datatambah datepicker" name="jatuhtempoke['+jmlcicilan+']" value="'+valtanggal+'"></td></tr>';
  if ($('#inputjenis_bayar').val()=='KPR'){
    tanggal.setMonth(tanggal.getMonth()+1);
    cicilan=hnet-uangtjadi-angkaum;
    rpcicilan='Rp. '+cicilan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    // tanggal.setMonth(tanggal.getMonth()+1);
    jmlcicilan++;
    texttanggal = ("0" + tanggal.getDate()).slice(-2)+'/'+("0" + (tanggal.getMonth() + 1)).slice(-2)+'/'+tanggal.getFullYear();
    valtanggal = tanggal.getFullYear()+'-'+("0" + (tanggal.getMonth() + 1)).slice(-2)+'-'+("0" + tanggal.getDate()).slice(-2);
    isitabelcicilan+='<tr><td><input class="datatambah" name="keteranganke['+jmlcicilan+']" value="Pelunasan Oleh KPR" /></td><td><input class="datatambah" name="cicilanke['+jmlcicilan+']" value="'+cicilan+'" /></td><td><input type="date" class="form-control datatambah datepicker" name="jatuhtempoke['+jmlcicilan+']" value="'+valtanggal+'"></td></tr>';
  }
  $('#tabelsimulasicicilan tbody').html(isitabelcicilan);
}
function openmodalsimulasicicilan() {
  resetsimulasicicilan();
  $('#modalsimulasicicilan').modal();
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['tambah'] || $this->keamanan->allowedaction['ubah']) { ?>
function openmodalid_unit(){
  $('#modalinputid_unit').modal();
  if (!$.fn.DataTable.isDataTable('#tabellistunit')){
    $('#tabellistunit').DataTable();
  }
}
function openmodalid_customer(){
  $('#modalinputid_customer').modal();
  if (!$.fn.DataTable.isDataTable('#tabellistcustomer')){
    $('#tabellistcustomer').DataTable();
  }
}
function openmodalid_marketing(){
  $('#modalinputid_marketing').modal();
  if (!$.fn.DataTable.isDataTable('#tabellistmarketing')){
    $('#tabellistmarketing').DataTable();
  }
}

function pilihunit(id){
  $('#inputid_unit').val(id);
  $('#inputtext_unit').val($('.id_unit'+id+' td:nth-child(1)').html()+' Tipe '+$('.id_unit'+id+' td:nth-child(2)').html()+' di '+$('.id_unit'+id+' td:nth-child(8)').html());
  $('#inputno_pesanan').val('TR'+id);
  $('#modalinputid_unit').modal('hide');
  hargajual=$('.id_unit'+id).attr('data-harga');
  $('#inputharga_jual').val(hargajual);
  $('#inputharga_jual').attr('min',hargajual);
  hitungharganet();
}
function pilihcustomer(id,nama,alamat){
  $('#inputid_customer').val(nama);
  $('#hidid_customer').val(id);
  $('#inputalamat_surat_menyurat').val(alamat);
  var linkimg = '<?=base_url()?>assets/images/customer/<?=$_SESSION['id_perusahaan']?>/no_identitas'+id+'.jpg';
  var defaultimg = '<?=base_url()?>assets/images/default/no_identitas.jpg';
  if (checkfileada(linkimg)){$('#boxtambah .imgidentitas').attr('src',linkimg);$('#boxtambah .linkimgidentitas').attr('href',linkimg);}else{$('#boxtambah .imgidentitas').attr('src',defaultimg);$('#boxtambah .linkimgidentitas').attr('href',linkimg);}
  linkimg = '<?=base_url()?>assets/images/customer/<?=$_SESSION['id_perusahaan']?>/npwp'+id+'.jpg';
  defaultimg = '<?=base_url()?>assets/images/default/npwp.jpg';
  if (checkfileada(linkimg)){$('#boxtambah .imgnpwp').attr('src',linkimg);$('#boxtambah .linkimgnpwp').attr('href',linkimg);}else{$('#boxtambah .imgnpwp').attr('src',defaultimg);$('#boxtambah .linkimgnpwp').attr('href',linkimg);}
  $('#modalinputid_customer').modal('hide');
}
function pilihmarketing(id,nama){
  $('#inputid_marketing').val(nama);
  $('#hidid_marketing').val(id);
  $('#modalinputid_marketing').modal('hide');
}
<?php } ?>
function bukapilihantemplate(id_pesanan) {
  id_pesanandipilih = id_pesanan;
  $('#modaltemplatedokumen').modal();
}
function pilihtemplate_dokumen(id_template=0) {
  $('#modaltemplatedokumen').modal('hide');
  id_template_dokumen=id_template;
  var html = datatemplatetransaksi[id_template];
  html=replacevariabeldokumen(html,listtransaksi[id_pesanandipilih]);
  $('iframe').contents().find('.wysihtml5-editor').html(html);
  $('#boxdetail').show();$('#boxindex').hide();
}
function cetakdokumen() {
  var html = $('iframe').contents().find('html').html();
  var w = window.open();
  w.document.write(html);
  w.print();
  w.close();
}
<?php if ($this->keamanan->allowedaction['hapus']) { ?>
  function hapusdataajax(id_pesanan) {
    if (confirm('Apakah Anda yakin ingin menghapus data transaksi ini ?')) {
      var request = $.ajax({
        url: "<?=base_url()?>ajax/hapusdatatransaksi",
        method: "POST",
        data: {
          'id_userutama': <?=$_SESSION['id_user']?>,
          'auth_keyutama': '<?=$_SESSION['auth_key']?>',
          '_csrf': '<?=$this->keamanan->generatecsrf()?>',
          'id_pesanan': id_pesanan
        },
        dataType: "json"
      });

      request.done(function( datahasil ) {
        console.log(datahasil);
        dataajax();
      });

      request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
        dataajax();
      });
    }
}
<?php } ?>
window.addEventListener('DOMContentLoaded', (event) => {
$('#filtertanggal').daterangepicker(
  {
    ranges   : {
      'Hari Ini'       : [moment(), moment()],
      'Kemarin'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      '7 Hari Terakhir' : [moment().subtract(6, 'days'), moment()],
      '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
      'Bulan Ini'  : [moment().startOf('month'), moment().endOf('month')],
      'Bulan Lalu'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    startDate: moment().subtract(29, 'days'),
    endDate  : moment()
  },
  function (start, end) {
    $('#inputfiltertanggal').val('\''+start.format('YYYY-MM-DD') + '\' Hingga \'' + end.format('YYYY-MM-DD')+'\'')
  }
);
$('#inputfiltertanggal').val('\''+moment().subtract(29, 'days').format('YYYY-MM-DD') + '\' Hingga \'' + moment().format('YYYY-MM-DD')+'\'');
$("a.popupsingle_image").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	600,
		'speedOut'		:	200,
		'overlayShow'	:	false
	});

  var templatealignment = {
        textAlign: function(context) {
          return "<li><div class='btn-group'>" +
              "<a class='btn btn-default' data-wysihtml5-command='justifyLeft' data-wysihtml5-command-value='&justifyLeft;' title= 'Align text left'>" +
              "<span class='glyphicon glyphicon-align-left'></span></a>" +
              "<a class='btn btn-default' data-wysihtml5-command='justifyCenter' data-wysihtml5-command-value='&justifyCenter;' title= 'Align text center'>" +
              "<span class='glyphicon glyphicon-align-center'></span></a>" +
              "<a class='btn btn-default' data-wysihtml5-command='justifyRight' data-wysihtml5-command-value='&justifyRight;' title= 'Align text right'>" +
              "<span class='glyphicon glyphicon-align-right'></span></a>" +
              "</li>";
        }
      };
  $('#textareatransaksi').wysihtml5({
    classes: {
      "wysiwyg-text-align-center": 1,
      "wysiwyg-text-align-justify": 1,
      "wysiwyg-text-align-left": 1,
      "wysiwyg-text-align-right": 1
    },
    "toolbar": {
      "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
      "emphasis": true, //Italics, bold, etc. Default true
      "lists": true, //(Un)ordered lists, e.g. Bullet"s, Numbers. Default true
      "html": false, //Button which allows you to edit the generated HTML. Default false
      "link": true, //Button to insert a link. Default true
      "image": true, //Button to insert an image. Default true,
      "color": true, //Button to change color of font
      "blockquote": false,
      'textAlign': true // custom defined buttons to align text see myCustomTemplates variable above
    },
    stylesheets: ["<?=base_url()?>assets/css/main.css"],
    customTemplates: templatealignment
  });

});
</script>
