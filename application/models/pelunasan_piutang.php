<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pelunasan_piutang extends CI_Model {

  function kolom($id='') {
    $listkolom = array(
      'id_pelunasan'=> ['label'=>'ID Pelusanan','default'=>0,'type'=>'text'],
      'kode_pelunasan'=> ['label'=>'No. Pesanan','default'=>'','type'=>'text'],
      'tanggal'=> ['label'=>'Tanggal','default'=>date('Y-m-d'),'type'=>'date'],
      'status'=> ['label'=>'Status','default'=>'Pending','type'=>'text'],
      'id_perusahaan'=> ['label'=>'Perusahaan','default'=>0,'type'=>'select'],
    );
    if (isset($listkolom[$id])) {
      return $listkolom[$id];
    } else {
      return $listkolom;
    }
  }

  function dataupdate($data) {
    $sql = null;
    if (isset($data['id_pelunasan'])) {
      $sql = array();
      $query = '';
      foreach ($data['id_pelunasan'] as $key => $value) {
        $sql[$key]='EXEC sp_pelunasan_piutang @id_userutama='.$data['id_userutama'].', @auth_keyutama=\''.$data['auth_keyutama'].'\',';
        foreach ($this->kolom() as $id => $label) {
          if (isset($data[$id])) {
            $sql[$key].=isset($data[$id][$key])?' @'.$id.'=\''.$data[$id][$key].'\',':'';
          }
        }
        $sql[$key]=substr($sql[$key], 0, -1);
        $sql[$key].=';';
        $query.=$sql[$key].' ';
      }
      if ($query!=''){
        $q=$this->db->query($query);
        //$sql=$q->result_array();
      }
    }
    return '';
  }

  function datainsert($data) {
    $query='EXEC sp_pelunasan_piutang @id_userutama='.$data['id_userutama'].', @auth_keyutama=\''.$data['auth_keyutama'].'\',';
    $data['id_pelunasan']=0;
    foreach ($this->kolom() as $id => $label) {
      if (isset($data[$id])) {
        $data[$id]=($id=='aktif')?(($data[$id]=='on')?1:0):$data[$id];
        $query.=' @'.$id.'=\''.$data[$id].'\',';
      }
    }
    $query=substr($query, 0, -1);
    $query.=';';
    if ($query!=''){
      $q=$this->db->query($query);
      $sql=$q->result_array();
    }
    return '';
  }

  function datahapus($data) {
    $data['id_userutama']=isset($data['id_userutama'])?$data['id_userutama']:0;
    $data['auth_keyutama']=isset($data['auth_keyutama'])?$data['auth_keyutama']:'-';
    $data['id_pelunasan']=isset($data['id_pelunasan'])?$data['id_pelunasan']:0;
    $query='EXEC sp_m_pesanan @id_userutama='.$data['id_userutama'].', @auth_keyutama=\''.$data['auth_keyutama'].'\', @id_pelunasan=\''.$data['id_pelunasan'].'\', @aksi=\'hapus\';';
    $q=$this->db->query($query);
    return '';
  }

  function pelunasan_piutangku($datainput){
  	$response = null;
      $data = array(
        'id_pelunasan'=>isset($datainput['id_pelunasan'])?$datainput['id_pelunasan']:0,
        'lengkap'=>isset($datainput['lengkap'])?$datainput['lengkap']:false,
        'aktif'=>isset($datainput['aktif'])?$datainput['aktif']:2,
        'page'=>isset($datainput['page'])?$datainput['page']:1,
        'perpage'=>isset($datainput['perpage'])?$datainput['perpage']:25,
        'nama_pelunasan_piutang'=>isset($datainput['nama_pelunasan_piutang'])?$datainput['nama_pelunasan_piutang']:'',
        'orderby'=>isset($datainput['orderby'])?$datainput['orderby']:'id_pelunasan DESC',
        'alldata'=>isset($datainput['alldata'])?$datainput['alldata']:false,
        'posisi'=>isset($datainput['posisi'])?$datainput['posisi']:'user',
        'id_userutama'=>isset($datainput['id_userutama'])?$datainput['id_userutama']:0,
        'auth_keyutama'=>isset($datainput['auth_keyutama'])?$datainput['auth_keyutama']:'-',
      );
  		$response = array();
  		$id_userutama = htmlspecialchars($data['id_userutama']);
      $auth_keyutama = htmlspecialchars($data['auth_keyutama']);
      $id_pelunasan = htmlspecialchars($data['id_pelunasan']);
      $nama_pelunasan_piutang = htmlspecialchars($data['nama_pelunasan_piutang']);
      $aktif = (int)$data['aktif'];
      $page = ((int)$data['page']>=1)?(int)$data['page']:1;
      $perpage = ((int)$data['perpage']>=1)?(int)$data['perpage']:25;
      $orderby = $data['orderby'];
  		$checkid = ($id_pelunasan!=0)?' AND id_pelunasan='.$id_pelunasan:'';
      $select = ($data['lengkap'])?'*':'id_pelunasan,status,id_perusahaan';
      $aktif = ($aktif==1||$aktif==0)?' AND aktif='.$aktif:'';
      $tambahan = ($nama_pelunasan_piutang!='')?' AND nama_pelunasan_piutang LIKE \'%'.$nama_pelunasan_piutang.'%\'':'';
      $tambahan = ' AND id_userutama='.$id_userutama.' AND auth_keyutama=\''.$auth_keyutama.'\'';
      $limit=($data['alldata'])?'':' OFFSET '.(($data['page']-1)*$data['perpage']).' ROWS FETCH NEXT '.$data['perpage'].' ROWS ONLY';
  		if ($data['posisi']=='owner') {
  			$q = $this->db->query('SELECT '.$select.' FROM v_pesanan_owner WHERE 1=1'.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit);
  		} else {
  			$q = $this->db->query('SELECT '.$select.' FROM v_pesanan_owner WHERE 1=1'.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit);
  		}
      //$q = $this->db->query('exec sp_data_pelunasan_piutang @id_usr ='.$id_user);
  		$response = ($id_pelunasan==0)?$q->result():$q->row();
      //$response = 'SELECT '.$select.' FROM v_pelunasan_piutang_owner WHERE id_user='.$id_user.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit;
      return $response;
    }


}
