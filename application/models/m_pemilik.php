<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_pemilik extends CI_Model {

  function getPemilik(){
 
    $response = array();
 
    // Select record
    $this->db->select('*');
    $q = $this->db->get('m_pemilik');
    $response = $q->result_array();

    return $response;
  }
}
