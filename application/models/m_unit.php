<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_unit extends CI_Model {

  function kolom($id='') {
    $listkolom = array(
      'id_unit'=> ['label'=>'ID Unit','default'=>0,'type'=>'text'],
      'nomor'=> ['label'=>'Nomor','default'=>'','type'=>'text'],
      'id_tipe'=> ['label'=>'Tipe','default'=>1,'type'=>'select'],
      'status'=> ['label'=>'Status','default'=>'a','type'=>'select']
    );
    if (isset($listkolom[$id])) {
      return $listkolom[$id];
    } else {
      return $listkolom;
    }
  }

  function kolomlengkap($id='') {
    $listkolom = array(
      'id_unit'=> ['label'=>'ID Uni','default'=>0,'type'=>'text'],
      'nomor'=> ['label'=>'Nomor','default'=>'','type'=>'text'],
      'nama_tipe'=> ['label'=>'Tipe','default'=>1,'type'=>'select'],
      'nama_cluster'=> ['label'=>'Cluster','default'=>1,'type'=>'select'],
      'harga'=> ['label'=>'Harga','default'=>0,'type'=>'number'],
      'lebar'=> ['label'=>'Lebar','default'=>0,'type'=>'number'],
      'panjang'=> ['label'=>'Panjang','default'=>0,'type'=>'number'],
      'luas_bangunan'=> ['label'=>'Luas','default'=>0,'type'=>'number'],
      'nama_perumahan'=> ['label'=>'Perumahan','default'=>0,'type'=>'select']
    );
    if (isset($listkolom[$id])) {
      return $listkolom[$id];
    } else {
      return $listkolom;
    }
  }

  function liststatus($id='') {
    $liststatus = array(
      ['status'=>'r','label'=> 'Ready'],
      ['status'=>'p','label'=> 'On Proccess'],
      ['status'=>'n','label'=> 'Not Ready'],
      ['status'=>'s','label'=> 'Sold'],
    );
    if (isset($liststatus[$id])) {
      return $liststatus[$id];
    } else {
      return $liststatus;
    }
  }

  function dataupdate($data) {
    $sql = null;
    if (isset($data['id_unit'])) {
      $sql = array();
      $query = '';
      foreach ($data['id_unit'] as $key => $value) {
        $sql[$key]='EXEC sp_m_unit @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\',';
        foreach ($this->kolom() as $id => $label) {
          if (isset($data[$id])) {
            $sql[$key].=isset($data[$id][$key])?' @'.$id.'=\''.$data[$id][$key].'\',':'';
          }
        }
        $sql[$key]=substr($sql[$key], 0, -1);
        $sql[$key].=';';
        $query.=$sql[$key].' ';
      }
      if ($query!=''){
        $q=$this->db->query($query);
        //$sql=$q->result_array();
      }
    }
    return '';
  }

  function datainsert($data) {
    $query='EXEC sp_m_unit @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\',';
    foreach ($this->kolom() as $id => $label) {
      if (isset($data[$id])) {
        $data[$id]=($id=='aktif')?(($data[$id]=='on')?1:0):$data[$id];
        $query.=' @'.$id.'=\''.$data[$id].'\',';
      }
    }
    $query=substr($query, 0, -1);
    $query.=';';
    if ($query!=''){
      $q=$this->db->query($query);
      //$sql=$q->result_array();
    }
    return '';
  }

  function datahapus($data) {
    $data['id_user']=isset($data['id_user'])?$data['id_user']:0;
    $data['auth_key']=isset($data['auth_key'])?$data['auth_key']:'-';
    $data['id_unit']=isset($data['id_unit'])?$data['id_unit']:0;
    $query='EXEC sp_m_unit @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\', @id_unit=\''.$data['id_unit'].'\', @aksi=\'hapus\';';
    $q=$this->db->query($query);
    return '';
  }

  function unitku($datainput){
  $q = $this->load->model('m_user');
	$response = null;
    $data = array(
      'id_unit'=>isset($datainput['id_unit'])?$datainput['id_unit']:0,
      'id_tipe'=>isset($datainput['id_tipe'])?$datainput['id_tipe']:0,
      'lengkap'=>isset($datainput['lengkap'])?$datainput['lengkap']:false,
      'status'=>isset($datainput['status'])?$datainput['status']:'a',
      'page'=>isset($datainput['page'])?$datainput['page']:1,
      'perpage'=>isset($datainput['perpage'])?$datainput['perpage']:25,
      'nomor'=>isset($datainput['nomor'])?$datainput['nomor']:'',
      'orderby'=>isset($datainput['orderby'])?$datainput['orderby']:'id_unit DESC',
      'alldata'=>isset($datainput['alldata'])?$datainput['alldata']:false,
      'posisi'=>isset($datainput['posisi'])?$datainput['posisi']:'user',
      'id_perusahaan'=>isset($datainput['id_perusahaan'])?$datainput['id_perusahaan']:0,
      'id_perumahan'=>isset($datainput['id_perumahan'])?$datainput['id_perumahan']:0,
      'id_user'=>isset($datainput['id_user'])?$datainput['id_user']:0,
      'auth_key'=>isset($datainput['auth_key'])?$datainput['auth_key']:'-',
    );
		$response = array();
		$id_user = htmlspecialchars($data['id_user']);
    $auth_key = htmlspecialchars($data['auth_key']);
		$id_tipe = htmlspecialchars($data['id_tipe']);
    $id_unit = htmlspecialchars($data['id_unit']);
    $nomor = htmlspecialchars($data['nomor']);
    $status = htmlspecialchars($data['status']);
    $id_perusahaan = ((int)$data['id_perusahaan']>=1)?(int)$data['id_perusahaan']:0;
    $id_perumahan = ((int)$data['id_perumahan']>=1)?(int)$data['id_perumahan']:0;
    $page = ((int)$data['page']>=1)?(int)$data['page']:1;
    $perpage = ((int)$data['perpage']>=1)?(int)$data['perpage']:25;
    $orderby = $data['orderby'];
    $checkid = ($id_unit!=0)?' AND id_unit='.$id_unit:'';
    $select = ($data['lengkap'])?'*':'id_unit,status,id_tipe,id_perusahaan';
    $status = ($status!='a')?' AND status=\''.$status.'\'':'';
    $tambahan = ($nomor!='')?' AND nomor LIKE \'%'.$nomor.'%\'':'';
    $tambahan .= ' AND id_user='.$id_user.' AND auth_key=\''.$auth_key.'\'';
    $tambahan .= ($id_perusahaan!=0)?' AND id_perusahaan='.$id_perusahaan:'';
    $tambahan .= ($id_perumahan!=0)?' AND id_perumahan='.$id_perumahan:'';
    $limit=($data['alldata'])?'':' OFFSET '.(($data['page']-1)*$data['perpage']).' ROWS FETCH NEXT '.$data['perpage'].' ROWS ONLY';
    $q = $this->db->query('SELECT '.$select.' FROM v_unitku WHERE id_user='.$id_user.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit);
		// if ($data['posisi']=='owner') {
		// 	$q = $this->db->query('SELECT '.$select.' FROM v_unit_owner WHERE id_user='.$id_user.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit);
		// } else {
		// 	$q = $this->db->query('SELECT '.$select.' FROM v_unit_owner WHERE id_user='.$id_user.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit);
		// }
    //$q = $this->db->query('exec sp_data_m_tipe @id_usr ='.$id_user);
		$response = ($id_unit==0)?$q->result():$q->row();
    // $response = 'SELECT '.$select.' FROM v_unit_owner WHERE id_user='.$id_user.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit;
    return $response;
  }


}
