<?php /*<div class="box">
  <div class="box-header">
    <h3 class="box-title"><?=ucfirst($action)?> marketing</h3>
  </div>
  <!-- /.box-header -->

  <div class="box-body">
    <form action="" method="post" enctype="application/x-www-form-urlencoded" class="row">
    */
    ?>
    <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#datamarketing" id="btntabmarketing" data-toggle="tab">Data Marketing</a></li>
              <li><a href="#dataperumahan" id="btntabperumahan" data-toggle="tab">Data Perumahan</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="datamarketing">
                <input class="datatambah" type="hidden" name="id_userutama" class="databerubah" value="<?=$_SESSION['id_user']?>">
                <input class="datatambah" type="hidden" name="auth_keyutama" class="databerubah" value="<?=$_SESSION['auth_key']?>">
                  <input class="datatambah" type="hidden" name="_csrf" value="<?=$this->keamanan->generatecsrf()?>">
                  <input class="datatambah" type="hidden" name="id_marketing" value="0">
                <?php
                foreach ($this->m_marketing->kolom() as $key => $value) {
                  if ($key!='id_marketing'){
                    $datas=($key=='aktif')?[['aktif'=>1,'id'=>'Aktif'],['aktif'=>0,'id'=>'Tidak Aktif']]:null;
                    $namevalue=($key=='aktif')?'id':'';
                    $datas=($key=='jenis')?$this->m_marketing->listjenis():$datas;
                    $namevalue=($key=='jenis')?'label':$namevalue;
                    echo $this->formku->generateinput($value['type'],$key,(isset($_POST[$key])?$_POST[$key]:$value['default']),$value['label'],$class='col-xs-6','fa-book',$datas,$namevalue);
                  }
                }
                ?>
                <div style="clear:both"></div>
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button id="btntambah" type="button" class="btn btn-success pull-right" onclick="$('#btntabperumahan').click()">Next »</button>
                <div style="clear:both"></div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="dataperumahan">
                <div style="clear:both"></div>
                <table id="tabeldataperumahan" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Perusahahan</th>
                    <th>Perumahan</th>
                  </tr>
                </thead>
                <tbody>
                <?php $htmlperusahaan = '';$htmlperumahan = '';
                $tempidpr=0; $idperusahaanaktif=0;
                foreach ($perumahanku as $key => $perumahan) {
                  if ($perumahan->id_perusahaan!=$tempidpr){
                    // $aktif = ($perumahan->aktif)?'':' disabled="disabled"';
                    // $styledis = ($perumahan->aktif)?'':' style="color:#555"';
                    // $htmlperusahaan .= '<div class="form-group"><div class="input-group"><input type="checkbox" name="perusahaan['.$perumahan->id_perusahaan.']"'.$aktif.'><b'.$styledis.'> '.$perumahan->nama_perusahaan.'</b></div></div>';
                    //if($tempidpr!=0) {$htmlperumahan.='</div>';$hide=' style="display:none"';}else{$hide='';}
                    //$htmlperumahan .= '<div id="checklistperumahan'.$perumahan->id_perusahaan.'" class="checklistperumahan">';
                    // $htmlperusahaan .= '<div class="form-group"><div class="input-group checkbox"><input type="checkbox" name="perusahaan['.$perumahan->id_perusahaan.']"><b onclick="$(\'.checklistperumahan\').hide();$(\'#checklistperumahan'.$perumahan->id_perusahaan.'\').show();"> '.$perumahan->nama_perusahaan.'</b></div></div>';
                    if($tempidpr==0){
                      $idperusahaanaktif=$perumahan->id_perusahaan;
                    }
                    if ($idperusahaanaktif==$perumahan->id_perusahaan){$classhead=' mmarketingdipilih';$display='';}else{$classhead='';$display=' style="display:none"';}
                    $htmlperusahaan .= '<div class="form-group divheadpr divheadpr'.$perumahan->id_perusahaan.$classhead.'"><div class="input-group checkbox"><label onclick="pilihperusahaan('.$perumahan->id_perusahaan.')"><b> '.$perumahan->nama_perusahaan.'</b></label></div></div>';
                    $htmlperumahan .= '<div class="form-group divpr divpr'.$perumahan->id_perusahaan.'"'.$display.'><div class="input-group checkbox"><label><input type="checkbox" onclick="$(\'.inputpr'.$perumahan->id_perusahaan.'\').prop(\'checked\', $(\'#inputpr'.$perumahan->id_perusahaan.'\').prop(\'checked\'));" id="inputpr'.$perumahan->id_perusahaan.'" name="perumahan['.$perumahan->id_perumahan.']"><b> Pilih Semuanya</b></label></div></div>';
                    $tempidpr=$perumahan->id_perusahaan;
                  }
                  if ($idperusahaanaktif==$perumahan->id_perusahaan){$display='';}else{$display=' style="display:none"';}
                  $htmlperumahan .= '<div class="form-group divpr divpr'.$perumahan->id_perusahaan.'"'.$display.'><div class="input-group checkbox"><label><input type="checkbox" class="inputpr'.$perumahan->id_perusahaan.' inputperumahan" name="perumahan['.$perumahan->id_perumahan.']"><b> '.$perumahan->nama_perumahan.'</b></label></div></div>';
                }
                echo '<tr><td style="padding:0">'.$htmlperusahaan.'</td><td>'.$htmlperumahan.'</td></tr>';
                ?>
                </tbody>
                </table>
                <button id="btntambah" type="button" class="btn btn-success" onclick="$('#btntabmarketing').click()">« Prev</button>
                <button id="btntambah" type="button" class="btn btn-primary pull-right" onclick="tambahdataajax()">Simpan</button>
                <div style="clear:both"></div>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
<?php /*
    </form>

  </div>
  <!-- /.box-body -->

</div>
<!-- /.box -->
*/ ?>
<script type="text/javascript">
function pilihperusahaan(id){
    $('.divheadpr').removeClass('mmarketingdipilih');
    $('.divheadpr'+id).addClass('mmarketingdipilih');
    $('.divpr').hide();$('.divpr'+id).show();
}
</script>
