<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct() {
		 parent::__construct();
	 }
	 public function index($page=1)
 	{
		$this->load->model('m_customer');
		$aktif=isset($_GET['aktif'])?$_GET['aktif']:2;
		$perpage=isset($_GET['perpage'])?$_GET['perpage']:25;
		$page=(int)$page;
		$this->setlayouts('utama','customer/utama',['model'=>$this->m_customer,'menudipilih'=>'Master Data']);
 	}
	public function tambah()
 {
	 	$this->load->model('m_customer');
		$this->setlayouts('utama','customer/_form',['action'=>'tambah','perusahaanku'=>$this->m_perusahaan->perusahaanku(0,false,1),'menudipilih'=>'Master Data']);
 }
 public function edit($id=-1)
{
	$this->load->model('m_customer');
	if ($datacustomer=$this->m_customer->customerku($id,true)) {
		$_POST=(array)$datacustomer;
		$this->setlayouts('utama','customer/_form',['action'=>'edit','perusahaanku'=>$this->m_perusahaan->perusahaanku(0,false,1),'menudipilih'=>'Master Data']);
	}
}
	public function listcustomer()
	{
			$this->setlayouts('listcustomer','utama',['customerku'=>$this->m_customer->customerku(0,false,1),'menudipilih'=>'dashboard']);
	}
	public function ganticustomer($id=1)
	{
			if ($this->m_customer->customerku($id)!=null) {
				$_SESSION['id_customer'] = $id;
				redirect('', 'refresh');
			}
	}
}
