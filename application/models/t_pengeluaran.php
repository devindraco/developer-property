<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_pengeluaran extends CI_Model {

  function kolom($id='') {
    $listkolom = array(
      'id_pengeluaran'=> ['label'=>'ID Pengeluaran','default'=>0,'type'=>'text'],
      'id_cluster'=> ['label'=>'Cluster','default'=>'','type'=>'pilihanpopup'],
      'id_perumahan'=> ['label'=>'Perumahan','default'=>'','type'=>'pilihanpopup'],
      'tanggal'=> ['label'=>'Tanggal','default'=>date('Y-m-d'),'type'=>'date'],
      'pengeluaran'=> ['label'=>'Kategori Pengeluaran','default'=>'','type'=>'text'],
      'keperluan'=> ['label'=>'Keperluan','default'=>'','type'=>'text'],
      'jumlah'=> ['label'=>'Jumlah','default'=>0,'type'=>'number'],
      'satuan'=> ['label'=>'Satuan','default'=>'','type'=>'text'],
      'harga_satuan'=> ['label'=>'Harga Satuan','default'=>0,'type'=>'number'],
      'total'=> ['label'=>'Total','default'=>0,'type'=>'number'],
      'bank'=> ['label'=>'Bank','default'=>'','type'=>'text'],
      'id_kwitansi'=> ['label'=>'Kwitansi','default'=>'','type'=>'pilihanpopup'],
      'id_user_input'=> ['label'=>'User Input','default'=>0,'type'=>'select'],
      'id_user_approve'=> ['label'=>'Approver','default'=>0,'type'=>'select'],
      'status'=> ['label'=>'Status','default'=>'Pending','type'=>'text']
    );
    if (isset($listkolom[$id])) {
      return $listkolom[$id];
    } else {
      return $listkolom;
    }
  }

  function liststatus($id='') {
    $liststatus = array(
      ['status'=>'Pending','label'=> 'Pending'],
      ['status'=>'Approve','label'=> 'Approve'],
      ['status'=>'Reject','label'=> 'Reject'],
    );
    if (isset($liststatus[$id])) {
      return $liststatus[$id];
    } else {
      return $liststatus;
    }
  }

  function dataupdate($data) {
    $sql = null;
    if (isset($data['id_pengeluaran'])) {
      $sql = array();
      $query = '';
      foreach ($data['id_pengeluaran'] as $key => $value) {
        $sql[$key]='EXEC sp_t_pengeluaran @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\',';
        foreach ($this->kolom() as $id => $label) {
          if (isset($data[$id])) {
            $sql[$key].=isset($data[$id][$key])?' @'.$id.'=\''.$data[$id][$key].'\',':'';
          }
        }
        $sql[$key]=substr($sql[$key], 0, -1);
        $sql[$key].=';';
        $query.=$sql[$key].' ';
      }
      if ($query!=''){
        $q=$this->db->query($query);
        //$sql=$q->result_array();
      }
    }
    return $query;
  }

  function datainsert($data) {
    $query='EXEC sp_t_pengeluaran @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\', @id_perusahaan=\''.$data['id_perusahaan'].'\',';
    $data['id_pengeluaran']=0;
    $data['id_user_approve']=0;
    $data['id_user_input']=$data['id_user'];
    $data['status']='Pending';
    foreach ($this->kolom() as $id => $kolom) {
      if (isset($data[$id])) {
        $query.=' @'.$id.'=\''.$data[$id].'\',';
      } else {
        $query.=' @'.$id.'=\''.$kolom['default'].'\',';
      }
    }
    $query=substr($query, 0, -1);
    $query.=';';
    if ($query!=''){
      $q=$this->db->query($query);
      $hasil=$q->row_array();
    }
    //print_r($sql);
    return $query;
  }

  function datahapus($data) {
    $data['id_user']=isset($data['id_user'])?$data['id_user']:0;
    $data['auth_key']=isset($data['auth_key'])?$data['auth_key']:'-';
    $data['id_pengeluaran']=isset($data['id_pengeluaran'])?$data['id_pengeluaran']:0;
    $query='EXEC sp_m_pengeluaran @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\', @id_pengeluaran=\''.$data['id_pengeluaran'].'\', @aksi=\'hapus\';';
    $q=$this->db->query($query);
    return '';
  }

  function approve($data) {
    $sql = null;
    $query = '';
    if (isset($data['id_pengeluaran'])) {
      $sql = array();
      $query='EXEC sp_approval_pengeluaran @id_userutama='.$data['id_user'].', @auth_keyutama=\''.$data['auth_key'].'\', @id_pengeluaran='.$data['id_pengeluaran'].', @status=\''.$data['status'].'\', @id_perusahaan='.$data['id_perusahaan'].';';
      if ($query!=''){
        $q=$this->db->query($query);
      }
    }
    return '';
  }



  function pengeluaranku($datainput){
  	$response = null;
      $data = array(
        'id_pengeluaran'=>isset($datainput['id_pengeluaran'])?$datainput['id_pengeluaran']:0,
        'lengkap'=>isset($datainput['lengkap'])?$datainput['lengkap']:false,
        'status'=>isset($datainput['status'])?$datainput['status']:'Semua',
        'page'=>isset($datainput['page'])?$datainput['page']:1,
        'perpage'=>isset($datainput['perpage'])?$datainput['perpage']:25,
        'orderby'=>isset($datainput['orderby'])?$datainput['orderby']:'id_pengeluaran DESC',
        'alldata'=>isset($datainput['alldata'])?$datainput['alldata']:false,
        'alldataselect'=>isset($datainput['alldataselect'])?$datainput['alldataselect']:false,
        'posisi'=>isset($datainput['posisi'])?$datainput['posisi']:'user',
        'rangetanggal'=>isset($datainput['rangetanggal'])?$datainput['rangetanggal']:'',
        'id_perusahaan'=>isset($datainput['id_perusahaan'])?$datainput['id_perusahaan']:0,
        'id_user'=>isset($datainput['id_user'])?$datainput['id_user']:0,
        'auth_key'=>isset($datainput['auth_key'])?$datainput['auth_key']:'-',
      );
  		$response = array();
  		$id_user = htmlspecialchars($data['id_user']);
      $auth_key = htmlspecialchars($data['auth_key']);
      $id_pengeluaran= htmlspecialchars($data['id_pengeluaran']);
      $status = htmlspecialchars($data['status']);
      $page = ((int)$data['page']>=1)?(int)$data['page']:1;
      $perpage = ((int)$data['perpage']>=1)?(int)$data['perpage']:25;
      $id_perusahaan = ((int)$data['id_perusahaan']>=1)?(int)$data['id_perusahaan']:0;
      $orderby = $data['orderby'];
      $rangetanggal=$data['rangetanggal'];
      if ($rangetanggal!=''){
          $rangetanggal = ' AND tanggal BETWEEN '.str_replace('Hingga','and',$rangetanggal);
      }
  		$checkid = ($id_pengeluaran!=0)?' AND id_pengeluaran='.$id_pengeluaran:'';
      $select = ($data['lengkap'])?'id_pengeluaran, pengeluaran, keperluan, jumlah, satuan, harga_satuan, total, bank, id_kwitansi, tanggal, status, id_user_input, nama_depan_user_input, nama_belakang_user_input, jabatan_user_input, aktif_user_input, id_cluster, id_user_approve, aktif_user_approve, nama_depan_user_approve, nama_belakang_user_approve, jabatan_user_approve, id_perumahan, aktif_cluster, nama_cluster, nama_perumahan, alamat_perumahan, kota_perumahan, propinsi_perumahan, telp_perumahan, website_perumahan, max_cicilan_cash, max_cicilan_kpr, max_cicilan_inhouse, max_persen_dp, default_uang_tanda_jadi, aktif_perumahan, id_perusahaan, nama_perusahaan, id_pemilik, alamat_perusahaan, kota_perusahaan, propinsi_perusahaan, telp_perusahaan, website_perusahaan, npwp_perusahaan, id_user, auth_key, nama_depan, nama_belakang, jabatan, aktif_user, no_kwitansi, nama_kwitansi, slug_kwitansi, tanggal_kwitansi, jenis_kwitansi
':'id_pengeluaran,status,id_perumahan,id_cluster,tanggal';
      $status = ($status!='Semua')?' AND status=\''.$status.'\'':'';
      $tambahan = ' AND id_user='.$id_user.' AND auth_key=\''.$auth_key.'\'';
      $tambahan .= ($id_perusahaan!=0)?' AND id_perusahaan='.$id_perusahaan:'';
      $tambahan .= $rangetanggal;
      // $tambahan .= ($data['alldataselect']||$data['lengkap'])?' AND urutan=0':'';
      $limit=($data['alldata'])?'':' OFFSET '.(($data['page']-1)*$data['perpage']).' ROWS FETCH NEXT '.$data['perpage'].' ROWS ONLY';
      $q = $this->db->query('SELECT '.$select.' FROM v_pengeluaranku WHERE 1=1'.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit);
  		// if ($data['posisi']=='owner') {
  		// 	$q = $this->db->query('SELECT '.$select.' FROM v_pengeluaran_owner WHERE 1=1'.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit);
  		// } else {
  		// 	$q = $this->db->query('SELECT '.$select.' FROM v_pengeluaran_owner WHERE 1=1'.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit);
  		// }
      //$q = $this->db->query('exec sp_data_t_pengeluaran @id_usr ='.$id_user);
  		$response = ($id_pengeluaran==0)?$q->result():$q->row();
      //$response = 'SELECT '.$select.' FROM v_pengeluaran_owner WHERE 1=1'.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit;
      return $response;
    }


}
