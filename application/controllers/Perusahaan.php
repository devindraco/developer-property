<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perusahaan extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct() {
		 parent::__construct();
	 }
	 public function index()
 	{
		$aktif=isset($_GET['aktif'])?$_GET['aktif']:2;
		$this->setlayouts('utama','perusahaan/utama',['perusahaanku'=>$this->m_perusahaan->perusahaanku(0,true,$aktif),'menudipilih'=>'Master Data']);
 	}
	public function tambah()
 {
		 $this->setlayouts('listperusahaan','perusahaan/_form',['action'=>'tambah','menudipilih'=>'Master Data']);
 }
 public function edit($id=-1)
{
	if ($dataperusahaan=$this->m_perusahaan->perusahaanku($id,true)) {
		$_POST=(array)$dataperusahaan;
		$this->setlayouts('listperusahaan','perusahaan/_form',['action'=>'ubah','menudipilih'=>'Master Data']);
	}
}
	public function listperusahaan()
	{
		$perusahaanku=$this->m_perusahaan->perusahaanku(0,false,1);
		if (!$perusahaanku) {
			redirect('perusahaan/tambah', 'refresh');
		} else {
			$this->setlayouts('listperusahaan','perusahaan/listperusahaan',['perusahaanku'=>$this->m_perusahaan->perusahaanku(0,false,1),'menudipilih'=>'dashboard']);
		}

	}
	public function gantiperusahaan($id=1)
	{
		$perusahaanku = $this->m_perusahaan->perusahaanku($id);
			if ($perusahaanku!=null) {
				$_SESSION['id_perusahaan'] = $id;
				$_SESSION['nama_perusahaan'] = $perusahaanku->nama_perusahaan;
				redirect('', 'refresh');
			}
	}
}
