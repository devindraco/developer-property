<?php
$id_user=isset($_SESSION['id_user'])?$_SESSION['id_user']:(isset($id_user)?$id_user:0);
$auth_key=isset($_SESSION['auth_key'])?$_SESSION['auth_key']:(isset($auth_key)?$auth_key:'');
$posisi=isset($_SESSION['posisi'])?$_SESSION['posisi']:(isset($posisi)?$posisi:'user');
$id_perusahaan=isset($_SESSION['id_perusahaan'])?$_SESSION['id_perusahaan']:(isset($id_perusahaan)?$id_perusahaan:0);
$id_perumahan=isset($id_perumahan)?$id_perumahan:0;
$live=isset($live)?$live:true; $edit=isset($edit)?$edit:true;
$id_nup=isset($id_nup)?$id_nup:0;
$listunit = $this->t_nup->listunitku(['id_user'=>$id_user,'lengkap'=>false,'posisi'=>$posisi,'auth_key'=>$auth_key,'id_perusahaan'=>$id_perusahaan,'id_nup'=>$id_nup,'id_perumahan'=>$id_perumahan,'orderby'=>'id_nup DESC','alldata'=>true]);

$jsdraw=isset($jsdraw)?$jsdraw:'';
$jsdrawend=isset($jsdrawend)?$jsdrawend:'';
 ?>
<style type="text/css">
.inner p {
  margin: 0;color:black;font-size: 12px;
}
canvas {
    pointer-events: none;       /* make the canvas transparent to the mouse - needed since canvas is position infront of image */
    position: absolute;
}
</style>
<!-- /.login-box-body -->

<div id="petabro"  style="width:900px">
  <canvas id='myCanvas'></canvas>
  <center><img id="gambarpetaperumahan" class="previewimagepeta" src="<?=base_url()?>assets/images/nup/peta.jpg" width="900px" alt="gambarpetaperumahan" usemap="#petaperumahan">
  <map name="petaperumahan">
    <?php
    foreach ($listunit as $key => $value) {
      echo '<area shape="poly" coords="'.$value->coords.'"';
      if ($live) {
        if ($value->status==2 || $value->nokupon!=0) {
          $jsdraw.='drawPoly(\''.$value->coords.'\',\'blue\'); ';
        } else {
          $jsdraw.='drawPoly(\''.$value->coords.'\',\'blue\',true); ';
        }
      } else{
        if ($edit) {
          $jsdraw.='drawPoly(\''.$value->coords.'\',\'yellow\'); ';
        } else {
          if ($value->status==2 || $value->nokupon!=0) {
            $jsdraw.='drawPoly(\''.$value->coords.'\',\'red\'); ';
          } else {
            $jsdraw.='drawPoly(\''.$value->coords.'\',\'red\',true); ';
            echo ' onclick="pilihunit(\''.$value->id_unit.'\',this.getAttribute(\'coords\'))"';
          }
          echo ' onmouseover="unithover(\''.$key.'\',this.getAttribute(\'coords\'),\'red\')" onmouseout="unitleave();"';
        }
      }
      echo '>';
    }
    ?>
  </map>
  </center>
</div>

<script type="text/javascript">
var posisix=0,posisiy=0;
listunit=<?=json_encode($listunit)?>;
window.addEventListener('DOMContentLoaded', (event) => {
  initpeta();
  $(window).resize(initpeta);
  var linkimg = '<?=base_url()?>assets/images/nup/<?=$id_nup?>/peta.jpg';
  var defaultimg = '<?=base_url()?>assets/images/nup/peta.jpg';
  if (checkfileada(linkimg)){$('#gambarpetaperumahan').attr('src',linkimg);}else{$('#gambarpetaperumahan').attr('src',defaultimg);}
  $(document).ready(function(){
    <?php if ($edit) {?>
      $('#petabro').click(function(event) {
          posisix = event.pageX - $(this).offset().left;
          posisiy = event.pageY - $(this).offset().top;
          if (typeof gantikoordinatunit == 'function') gantikoordinatunit(posisix,posisiy);
      });
      <?php } ?>
  });
});
var hdc;
function initpeta()
{
    // get the target image
    var img = document.getElementById('gambarpetaperumahan');

    var x,y, w,h;

    // get it's position and width+height
    x = img.offsetLeft;
    y = img.offsetTop;
    w = img.clientWidth;
    h = img.clientHeight;

    // move the canvas, so it's contained by the same parent as the image
    var imgParent = img.parentNode;
    var can = document.getElementById('myCanvas');
    imgParent.appendChild(can);

    // place the canvas in front of the image
    can.style.zIndex = 1;

    // position it over the image
    can.style.left = x+'px';
    can.style.top = y+'px';

    // make same size as the image
    can.setAttribute('width', w+'px');
    can.setAttribute('height', h+'px');

    // get it's context
    hdc = can.getContext('2d');

    // set the 'default' values for the colour/width of fill/stroke operations
    hdc.fillStyle = '#000';
    hdc.strokeStyle = 'red';
    hdc.lineWidth = 2;
    <?= ($jsdraw!='')?$jsdraw:''; ?>
    <?= ($jsdrawend!='')?$jsdrawend:''; ?>
}
function drawPoly(coOrdStr,color,stroke=false)
{
    hdc.globalCompositeOperation = 'source-over';
    if (stroke) hdc.strokeStyle = color; else hdc.fillStyle = color;
    var mCoords = coOrdStr.split(',');
    hdc.beginPath();
    $.each(mCoords, function( index, koordinat ) {
      if (index%2==0) {
        if (index==0) hdc.moveTo(mCoords[index], mCoords[index+1]); else hdc.lineTo(mCoords[index], mCoords[index+1]);
      }
    });
    hdc.closePath();
    if (stroke) hdc.stroke(); else hdc.fill();

    //hdc.fillRect(left,top,right-left,bot-top);
}
function clearPoly(coOrdStr) {
  hdc.globalCompositeOperation = 'destination-out';
  var mCoords = coOrdStr.split(',');
  hdc.beginPath();
  $.each(mCoords, function( index, koordinat ) {
    if (index%2==0) {
      if (index==0) hdc.moveTo(mCoords[index], mCoords[index+1]); else hdc.lineTo(mCoords[index], mCoords[index+1]);
    }
  });
  hdc.closePath();
  hdc.fill();
  hdc.fill();
}
function myLeave()
{
    var canvas = document.getElementById('myCanvas');
    hdc.clearRect(0, 0, canvas.width, canvas.height);
    <?= ($jsdraw!='')?$jsdraw:''; ?>

}


</script>
