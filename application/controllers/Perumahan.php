<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perumahan extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct() {
		 parent::__construct();
	 }
	 public function index($page=1)
 	{
		$this->load->model('m_perumahan');
		$aktif=isset($_GET['aktif'])?$_GET['aktif']:2;
		$perpage=isset($_GET['perpage'])?$_GET['perpage']:25;
		$page=(int)$page;
		$this->setlayouts('utama','perumahan/utama',['model'=>$this->m_perumahan,'menudipilih'=>'Master Data']);
 	}
	public function tambah()
 {
	 	$this->load->model('m_perumahan');
		$this->setlayouts('utama','perumahan/_form',['action'=>'tambah','perusahaanku'=>$this->m_perusahaan->perusahaanku(0,false,1),'menudipilih'=>'Master Data']);
 }
 public function edit($id=-1)
{
	$this->load->model('m_perumahan');
	if ($dataperumahan=$this->m_perumahan->perumahanku($id,true)) {
		$_POST=(array)$dataperumahan;
		$this->setlayouts('utama','perumahan/_form',['action'=>'edit','perusahaanku'=>$this->m_perusahaan->perusahaanku(0,false,1),'menudipilih'=>'Master Data']);
	}
}
	public function detail($id_perumahan=0)
	{
		$this->load->model('m_unit');
			$this->setlayouts('utama','perumahan/_detailperumahan',['perumahanku'=>$this->m_unit->unitku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key'],'id_perumahan'=>$id_perumahan,'id_perusahaan'=>$_SESSION['id_perusahaan']]),'menudipilih'=>'dashboard']);
	}
	public function listperumahan()
	{
		$this->load->model('m_perumahan');
			$this->setlayouts('utama','perumahan/_listperumahan',['perumahanku'=>$this->m_perumahan->perumahanku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key'],'id_perusahaan'=>$_SESSION['id_perusahaan']]),'menudipilih'=>'dashboard']);
	}
	public function gantiperumahan($id=1)
	{
			if ($this->m_perumahan->perumahanku($id)!=null) {
				$_SESSION['id_perumahan'] = $id;
				redirect('', 'refresh');
			}
	}
}
