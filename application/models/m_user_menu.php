<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_user_menu extends CI_Model {

  function menuicon($id_menu) {
    $arricon =array(
      1 => 'files-o',
      2 => 'money',
      3 => 'money',
      4 => 'money',
      5 => 'money',
      6 => 'money',
      25 => 'check',
      53 =>'th',
      57 =>'calendar',
    );
    return isset($arricon[$id_menu])?$arricon[$id_menu]:'circle-o';
  }

  function listmenuku($datainput){
    $response = '';
    $data = array(
      'id_user'=>isset($datainput['id_user'])?$datainput['id_user']:0,
      'lengkap'=>isset($datainput['lengkap'])?$datainput['lengkap']:false,
      'aktif'=>isset($datainput['aktif'])?$datainput['aktif']:2,
      'nama'=>isset($datainput['nama'])?$datainput['nama']:'',
      'posisi'=>isset($datainput['posisi'])?$datainput['posisi']:'user',
      'id_userutama'=>isset($datainput['id_userutama'])?$datainput['id_userutama']:0,
      'auth_keyutama'=>isset($datainput['auth_keyutama'])?$datainput['auth_keyutama']:'-',
    );
    $id_user = (int)($data['id_user']);
    $id_userutama = (int)($data['id_userutama']);
    $auth_keyutama = htmlspecialchars($data['auth_keyutama']);
    $q = $this->db->query('SELECT id_menu, id_parent, urutan, nama_menu, slug, act_tambah, act_ubah, act_hapus FROM v_user_menu WHERE id_user='.$id_user.' ORDER BY id_parent, urutan');
    $response = $q->result_array();
    return $response;
  }


  function getMenudata(){
    $id_user = $_SESSION['id_user'];
    $auth_key = $_SESSION['auth_key'];
    $response = array();
    if ($_SESSION['posisi']=='owner') {
      $this->load->model('m_user');
      $quser = $this->m_user->getUser(['id_user'=>$id_user,'auth_key'=>$auth_key]);
      if ($quser!=null) {
        if ($quser->auth_key==$auth_key) {
          $q = $this->db->query('SELECT *,1 as act_tambah,1 as act_ubah,1 as act_hapus FROM m_menu ORDER BY id_parent, urutan');
          $response = $q->result_array();
        }
      }
    } else {
      $q = $this->db->query('SELECT * FROM v_user_menu WHERE id_user='.$id_user.' AND auth_key=\''.$auth_key.'\' ORDER BY id_parent, urutan');
      $response = $q->result_array();
    }
    return $response;
  }

  function getMenuanak($induk,$data,$type="array",$menudipilih='dashboard'){
    $datahasil = array(); $html='';
    foreach ($data as $key => $value) {
      if ($value['id_parent']==$induk) {
        $anak = $this->getMenuanak($value['id_menu'],$data,$type,$menudipilih);
          if ($anak!=null) {
            $menudashboard = '';
            if ($key==0) {
              $activedash = ($menudipilih=='dashboard')?' active':'';
              if ($value['nama_menu']=='Master Data') {
                  $html.='<li class="header">Master Menu</li>';
                  $menudashboard = '<li class="header">Menu Utama</li>';
              } else {
                $html.='<li class="header">Menu Utama</li>';
              }
            }
            $active = ($menudipilih==$value['nama_menu'])?' active':'';
            $html.='<li class="treeview'.$active.'">
              <a href="#">
                <i class="fa fa-'.$this->menuicon($value['id_menu']).'"></i>
                <span>'.$value['nama_menu'].'</span>
    			<span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
              '.$anak.'
              </ul>
            </li>'.$menudashboard;
          } else {
            $html.='<li><a href="'.base_url().$value["slug"].'"><i class="fa fa-'.$this->menuicon($value['id_menu']).'"></i> <span>'.$value['nama_menu'].'</span></a></li>';
          }
          $datahasil[]=$value;
      }
    }
    return ($type=='array')?$datahasil:$html;
  }
  function getMenu($menudipilih='dashboard'){
    return $this->getMenuanak(NULL,$this->keamanan->usermenu,'html',$menudipilih);
  }


}
