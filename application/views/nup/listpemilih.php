<?php $perusahaanku=$this->m_perusahaan->perusahaanku(); ?>
<div class="box">
  <div class="box-header">
    <h3 class="box-title">List Pemilih</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <button onclick="$('#filter').toggle(300);" class="btn btn-primary pull-right"><i class="fa fa-gear"></i></button>
    <div id="filter" class="row" style="display:none">
    <h4 class="col-xs-12">Filer</h4>
    <div class="form-group col-xs-3">
      <label>Tampilkan</label>
      <select id="perpage" name="perpage" class="form-control filterinput">
        <option value="25" selected>25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="300">100</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Urutkan</label>
      <select id="orderby" name="orderby" class="form-control filterinput">
        <option value="id_nup DESC" selected>Terbaru</option>
        <option value="id_nup ASC">Terlama</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Nomor</label>
      <input type="text" class="form-control filterinput" name="nomor" id="nomor">
    </div>
    <div class="form-group col-xs-12">
      <button id="filterbutton" onclick="$('#filter').toggle(300);fitersubmit();" class="btn btn-primary pull-right"> Terapkan </button>
    </div>
  </div>
  <div>
    <table id="tabelnup" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th style="width:40px">No.</th>
        <th style="width:30px"></th>
        <th>No. Kupon</th>
        <th>Unit</th>
      </tr>
      </thead>
      <tbody>
        <tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>
      </tbody>
      <tfoot>
      <tr>
        <th>No.</th>
        <th></th>
        <th>No. Kupon</th>
        <th>Unit</th>
      </tr>
      </tfoot>
    </table>
  </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer clearfix">
    <button id="btnproses" class="btn btn-success" onclick="prosesdata();">Proses</button>
  </div>
</div>
<!-- /.box -->

<script type="text/javascript">
function dataajax() {
  $( "#tabelnup>tbody" ).html('<tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>');
  var request = $.ajax({
    url: "<?=base_url()?>ajax/listpemilihnup",
    method: "POST",
    data: {
      'posisi': '<?=$_SESSION['posisi']?>',
      'orderby': $("#orderby").val(),
      'id_nup': <?=$id_nup?>,
      'status': 1,
      'id_user': <?=$_SESSION['id_user']?>,
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>',
      'auth_key': '<?=$_SESSION['auth_key']?>',
      '_csrf': '<?=$this->keamanan->generatecsrf()?>',
      'alldata':true
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    var hasil = '';
    console.log(datahasil);
    $.each(datahasil, function( index, nup ) {
      var status = (nup["aktif"]==1)?'Aktif':'Tidak Aktif';
      hasil +='<tr><td>'+(index+1)+'</td><td><input type="checkbox" name="check['+nup["id_unit"]+']" /></td>'+
      '<td>'+nup["nokupon"]+'</td>'+
      '<td>'+nup["nomor"]+' Tipe '+nup["nama_tipe"]+'</td></tr>';
    });
    $( "#tabelnup>tbody" ).html(hasil);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
</script>
