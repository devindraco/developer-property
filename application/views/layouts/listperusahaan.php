<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$action=isset($action)?$action:'index';
$title=isset($title)?$title:'Pilih Perusahaan sebagai ('.$_SESSION['nama'].')';
?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=$title?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/sid/assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/sid/assets/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="/sid/assets/adminlte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/sid/assets/adminlte/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="/sid/assets/adminlte/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<style type="text/css">
.small-box {

    border-radius: 2px;
    position: relative;
    display: block;
    margin-bottom: 20px;
    box-shadow: 0 1px 1px rgba(0,0,0,0.1);
	min-height:140px;
	background-color: #00c0ef !important;
	color:white!important;
}
.small-box .icon {
    -webkit-transition: all .3s linear;
    -o-transition: all .3s linear;
    transition: all .3s linear;
    position: absolute;
    top: -10px;
    right: 10px;
    z-index: 0;
    font-size: 90px;
    color: rgba(0,0,0,0.15);
}
.small-box h3 {
    font-size: 28px;
    font-weight: bold;
    margin: 0 0 10px 0;
    white-space: normal;
    padding: 0;
	color:white!important;
}
.small-box .inner {
    padding: 10px;
	min-height:140px;
}
</style>
<div class="container">
  <div class="login-logo" style="margin-top:30px">
    <?=$_SESSION['nama']?>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <?php if ($action=='tambah'||$action=='ubah') echo '<h2 class="message">'.ucfirst($action).' Perusahaan</h2>';?>
<?=$contents?>
<?=($this->keamanan->allowedaction['tambah'] && $action=='tambah')?'<button id="btntambah" type="button" class="btn btn-primary pull-right" onclick="tambahdataajax()">Simpan</button>':'';?>
<?=($this->keamanan->allowedaction['ubah'] && $action=='ubah')?'<button id="btnubah" type="button" class="btn btn-primary pull-right" onclick="updatedataajax()">Simpan</button>':'';?>
<div style="clear:both"></div>
</div>

</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="/sid/assets/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="/sid/assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="/sid/assets/adminlte/plugins/iCheck/icheck.min.js"></script>
<script src="<?=base_url()?>assets/adminlte/dist/js/adminlte.min.js"></script>
<?=($this->keamanan->allowedaction['ubah']||$this->keamanan->allowedaction['tambah'])?'<script src="'.base_url().'assets/js/form.js"></script>':'';?>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
  <?php if ($this->keamanan->allowedaction['ubah'] && $action=='ubah') { ?>
    function dataajax() {
      $(location).attr('href','<?=base_url()?>perusahaan/listperusahaan');
    }
    function updatedataajax() {
      var request = $.ajax({
        url: "<?=base_url()?>ajax/updatedataperusahaan",
        method: "POST",
        data: $(".login-box-body .databerubah").serialize(),
        dataType: "json"
      });

      request.done(function( datahasil ) {
        dataajax();
      });

      request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
        dataajax();
      });
  }
  <?php } ?>
  <?php if ($this->keamanan->allowedaction['tambah'] && $action=='tambah') { ?>
    function tambahdataajax() {
      var request = $.ajax({
        url: "<?=base_url()?>ajax/tambahdataperusahaan",
        method: "POST",
        data: $(".login-box-body .datatambah").serialize(),
        dataType: "json"
      });

      request.done(function( datahasil ) {
        //console.log(datahasil);
        $(location).attr('href','<?=base_url()?>perusahaan/listperusahaan');
      });

      request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
      });
  }
  <?php } ?>
</script>
</body>
</html>
