<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_marketing extends CI_Model {

  function kolom($id='') {
    $listkolom = array(
      'id_marketing'=> ['label'=>'ID Marketing','default'=>0,'type'=>'text'],
      'nama_marketing'=> ['label'=>'Nama Marketing','default'=>'','type'=>'text'],
      'id_perusahaan'=> ['label'=>'Perusahaan','default'=>0,'type'=>'select'],
      'alamat'=> ['label'=>'Alamat','default'=>'','type'=>'textarea'],
      'kota'=> ['label'=>'Kota','default'=>'Surabaya','type'=>'text'],
      'propinsi'=> ['label'=>'Propinsi','default'=>'Jawa Timur','type'=>'text'],
      'telp'=> ['label'=>'Telepon','default'=>0,'type'=>'text'],
      'hp'=> ['label'=>'HP','default'=>0,'type'=>'text'],
      'jenis'=> ['label'=>'Jenis','default'=>1,'type'=>'select'],
      'aktif'=> ['label'=>'Status','default'=>1,'type'=>'checkbox']
    );
    if (isset($listkolom[$id])) {
      return $listkolom[$id];
    } else {
      return $listkolom;
    }
  }

  function kolomlengkap($id='') {
    $listkolom = array(
      'id_marketing'=> ['label'=>'ID Marketing','default'=>0,'type'=>'text'],
      'nama_marketing'=> ['label'=>'Nama Marketing','default'=>'','type'=>'text'],
      'jenis'=> ['label'=>'Jenis','default'=>1,'type'=>'select'],
      'alamat'=> ['label'=>'Alamat','default'=>'','type'=>'textarea'],
      'kota'=> ['label'=>'Kota','default'=>'Surabaya','type'=>'text'],
      'propinsi'=> ['label'=>'Propinsi','default'=>'Jawa Timur','type'=>'text'],
      'telp'=> ['label'=>'Telepon','default'=>0,'type'=>'text'],
      'hp'=> ['label'=>'HP','default'=>0,'type'=>'text']
    );
    if (isset($listkolom[$id])) {
      return $listkolom[$id];
    } else {
      return $listkolom;
    }
  }

  function listjenis($id='') {
    $liststatus = array(
      ['jenis'=>1,'label'=> 'Inhouse'],
      ['jenis'=>2,'label'=> 'Agency'],
      ['jenis'=>3,'label'=> 'Person']
    );
    if (isset($liststatus[$id])) {
      return $liststatus[$id];
    } else {
      return $liststatus;
    }
  }

  function dataupdate($data) {
    $sql = null;
    if (isset($data['id_marketing'])) {
      $sql = array();
      $query = '';
      foreach ($data['id_marketing'] as $key => $value) {
        $sql[$key]='EXEC sp_m_marketing @id_userutama='.$data['id_userutama'].', @auth_keyutama=\''.$data['auth_keyutama'].'\',';
        foreach ($this->kolom() as $id => $label) {
          if (isset($data[$id])) {
            $sql[$key].=isset($data[$id][$key])?' @'.$id.'=\''.$data[$id][$key].'\',':'';
          }
        }
        $sql[$key]=substr($sql[$key], 0, -1);
        $sql[$key].=';';
        $query.=$sql[$key].' ';
      }
      if ($query!=''){
        $q=$this->db->query($query);
        //$sql=$q->result_array();
      }
    }
    return '';
  }

  function datainsert($data) {
    $query='EXEC sp_m_marketing @id_userutama='.$data['id_userutama'].', @auth_keyutama=\''.$data['auth_keyutama'].'\',';
    $data['id_marketing']=0;
    foreach ($this->kolom() as $id => $label) {
      if (isset($data[$id])) {
        $data[$id]=($id=='aktif')?(($data[$id]=='on')?1:0):$data[$id];
        $query.=' @'.$id.'=\''.$data[$id].'\',';
      }
    }
    $query=substr($query, 0, -1);
    $query.=';';
    if ($query!=''){
      $q=$this->db->query($query);
      $sql=$q->result_array();
    }
    return '';
  }

  function datahapus($data) {
    $data['id_userutama']=isset($data['id_userutama'])?$data['id_userutama']:0;
    $data['auth_keyutama']=isset($data['auth_keyutama'])?$data['auth_keyutama']:'-';
    $data['id_marketing']=isset($data['id_marketing'])?$data['id_marketing']:0;
    $query='EXEC sp_m_marketing @id_userutama='.$data['id_userutama'].', @auth_keyutama=\''.$data['auth_keyutama'].'\', @id_marketing=\''.$data['id_marketing'].'\', @aksi=\'hapus\';';
    $q=$this->db->query($query);
    return '';
  }

  function marketingku($datainput){
  	$response = null;
      $data = array(
        'id_marketing'=>isset($datainput['id_marketing'])?$datainput['id_marketing']:0,
        'lengkap'=>isset($datainput['lengkap'])?$datainput['lengkap']:false,
        'aktif'=>isset($datainput['aktif'])?$datainput['aktif']:2,
        'page'=>isset($datainput['page'])?$datainput['page']:1,
        'perpage'=>isset($datainput['perpage'])?$datainput['perpage']:25,
        'nama_marketing'=>isset($datainput['nama_marketing'])?$datainput['nama_marketing']:'',
        'orderby'=>isset($datainput['orderby'])?$datainput['orderby']:'id_marketing DESC',
        'alldata'=>isset($datainput['alldata'])?$datainput['alldata']:false,
        'posisi'=>isset($datainput['posisi'])?$datainput['posisi']:'user',
        'id_perusahaan'=>isset($datainput['id_perusahaan'])?$datainput['id_perusahaan']:0,
        'id_user'=>isset($datainput['id_userutama'])?$datainput['id_userutama']:0,
        'auth_key'=>isset($datainput['auth_keyutama'])?$datainput['auth_keyutama']:'-',
      );
  		$response = array();
  		$id_user = htmlspecialchars($data['id_user']);
      $auth_key = htmlspecialchars($data['auth_key']);
      $id_marketing = htmlspecialchars($data['id_marketing']);
      $nama_marketing = htmlspecialchars($data['nama_marketing']);
      $aktif = (int)$data['aktif'];
      $id_perusahaan = ((int)$data['id_perusahaan']>=1)?(int)$data['id_perusahaan']:0;
      $page = ((int)$data['page']>=1)?(int)$data['page']:1;
      $perpage = ((int)$data['perpage']>=1)?(int)$data['perpage']:25;
      $orderby = $data['orderby'];
  		$checkid = ($id_marketing!=0)?' AND id_marketing='.$id_marketing:'';
      $select = ($data['lengkap'])?'*':'id_marketing,aktif,id_perusahaan,nama_marketing';
      $aktif = ($aktif==1||$aktif==0)?' AND aktif='.$aktif:'';
      $tambahan = ($nama_marketing!='')?' AND nama_marketing LIKE \'%'.$nama_marketing.'%\'':'';
      $tambahan .= ' AND id_user='.$id_user.' AND auth_key=\''.$auth_key.'\'';
      $tambahan .= ($id_perusahaan!=0)?' AND id_perusahaan ='.$id_perusahaan:'';
      $limit=($data['alldata'])?'':' OFFSET '.(($data['page']-1)*$data['perpage']).' ROWS FETCH NEXT '.$data['perpage'].' ROWS ONLY';
      $q = $this->db->query('SELECT '.$select.' FROM v_marketingku WHERE 1=1'.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit);
  		// if ($data['posisi']=='owner') {
  		// 	$q = $this->db->query('SELECT '.$select.' FROM v_marketing_owner WHERE 1=1'.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit);
  		// } else {
  		// 	$q = $this->db->query('SELECT '.$select.' FROM v_marketing_owner WHERE 1=1'.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit);
  		// }
      //$q = $this->db->query('exec sp_data_m_marketing @id_usr ='.$id_user);
  		$response = ($id_marketing==0)?$q->result():$q->row();
      //$response = 'SELECT '.$select.' FROM v_marketing_owner WHERE id_user='.$id_user.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit;
      return $response;
    }


}
