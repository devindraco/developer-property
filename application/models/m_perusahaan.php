<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_perusahaan extends CI_Model {
  function kolom($id='') {
    $listkolom = array(
      'id_perusahaan'=> ['label'=>'ID Perusahaan','default'=>0,'type'=>'text'],
      'id_pemilik'=> ['label'=>'Pemilik','default'=>0,'type'=>'select'],
      'nama_perusahaan'=> ['label'=>'Nama Perusahaan','default'=>'','type'=>'text'],
      'alamat'=> ['label'=>'Alamat','default'=>'','type'=>'textarea'],
      'kota'=> ['label'=>'Kota','default'=>'Surabaya','type'=>'text'],
      'propinsi'=> ['label'=>'Propinsi','default'=>'Jawa Timur','type'=>'text'],
      'telp'=> ['label'=>'Telepon','default'=>0,'type'=>'text'],
      'website'=> ['label'=>'Website','default'=>'','type'=>'text'],
      'npwp'=> ['label'=>'NPWP','default'=>'','type'=>'text'],
      'aktif'=> ['label'=>'Status','default'=>1,'type'=>'checkbox']
    );
    if (isset($listkolom[$id])) {
      return $listkolom[$id];
    } else {
      return $listkolom;
    }
  }

  function datainsert($data) {
    $data['id_user']=isset($data['id_user'])?$data['id_user']:0;
    $data['auth_key']=isset($data['auth_key'])?$data['auth_key']:'-';
    $query='EXEC sp_m_perusahaan @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\',';
    foreach ($this->kolom() as $id => $label) {
      if (isset($data[$id])) {
        $data[$id]=($id=='aktif')?(($data[$id]=='on')?1:0):$data[$id];
        $query.=' @'.$id.'=\''.$data[$id].'\',';
      }
    }
    $query=substr($query, 0, -1);
    $query.=';';
    if ($query!=''){
      $q=$this->db->query($query);
      $sql=$q->result_array();
    }
    return '';
  }

  function datahapus($data) {
    $data['id_user']=isset($data['id_user'])?$data['id_user']:0;
    $data['auth_key']=isset($data['auth_key'])?$data['auth_key']:'-';
    $data['id_perusahaan']=isset($data['id_perusahaan'])?$data['id_perusahaan']:0;
    $query='EXEC sp_m_perusahaan @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\', @id_perusahaan=\''.$data['id_perusahaan'].'\', @aksi=\'hapus\';';
    $q=$this->db->query($query);
    return '';
  }

  function perusahaanku($id_perusahaan=0,$lengkap=false,$aktif=1){
	$response = null;
	if ($this->session->has_userdata('id_user') && $this->session->has_userdata('posisi')) {
		$response = array();
		$id_user = htmlspecialchars($_SESSION['id_user']);
		$id_perusahaan = htmlspecialchars($id_perusahaan);
    $aktif = (int)$aktif;
		$querytambahan = ($id_perusahaan!=0)?' AND id_perusahaan='.$id_perusahaan:'';
    // $select = ($lengkap)?'pr.*':'pr.id_perusahaan, pr.nama_perusahaan';
    $select = ($lengkap)?'*':'id_perusahaan, nama_perusahaan';
    $aktif = ($aktif==1||$aktif==0)?' AND aktif='.$aktif:'';
    $q = $this->db->query('SELECT '.$select.' FROM v_perusahaanku WHERE id_user='.$id_user.$querytambahan.$aktif.' ORDER BY id_perusahaan DESC');
		// if ($_SESSION['posisi']=='owner') {
		// 	$q = $this->db->query('SELECT '.$select.' FROM m_perusahaan pr WHERE id_pemilik='.$id_user.$querytambahan.$aktif);
		// } else {
		// 	$q = $this->db->query('SELECT '.$select.' FROM m_user_perumahan up LEFT OUTER JOIN m_perumahan pe ON up.id_perumahan=pe.id_perumahan LEFT OUTER JOIN m_perusahaan pr ON pe.id_perusahaan=pr.id_perusahaan WHERE id_user='.$id_user.$querytambahan.$aktif);
		// }
		$response = ($id_perusahaan==0)?$q->result():$q->row();
	}
    return $response;
  }

  function perumahanku($id_perusahaan=0,$lengkap=false,$aktif=1){
	$response = null;
	if ($this->session->has_userdata('id_user') && $this->session->has_userdata('posisi')) {
		$response = array();
		$id_user = htmlspecialchars($_SESSION['id_user']);
		$id_perusahaan = htmlspecialchars($id_perusahaan);
    $aktif = (int)$aktif;
		$querytambahan = ($id_perusahaan!=0)?' AND pr.id_perusahaan='.$id_perusahaan:'';
    $select = ($lengkap)?'*':'pr.id_perusahaan,nama_perusahaan,id_perumahan,nama_perumahan,pe.aktif';
    $aktif = ($aktif==1||$aktif==0)?' AND pr.aktif='.$aktif.' AND pe.aktif='.$aktif:'';
    // $aktif = ($aktif==1||$aktif==0)?' AND pr.aktif='.$aktif.' AND (pe.aktif='.$aktif.' OR pe.aktif IS NULL)':'';
		if ($_SESSION['posisi']=='owner') {
			$q = $this->db->query('SELECT '.$select.' FROM m_perusahaan pr LEFT OUTER JOIN m_perumahan pe ON pr.id_perusahaan=pe.id_perusahaan WHERE pr.id_pemilik='.$id_user.$querytambahan.$aktif.' ORDER BY pr.id_perusahaan DESC');
		} else {
			$q = $this->db->query('SELECT '.$select.' FROM m_perusahaan pr LEFT OUTER JOIN m_perumahan pe ON pr.id_perusahaan=pe.id_perusahaan WHERE pr.id_pemilik='.$id_user.$querytambahan.$aktif.' ORDER BY pr.id_perusahaan DESC');
		}
		$response = ($id_perusahaan==0)?$q->result():$q->row();
	}
    return $response;
  }

}
