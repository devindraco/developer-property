<?php
$error=isset($error)?$error:'';
?>
  <p class="login-box-msg">List Perusahaan</p>
<?php
foreach($perusahaanku as $key => $perusahaan) {
  echo '<div class="col-md-4 col-sm-6"><div class="small-box">
    <a href="/sid/perusahaan/gantiperusahaan/'.$perusahaan->id_perusahaan.'">
          <div class="inner">
            <h3>'.$perusahaan->nama_perusahaan.'</h3>
            <p>&nbsp;</p>
          </div>
          <div class="icon">
            <i class="fa fa-home"></i>
          </div>
    </a>
        </div>
    </div>';
}
if ($this->keamanan->allowedaction['tambah']) {
 echo '<div class="col-sm-3" style="float:left;width:170px"><div class="small-box" style="background-color:orange!important">
    <a href="/sid/perusahaan/tambah">
          <div class="inner">
            <div style="top:10px;right:35px;position:absolute;font-size:90px;color:white"><i class="fa fa-plus"></i></div>
            <p>&nbsp;</p>
          </div>
          <div class="icon">

          </div>
    </a>
        </div>
    </div>';
  }
echo '<div style="clear:both">&nbsp;</div>';
if ($error!='') {
  echo '<div class="alert alert-danger alert-dismissible" style="margin-top: 20px;">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              '.$error.'
            </div>';
} ?>
<!-- /.login-box-body -->
