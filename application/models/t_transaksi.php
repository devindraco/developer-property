<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_transaksi extends CI_Model {

  function kolom($id='') {
    $listkolom = array(
      'id_pesanan'=> ['label'=>'ID transaksi','default'=>0,'type'=>'text'],
      'no_pesanan'=> ['label'=>'No. Pesanan','default'=>'','type'=>'text'],
      'id_unit'=> ['label'=>'Unit','default'=>'','type'=>'pilihanpopup'],
      'id_customer'=> ['label'=>'Customer','default'=>'','type'=>'pilihanpopup'],
      'tanggal'=> ['label'=>'Tanggal','default'=>date('Y-m-d'),'type'=>'date'],
      'alamat_surat_menyurat'=> ['label'=>'Alamat Surat Menyurat','default'=>'','type'=>'textarea'],
      'harga_jual'=> ['label'=>'Harga Jual','default'=>0,'type'=>'number'],
      'jenis_bayar'=> ['label'=>'Jenis Pembelian','default'=>'Cash','type'=>'select'],
      'nama_diskon'=> ['label'=>'Nama Diskon','default'=>'','type'=>'text'],
      'diskon1'=> ['label'=>'Diskon 1','default'=>0,'type'=>'number'],
      'diskon2'=> ['label'=>'Diskon 2','default'=>0,'type'=>'number'],
      'diskon3'=> ['label'=>'Diskon 3','default'=>0,'type'=>'number'],
      'diskon1_rp'=> ['label'=>'Diskon Rupiah 1','default'=>0,'type'=>'number'],
      'diskon2_rp'=> ['label'=>'Diskon Rupiah 1','default'=>0,'type'=>'number'],
      'diskon3_rp'=> ['label'=>'Diskon Rupiah 1','default'=>0,'type'=>'number'],
      'cashback'=> ['label'=>'Cashback','default'=>0,'type'=>'number'],
      'harga_net'=> ['label'=>'Harga NET','default'=>0,'type'=>'number'],
      'uang_tanda_jadi'=> ['label'=>'Uang Tanda Jadi','default'=>5000000,'type'=>'number'],
      'uang_muka'=> ['label'=>'Uang Muka','default'=>0,'type'=>'number'],
      'uang_muka_rp'=> ['label'=>'Uang Muka','default'=>0,'type'=>'number'],
      'lama_cicilan'=> ['label'=>'Lama Cicilan','default'=>3,'type'=>'number'],
      'catatan'=> ['label'=>'Catatan','default'=>'','type'=>'textarea'],
      'id_marketing'=> ['label'=>'Nama Marketing','default'=>'','type'=>'pilihanpopup'],
      'tanggal_approve'=> ['label'=>'Tanggal Approve','default'=>date('Y-m-d'),'type'=>'date'],
      'id_user_approve'=> ['label'=>'Approver','default'=>0,'type'=>'select'],
      'status'=> ['label'=>'Status','default'=>'Pending','type'=>'text']
    );
    if (isset($listkolom[$id])) {
      return $listkolom[$id];
    } else {
      return $listkolom;
    }
  }

  function kolomkecil($id='') {
    $listkolom = array(
      'id_pesanan'=> ['label'=>'ID transaksi','default'=>0,'type'=>'text'],
      'no_pesanan'=> ['label'=>'No. Pesanan','default'=>'','type'=>'text'],
      'id_unit'=> ['label'=>'Unit','default'=>'','type'=>'pilihanpopup'],
      'nama_perumahan'=> ['label'=>'Perumahan','default'=>'','type'=>'pilihanpopup'],
      'id_customer'=> ['label'=>'Customer','default'=>'','type'=>'pilihanpopup'],
      'tanggal'=> ['label'=>'Tanggal','default'=>date('Y-m-d'),'type'=>'date'],
      'alamat_surat_menyurat'=> ['label'=>'Alamat Surat Menyurat','default'=>'','type'=>'textarea'],
      'harga_jual'=> ['label'=>'Harga Jual','default'=>0,'type'=>'number'],
      'jenis_bayar'=> ['label'=>'Jenis Pembelian','default'=>'Cash','type'=>'select'],
      'nama_diskon'=> ['label'=>'Nama Diskon','default'=>'','type'=>'text'],
      'diskon1'=> ['label'=>'Diskon','default'=>0,'type'=>'number'],
      'diskon1_rp'=> ['label'=>'Diskon','default'=>0,'type'=>'number'],
      'cashback'=> ['label'=>'Cashback','default'=>0,'type'=>'number'],
      'harga_net'=> ['label'=>'Harga NET','default'=>0,'type'=>'number'],
      'uang_tanda_jadi'=> ['label'=>'Uang Tanda Jadi','default'=>5000000,'type'=>'number'],
      'uang_muka'=> ['label'=>'Uang Muka','default'=>0,'type'=>'number'],
      'uang_muka_rp'=> ['label'=>'Uang Muka','default'=>0,'type'=>'number'],
      'lama_cicilan'=> ['label'=>'Lama Cicilan','default'=>3,'type'=>'number'],
      'catatan'=> ['label'=>'Catatan','default'=>'','type'=>'textarea'],
      'id_marketing'=> ['label'=>'Nama Marketing','default'=>'','type'=>'pilihanpopup'],
      'tanggal_approve'=> ['label'=>'Tanggal Approve','default'=>date('Y-m-d'),'type'=>'date'],
      'id_user_approve'=> ['label'=>'Approver','default'=>0,'type'=>'select'],
      'status'=> ['label'=>'Status','default'=>'Pending','type'=>'text']
    );
    if (isset($listkolom[$id])) {
      return $listkolom[$id];
    } else {
      return $listkolom;
    }
  }
function listkolomdokumen($id='') {
  $listkolom = array(
    'id_pesanan' => 'id_pesanan', // --------- Pesanan ----------
    'no_pesanan' => 'no_pesanan',
    'harga_jual' => 'harga_jual',
    'harga_net' => 'harga_net',
    'jenis_bayar' => 'jenis_bayar',
    'lama_cicilan' => 'lama_cicilan',
    'alamat_surat_menyurat' => 'alamat_surat_menyurat',
    'nama_diskon' => 'nama_diskon',
    'diskon1' => 'diskon1',
    //'diskon1_rp' => 'diskon1_rp',
    // 'diskon2' => 'diskon2',
    // 'diskon2_rp' => 'diskon2_rp',
    // 'diskon3' => 'diskon3',
    // 'diskon3_rp' => 'diskon3_rp',
    'cashback' => 'cashback',
    'catatan' => 'catatan',
    'tanggal' => 'tanggal',
    'tanggal_approve' => 'tanggal_approve',
    'uang_muka' => 'uang_muka',
    'uang_muka_rp' => 'uang_muka_rp',
    //'detail_transaksi' => 'detail_transaksi',
    'uang_tanda_jadi' => 'uang_tanda_jadi',
    'id_customer' => 'id_customer', // --------- Customer ----------
    'nama_customer' => 'nama_customer',
    'alamat_customer' => 'alamat_customer',
    'telp_customer' => 'telp_customer',
    'hp1' => 'hp1',
    'hp2' => 'hp2',
    'jenis_identitas' => 'jenis_identitas',
    'no_identitas' => 'no_identitas',
    'email_customer' => 'email_customer',
    'npwp_customer' => 'npwp_customer',
    'id_unit' => 'id_unit', // --------- UNIT ----------
    'unit' => 'unit',
    'nomor' => 'nomor',
    'id_cluster' => 'id_cluster', // --------- CLUSTER ----------
    'nama_cluster' => 'nama_cluster',
    'id_tipe' => 'id_tipe', // --------- TIPE ----------
    'nama_tipe' => 'nama_tipe',
    'catatan_tipe' => 'catatan_tipe',
    'luas_bangunan' => 'luas_bangunan',
    'panjang' => 'panjang',
    'lebar' => 'lebar',
    'id_perumahan' => 'id_perumahan', // --------- Perumahan ----------
    'nama_perumahan' => 'nama_perumahan',
    'alamat_perumahan' => 'alamat_perumahan',
    'kota_perumahan' => 'kota_perumahan',
    'propinsi_perumahan' => 'propinsi_perumahan',
    'telp_perumahan' => 'telp_perumahan',
    'website_perumahan' => 'website_perumahan',
    'bank_perumahan' => 'bank_perumahan',
    'id_perusahaan' => 'id_perusahaan', // --------- Perusahaan ----------
    'nama_perusahaan' => 'nama_perusahaan',
    'alamat_perusahaan' => 'alamat_perusahaan',
    'kota_perusahaan' => 'kota_perusahaan',
    'propinsi_perusahaan' => 'propinsi_perusahaan',
    'telp_perusahaan' => 'telp_perusahaan',
    'website_perusahaan' => 'website_perusahaan',
    'npwp_perusahaan' => 'npwp_perusahaan',
    'id_marketing' => 'id_marketing', // --------- Marketing ----------
    'nama_marketing' => 'nama_marketing',
    'alamat_marketing' => 'alamat_marketing',
    'kota_marketing' => 'kota_marketing',
    'propinsi_marketing' => 'propinsi_marketing',
    'telp_marketing' => 'telp_marketing',
    'hp_marketing' => 'hp_marketing',
    'jenis_marketing' => 'jenis_marketing',
    'id_user_pembuat' => 'id_user_pembuat', // --------- Pembuat ----------
    'nama_depan_pembuat' => 'nama_depan_pembuat',
    'nama_belakang_pembuat' => 'nama_belakang_pembuat',
    'jabatan_pembuat' => 'jabatan_pembuat',
    'id_user_approve' => 'id_user_approve', // --------- Approver ----------
    'nama_depan_approve' => 'nama_depan_approve',
    'nama_belakang_approve' => 'nama_belakang_approve',
    'jabatan_approve' => 'jabatan_approve'
  );


  if (isset($listkolom[$id])) {
    return $listkolom[$id];
  } else {
    return $listkolom;
  }
}
  function liststatus($id='') {
    $liststatus = array(
      ['status'=>'Pending','label'=> 'Pending'],
      ['status'=>'Approve','label'=> 'Approve'],
      ['status'=>'Reject','label'=> 'Reject'],
      ['status'=>'Lunas','label'=> 'Lunas'],
      ['status'=>'Done','label'=> 'Done'],
    );
    if (isset($liststatus[$id])) {
      return $liststatus[$id];
    } else {
      return $liststatus;
    }
  }

  function dataupdate($data) {
    $sql = null;
    if (isset($data['id_pesanan'])) {
      $sql = array();
      $query = '';
      foreach ($data['id_pesanan'] as $key => $value) {
        $sql[$key]='EXEC sp_t_pesanan @id_userutama='.$data['id_userutama'].', @auth_keyutama=\''.$data['auth_keyutama'].'\',';
        foreach ($this->kolom() as $id => $label) {
          if (isset($data[$id])) {
            $sql[$key].=isset($data[$id][$key])?' @'.$id.'=\''.$data[$id][$key].'\',':'';
          }
        }
        $sql[$key]=substr($sql[$key], 0, -1);
        $sql[$key].=';';
        $query.=$sql[$key].' ';
      }
      if ($query!=''){
        $q=$this->db->query($query);
        //$sql=$q->result_array();
      }
    }
    return '';
  }

  function datainsert($data) {
    $query='EXEC sp_t_pesanan @id_userutama='.$data['id_userutama'].', @id_user='.$data['id_userutama'].', @auth_keyutama=\''.$data['auth_keyutama'].'\',';
    $data['id_pesanan']=0;
    $pakaidiskon = false;$uangmukarp = '';
    if (isset($data['checkdiskon'])) { if ($data['checkdiskon']=='on') { $pakaidiskon = true;}}
    if (isset($data['typeuang_muka'])) { if ($data['typeuang_muka']==2 && isset($data['uang_muka'])) { $uangmukarp = $data['uang_muka'];}}
    foreach ($this->kolom() as $id => $kolom) {
      if (isset($data[$id])) {
        $data[$id]=($id=='aktif')?(($data[$id]=='on')?1:0):$data[$id];
        $data[$id]=($id=='id_user_approve')?0:$data[$id];
        $data[$id]=($id=='tanggal_approve')?null:$data[$id];
        if (!$pakaidiskon) { $data[$id]=($id=='nama_diskon')?'':$data[$id];
          $data[$id]=($id=='diskon1')?0:$data[$id];$data[$id]=($id=='diskon2')?0:$data[$id];$data[$id]=($id=='diskon3')?0:$data[$id];
          $data[$id]=($id=='diskon1rp')?0:$data[$id];$data[$id]=($id=='diskon2rp')?0:$data[$id];$data[$id]=($id=='diskon3rp')?0:$data[$id];
        }
        $data[$id]=($id=='uang_muka'&&$uangmukarp!='')?0:$data[$id];
        $data[$id]=($id=='uang_muka_rp'&&$uangmukarp!='')?$uangmukarp:$data[$id];
        $query.=' @'.$id.'=\''.$data[$id].'\',';
      } else {
        $query.=' @'.$id.'=\''.$kolom['default'].'\',';
      }
    }
    $query=substr($query, 0, -1);
    $query.=';';
    if ($query!=''){
      $q=$this->db->query($query);
      $hasil=$q->row_array();
    }
    $query='';
    if (isset($data['cicilanke'])&&isset($data['jatuhtempoke'])&&isset($data['keteranganke'])&& isset($hasil['id_pesanan'])) {
      $query='INSERT INTO t_pesanan_dtl VALUES ('.$hasil["id_pesanan"].',\''.$data["uang_tanda_jadi"].'\',\'Uang Tanda Jadi\',0,\''.$data["tanggal"].'\'),';
      foreach ($data['cicilanke'] as $idcicilan => $cicilanke) {
        $query.=' ('.$hasil["id_pesanan"].',\''.$data["cicilanke"][$idcicilan].'\',\''.$data["keteranganke"][$idcicilan].'\','.$idcicilan.',\''.$data["jatuhtempoke"][$idcicilan].'\'),';
      }
      $query=substr($query, 0, -1);
      $query.=';';
    }
    if ($query!=''){
      $q=$this->db->query($query);
      //$sql=$this->db->error()['message'];
    }
    //print_r($sql);
    return $query;
  }

  function datahapus($data) {
    $data['id_userutama']=isset($data['id_userutama'])?$data['id_userutama']:0;
    $data['auth_keyutama']=isset($data['auth_keyutama'])?$data['auth_keyutama']:'-';
    $data['id_pesanan']=isset($data['id_pesanan'])?$data['id_pesanan']:0;
    $query='EXEC sp_m_pesanan @id_userutama='.$data['id_userutama'].', @auth_keyutama=\''.$data['auth_keyutama'].'\', @id_pesanan=\''.$data['id_pesanan'].'\', @aksi=\'hapus\';';
    $q=$this->db->query($query);
    return '';
  }

  function approve($data) {
    $sql = null;
    $query = '';
    if (isset($data['id_pesanan'])) {
      $sql = array();
      $query='EXEC sp_approval_pesanan @id_userutama='.$data['id_user'].', @auth_keyutama=\''.$data['auth_key'].'\', @id_pesanan='.$data['id_pesanan'].', @status=\''.$data['status'].'\', @id_perusahaan='.$data['id_perusahaan'].', @tanggal_approve=\''.date('Y-m-d').'\';';
      if ($query!=''){
        $q=$this->db->query($query);
      }
    }
    return '';
  }

  function transaksidetailku($datainput){
    $response = null;
      $data = array(
        'id_pesanan'=>isset($datainput['id_pesanan'])?$datainput['id_pesanan']:0,
        'posisi'=>isset($datainput['posisi'])?$datainput['posisi']:'user',
        'no_pesanan'=>isset($datainput['no_pesanan'])?$datainput['no_pesanan']:'',
        'status'=>isset($datainput['status'])?$datainput['status']:'Semua',
        'id_userutama'=>isset($datainput['id_userutama'])?$datainput['id_userutama']:0,
        'auth_keyutama'=>isset($datainput['auth_keyutama'])?$datainput['auth_keyutama']:'-',
        'id_perusahaan'=>isset($datainput['id_perusahaan'])?$datainput['id_perusahaan']:0,
      );
      $response = array();
      $id_userutama = htmlspecialchars($data['id_userutama']);
      $auth_keyutama = htmlspecialchars($data['auth_keyutama']);
      $id_pesanan= htmlspecialchars($data['id_pesanan']);
      $status = htmlspecialchars($data['status']);
      $no_pesanan = htmlspecialchars($data['no_pesanan']);
      $id_perusahaan = ((int)$data['id_perusahaan']>=1)?(int)$data['id_perusahaan']:0;
      $checkid = ($id_pesanan!=0)?' AND id_pesanan='.$id_pesanan:'';
      $select = 'id_pesanan,cicilan,keterangan,urutan,jatuh_tempo';
      $status = ($status!='Semua')?' AND status=\''.$status.'\'':'';
      $tambahan = ($no_pesanan!='')?' AND no_pesanan LIKE \'%'.$no_pesanan.'%\'':'';
      $tambahan .= ' AND id_userutama='.$id_userutama.' AND auth_keyutama=\''.$auth_keyutama.'\'';
      $tambahan .= ($id_perusahaan!=0)?' AND id_perusahaan='.$id_perusahaan:'';
      if ($data['posisi']=='owner') {
        $q = $this->db->query('SELECT '.$select.' FROM v_pesanan_owner WHERE 1=1'.$checkid.$status.$tambahan.' ORDER BY urutan');
      } else {
        $q = $this->db->query('SELECT '.$select.' FROM v_pesanan_owner WHERE 1=1'.$checkid.$status.$tambahan.' ORDER BY urutan');
      }
      //$q = $this->db->query('exec sp_data_t_transaksi @id_usr ='.$id_user);
      $response = $q->result_array();
      // $response = 'SELECT '.$select.' FROM v_pesanan_owner WHERE 1=1'.$checkid.$status.$tambahan.' ORDER BY urutan';
      return $response;
  }

  function transaksiku($datainput){
  	$response = null;
      $data = array(
        'id_pesanan'=>isset($datainput['id_pesanan'])?$datainput['id_pesanan']:0,
        'lengkap'=>isset($datainput['lengkap'])?$datainput['lengkap']:false,
        'status'=>isset($datainput['status'])?$datainput['status']:'Semua',
        'page'=>isset($datainput['page'])?$datainput['page']:1,
        'perpage'=>isset($datainput['perpage'])?$datainput['perpage']:25,
        'no_pesanan'=>isset($datainput['no_pesanan'])?$datainput['no_pesanan']:'',
        'orderby'=>isset($datainput['orderby'])?$datainput['orderby']:'id_pesanan DESC',
        'alldata'=>isset($datainput['alldata'])?$datainput['alldata']:false,
        'alldataselect'=>isset($datainput['alldataselect'])?$datainput['alldataselect']:false,
        'posisi'=>isset($datainput['posisi'])?$datainput['posisi']:'user',
        'rangetanggal'=>isset($datainput['rangetanggal'])?$datainput['rangetanggal']:'',
        'id_perusahaan'=>isset($datainput['id_perusahaan'])?$datainput['id_perusahaan']:0,
        'id_userutama'=>isset($datainput['id_userutama'])?$datainput['id_userutama']:0,
        'auth_keyutama'=>isset($datainput['auth_keyutama'])?$datainput['auth_keyutama']:'-',
      );
  		$response = array();
  		$id_userutama = htmlspecialchars($data['id_userutama']);
      $auth_keyutama = htmlspecialchars($data['auth_keyutama']);
      $id_pesanan= htmlspecialchars($data['id_pesanan']);
      $no_pesanan = htmlspecialchars($data['no_pesanan']);
      $status = htmlspecialchars($data['status']);
      $page = ((int)$data['page']>=1)?(int)$data['page']:1;
      $perpage = ((int)$data['perpage']>=1)?(int)$data['perpage']:25;
      $id_perusahaan = ((int)$data['id_perusahaan']>=1)?(int)$data['id_perusahaan']:0;
      $orderby = $data['orderby'];
      $rangetanggal=$data['rangetanggal'];
      if ($rangetanggal!=''){
          $rangetanggal = ' AND tanggal BETWEEN '.str_replace('Hingga','and',$rangetanggal);
      }
  		$checkid = ($id_pesanan!=0)?' AND id_pesanan='.$id_pesanan:'';
      $select = ($data['lengkap'])?'id_pesanan,id_marketing,id_unit,id_customer,id_user,no_pesanan,alamat_surat_menyurat,tanggal,jenis_bayar,lama_cicilan,harga_jual,nama_diskon,diskon1,diskon2,diskon3,diskon1_rp,diskon2_rp,diskon3_rp,cashback,harga_net,uang_tanda_jadi,uang_muka,uang_muka_rp,catatan,tanggal_approve,id_user_approve,status,nomor,nama_perumahan,nama_tipe,nama_customer,nama_marketing':'id_pesanan,status,id_perusahaan,no_pesanan';
      $select = ($data['alldataselect'])?'id_pesanan, no_pesanan, alamat_surat_menyurat, tanggal, jenis_bayar, lama_cicilan, harga_jual, nama_diskon, diskon1, diskon2, diskon3, diskon1_rp, diskon2_rp, diskon3_rp, cashback, harga_net, uang_tanda_jadi, uang_muka, uang_muka_rp, catatan, status, tanggal_approve, id_unit, nomor, status_unit, id_tipe, aktif_unit, nama_tipe, catatan_tipe, luas_bangunan, panjang, lebar, harga, id_cluster, aktif_cluster, nama_cluster, id_perumahan, nama_perumahan, alamat_perumahan, kota_perumahan, propinsi_perumahan, telp_perumahan, website_perumahan, max_cicilan_cash, max_cicilan_kpr, max_cicilan_inhouse, max_persen_dp, default_uang_tanda_jadi, aktif_perumahan, bank_perumahan, id_perusahaan, nama_perusahaan, id_pemilik, alamat_perusahaan, kota_perusahaan, propinsi_perusahaan, telp_perusahaan, website_perusahaan, npwp_perusahaan, aktif_perusahaan, id_user, auth_key, nama_depan, nama_belakang, jabatan, aktif_user, id_marketing, nama_marketing, alamat_marketing, kota_marketing, propinsi_marketing, telp_marketing, hp_marketing, jenis_marketing, aktif_marketing, id_customer, nama_customer, alamat_customer, telp_customer, hp1, hp2, jenis_identitas, no_identitas, email_customer, npwp_customer, aktif_customer, id_user_pembuat, nama_depan_pembuat, nama_belakang_pembuat, jabatan_pembuat, id_user_approve, nama_depan_approve, nama_belakang_approve, jabatan_approve, cicilan, keterangan, urutan, jatuh_tempo':$select;
      $status = ($status!='Semua')?' AND status=\''.$status.'\'':'';
      $tambahan = ($no_pesanan!='')?' AND no_pesanan LIKE \'%'.$no_pesanan.'%\'':'';
      $tambahan .= ' AND id_user='.$id_userutama.' AND auth_key=\''.$auth_keyutama.'\'';
      $tambahan .= ($id_perusahaan!=0)?' AND id_perusahaan='.$id_perusahaan:'';
      $tambahan .= $rangetanggal;
      $tambahan .= ($data['alldataselect']||$data['lengkap'])?' AND urutan=0':'';
      $limit=($data['alldata'])?'':' OFFSET '.(($page-1)*$page).' ROWS FETCH NEXT '.$perpage.' ROWS ONLY';
      $q = $this->db->query('SELECT '.$select.' FROM v_pesananku WHERE 1=1'.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit);
  		// if ($data['posisi']=='owner') {
  		// 	$q = $this->db->query('SELECT '.$select.' FROM v_pesananku WHERE 1=1'.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit);
  		// } else {
  		// 	$q = $this->db->query('SELECT '.$select.' FROM v_pesanan_owner WHERE 1=1'.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit);
  		// }
      //$q = $this->db->query('exec sp_data_t_transaksi @id_usr ='.$id_user);
  		$response = ($id_pesanan==0)?$q->result():$q->row();
      //$response = 'SELECT '.$select.' FROM v_pesanan_owner WHERE 1=1'.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit;
      return $response;
    }


}
