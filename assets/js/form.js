/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/
 // Document Function
if (typeof(dataajax)=='function') {
 function changepage(next=true) {
   if (next) page+=1; else if(page>1) page-=1;
   dataajax();
 }
 function fitersubmit() {
   page=1;
   dataajax();
 }
}
function inputenter(aksi,e,ini) {
 if(e.keyCode == 13){
   if (aksi=='tambah') {
    $('#btntambah').click();
  } else if (aksi=='ubah') {
   $('.inputno'+ini).addClass('databerubah');
   $('#btnubah').click();
 }
}
}
 function inputchange(ini) {
   $('.inputno'+ini).addClass('databerubah');
 }
 document.onkeydown = function(){
  if(window.event && window.event.keyCode == 113)
  {
     ubahdata(false);
  }
 }
 function formdiindex(type='text',id=1,name='name',value='Nama',datas,textselect='nama',link='',required=' required') {
   var formtext = '';
   var nilai = '';
   switch (type) {
     case 'hidden':
       formtext ='<p>'+value+'</p><input type="hidden" class="inputno'+id+'" name="'+name+'['+id+']" id="'+name+'['+id+']" idno="'+id+'" value="'+id+'"'+required+'">';
       break;
     case 'hiddenval':
       formtext ='<p>'+value+'</p><input type="hidden" class="inputno'+id+'" name="'+name+'['+id+']" id="'+name+'['+id+']" idno="'+id+'" value="'+value+'"'+required+'">';
       break;
    case 'pilihanpopup' :
         formtext ='<p>'+value+'</p><input id="tombol'+name+id+'" type="text" class="inputno'+id+'" name="'+name+'['+id+']" value="'+datas[textselect]+'" '+required+' readonly onclick="openmodal'+$name+'()" onkeydown="openmodal'+$name+'()" style="cursor:pointer"><input id="'+name+id+'" type="text" class="inputno'+id+'" name="'+name+'['+id+']" value="'+value+'" '+required+' >';
         break;
     case 'select':
       formtext ='<select class="form-control inputubah inputno'+id+'" id="'+name+'['+id+']" idno="'+id+'" name="'+name+'['+id+']" style="display:none">';
       $.each(datas, function( index, data ) {
           var dipilih=(value==data[name])?' selected="selected"':'';
           nilai=(value==data[name])?data[textselect]:nilai;
           formtext +='<option value="'+data[name]+'"'+dipilih+'>'+data[textselect]+'</option>';
       });
       formtext +='</select>';
       formtext +=(link=='')?'<p class="textubah">'+nilai+'</p>':'<a class="textubah" target="_blank" href="'+link+'">'+nilai+'</a>';
       break;
     case 'text':
       formtext ='<p class="textubah">'+value+'</p><input type="text" class="form-control inputubah inputno'+id+'" name="'+name+'['+id+']" id="'+name+'['+id+']" idno="'+id+'" value="'+value+'"'+required+' style="display:none" onkeydown="inputenter(\'ubah\',event,$(this).attr(\'idno\'))" onchange="inputchange($(this).attr(\'idno\'))">';
       break;
     case 'number':
       formtext ='<p class="textubah">'+value+'</p><input type="number" class="form-control inputubah inputno'+id+'" name="'+name+'['+id+']" id="'+name+'['+id+']" idno="'+id+'" value="'+value+'"'+required+' style="display:none" onkeydown="inputenter(\'ubah\',event,$(this).attr(\'idno\'))" onchange="inputchange($(this).attr(\'idno\'))">';
       break;
   }
   return formtext;
 }
 function ubahdata(reset=false,submit=false) {
   if ($('#btnubah').text()=='Simpan' || reset) {
     $('#btnubah').text('Ubah');
     if (submit) {
       if (typeof(updatedataajax)=='function') {
         updatedataajax();
       }
     } else {
       $('tbody .databerubah').removeClass('databerubah');
       $('.textubah').show();$('.inputubah').hide();
     }
   } else if ($('#btnubah').text()=='Ubah') {
     $('tbody .databerubah').removeClass('databerubah');
     $('.textubah').hide();$('.inputubah').show();
     $('#btnubah').text('Simpan');
     $('.inputubah').first().focus();
   }
 }
 function getforminputdata(formid=''){
    var arr='{';
    $.each( $("#"+formid+" :input").serializeArray(), function( i, field ) {
      var data = field.name.replace('[', '":{'); data = data.replace(']', '');
          arr+='"'+ data+ ':"' +field.value+'"},';
        });
    arr+='}';
    return arr;
 }
