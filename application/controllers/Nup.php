<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nup extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct() {
		 parent::__construct();
	 }
	 public function index($page=1)
 	{
		$this->load->model('m_perumahan');
    $this->load->model('m_unit');
		$this->load->model('t_nup');

		$aktif=isset($_GET['aktif'])?$_GET['aktif']:2;
		$perpage=isset($_GET['perpage'])?$_GET['perpage']:25;
		$page=(int)$page;
		$this->setlayouts('utama','nup/utama',['model'=>$this->t_nup,'menudipilih'=>'NUP']);
 	}

 public function live($id_nup)
 {
	$this->load->model('m_perumahan');
	$this->load->model('m_unit');
	$this->load->model('t_nup');
	$id_user=isset($_SESSION['id_user'])?$_SESSION['id_user']:(isset($id_user)?$id_user:0);
	$auth_key=isset($_SESSION['auth_key'])?$_SESSION['auth_key']:(isset($auth_key)?$auth_key:'');
	$posisi=isset($_SESSION['posisi'])?$_SESSION['posisi']:(isset($posisi)?$posisi:'user');
	$id_perusahaan=isset($_SESSION['id_perusahaan'])?$_SESSION['id_perusahaan']:(isset($id_perusahaan)?$id_perusahaan:0);
	$id_perumahan=isset($id_perumahan)?$id_perumahan:0;
	$id_nup=isset($id_nup)?$id_nup:0;
	$nup = $this->t_nup->nupku(['id_user'=>$id_user,'lengkap'=>true,'posisi'=>$posisi,'auth_key'=>$auth_key,'id_perusahaan'=>$id_perusahaan,'id_nup'=>$id_nup,'id_perumahan'=>$id_perumahan,'aktif'=>1]);
	if ($nup!=null) {
		$this->setlayouts('kosong','nup/live',['id_nup'=>$id_nup,'menudipilih'=>'[LIVE] NUP '.$nup->nama_nup]);
	}
 }

 public function edit($id_nup)
 {
	$this->load->model('m_perumahan');
	$this->load->model('m_unit');
	$this->load->model('t_nup');
	$id_user=isset($_SESSION['id_user'])?$_SESSION['id_user']:(isset($id_user)?$id_user:0);
	$auth_key=isset($_SESSION['auth_key'])?$_SESSION['auth_key']:(isset($auth_key)?$auth_key:'');
	$posisi=isset($_SESSION['posisi'])?$_SESSION['posisi']:(isset($posisi)?$posisi:'user');
	$id_perusahaan=isset($_SESSION['id_perusahaan'])?$_SESSION['id_perusahaan']:(isset($id_perusahaan)?$id_perusahaan:0);
	$id_perumahan=isset($id_perumahan)?$id_perumahan:0;
	$id_nup=isset($id_nup)?$id_nup:0;
	$nup = $this->t_nup->nupku(['id_user'=>$id_user,'lengkap'=>true,'posisi'=>$posisi,'auth_key'=>$auth_key,'id_perusahaan'=>$id_perusahaan,'id_nup'=>$id_nup,'id_perumahan'=>$id_perumahan,'aktif'=>1]);
	if ($nup!=null) {
		$this->setlayouts('kosong','nup/edit',['id_nup'=>$id_nup,'id_perumahan'=>$nup->id_perumahan,'menudipilih'=>'NUP '.$nup->nama_nup]);
	}
 }

public function listpemilih($id_nup)
 {
 $this->load->model('m_perumahan');
 $this->load->model('m_unit');
 $this->load->model('t_nup');
 $id_user=isset($_SESSION['id_user'])?$_SESSION['id_user']:(isset($id_user)?$id_user:0);
 $auth_key=isset($_SESSION['auth_key'])?$_SESSION['auth_key']:(isset($auth_key)?$auth_key:'');
 $posisi=isset($_SESSION['posisi'])?$_SESSION['posisi']:(isset($posisi)?$posisi:'user');
 $id_perusahaan=isset($_SESSION['id_perusahaan'])?$_SESSION['id_perusahaan']:(isset($id_perusahaan)?$id_perusahaan:0);
 $id_perumahan=isset($id_perumahan)?$id_perumahan:0;
 $id_nup=isset($id_nup)?$id_nup:0;
 $nup = $this->t_nup->nupku(['id_user'=>$id_user,'lengkap'=>true,'posisi'=>$posisi,'auth_key'=>$auth_key,'id_perusahaan'=>$id_perusahaan,'id_nup'=>$id_nup,'id_perumahan'=>$id_perumahan,'aktif'=>1]);
 if ($nup!=null) {
	 $this->setlayouts('utama','nup/listpemilih',['id_nup'=>$id_nup,'id_perumahan'=>$nup->id_perumahan,'menudipilih'=>'NUP ']);
 }
}

 public function pilih($id_nup)
 {
	$this->load->model('m_perumahan');
	$this->load->model('m_unit');
	$this->load->model('t_nup');
	$id_user=isset($_SESSION['id_user'])?$_SESSION['id_user']:(isset($id_user)?$id_user:0);
	$auth_key=isset($_SESSION['auth_key'])?$_SESSION['auth_key']:(isset($auth_key)?$auth_key:'');
	$posisi=isset($_SESSION['posisi'])?$_SESSION['posisi']:(isset($posisi)?$posisi:'user');
	$id_perusahaan=isset($_SESSION['id_perusahaan'])?$_SESSION['id_perusahaan']:(isset($id_perusahaan)?$id_perusahaan:0);
	$id_perumahan=isset($id_perumahan)?$id_perumahan:0;
	$id_nup=isset($id_nup)?$id_nup:0;
	$nup = $this->t_nup->nupku(['id_user'=>$id_user,'lengkap'=>true,'posisi'=>$posisi,'auth_key'=>$auth_key,'id_perusahaan'=>$id_perusahaan,'id_nup'=>$id_nup,'id_perumahan'=>$id_perumahan,'aktif'=>1]);
	if ($nup!=null) {
		$this->setlayouts('kosong','nup/pilih',['id_nup'=>$id_nup,'menudipilih'=>'NUP '.$nup->nama_nup]);
	}
 }

	public function listmarketing()
	{
			$this->setlayouts('listmarketing','utama',['marketingku'=>$this->m_marketing->marketingku(0,false,1),'menudipilih'=>'dashboard']);
	}
}
