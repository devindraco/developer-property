<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct() {
		 parent::__construct();
	 }
	 public function index($page=1)
 	{
		$this->load->model('m_template_dokumen');
		$this->load->model('t_transaksi');
		$aktif=isset($_GET['aktif'])?$_GET['aktif']:2;
		$perpage=isset($_GET['perpage'])?$_GET['perpage']:25;
		$page=(int)$page;
		$this->setlayouts('utama','template_dokumen/utama',['model'=>$this->m_template_dokumen,'menudipilih'=>'Master Data']);
 	}
	public function tambah()
 {
	 	$this->load->model('m_template_dokumen');
		$this->setlayouts('utama','template_dokumen/_form',['action'=>'tambah','perusahaanku'=>$this->m_perusahaan->perusahaanku(0,false,1),'menudipilih'=>'Master Data']);
 }
 public function edit($id=-1)
{
	$this->load->model('m_template_dokumen');
	if ($datatemplate_dokumen=$this->m_template_dokumen->template_dokumenku($id,true)) {
		$_POST=(array)$datatemplate_dokumen;
		$this->setlayouts('utama','template_dokumen/_form',['action'=>'edit','perusahaanku'=>$this->m_perusahaan->perusahaanku(0,false,1),'menudipilih'=>'Master Data']);
	}
}
	public function detail($id_template_dokumen=0)
	{
		$this->load->model('m_unit');
			$this->setlayouts('utama','template_dokumen/_detailtemplate_dokumen',['template_dokumenku'=>$this->m_unit->unitku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key'],'id_template_dokumen'=>$id_template_dokumen,'id_perusahaan'=>$_SESSION['id_perusahaan']]),'menudipilih'=>'dashboard']);
	}
	public function listtemplate_dokumen()
	{
		$this->load->model('m_template_dokumen');
			$this->setlayouts('utama','template_dokumen/_listtemplate_dokumen',['template_dokumenku'=>$this->m_template_dokumen->template_dokumenku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key'],'id_perusahaan'=>$_SESSION['id_perusahaan']]),'menudipilih'=>'dashboard']);
	}
	public function gantitemplate_dokumen($id=1)
	{
			if ($this->m_template_dokumen->template_dokumenku($id)!=null) {
				$_SESSION['id_template_dokumen'] = $id;
				redirect('', 'refresh');
			}
	}
}
