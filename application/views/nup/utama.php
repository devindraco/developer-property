<?php $perusahaanku=$this->m_perusahaan->perusahaanku();
$perumahanku=$this->m_perumahan->perumahanku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]); ?>
<div class="box">
  <div class="box-header">
    <h3 class="box-title">List NUP</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <?=($this->keamanan->allowedaction['tambah'])?'<button class="btn btn-primary" onclick="$(\'#modal-tambah\').modal();$(\'.modal input.form-control\').first().focus()">Tambah</button>':''; ?>
    <button onclick="$('#filter').toggle(300);" class="btn btn-primary pull-right"><i class="fa fa-gear"></i></button>
    <div id="filter" class="row" style="display:none">
    <h4 class="col-xs-12">Filer</h4>
    <div class="form-group col-xs-3">
      <label>Status</label>
      <select id="status" name="status" class="form-control filterinput">
        <option value="a" selected>Semua</option>
        <?php foreach ($model->liststatus() as $key => $status) {
          echo '<option value="'.$status['status'].'">'.$status['label'].'</option>';
        } ?>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Tampilkan</label>
      <select id="perpage" name="perpage" class="form-control filterinput">
        <option value="25" selected>25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="300">100</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Urutkan</label>
      <select id="orderby" name="orderby" class="form-control filterinput">
        <option value="id_nup DESC" selected>Terbaru</option>
        <option value="id_nup ASC">Terlama</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Nomor</label>
      <input type="text" class="form-control filterinput" name="nomor" id="nomor">
    </div>
    <div class="form-group col-xs-12">
      <button id="filterbutton" onclick="$('#filter').toggle(300);fitersubmit();" class="btn btn-primary pull-right"> Terapkan </button>
    </div>
  </div>
  <div class="box-table">
    <table id="tabelnup" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <th></th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_nup') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      <?php if ($this->keamanan->allowedaction['ubah']) { ?>
      <tr style="display:none"><td><input type="hidden" name="id_user" class="databerubah" value="<?=$_SESSION['id_user']?>"></td><td><input type="hidden" name="auth_key" class="databerubah" value="<?=$_SESSION['auth_key']?>">
      <input type="hidden" name="_csrf" class="databerubah" value="<?=$this->keamanan->generatecsrf()?>"><input type="hidden" name="id_perusahaan" class="databerubah" value="<?=$_SESSION['id_perusahaan']?>"></td></tr>
    <?php } ?>
      </thead>
      <tbody>
        <tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>
      </tbody>
      <tfoot>
      <tr>
        <th>No.</th>
        <th></th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_nup') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      </tfoot>
    </table>
  </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-left">
      <li><?=($this->keamanan->allowedaction['ubah'])?'<button id="btnubah" class="btn btn-success" onclick="ubahdata(false,true);">Ubah</button>':''; ?></li>
    </ul>

    <ul class="pagination pagination-sm no-margin pull-right">
      <li><button id="btnprev" class="btn btn-default" onclick="changepage(false);">« Prev</button></li>
      <li><button id="btnnext" class="btn btn-default" onclick="changepage();">Next »</button></li>
    </ul>
  </div>
</div>
<!-- /.box -->
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
<div class="modal fade" id="modal-tambah">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah NUP</h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('nup/_form',['perumahanku'=>$perumahanku]); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button id="btntambah" type="button" class="btn btn-primary" onclick="tambahdataajax()">Simpan</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" id="modalinputid_unit">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Unit List <?=$_SESSION['nama_perusahaan']?></h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('unit/_listlengkap'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>
<script type="text/javascript">
var perpage = 25;
var page = 1;
var listperumahanku = <?=json_encode($perumahanku)?>;
var liststatus = <?=json_encode($model->liststatus())?>;

function dataajax() {
  perpage=$("#perpage").val();
  $( "#tabelnup>tbody" ).html('<tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>');
  var request = $.ajax({
    url: "<?=base_url()?>ajax/datanup",
    method: "POST",
    data: {
      'status': $('#status').val(),
      'nomor': $('#nomor').val(),
      'lengkap': 1,
      'page': page,
      'perpage': perpage,
      'posisi': '<?=$_SESSION['posisi']?>',
      'orderby': $("#orderby").val(),
      'id_user': <?=$_SESSION['id_user']?>,
      'auth_key': '<?=$_SESSION['auth_key']?>'
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    var hasil = '';
    console.log(datahasil);
    $.each(datahasil, function( index, nup ) {
      var status = (nup["aktif"]==1)?'Aktif':'Tidak Aktif';
      hasil +='<tr><?php
      $action='';
      if ($this->keamanan->allowedaction['hapus']) {
          $action.='<button class="btn btn-link" tabindex="-1" onclick="hapusdataajax(\'+nup["id_nup"]+\')"><i class="fa fa-trash"></i> Hapus</button> ';
      }
      $action.='<br/><a class="btn btn-link" target="_blank" tabindex="-1" href="'.base_url().'nup/edit/\'+nup["id_nup"]+\'"><i class="fa fa-building"></i> Edit Unit</a> ';
      $action.='<br/><a class="btn btn-link" target="_blank" tabindex="-1" href="'.base_url().'nup/pilih/\'+nup["id_nup"]+\'"><i class="fa fa-check"></i> Pilih Unit</a> ';
      $action.='<br/><a class="btn btn-link" target="_blank" tabindex="-1" href="'.base_url().'nup/listpemilih/\'+nup["id_nup"]+\'"><i class="fa fa-user"></i> List Pemilih</a> ';
      $action.='<br/><a class="btn btn-link" target="_blank" tabindex="-1" href="'.base_url().'nup/live/\'+nup["id_nup"]+\'"><i class="fa fa-desktop"></i> Live View</a> ';
      echo '<td>\'+formdiindex(\'hidden\',nup["id_nup"],\'id_nup\',(((page-1)*perpage)+index+1))+\'</td>';
      echo '<td style="min-width:0;padding:0">'.$action.'</td>';
      echo '<td>\'+formdiindex(\'text\',nup["id_nup"],\'kode_nup\',nup["kode_nup"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',nup["id_nup"],\'nama_nup\',nup["nama_nup"])+\'</td>';
      echo '<td>\'+formdiindex(\'select\',nup["id_nup"],\'id_perumahan\',nup["id_perumahan"],listperumahanku,\'nama_perumahan\',\''.base_url().'perumahan\')+\'</td>';
      echo '<td>\'+formdiindex(\'text\',nup["id_nup"],\'tanggal\',nup["tanggal"])+\'</td>';
      echo '<td>\'+formdiindex(\'select\',nup["id_nup"],\'status\',nup["status"],liststatus,\'label\')+\'</td>';
    /*  echo '<td>';
      if ($this->keamanan->allowedaction['ubah']) {
        echo '<a tabindex="-1" href="'.base_url().'nup/edit/\'+nup["id_nup"]+\'"><i class="fa fa-pencil"></i></a> ';
      }
      if ($this->keamanan->allowedaction['hapus']) {
          echo '<a tabindex="-1" href="'.base_url().'nup/hapus/\'+nup["id_nup"]+\'"><i class="fa fa-trash"></i></a> ';
      }
      echo '</td>'; */
      ?></tr>';
    });
    $( "#tabelnup>tbody" ).html(hasil);
    if (datahasil.length<perpage) $('#btnnext').attr("disabled", true); else $('#btnnext').attr("disabled", false);
    if (page<=1) $('#btnprev').attr("disabled", true); else $('#btnprev').attr("disabled", false);
    ubahdata(true);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
<?php if ($this->keamanan->allowedaction['ubah']) { ?>
  function updatedataajax() {
    var request = $.ajax({
      url: "<?=base_url()?>ajax/updatedatanup",
      method: "POST",
      data: $("#tabelnup .databerubah").serialize(),
      dataType: "json"
    });

    request.done(function( datahasil ) {
      console.log(datahasil);
      dataajax();
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      dataajax();
    });
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
  function tambahdataajax() {
    $('#modal-tambah').modal('toggle');;
    var request = $.ajax({
      url: "<?=base_url()?>ajax/tambahdatanup",
      method: "POST",
      data: $("#modal-tambah .datatambah").serialize(),
      dataType: "json"
    });

    request.done(function( datahasil ) {
      console.log(datahasil);
      dataajax();
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      dataajax();
    });
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['hapus']) { ?>
  function hapusdataajax(id_nup) {
    if (confirm('Apakah Anda yakin ingin menghapus data nup ini ?')) {
      var request = $.ajax({
        url: "<?=base_url()?>ajax/hapusdatanup",
        method: "POST",
        data: {
          'id_user': <?=$_SESSION['id_user']?>,
          'auth_key': '<?=$_SESSION['auth_key']?>',
          '_csrf': '<?=$this->keamanan->generatecsrf()?>',
          'id_nup': id_nup
        },
        dataType: "json"
      });

      request.done(function( datahasil ) {
        console.log(datahasil);
        dataajax();
      });

      request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
        dataajax();
      });
    }
}
<?php } ?>
</script>
