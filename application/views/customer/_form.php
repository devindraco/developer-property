<form id="formcustomer" method="post" enctype="multipart/form-data"  action="">
    <input class="datatambah" type="hidden" name="id_user" value="<?=$_SESSION['id_user']?>">
    <input class="datatambah" type="hidden" name="auth_key" value="<?=$_SESSION['auth_key']?>">
      <input class="datatambah" type="hidden" name="_csrf" value="<?=$this->keamanan->generatecsrf()?>">
      <input class="datatambah" type="hidden" name="id_customer" value="0">
    <?php
    foreach ($this->m_customer->kolom() as $key => $value) {
      if ($key!='id_customer'){
        $datas=($key=='id_perusahaan')?$perusahaanku:null;
        $namevalue=($key=='id_perusahaan')?'nama_perusahaan':'';
        $datas=($key=='aktif')?[['aktif'=>1,'id'=>'Aktif'],['aktif'=>0,'id'=>'Tidak Aktif']]:$datas;
        $namevalue=($key=='aktif')?'id':$namevalue;
        echo ($key=='jenis_identitas')?'<h4 style="clear:both">Identitas</h4>':'';
        echo $this->formku->generateinput($value['type'],$key,(isset($_POST[$key])?$_POST[$key]:$value['default']),$value['label'],$class='col-xs-6','fa-book',$datas,$namevalue);
        if($key=='no_identitas'||$key=='npwp'){
          $namafoto = ($key=='no_identitas')?'Identitas':'NPWP';
          echo '<div class="form-group col-xs-6 formimageFile'.$key.'">
            <label>Foto '.$namafoto.'</label>
            <input type="file" class="inputimage datatambah" style="display:none" data-name="imageFile'.$key.'" name="inputimageFile'.$key.'" id="inputimageFile'.$key.'" accept="image/*">
          <img class="previewimageimageFile'.$key.' previewimageimageFile img-responsive" src="'.base_url().'assets/images/default/'.$key.'.jpg" style="cursor:pointer" onclick="$(\'#inputimageFile'.$key.'\').click()">
          </div>';
        }
      }
    }
    ?>
    <div style="clear:both"></div>
  </form>
