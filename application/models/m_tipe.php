<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_tipe extends CI_Model {

  function kolom($id='') {
    $listkolom = array(
      'id_tipe'=> ['label'=>'ID Tipe','default'=>0,'type'=>'text'],
      'nama_tipe'=> ['label'=>'Nama Tipe','default'=>'','type'=>'text'],
      'id_cluster'=> ['label'=>'Cluster','default'=>1,'type'=>'select'],
      'harga'=> ['label'=>'Harga','default'=>0,'type'=>'number'],
      'lebar'=> ['label'=>'Lebar','default'=>0,'type'=>'number'],
      'panjang'=> ['label'=>'Panjang','default'=>0,'type'=>'number'],
      'luas_bangunan'=> ['label'=>'Luas Bangunan','default'=>0,'type'=>'number'],
      'catatan'=> ['label'=>'Catatan','default'=>'','type'=>'text'],
      'aktif'=> ['label'=>'Status','default'=>1,'type'=>'checkbox']
    );
    if (isset($listkolom[$id])) {
      return $listkolom[$id];
    } else {
      return $listkolom;
    }
  }

  function dataupdate($data) {
    $sql = null;
    if (isset($data['id_tipe'])) {
      $sql = array();
      $query = '';
      foreach ($data['id_tipe'] as $key => $value) {
        $sql[$key]='EXEC sp_m_tipe @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\',';
        foreach ($this->kolom() as $id => $label) {
          if (isset($data[$id])) {
            $sql[$key].=isset($data[$id][$key])?' @'.$id.'=\''.$data[$id][$key].'\',':'';
          }
        }
        $sql[$key]=substr($sql[$key], 0, -1);
        $sql[$key].=';';
        $query.=$sql[$key].' ';
      }
      if ($query!=''){
        $q=$this->db->query($query);
        //$sql=$q->result_array();
      }
    }
    return '';
  }

  function datainsert($data) {
    $query='EXEC sp_m_tipe @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\',';
    foreach ($this->kolom() as $id => $label) {
      if (isset($data[$id])) {
        $data[$id]=($id=='aktif')?(($data[$id]=='on')?1:0):$data[$id];
        $query.=' @'.$id.'=\''.$data[$id].'\',';
      }
    }
    $query=substr($query, 0, -1);
    $query.=';';
    if ($query!=''){
      $q=$this->db->query($query);
      //$sql=$q->result_array();
    }
    return '';
  }

  function datahapus($data) {
    $data['id_user']=isset($data['id_user'])?$data['id_user']:0;
    $data['auth_key']=isset($data['auth_key'])?$data['auth_key']:'-';
    $data['id_tipe']=isset($data['id_tipe'])?$data['id_tipe']:0;
    $query='EXEC sp_m_tipe @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\', @id_tipe=\''.$data['id_tipe'].'\', @aksi=\'hapus\';';
    $q=$this->db->query($query);
    return '';
  }

  function tipeku($datainput){
  $q = $this->load->model('m_user');
	$response = null;
    $data = array(
      'id_tipe'=>isset($datainput['id_tipe'])?$datainput['id_tipe']:0,
      'id_cluster'=>isset($datainput['id_cluster'])?$datainput['id_cluster']:0,
      'lengkap'=>isset($datainput['lengkap'])?$datainput['lengkap']:false,
      'aktif'=>isset($datainput['aktif'])?$datainput['aktif']:2,
      'page'=>isset($datainput['page'])?$datainput['page']:1,
      'perpage'=>isset($datainput['perpage'])?$datainput['perpage']:25,
      'nama_tipe'=>isset($datainput['nama_tipe'])?$datainput['nama_tipe']:'',
      'orderby'=>isset($datainput['orderby'])?$datainput['orderby']:'id_tipe DESC',
      'alldata'=>isset($datainput['alldata'])?$datainput['alldata']:false,
      'posisi'=>isset($datainput['posisi'])?$datainput['posisi']:'user',
      'id_user'=>isset($datainput['id_user'])?$datainput['id_user']:0,
      'auth_key'=>isset($datainput['auth_key'])?$datainput['auth_key']:'-',
    );
		$response = array();
		$id_user = htmlspecialchars($data['id_user']);
    $auth_key = htmlspecialchars($data['auth_key']);
		$id_cluster = htmlspecialchars($data['id_cluster']);
    $id_tipe = htmlspecialchars($data['id_tipe']);
    $nama_tipe = htmlspecialchars($data['nama_tipe']);
    $aktif = (int)$data['aktif'];
    $page = ((int)$data['page']>=1)?(int)$data['page']:1;
    $perpage = ((int)$data['perpage']>=1)?(int)$data['perpage']:25;
    $orderby = $data['orderby'];
		$checkid = ($id_tipe!=0)?' AND id_tipe='.$id_tipe:'';
    $select = ($data['lengkap'])?'*':'id_tipe,aktif,id_cluster,id_perusahaan,nama_tipe';
    $aktif = ($aktif==1||$aktif==0)?' AND aktif='.$aktif:'';
    $tambahan = ($nama_tipe!='')?' AND nama_tipe LIKE \'%'.$nama_tipe.'%\'':'';
    $tambahan = ' AND id_user='.$id_user.' AND auth_key=\''.$auth_key.'\'';
    $limit=($data['alldata'])?'':' OFFSET '.(($data['page']-1)*$data['perpage']).' ROWS FETCH NEXT '.$data['perpage'].' ROWS ONLY';
    $q = $this->db->query('SELECT '.$select.' FROM v_tipeku WHERE id_user='.$id_user.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit);
		// if ($data['posisi']=='owner') {
		// 	$q = $this->db->query('SELECT '.$select.' FROM v_tipe_owner WHERE id_user='.$id_user.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit);
		// } else {
		// 	$q = $this->db->query('SELECT '.$select.' FROM v_tipe_owner WHERE id_user='.$id_user.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit);
		// }
    //$q = $this->db->query('exec sp_data_m_cluster @id_usr ='.$id_user);
		$response = ($id_cluster==0)?$q->result():$q->row();
    //$response = 'SELECT '.$select.' FROM v_cluster_owner WHERE id_user='.$id_user.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit;
    return $response;
  }


}
