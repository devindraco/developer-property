/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/
 function formatDate(date) {
  var monthNames = [
    "Januari", "Februari", "Maret",
    "April", "Mei", "Jeni", "Juli",
    "Agustus", "September", "Oktober",
    "Nopember", "Desember"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}

function replacevariabeldokumen(teks,datas){
  $.each(datas, function( index, data ) {
    var datalama=new RegExp('{{'+index+'}}','g');
    teks=teks.replace(datalama,data);
  });
  return teks;
}

function checkfileada(image_url) {
  var http = new XMLHttpRequest();

    http.open('HEAD', image_url, false);
    http.send();

    return http.status != 404;

}

function htmlinputimage(name,kelas) {
  var html='<label>Foto</label><input type="file" class="'+kelas+' inputimage datatambah" style="display:none" data-name="'+name+'" name="input'+name+'" id="input'+name+'" accept="image/*">'+
            '<img class="previewimage'+name+' img-responsive" src="/sid/assets/images/default/kwitansi.jpg" style="cursor:pointer" onclick="$(\'#input'+name+'\').click()" />';
  return html;
}

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('.previewimage'+$(input).attr('data-name')).attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}
// After Document Load
$(function () {

  'use strict';

if (typeof(dataajax)=='function') {
  dataajax();
}
$('.filterinput').keydown(function (e){
    if(e.keyCode == 13){
        $('#filterbutton').click();
    }
});


$(".inputimage").change(function() {
  readURL(this);
});

  // Make the dashboard widgets sortable Using jquery UI
  $('.connectedSortable').sortable({
    containment         : $('section.content'),
    placeholder         : 'sort-highlight',
    connectWith         : '.connectedSortable',
    handle              : '.box-header, .nav-tabs',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });
  $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');
  $('form:first *:input[type!=hidden]:first').focus();
  // jQuery UI sortable for the todo list
  $('.todo-list').sortable({
    placeholder         : 'sort-highlight',
    handle              : '.handle',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });

if ( $( "#tabelperusahaan" ).length ) {
  var table = $('#tabelperusahaan').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false,
    'lengthMenu'  : [[10, 25, 50, -1], [10, 25, 50, 'All']]
  });
}
  // bootstrap WYSIHTML5 - text editor
  $('.textarea').wysihtml5();

  $('.daterange').daterangepicker({
    ranges   : {
      'Today'       : [moment(), moment()],
      'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month'  : [moment().startOf('month'), moment().endOf('month')],
      'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    startDate: moment().subtract(29, 'days'),
    endDate  : moment()
  }, function (start, end) {
    window.alert('You chose: ' + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
  });

  /* jQueryKnob */
  $('.knob').knob();

  // jvectormap data
  var visitorsData = {
    US: 398, // USA
    SA: 400, // Saudi Arabia
    CA: 1000, // Canada
    DE: 500, // Germany
    FR: 760, // France
    CN: 300, // China
    AU: 700, // Australia
    BR: 600, // Brazil
    IN: 800, // India
    GB: 320, // Great Britain
    RU: 3000 // Russia
  };
  // World map by jvectormap
  $('#world-map').vectorMap({
    map              : 'world_mill_en',
    backgroundColor  : 'transparent',
    regionStyle      : {
      initial: {
        fill            : '#e4e4e4',
        'fill-opacity'  : 1,
        stroke          : 'none',
        'stroke-width'  : 0,
        'stroke-opacity': 1
      }
    },
    series           : {
      regions: [
        {
          values           : visitorsData,
          scale            : ['#92c1dc', '#ebf4f9'],
          normalizeFunction: 'polynomial'
        }
      ]
    },
    onRegionLabelShow: function (e, el, code) {
      if (typeof visitorsData[code] != 'undefined')
        el.html(el.html() + ': ' + visitorsData[code] + ' new visitors');
    }
  });

  // Sparkline charts
  var myvalues = [1000, 1200, 920, 927, 931, 1027, 819, 930, 1021];
  $('#sparkline-1').sparkline(myvalues, {
    type     : 'line',
    lineColor: '#92c1dc',
    fillColor: '#ebf4f9',
    height   : '50',
    width    : '80'
  });
  myvalues = [515, 519, 520, 522, 652, 810, 370, 627, 319, 630, 921];
  $('#sparkline-2').sparkline(myvalues, {
    type     : 'line',
    lineColor: '#92c1dc',
    fillColor: '#ebf4f9',
    height   : '50',
    width    : '80'
  });
  myvalues = [15, 19, 20, 22, 33, 27, 31, 27, 19, 30, 21];
  $('#sparkline-3').sparkline(myvalues, {
    type     : 'line',
    lineColor: '#92c1dc',
    fillColor: '#ebf4f9',
    height   : '50',
    width    : '80'
  });

  // The Calender
  $('#calendar').datepicker();

  // SLIMSCROLL FOR CHAT WIDGET
  $('#chat-box').slimScroll({
    height: '250px'
  });

  /* Morris.js Charts */
  // Sales chart
  if ( $( "#revenue-chart" ).length ) {
    var area = new Morris.Area({
      element   : 'revenue-chart',
      resize    : true,
      data      : [
        { y: '2011 Q1', item1: 2666, item2: 2666 },
        { y: '2011 Q2', item1: 2778, item2: 2294 },
        { y: '2011 Q3', item1: 4912, item2: 1969 },
        { y: '2011 Q4', item1: 3767, item2: 3597 },
        { y: '2012 Q1', item1: 6810, item2: 1914 },
        { y: '2012 Q2', item1: 5670, item2: 4293 },
        { y: '2012 Q3', item1: 4820, item2: 3795 },
        { y: '2012 Q4', item1: 15073, item2: 5967 },
        { y: '2013 Q1', item1: 10687, item2: 4460 },
        { y: '2013 Q2', item1: 8432, item2: 5713 }
      ],
      xkey      : 'y',
      ykeys     : ['item1', 'item2'],
      labels    : ['Item 1', 'Item 2'],
      lineColors: ['#a0d0e0', '#3c8dbc'],
      hideHover : 'auto'
    });
  }

});

var daftarAngka=new Array("","Satu","Dua","Tiga","Empat","Lima","Enam","Tujuh","Delapan","Sembilan");
function terbilang(nilai){
var temp='';
var hasilBagi,sisaBagi;
//batas untuk ribuan
var batas=3;
//untuk menentukan ukuran array, jumlahnya sesuaikan dengan jumlah anggota dari array gradeNilai[]
var maxBagian = 5;
var gradeNilai=new Array("","Ribu","Juta","Milyar","Triliun");
//cek apakah ada angka 0 didepan ==> 00098, harus diubah menjadi 98
nilai = hapusNolDiDepan(nilai);
var nilaiTemp = ubahStringKeArray(batas, maxBagian, nilai);
//ubah menjadi bentuk terbilang
var j = nilai.length;
//menentukan batas array
var banyakBagian = (j % batas) == 0 ? (j / batas) : Math.round(j / batas + 0.5);
var h=0;
	for(var i = banyakBagian - 1; i >=0; i-- ){
		var nilaiSementara = parseInt(nilaiTemp[h]);
		if (nilaiSementara == 1 && i == 1){
			temp +="Seribu ";
			}
		else {
			temp +=ubahRatusanKeHuruf(nilaiTemp[h])+" ";
// cek apakah string bernilai 000, maka jangan tambahkan gradeNilai[i]
			if(nilaiTemp[h] != "000"){
				temp += gradeNilai[i]+" ";
				}
			}
		h++;
		}
return temp;
}
function ubahStringKeArray(batas, maxBagian,kata){
// maksimal 999 milyar
var temp= new Array(maxBagian);
var j = kata.length;
//menentukan batas array
var banyakBagian = (j % batas) == 0 ? (j / batas) : Math.round(j / batas + 0.5);
	for(var i = banyakBagian - 1; i >= 0 ; i--){
		var k = j - batas;
		if(k < 0) k = 0;
			temp[i]=kata.substring(k,j);
		j = k ;
		if (j == 0)
		break;
		}
 return temp;
 }

 function ubahRatusanKeHuruf(nilai){
//maksimal 3 karakter
var batas = 2;
//membagi string menjadi 2 bagian, misal 123 ==> 1 dan 23
var maxBagian = 2;
var temp = ubahStringKeArray(batas, maxBagian, nilai);
var j = nilai.length;
var hasil="";
//menentukan batas array
var banyakBagian = (j % batas) == 0 ? (j / batas) : Math.round(j / batas + 0.5);
	for(var i = 0; i < banyakBagian ;i++){
//cek string yang memiliki panjang lebih dari satu ==> belasan atau puluhan
		if(temp[i].length > 1){
//cek untuk yang bernilai belasan ==> angka pertama 1 dan angka kedua 0 - 9, seperti 11,16 dst
			if(temp[i].charAt(0) == '1'){
				if(temp[i].charAt(1) == '1') {
					hasil += "Sebelas";
					}
				else if(temp[i].charAt(1) == '0') {
					hasil += "Sepuluh";
					}
			else hasil += daftarAngka[temp[i].charAt(1) - '0']+ " Belas ";
				}
 //cek untuk string dengan format angka  pertama 0 ==> 09,05 dst
			else if(temp[i].charAt(0) == '0'){
			hasil += daftarAngka[temp[i].charAt(1) - '0'] ;
			}
 //cek string dengan format selain angka pertama 0 atau 1
			else
			hasil += daftarAngka[temp[i].charAt(0) - '0']+ " Puluh " +daftarAngka[temp[i].charAt(1) - '0'] ;
			}
		else {
//cek string yang memiliki panjang = 1 dan berada pada posisi ratusan
			if(i == 0 && banyakBagian !=1){
				if (temp[i].charAt(0) == '1')
					hasil+=" Seratus ";
				else if (temp[i].charAt(0) == '0')
					hasil+=" ";
				else hasil+= daftarAngka[parseInt(temp[i])]+" Ratus ";
			}
//string dengan panjang satu dan tidak berada pada posisi ratusan ==> satuan
			else hasil+= daftarAngka[parseInt(temp[i])];
			}
	}
return hasil;
}
function hapusNolDiDepan(nilai){
  while(nilai.indexOf("0") == 0){
  	nilai = nilai.substring(1, nilai.length);
  	}
return nilai;
}
