<?php $perusahaanku=$this->m_perusahaan->perusahaanku(0,false,1);?>
<div class="box">
  <div class="box-header">
    <h3 class="box-title">List Customer</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <?=($this->keamanan->allowedaction['tambah'])?'<button class="btn btn-primary" onclick="$(\'#modal-tambah\').modal();$(\'.modal input.form-control\').first().focus()">Tambah</button>':''; ?>
    <button onclick="$('#filter').toggle(300);" class="btn btn-primary pull-right"><i class="fa fa-gear"></i></button>
    <div id="filter" class="row" style="display:none">
    <h4 class="col-xs-12">Filer</h4>
    <div class="form-group col-xs-3">
      <label>Status</label>
      <select id="aktif" name="aktif" class="form-control filterinput">
        <option value="2" selected>Semua</option>
        <option value="1">Aktif</option>
        <option value="0">Tidak Aktif</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Tampilkan</label>
      <select id="perpage" name="perpage" class="form-control filterinput">
        <option value="25" selected>25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="300">100</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Urutkan</label>
      <select id="orderby" name="orderby" class="form-control filterinput">
        <option value="id_customer DESC" selected>Terbaru</option>
        <option value="id_customer ASC">Terlama</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Kota</label>
      <input type="text" class="form-control filterinput" name="kota" id="kota">
    </div>
    <div class="form-group col-xs-12">
      <button id="filterbutton" onclick="$('#filter').toggle(300);fitersubmit();" class="btn btn-primary pull-right"> Terapkan </button>
    </div>
  </div>
  <div class="box-table">
    <table id="tabelcustomer" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <th></th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_customer') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      <?php if ($this->keamanan->allowedaction['ubah']) { ?>
      <tr style="display:none"><td><input type="hidden" name="id_user" class="databerubah" value="<?=$_SESSION['id_user']?>"></td><td><input type="hidden" name="auth_key" class="databerubah" value="<?=$_SESSION['auth_key']?>">
      <input type="hidden" name="_csrf" class="databerubah" value="<?=$this->keamanan->generatecsrf()?>"></td></tr>
    <?php } ?>
      </thead>
      <tbody>
        <tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>
      </tbody>
      <tfoot>
      <tr>
        <th>No.</th>
        <th></th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_customer') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      </tfoot>
    </table>
  </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-left">
      <li><?=($this->keamanan->allowedaction['ubah'])?'<button id="btnubah" class="btn btn-success" onclick="ubahdata(false,true);">Ubah</button>':''; ?></li>
    </ul>

    <ul class="pagination pagination-sm no-margin pull-right">
      <li><button id="btnprev" class="btn btn-default" onclick="changepage(false);">« Prev</button></li>
      <li><button id="btnnext" class="btn btn-default" onclick="changepage();">Next »</button></li>
    </ul>
  </div>
</div>
<!-- /.box -->
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
<div class="modal fade" id="modal-tambah">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah customer</h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('customer/_form', ['perusahaanku'=>$perusahaanku]); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button id="btntambah" type="button" class="btn btn-primary" onclick="tambahdataajax()">Simpan</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>
<?php if ($this->keamanan->allowedaction['ubah']) { ?>
<div class="modal fade" id="modal-dokumen">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Dokumen Customer</h4>
      </div>
      <div class="modal-body">
        <form id="formdokumen" method="post" enctype="multipart/form-data"  action="">
          <input class="data" type="hidden" name="id_user" value="<?=$_SESSION['id_user']?>">
          <input class="data" type="hidden" name="auth_key" value="<?=$_SESSION['auth_key']?>">
          <input class="data" type="hidden" name="lengkap" value="1">
          <input class="data" type="hidden" name="id_perusahaan" value="<?=$_SESSION['id_perusahaan']?>">
            <input class="data" type="hidden" name="_csrf" value="<?=$this->keamanan->generatecsrf()?>">
            <input id="dokumenid_customer" class="data" type="hidden" name="id_customer" value="0">
          <div class="form-group col-xs-12 dokumen_nama_customer">
            <h4>Nama Customer</h4>
          </div>
          <div class="form-group col-xs-6 dokumenimagefileno_identitas">
            <label>Foto Identitas</label>
            <input type="file" class="inputimage datatambah" style="display:none" data-name="dokumenimageFileno_identitas" name="inputimageFileno_identitas" id="dokumenimageFileno_identitas" accept="image/*">
          <img class="previewimagedokumenimageFileno_identitas previewimageimageFile img-responsive" src="<?=base_url()?>assets/images/default/no_identitas.jpg" style="cursor:pointer" onclick="$('#dokumenimageFileno_identitas').click()">
          </div>
          <div class="form-group col-xs-6 dokumenimageFilenpwp">
            <label>Foto NPWP</label>
            <input type="file" class="inputimage datatambah" style="display:none" data-name="dokumenimageFilenpwp" name="inputimageFilenpwp" id="dokumenimageFilenpwp" accept="image/*">
          <img class="previewimagedokumenimageFilenpwp previewimageimageFile img-responsive" src="<?=base_url()?>assets/images/default/npwp.jpg" style="cursor:pointer" onclick="$('#dokumenimageFilenpwp').click()">
          </div>
          <div style="clear:both"></div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button id="btnsimpandokumen" type="button" class="btn btn-primary" onclick="ubahdokumen()">Simpan</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>
<script type="text/javascript">
var perpage = 25;
var page = 1;
var listperusahaanku = <?=json_encode($perusahaanku)?>;
var liststatus = [{'aktif':1,'id':'Aktif'},{'aktif':0,'id':'Tidak Aktif'}];

function dataajax() {
  perpage=$("#perpage").val();
  $( "#tabelcustomer>tbody" ).html('<tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>');
  var request = $.ajax({
    url: "<?=base_url()?>ajax/datacustomer",
    method: "POST",
    data: {
      'aktif': $('#aktif').val(),
      'kota': $('#kota').val(),
      'lengkap': 1,
      'page': page,
      'perpage': perpage,
      'posisi': '<?=$_SESSION['posisi']?>',
      'orderby': $("#orderby").val(),
      'id_user': <?=$_SESSION['id_user']?>,
      'auth_key': '<?=$_SESSION['auth_key']?>'
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    var hasil = '';
    console.log(datahasil);
    $.each(datahasil, function( index, customer ) {
      var status = (customer["aktif"]==1)?'Aktif':'Tidak Aktif';
      hasil +='<tr><?php
      $aksi='';
      if ($this->keamanan->allowedaction['ubah']) {
          $aksi.='<button class="btn btn-link" tabindex="-1" onclick="bukamodaldokumen(\'+customer["id_customer"]+\',\\\'\'+customer["nama_customer"]+\'\\\')"><i class="fa fa-file"> Dokumen</i></button> ';
      }
      if ($this->keamanan->allowedaction['hapus']) {
          $aksi.='<button class="btn btn-link" tabindex="-1" onclick="hapusdataajax(\'+customer["id_customer"]+\')"><i class="fa fa-trash"> Hapus</i></button> ';
      }
      echo '<td>\'+formdiindex(\'hidden\',customer["id_customer"],\'id_customer\',(((page-1)*perpage)+index+1))+\'</td>';
      echo '<td style="min-width:0;padding:0">'.$aksi.'</td>';
      echo '<td>\'+formdiindex(\'text\',customer["id_customer"],\'nama_customer\',customer["nama_customer"])+\'</td>';
      echo '<td>\'+formdiindex(\'select\',customer["id_customer"],\'id_perusahaan\',customer["id_perusahaan"],listperusahaanku,\'nama_perusahaan\',\''.base_url().'perusahaan/edit/\'+customer["id_perusahaan"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',customer["id_customer"],\'email\',customer["email"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',customer["id_customer"],\'telp\',customer["telp"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',customer["id_customer"],\'alamat\',customer["alamat"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',customer["id_customer"],\'hp1\',customer["hp1"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',customer["id_customer"],\'hp2\',customer["hp2"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',customer["id_customer"],\'jenis_identitas\',customer["jenis_identitas"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',customer["id_customer"],\'no_identitas\',customer["no_identitas"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',customer["id_customer"],\'npwp\',customer["npwp"])+\'</td>';
      echo '<td>\'+formdiindex(\'select\',customer["id_customer"],\'aktif\',customer["aktif"],liststatus,\'id\')+\'</td>';


    /*  echo '<td>';
      if ($this->keamanan->allowedaction['ubah']) {
        echo '<a tabindex="-1" href="'.base_url().'customer/edit/\'+customer["id_customer"]+\'"><i class="fa fa-pencil"></i></a> ';
      }
      if ($this->keamanan->allowedaction['hapus']) {
          echo '<a tabindex="-1" href="'.base_url().'customer/hapus/\'+customer["id_customer"]+\'"><i class="fa fa-trash"></i></a> ';
      }
      echo '</td>'; */
      ?></tr>';
    });
    $( "#tabelcustomer>tbody" ).html(hasil);
    if (datahasil.length<perpage) $('#btnnext').attr("disabled", true); else $('#btnnext').attr("disabled", false);
    if (page<=1) $('#btnprev').attr("disabled", true); else $('#btnprev').attr("disabled", false);
    ubahdata(true);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
<?php if ($this->keamanan->allowedaction['ubah']) { ?>
  function updatedataajax() {
    var request = $.ajax({
      url: "<?=base_url()?>ajax/updatedatacustomer",
      method: "POST",
      data: $("#tabelcustomer .databerubah").serialize(),
      dataType: "json"
    });

    request.done(function( datahasil ) {
      dataajax();
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      dataajax();
    });
}
function bukamodaldokumen(id_customer,nama_customer='') {
  $('#formdokumen #dokumenid_customer').val(id_customer);
  $('#formdokumen .dokumen_nama_customer h4').html(nama_customer);
  var linkimg = '<?=base_url()?>assets/images/customer/<?=$_SESSION['id_perusahaan']?>/no_identitas'+id_customer+'.jpg';
  var defaultimg = '<?=base_url()?>assets/images/default/no_identitas.jpg';
  if (checkfileada(linkimg)){$('#formdokumen .previewimagedokumenimageFileno_identitas').attr('src',linkimg);}else{$('#formdokumen .previewimagedokumenimageFileno_identitas').attr('src',defaultimg);}
  linkimg = '<?=base_url()?>assets/images/customer/<?=$_SESSION['id_perusahaan']?>/npwp'+id_customer+'.jpg';
  defaultimg = '<?=base_url()?>assets/images/default/npwp.jpg';
  if (checkfileada(linkimg)){$('#formdokumen .previewimagedokumenimageFilenpwp').attr('src',linkimg);}else{$('#formdokumen .previewimagedokumenimageFilenpwp').attr('src',defaultimg);}
  $('#modal-dokumen').modal();
}
function ubahdokumen() {
  var formData = new FormData(document.getElementById('formdokumen'));
  var request = $.ajax({
    url: "<?=base_url()?>ajax/updatedatacustomerdokumen",
    method: "POST",
    data: formData,
    processData: false,
    contentType: false,
    dataType: "json"
  });

  request.done(function( datahasil ) {
    console.log(datahasil);
    dataajax();
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
    dataajax();
  });
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
  function tambahdataajax() {
    var formData = new FormData(document.getElementById('formcustomer'));
    $('#modal-tambah').modal('toggle');
    var request = $.ajax({
      url: "<?=base_url()?>ajax/tambahdatacustomer",
      method: "POST",
      data: formData,
      processData: false,
      contentType: false,
      dataType: "json"
    });

    request.done(function( datahasil ) {
      dataajax();
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      dataajax();
    });
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['hapus']) { ?>
  function hapusdataajax(id_customer) {
    if (confirm('Apakah Anda yakin ingin menghapus data customer ini ?')) {
      var request = $.ajax({
        url: "<?=base_url()?>ajax/hapusdatacustomer",
        method: "POST",
        data: {
          'id_user': <?=$_SESSION['id_user']?>,
          'auth_key': '<?=$_SESSION['auth_key']?>',
          '_csrf': '<?=$this->keamanan->generatecsrf()?>',
          'id_customer': id_customer
        },
        dataType: "json"
      });

      request.done(function( datahasil ) {
        console.log(datahasil);
        dataajax();
      });

      request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
        dataajax();
      });
    }
}
<?php } ?>
</script>
