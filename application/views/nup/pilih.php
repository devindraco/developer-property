<?php $id_nup=isset($id_nup)?$id_nup:0; ?>
<script type="text/javascript">
var socket;
var listunit=null;
var nokupon=null;
var iddevice='<?=$this->keamanan->generateid_device(rand(0, 999).$_SESSION['id_user'])?>';
window.addEventListener('DOMContentLoaded', (event) => {
  $('.content-header').prepend('<span class="pull-right form-inline"><div class="input-group"><div class="input-group-addon"><i class="fa fa-credit-card"></i></div><input id="nokupon" name="nokupon" type="number" class="form-control" placeholder="No. Kupon" style=""></div></span>');
  $('.content-header').attr('style','width:900px');
  socket = io.connect( 'http://'+window.location.hostname+':3000' );
  socket.emit('new_message', {
    message: 'Pilih Login',
  });
  socket.on( 'new_message', function( data ) {
    if (data.message=='update') {
      if (!$('#modal-pemberitahuan').is(':visible')) location.reload();
    } else if (data.iddevice==iddevice) {
      if (data.message=='kuponused') {
        pemberitahuan('Gagal','Mohon Maaf No. Kupon Sudah Digunakan');
        //location.reload();
      } else if (data.message=='booked') {
        pemberitahuan('Gagal','Mohon Maaf Unit Sudah Dibooking');
        //location.reload();
      } else if (data.message=='bookingunit'){
        idunitdipilih = data.id_unit;
        coordunitdipilih='';
        nokupon=data.nokupon;
        simpandatanupkoordinat();
      }
    }
  });
  $('#modal-pemberitahuan').on('hidden', function () {
    if ($('#modal-pemberitahuan .modal-title').val()!='Info') location.reload();
  });

});
function unithover(keyunit,coOrdStr,color,stroke=false) {
  if (typeof listunit[keyunit]!='undefined') {
  var html = '<h3 style="color:black">'+listunit[keyunit]["nomor"]+'</h3><p>LB : '+listunit[keyunit]["luas_bangunan"]+'m</p><p>Size : '+listunit[keyunit]["panjang"]+'mx'+listunit[keyunit]["lebar"]+'m</p><p>Rp. '+listunit[keyunit]["harga"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'</p>';
    $('#tampilunithover').html(html);
  }
  drawPoly(coOrdStr,color,stroke);
}
function unitleave() {
  $('#tampilunithover').html('');
  myLeave();
}
function pilihunit(id_unit,coord) {
  var nomor = parseInt($('#nokupon').val());
  if (!isNaN(nomor)) {
    idunitdipilih=id_unit;
    coordunitdipilih=coord;
    nokupon=nomor;
    bookunit();
  } else {
    pemberitahuan('Info','Mohon Masukkan Nomor Kupon');
  }
}
function bookunit(){
  socket.emit('new_message', {
    message: 'bookingunit',
    id_unit: idunitdipilih,
    iddevice: iddevice,
    nokupon: nokupon,
  });
}
function simpandatanupkoordinat() {
  var request = $.ajax({
    url: "<?=base_url()?>ajax/simpandatanupkoordinat",
    method: "POST",
    data: {
      'id_user':<?=$_SESSION['id_user']?>,
      'auth_key':'<?=$_SESSION['auth_key']?>',
      '_csrf':'<?=$this->keamanan->generatecsrf()?>',
      'id_nup':<?=$id_nup?>,
      'id_unit':idunitdipilih,
      'nokupon':nokupon,
      'status':1
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    if (datahasil==null) {
      pemberitahuan('Gagal','Mohon Maaf Unit Sudah Dibooking');
    } else {
      pemberitahuan('Success','Selamat !! Anda Berhasil Booking Unit Ini.');
      socket.emit('new_message', {
        message: 'update',
      });
    }
  });

  request.fail(function( jqXHR, textStatus ) {
    pemberitahuan('Error',"Terjadi Masalah, Silahkan Coba Lagi");
    //pemberitahuan('Info', "Request failed: " + textStatus );
    //location.reload();
  });
}
function pemberitahuan(judul='Peringatan',isi='') {
  $('#modal-pemberitahuan').modal();
  $('#modal-pemberitahuan .modal-title').html(judul);
  $('#modal-pemberitahuan .modal-body').html(isi);
}
</script>
<div style="float:left;min-width:900px">
<?php $this->load->view('nup/tampilanpeta',['edit'=>false,'live'=>false,'id_nup'=>$id_nup]); ?>
</div>
<div style="float:left;min-width:300px">
  <h3>Data Unit</h3>
  <div class="small-box"><div  id="tampilunithover" class="inner" style="min-height:125px">
  </div><div class="icon"><i class="fa fa-home"></i></div></div>
</div>
<div style="clear:both">&nbsp;</div>

<div class="modal fade in" id="modal-pemberitahuan">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Info</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
