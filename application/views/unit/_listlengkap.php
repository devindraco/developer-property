<?php
$this->load->model('m_unit');
$id_user=isset($_SESSION['id_user'])?$_SESSION['id_user']:(isset($id_user)?$id_user:0);
$auth_key=isset($_SESSION['auth_key'])?$_SESSION['auth_key']:(isset($auth_key)?$auth_key:'');
$posisi=isset($_SESSION['posisi'])?$_SESSION['posisi']:(isset($posisi)?$posisi:'user');
$id_perusahaan=isset($_SESSION['id_perusahaan'])?$_SESSION['id_perusahaan']:(isset($id_perusahaan)?$id_perusahaan:0);
$id_perumahan=isset($id_perumahan)?$id_perumahan:0;
$datas = $this->m_unit->unitku(['id_user'=>$id_user,'lengkap'=>true,'posisi'=>$posisi,'auth_key'=>$auth_key,'id_perusahaan'=>$id_perusahaan,'id_perumahan'=>$id_perumahan,'aktif'=>1,'status'=>'r']);
$model=$this->m_unit;
 ?>
<table id="tabellistunit" class="table table-bordered table-striped">
  <thead>
  <tr>
    <?php
    foreach ($model->kolomlengkap() as $key => $value) {
      if ($key!='id_unit'){
        echo '<th>'.$value['label'].'</th>';
      }
    }
    ?>
  </tr>
  </thead>
  <tbody>
    <?php
    foreach ($datas as $keydt => $data) {
      echo '<tr class="id_unit'.$data->id_unit.'" onclick="pilihunit(\''.$data->id_unit.'\')" data-harga="'.$data->harga.'">';
      foreach ($model->kolomlengkap() as $keyk => $kolom) {
        if ($keyk!='id_unit'){
          if ($keyk=='harga'){
            echo '<td>Rp. '.number_format($data->$keyk,0,',','.').'</td>';
          } else {
              echo '<td>'.$data->$keyk.'</td>';
          }
        }
      }
      echo '</tr>';
    }
    ?>
  </tbody>
  <tfoot>
  <tr>
    <?php
    foreach ($model->kolomlengkap() as $key => $value) {
      if ($key!='id_unit'){
        echo '<th>'.$value['label'].'</th>';
      }
    }
    ?>
  </tr>
  </tfoot>
</table>
