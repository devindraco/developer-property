<?php
$namaperumahan='';
foreach($perumahanku as $key => $perumahan) {
  if ($namaperumahan==''){
    $namaperumahan = $perumahan->nama_perumahan;
    echo $perumahan->alamat_perumahan.'<br/>Kota '.$perumahan->kota_perumahan.', '.$perumahan->propinsi_perumahan;
    echo '<div style="clear:both;height:30px;"></div>';
  }
  $stylebg = ($perumahan->status!='r')?' style="background-color: #ff7a7a;"':'';
  echo '<div class="col-md-3 col-sm-4"'.$stylebg.'><div class="small-box">
    '.(($perumahan->status=='r')?'<a href="/sid/transaksi/rumah?id_unit='.$perumahan->id_unit.'">':'<span>').'
          <div class="inner" style="min-height:100px">
            <h3 style="color:black">'.$perumahan->nomor.'</h3>
            <p>LB : '.$perumahan->luas_bangunan.'m</p>
            <p>Size : '.$perumahan->panjang.'mx'.$perumahan->lebar.'m</p>
            <p>Rp. '.number_format( $perumahan->harga, 0, ',', '.' ).'</p>
          </div>
          <div class="icon">
            <i class="fa fa-home"></i>
          </div>
    '.(($perumahan->status=='r')?'</a>':'</span>').'
        </div>
    </div>';
}
?>
<div style="clear:both"></div>
<style type="text/css">
.inner p {
  margin: 0;color:black;font-size: 12px;
}
</style>
<script type="text/javascript">
window.addEventListener('DOMContentLoaded', (event) => {
  $('.content-header>h1').html('<?=$namaperumahan?>');
});
</script>
<!-- /.login-box-body -->
