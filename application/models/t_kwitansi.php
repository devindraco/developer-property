<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_kwitansi extends CI_Model {

  function kolom($id='') {
    $listkolom = array(
      'id_kwitansi'=> ['label'=>'ID Kwitansi','default'=>0,'type'=>'text'],
      'no_kwitansi'=> ['label'=>'Nomor Kwitansi','default'=>'','type'=>'text'],
      'nama_kwitansi'=> ['label'=>'Nama Kwitansi','default'=>'','type'=>'text'],
      'jenis_kwitansi'=> ['label'=>'Jenis Kwitansi','default'=>'','type'=>'text'],
      'slug'=> ['label'=>'Slug','default'=>'','type'=>'text'],
      'tanggal'=> ['label'=>'Tanggal Kwitansi','default'=>date('Y-m-d'),'type'=>'date'],
    );
    if (isset($listkolom[$id])) {
      return $listkolom[$id];
    } else {
      return $listkolom;
    }
  }

  function listjenis_kwitansi($id='') {
    $liststatus = array(
      ['jenis_kwitansi'=>'ppjb','label'=> 'PPJB'],
      ['jenis_kwitansi'=>'transaksi','label'=> 'Transaksi'],
      ['jenis_kwitansi'=>'hutang','label'=> 'Surat Hutang'],
      ['jenis_kwitansi'=>'piutang','label'=> 'Surat Piutang'],
    );
    if (isset($liststatus[$id])) {
      return $liststatus[$id];
    } else {
      return $liststatus;
    }
  }

  function datasimpan($data,$file=null){
    $query = '';
    if (isset($data['id_kwitansi'])) {
      $data['aksi'] = ($data['id_kwitansi']==0)?'tambah':'ubah';
      $data['slug'] = $file['name'];
      $query='EXEC sp_t_kwitansi @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\',@id_kwitansi='.$data['id_kwitansi'].',@jenis_kwitansi=\''.$data['jenis_kwitansi'].'\',@no_kwitansi=\''.$data['no_kwitansi'].'\',@nama_kwitansi=\''.$data['nama_kwitansi'].'\',@tanggal=\''.$data['tanggal'].'\', @slug=\''.$data['slug'].'\',@id_perusahaan='.$data['id_perusahaan'].',@aksi=\''.$data['aksi'].'\';';
    }
    $sql='--';
    if ($query!=''){
      $q=$this->db->query($query);
      //$sql=$q->row_array();
      if ($sql=$q->row_array()){
        if ($file!=null) {
          $target_path = BASEPATH.'../assets/images/kwitansi/'.$data['id_perusahaan'].'/';
          if(!file_exists($target_path)){ mkdir($target_path, '0777', true);}
          $target_path = $target_path.basename($sql['slug']);
          move_uploaded_file($file['tmp_name'], $target_path);
        }
      }
    }
    return $sql;
  }

  function kwitansiku($datainput){

    $response = null;
      $data = array(
        'id_kwitansi'=>isset($datainput['id_kwitansi'])?$datainput['id_kwitansi']:0,
        'no_kwitansi'=>isset($datainput['no_kwitansi'])?$datainput['no_kwitansi']:'',
        'posisi'=>isset($datainput['posisi'])?$datainput['posisi']:'user',
        'jenis_kwitansi'=>isset($datainput['jenis_kwitansi'])?$datainput['jenis_kwitansi']:'Semua',
        'aktif'=>isset($datainput['aktif'])?$datainput['aktif']:'Semua',
        'id_user'=>isset($datainput['id_user'])?$datainput['id_user']:0,
        'orderby'=>isset($datainput['orderby'])?$datainput['orderby']:'tanggal DESC',
        'auth_key'=>isset($datainput['auth_key'])?$datainput['auth_key']:'-',
        'rangetanggal'=>isset($datainput['rangetanggal'])?$datainput['rangetanggal']:'',
        'id_perusahaan'=>isset($datainput['id_perusahaan'])?$datainput['id_perusahaan']:0,
        'alldata'=>isset($datainput['alldata'])?$datainput['alldata']:false,
        'page'=>isset($datainput['page'])?$datainput['page']:1,
        'perpage'=>isset($datainput['perpage'])?$datainput['perpage']:25,
      );
      $response = array();
      $id_user = htmlspecialchars($data['id_user']);
      $auth_key = htmlspecialchars($data['auth_key']);
      $no_kwitansi= htmlspecialchars($data['no_kwitansi']);
      $id_kwitansi= htmlspecialchars($data['id_kwitansi']);
      $aktif = htmlspecialchars($data['aktif']);
      $jenis_kwitansi = htmlspecialchars($data['jenis_kwitansi']);
      $rangetanggal=$data['rangetanggal'];
      if ($rangetanggal!=''){
          $rangetanggal = ' AND tanggal BETWEEN '.str_replace('Hingga','and',$rangetanggal);
      }
      $id_perusahaan = ((int)$data['id_perusahaan']>=1)?(int)$data['id_perusahaan']:0;
      $checkid = ($id_kwitansi!=0)?' AND id_kwitansi='.$id_kwitansi:'';
      $page = ((int)$data['page']>=1)?(int)$data['page']:1;
      $perpage = ((int)$data['perpage']>=1)?(int)$data['perpage']:25;
      $orderby = $data['orderby'];
      $select = 'id_kwitansi, no_kwitansi, nama_kwitansi, slug, tanggal, id_perusahaan, jenis_kwitansi, id_pengeluaran, pengeluaran, keperluan, jumlah, satuan, harga_satuan, total, bank, tanggal_pengeluaran, status, id_user_input, nama_depan_user_input, nama_belakang_user_input, jabatan_user_input, aktif_user_input, id_user_approve, aktif_user_approve, nama_depan_user_approve, nama_belakang_user_approve, jabatan_user_approve, id_perumahan, id_cluster, aktif_cluster, nama_cluster, nama_perumahan, alamat_perumahan, kota_perumahan, propinsi_perumahan, telp_perumahan, website_perumahan, max_cicilan_cash, max_cicilan_kpr, max_cicilan_inhouse, max_persen_dp, default_uang_tanda_jadi, aktif_perumahan, nama_perusahaan, id_pemilik, alamat_perusahaan, kota_perusahaan, propinsi_perusahaan, telp_perusahaan, website_perusahaan, npwp_perusahaan, id_user, auth_key, nama_depan, nama_belakang, jabatan, aktif_user';
      $aktif = ($aktif!='Semua')?' AND aktif=\''.$aktif.'\'':'';
      $tambahan = ($jenis_kwitansi!='Semua')?' AND jenis_kwitansi LIKE \'%'.$jenis_kwitansi.'%\'':'';
      $tambahan .= ($no_kwitansi!='')?' AND no_kwitansi LIKE \'%'.$no_kwitansi.'%\'':'';
      $tambahan .= ' AND id_user='.$id_user.' AND auth_key=\''.$auth_key.'\'';
      $tambahan .= $rangetanggal;
      $tambahan .= ($id_perusahaan!=0)?' AND id_perusahaan='.$id_perusahaan:'';
      $limit=($data['alldata'])?'':' OFFSET '.(($page-1)*$page).' ROWS FETCH NEXT '.$perpage.' ROWS ONLY';
      $q = $this->db->query('SELECT '.$select.' FROM v_kwitansiku WHERE 1=1'.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit);
      // if ($data['posisi']=='owner') {
      //   $q = $this->db->query('SELECT '.$select.' FROM v_kwitansi_owner WHERE 1=1'.$checkid.$aktif.$tambahan.' ORDER BY id_kwitansi');
      // } else {
      //   $q = $this->db->query('SELECT '.$select.' FROM v_kwitansi_owner WHERE 1=1'.$checkid.$aktif.$tambahan.' ORDER BY id_kwitansi');
      // }
      //$q = $this->db->query('exec sp_data_t_transaksi @id_usr ='.$id_user);
      $response = ($id_kwitansi==0)?$q->result_array():$q->row_array();
      if ($id_kwitansi!=0 && $response!=null) {
        //$response = file_get_contents(base_url().'assets/kwitansi/'.$response["id_perusahaan"].'/'.$response["id_kwitansi"].'.txt');
      }
      //$response = 'SELECT '.$select.' FROM v_kwitansi_owner WHERE 1=1'.$checkid.$aktif.$tambahan.' ORDER BY id_kwitansi';
      return $response;
  }
}
