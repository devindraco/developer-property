<?php $perusahaanku=$this->m_perusahaan->perusahaanku();
//$marketingku=$this->m_marketing->marketingku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisiutama'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
//$clusterku=$this->m_cluster->clusterku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
//$perumahanku=$this->m_perumahan->perumahanku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
?><div id="boxindex" class="box">
  <div class="box-header">
    <h3 class="box-title">List Pengeluaran</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <?=($this->keamanan->allowedaction['tambah'])?'<button class="btn btn-primary" onclick="bukaformtambah()">Tambah</button>':''; ?>
    <button onclick="$('#filter').toggle(300);" class="btn btn-primary pull-right"><i class="fa fa-gear"></i></button>
    <div id="filter" class="row" style="display:none">
    <h4 class="col-xs-12">Filer</h4>
    <div class="form-group col-xs-6 col-md-3">
      <label>Status</label>
      <select id="status" name="status" class="form-control filterinput">
        <option value="Semua" selected>Semua</option>
        <?php foreach ($model->liststatus() as $key => $status) {
          echo '<option value="'.$status['status'].'"'.$dipilih.'>'.$status['label'].'</option>';
        } ?>
      </select>
    </div>
    <div class="form-group col-xs-6 col-md-3">
      <label>Jumlah</label>
      <select id="perpage" name="perpage" class="form-control filterinput">
        <option value="25" selected>25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="300">100</option>
      </select>
    </div>
    <div class="form-group col-xs-6 col-md-3">
      <label>Urutkan</label>
      <select id="orderby" name="orderby" class="form-control filterinput">
        <option value="id_pengeluaran DESC" selected>Terbaru</option>
        <option value="id_pengeluaran ASC">Terlama</option>
      </select>
    </div>
    <!-- Date range -->
    <div class="form-group col-xs-6 col-md-3">
      <label>Tanggal</label>

      <div class="input-group">
        <div class="input-group-addon">
          <i class="fa fa-calendar"></i>
        </div>
        <input type="hidden" class="form-control pull-right" id="inputfiltertanggal">
        <input type="text" class="form-control pull-right" id="filtertanggal">
      </div>
      <!-- /.input group -->
    </div>
    <div class="form-group col-xs-6 col-md-3">
      <label>Nomor pengeluaran</label>
      <input type="text" class="form-control filterinput" name="no_pengeluaran" id="no_pengeluaran">
    </div>

    <div class="form-group col-xs-12">
      <button id="filterbutton" onclick="$('#filter').toggle(300);fitersubmit();" class="btn btn-primary pull-right"> Terapkan </button>
    </div>
  </div>
  <div class="box-table">
    <table id="tabelpengeluaran" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_pengeluaran' && $key!='diskon1_rp' && $key!='uang_muka') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      <?php if ($this->keamanan->allowedaction['ubah']) { ?>
      <tr style="display:none"><td><input type="hidden" name="id_user" class="databerubah" value="<?=$_SESSION['id_user']?>"></td><td><input type="hidden" name="auth_key" class="databerubah" value="<?=$_SESSION['auth_key']?>">
      <input type="hidden" name="_csrf" class="databerubah" value="<?=$this->keamanan->generatecsrf()?>"></td></tr>
    <?php } ?>
      </thead>
      <tbody>
        <tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>
      </tbody>
      <tfoot>
      <tr>
        <th>No.</th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_pengeluaran' && $key!='diskon1_rp' && $key!='uang_muka') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      </tfoot>
    </table>
  </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-left">
      <?php /*<li><?=($this->keamanan->allowedaction['ubah'])?'<button id="btnubah" class="btn btn-success" onclick="ubahdata(false,true);">Ubah</button>':''; ?></li> */ ?>
    </ul>

    <ul class="pagination pagination-sm no-margin pull-right">
      <li><button id="btnprev" class="btn btn-default" onclick="changepage(false);">« Prev</button></li>
      <li><button id="btnnext" class="btn btn-default" onclick="changepage();">Next »</button></li>
    </ul>
  </div>
</div>
<!-- /.box -->

<?php if ($this->keamanan->allowedaction['tambah']) { ?>
  <div id="boxtambah" class="box" style="display:none">
    <div class="box-header">
      <h3 class="box-title">Tambah pengeluaran</h3><button type="button" class="close pull-right" onclick="$('#boxtambah').hide();$('#boxindex').show();dataajax();" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?php $this->load->view('pengeluaran/_form'); ?>
        <button id="btntambah" type="button" class="btn btn-primary" onclick="tambahdataajax()">Simpan</button>
  </div>
</div>

<div class="modal fade" id="modaltambahkwitansi">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah Kwitansi</h4>
      </div>
      <div class="modal-body">
          <button class="btn btn-primary" onclick="$('#modaltambahkwitansi').modal('hide');setTimeout(function(){$('#modalinputid_kwitansi').modal()},500);" style="margin-bottom:10px">List Kwitansi</button>
          <form id="formpengeluaran" style="clear:both" method="post" enctype="multipart/form-data"  action="">
            <?php $this->load->view('kwitansi/_form'); ?>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button class="btn btn-primary pull-right" onclick="pilihkwitansibaru()" style="margin-bottom:10px">Simpan</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php }
if ($this->keamanan->allowedaction['tambah'] || $this->keamanan->allowedaction['ubah']) { ?>
<div class="modal fade" id="modalinputid_cluster">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Cluster List <?=$_SESSION['nama_perusahaan']?></h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('cluster/_listlengkap'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modalinputid_perumahan">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Perumahan List <?=$_SESSION['nama_perusahaan']?></h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('perumahan/_listlengkap'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modalinputid_kwitansi">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Kwitansi List <?=$_SESSION['nama_perusahaan']?></h4>
      </div>
      <div class="modal-body">
        <button class="btn btn-primary" onclick="$('#modalinputid_kwitansi').modal('hide');setTimeout(function(){$('#modaltambahkwitansi').modal()},500);" style="margin-bottom:10px">Buat Baru</button>
        <?php $this->load->view('kwitansi/_listlengkap'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php } ?>
<script type="text/javascript">
var perpage = 25;
var page = 1;
<?php /* var listperusahaanku = <?=json_encode($perusahaanku)?>;
var listclusterku = <?=json_encode($clusterku)?>;
var listperumahanku = <?=json_encode($perumahanku)?>; */?>
var listkolom = <?=json_encode($model->kolom())?>;
var liststatus = [{'aktif':1,'id':'Aktif'},{'aktif':0,'id':'Tidak Aktif'}];
var hargajual = 0;
var angkaum=0;

function dataajax() {
  perpage=$("#perpage").val();
  $( "#tabelpengeluaran>tbody" ).html('<tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>');
  var request = $.ajax({
    url: "<?=base_url()?>ajax/datapengeluaran",
    method: "POST",
    data: {
      'status': $('#status').val(),
      'no_pengeluaran': $('#no_pengeluaran').val(),
      'rangetanggal': $('#inputfiltertanggal').val(),
      'lengkap': 1,
      'page': page,
      'perpage': perpage,
      'posisi': '<?=$_SESSION['posisi']?>',
      'orderby': $("#orderby").val(),
      'id_user': <?=$_SESSION['id_user']?>,
      'auth_key': '<?=$_SESSION['auth_key']?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    var hasil = '';
    console.log(datahasil);
    $.each(datahasil, function( index, pengeluaran ) {
      hasil +='<tr><?php
      $hapus='';
      echo '<td>\'+(((page-1)*perpage)+index+1)+\'</td>';
      foreach ($model->kolom() as $key => $value) {
        if ($key!='id_pengeluaran') {
          if ($key=='id_cluster') {
            echo '<td>\'+pengeluaran["nama_cluster"]+\'</td>';
          } else if ($key=='id_user_input') {
              echo '<td>\'+pengeluaran["nama_depan_user_input"]+\'</td>';
          } else if ($key=='id_kwitansi') {
                echo '<td><a class="popupsingle_image" data-fancybox="listkwitansi" href="'.base_url().'assets/images/kwitansi/\'+pengeluaran["id_perusahaan"]+\'/\'+pengeluaran["slug_kwitansi"]+\'"><img class="img-responsive" style="width:100px;" src="'.base_url().'assets/images/kwitansi/\'+pengeluaran["id_perusahaan"]+\'/\'+pengeluaran["slug_kwitansi"]+\'" /></a></td>';
          } else if ($key=='id_user_approve') {
                echo '<td>\'+pengeluaran["nama_depan_user_approve"]+\'</td>';
          } else if ($key=='harga_satuan'||$key=='total') {
            echo '\'; pengeluaran["'.$key.'"]=Math.trunc(pengeluaran["'.$key.'"]);';
            echo ' hasil +=\'<td>Rp. \'+pengeluaran["'.$key.'"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+\'</td>';
          } else {
            if ($key=='id_marketing') $key='nama_marketing';
            if ($key=='id_perumahan') $key='nama_perumahan';
            echo '<td>\'+pengeluaran["'.$key.'"]+\'</td>';
          }
        }
      }
      ?></tr>';
    });
    $( "#tabelpengeluaran>tbody" ).html(hasil);
    if (datahasil.length<perpage) $('#btnnext').attr("disabled", true); else $('#btnnext').attr("disabled", false);
    if (page<=1) $('#btnprev').attr("disabled", true); else $('#btnprev').attr("disabled", false);
    ubahdata(true);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}

<?php if ($this->keamanan->allowedaction['ubah']) { ?>
  function updatedataajax() {
    var request = $.ajax({
      url: "<?=base_url()?>ajax/updatedatapengeluaran",
      method: "POST",
      data: $("#tabelpengeluaran .databerubah").serialize(),
      dataType: "json"
    });

    request.done(function( datahasil ) {
      console.log(datahasil);
      dataajax();
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      dataajax();
    });
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
  function resettambah(datas=listkolom) {
    $.each(datas, function( index, data ) {
      $('#input'+index).val(data["default"]);
    });
  }
  function bukaformtambah(){
    resettambah();
    $('#boxindex').hide();$('#boxtambah').show();$('.boxtambah input.form-control').first().focus();
  }
function resetformtambah(){
  $('.formsatuan').remove();
  $('.formjumlah>.input-group').append('<div class="input-group-addon" style="border: 0;padding: 0;width: 40%;"><input id="inputsatuan" type="text" class="form-control datatambah" name="satuan" value="" required="" placeholder="Satuan"></div>');
  $('#inputtotal').prop('readonly',true);
  $('#inputid_cluster').attr({'name':'text_cluster','id':'inputtext_cluster'});
  $('.formid_cluster .input-group').append('<input type="hidden" name="id_cluster" class="datatambah" id="inputid_cluster">');
  $('#inputid_perumahan').attr({'name':'text_perumahan','id':'inputtext_perumahan'});
  $('.formid_perumahan .input-group').append('<input type="hidden" name="id_perumahan" class="datatambah" id="inputid_perumahan">');
  $('#inputid_kwitansi').attr({'name':'text_kwitansi','id':'inputtext_kwitansi'});
  $('#boxtambah .formtanggal').after($('.formid_kwitansi'));
  $('#modaltambahkwitansi #formpengeluaran').prepend('<div class="form-group col-xs-6 formimageFile">'+htmlinputimage('imageFile','')+'</div>');
  $('#boxtambah .formid_kwitansi .input-group').append('<input type="hidden" name="id_kwitansi" class="datatambah" id="inputid_kwitansi">');
  $('#modaltambahkwitansi .formid_kwitansi .input-group').append('<input type="hidden" name="id_kwitansi" class="datatambah" id="inputid_kwitansi">');
  $('#boxtambah .formid_kwitansi').append('<a id="popupsingle_image" data-fancybox="kwitansi" href="<?=base_url()?>assets/images/default/kwitansi.jpg"><img class="img-responsive" style="width:100%;" src="<?=base_url()?>assets/images/default/kwitansi.jpg" /></a>');
  $('#modaltambahkwitansi .formid_kwitansi').append('<a id="popupsingle_imagebaru" data-fancybox="kwitansibaru" href="<?=base_url()?>assets/images/default/kwitansi.jpg"><img class="img-responsive" style="width:100%;" src="<?=base_url()?>assets/images/default/kwitansi.jpg" /></a>');
  $('#modaltambahkwitansi #inputid_kwitansi').attr('name','id_kwitansibaru');
  $('#inputharga_satuan,#inputjumlah').attr({'min':'0','onkeyup':'hitunghargatotal()','onchange': 'hitunghargatotal()'});
}

function tambahdataajax() {
  var formData = new FormData(document.getElementById('formpengeluaran'));
  var data = $('#boxtambah .datatambah').serializeArray();
  for (var i=0; i<data.length; i++) formData.append(data[i].name, data[i].value);

  $('#modal-tambah').modal('toggle');
  var request = $.ajax({
    url: "<?=base_url()?>ajax/tambahdatapengeluaran",
    method: "POST",
    data: formData,
    processData: false,
    contentType: false,
    dataType: "json"
  });

  request.done(function( datahasil ) {
    console.log(datahasil);
    $('#boxtambah').hide();$('#boxindex').show();
    dataajax();
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( 'Mohon Cek Kembali Data Anda');
  });
}
window.addEventListener('DOMContentLoaded', (event) => {
  resetformtambah();
    <?php if ($id_cluster!=0) {
      echo 'bukaformtambah();pilihcluster('.$id_cluster.');';
    } ?>
});

<?php } ?>
function hitunghargatotal() {
  $('#inputtotal').val(parseInt($('#inputjumlah').val())*parseInt($('#inputharga_satuan').val()));
}
<?php if ($this->keamanan->allowedaction['tambah'] || $this->keamanan->allowedaction['ubah']) { ?>
function openmodalid_cluster(){
  $('#modalinputid_cluster').modal();
  if (!$.fn.DataTable.isDataTable('#tabellistcluster')){
    $('#tabellistcluster').DataTable();
  }
}
function openmodalid_perumahan(){
  $('#modalinputid_perumahan').modal();
  if (!$.fn.DataTable.isDataTable('#tabellistperumahan')){
    $('#tabellistperumahan').DataTable();
  }
}

function openmodalid_kwitansi(){
  if ($('#inputtext_kwitansi').val()!=''&&$('#inputid_kwitansi').val()==0) {
    $('#modaltambahkwitansi').modal();
  } else {
    $('#modalinputid_kwitansi').modal();
    if (!$.fn.DataTable.isDataTable('#tabellistkwitansi')){
      $('#tabellistkwitansi').DataTable();
    }
  }
}

function pilihcluster(id,id_perumahan){
  $('#inputid_cluster').val(id);
  $('#inputtext_cluster').val($('.id_cluster'+id+' td:nth-child(1)').html());
  $('#inputid_perumahan').val(id_perumahan);
  $('#inputtext_perumahan').val($('.id_cluster'+id+' td:nth-child(2)').html());
  $('#modalinputid_cluster').modal('hide');
}
function pilihperumahan(id,nama){
  if ($('#inputid_perumahan').val()!=id){
    $('#inputtext_perumahan').val(nama);
    $('#inputid_perumahan').val(id);
    $('#inputid_cluster').val('0');
    $('#inputtext_cluster').val('');
  }
  $('#modalinputid_perumahan').modal('hide');
}
function pilihkwitansi(id,nomor,slug){
  $('#inputtext_kwitansi').val(nomor);
  $('#inputid_kwitansi').val(id);
  $('#popupsingle_image').attr('href','<?=base_url()?>assets/images/kwitansi/<?=$_SESSION["id_perusahaan"]?>/'+slug);
  $('#popupsingle_image>img.img-responsive').attr('src','<?=base_url()?>assets/images/kwitansi/<?=$_SESSION["id_perusahaan"]?>/'+slug);
  $('#modalinputid_kwitansi').modal('hide');
}
function pilihkwitansibaru(){
  if ($('#inputno_kwitansi').val()!=''){
    $('#inputtext_kwitansi').val($('#inputno_kwitansi').val());
    $('#inputid_kwitansi').val(0);
    $('#popupsingle_image>img.img-responsive').attr('src',$('#modaltambahkwitansi .previewimageimageFile').attr('src'));
    $('#popupsingle_image').attr('href',$('#modaltambahkwitansi .previewimageimageFile').attr('src'));
    $('#modaltambahkwitansi').modal('hide');
  } else {
    alert('Mohon Isi data secara lengkap');
  }
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['hapus']) { ?>
  function hapusdataajax(id_pengeluaran) {
    if (confirm('Apakah Anda yakin ingin menghapus data pengeluaran ini ?')) {
      var request = $.ajax({
        url: "<?=base_url()?>ajax/hapusdatapengeluaran",
        method: "POST",
        data: {
          'id_user': <?=$_SESSION['id_user']?>,
          'auth_key': '<?=$_SESSION['auth_key']?>',
          '_csrf': '<?=$this->keamanan->generatecsrf()?>',
          'id_pengeluaran': id_pengeluaran
        },
        dataType: "json"
      });

      request.done(function( datahasil ) {
        console.log(datahasil);
        dataajax();
      });

      request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
        dataajax();
      });
    }
}
<?php } ?>
window.addEventListener('DOMContentLoaded', (event) => {
$('#filtertanggal').daterangepicker(
  {
    ranges   : {
      'Hari Ini'       : [moment(), moment()],
      'Kemarin'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      '7 Hari Terakhir' : [moment().subtract(6, 'days'), moment()],
      '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
      'Bulan Ini'  : [moment().startOf('month'), moment().endOf('month')],
      'Bulan Lalu'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    startDate: moment().subtract(29, 'days'),
    endDate  : moment()
  },
  function (start, end) {
    $('#inputfiltertanggal').val('\''+start.format('YYYY-MM-DD') + '\' Hingga \'' + end.format('YYYY-MM-DD')+'\'')
  }
);
$('#inputfiltertanggal').val('\''+moment().subtract(29, 'days').format('YYYY-MM-DD') + '\' Hingga \'' + moment().format('YYYY-MM-DD')+'\'');

});
</script>
