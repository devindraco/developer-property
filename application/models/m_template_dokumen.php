<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_template_dokumen extends CI_Model {

  function kolom($id='') {
    $listkolom = array(
      'id_template_dokumen'=> ['label'=>'ID Template','default'=>0,'type'=>'text'],
      'nama_template'=> ['label'=>'Nama Template','default'=>'','type'=>'text'],
      'jenis_template'=> ['label'=>'Jenis','default'=>'','type'=>'text'],
    );
    if (isset($listkolom[$id])) {
      return $listkolom[$id];
    } else {
      return $listkolom;
    }
  }

  function listjenis_template($id='') {
    $liststatus = array(
      ['jenis_template'=>'ppjb','label'=> 'PPJB'],
      ['jenis_template'=>'transaksi','label'=> 'Transaksi'],
      ['jenis_template'=>'hutang','label'=> 'Surat Hutang'],
      ['jenis_template'=>'piutang','label'=> 'Surat Piutang'],
    );
    if (isset($liststatus[$id])) {
      return $liststatus[$id];
    } else {
      return $liststatus;
    }
  }

  function simpan_template_dokumenku($data){
    $query = '';
    if (isset($data['id_template_dokumen'])) {
      $data['aksi'] = ($data['id_template_dokumen']==0)?'tambah':'ubah';
      $query='EXEC sp_m_template_dokumen @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\',@id_template_dokumen='.$data['id_template_dokumen'].',@jenis_template=\''.$data['jenis_template'].'\',@nama_template=\''.$data['nama_template'].'\',@id_perusahaan='.$data['id_perusahaan'].',@aksi=\''.$data['aksi'].'\';';
    }
    $sql='--';
    if ($query!=''){
      $q=$this->db->query($query);
      //$sql=$q->row_array();
      if ($sql=$q->row_array()){
        $template = file_put_contents(BASEPATH.'../assets/template/'.$sql["id_perusahaan"].'/'.$sql["id_template_dokumen"].'.txt',$data['template']);
      }
    }
    return '';
  }

  function template_dokumenku($datainput){

    $response = null;
      $data = array(
        'id_template_dokumen'=>isset($datainput['id_template_dokumen'])?$datainput['id_template_dokumen']:0,
        'posisi'=>isset($datainput['posisi'])?$datainput['posisi']:'user',
        'jenis_template'=>isset($datainput['jenis_template'])?$datainput['jenis_template']:'Semua',
        'aktif'=>isset($datainput['aktif'])?$datainput['aktif']:'Semua',
        'id_user'=>isset($datainput['id_user'])?$datainput['id_user']:0,
        'auth_key'=>isset($datainput['auth_key'])?$datainput['auth_key']:'-',
        'id_perusahaan'=>isset($datainput['id_perusahaan'])?$datainput['id_perusahaan']:0,
      );
      $response = array();
      $id_user = htmlspecialchars($data['id_user']);
      $auth_key = htmlspecialchars($data['auth_key']);
      $id_template_dokumen= htmlspecialchars($data['id_template_dokumen']);
      $aktif = htmlspecialchars($data['aktif']);
      $jenis_template = htmlspecialchars($data['jenis_template']);
      $id_perusahaan = ((int)$data['id_perusahaan']>=1)?(int)$data['id_perusahaan']:0;
      $checkid = ($id_template_dokumen!=0)?' AND id_template_dokumen='.$id_template_dokumen:'';
      $select = 'id_template_dokumen,nama_template, jenis_template,id_perusahaan';
      $aktif = ($aktif!='Semua')?' AND aktif=\''.$aktif.'\'':'';
      $tambahan = ($jenis_template!='Semua')?' AND jenis_template LIKE \'%'.$jenis_template.'%\'':'';
      $tambahan .= ' AND id_user='.$id_user.' AND auth_key=\''.$auth_key.'\'';
      $tambahan .= ($id_perusahaan!=0)?' AND id_perusahaan='.$id_perusahaan:'';
      $q = $this->db->query('SELECT '.$select.' FROM v_template_dokumenku WHERE 1=1'.$checkid.$aktif.$tambahan.' ORDER BY id_template_dokumen');
      // if ($data['posisi']=='owner') {
      //   $q = $this->db->query('SELECT '.$select.' FROM v_template_dokumen_owner WHERE 1=1'.$checkid.$aktif.$tambahan.' ORDER BY id_template_dokumen');
      // } else {
      //   $q = $this->db->query('SELECT '.$select.' FROM v_template_dokumen_owner WHERE 1=1'.$checkid.$aktif.$tambahan.' ORDER BY id_template_dokumen');
      // }
      //$q = $this->db->query('exec sp_data_t_transaksi @id_usr ='.$id_user);
      $response = ($id_template_dokumen==0)?$q->result_array():$q->row_array();
      if ($id_template_dokumen!=0 && $response!=null) {
        $response = file_get_contents(base_url().'assets/template/'.$response["id_perusahaan"].'/'.$response["id_template_dokumen"].'.txt');
      }
      //$response = 'SELECT '.$select.' FROM v_template_dokumen_owner WHERE 1=1'.$checkid.$aktif.$tambahan.' ORDER BY id_template_dokumen';
      return $response;
  }
}
