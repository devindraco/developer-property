<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approval extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct() {
		 parent::__construct();
	 }
	public function rumah($page=1)
 {
	 $this->load->model('t_transaksi');
	 $this->load->model('m_marketing');
	 $this->load->model('m_unit');
	 $this->load->model('m_customer');
	 $aktif=isset($_GET['aktif'])?$_GET['aktif']:2;
	 $perpage=isset($_GET['perpage'])?$_GET['perpage']:25;
	 $page=(int)$page;
	 $this->setlayouts('utama','approval/rumah',['model'=>$this->t_transaksi,'menudipilih'=>'Approval']);
 }
 public function pengeluaran($page=1)
{
	$this->load->model('t_pengeluaran');
	$this->load->model('m_cluster');
 $this->load->model('m_perumahan');
 $this->load->model('t_kwitansi');
	$aktif=isset($_GET['aktif'])?$_GET['aktif']:2;
	$perpage=isset($_GET['perpage'])?$_GET['perpage']:25;
	$page=(int)$page;
	$this->setlayouts('utama','approval/pengeluaran',['model'=>$this->t_pengeluaran,'id_cluster'=>0,'menudipilih'=>'Approval']);
}
}
