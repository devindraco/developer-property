<?php $perusahaanku=$this->m_perusahaan->perusahaanku();
//$marketingku=$this->m_marketing->marketingku(['id_userutama'=>$_SESSION['id_user'],'lengkap'=>true,'posisiutama'=>$_SESSION['posisi'],'auth_keyutama'=>$_SESSION['auth_key']]);
//$unitku=$this->m_unit->unitku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
//$customerku=$this->m_customer->customerku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
?><div id="boxindex" class="box">
  <div class="box-header">
    <h3 class="box-title">List Transaksi</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div id="filter" class="row">
    <h4 class="col-xs-12">Filer</h4>
    <div class="form-group col-xs-6 col-md-3">
      <label>Status</label>
      <select id="status" name="status" class="form-control filterinput">
        <option value="Semua">Semua</option>
        <?php foreach ($model->liststatus() as $key => $status) {
          $dipilih=($status['status']=='Pending')?' selected':'';
          echo '<option value="'.$status['status'].'"'.$dipilih.'>'.$status['label'].'</option>';
        } ?>
      </select>
    </div>
    <div class="form-group col-xs-6 col-md-3">
      <label>Jumlah</label>
      <select id="perpage" name="perpage" class="form-control filterinput">
        <option value="25" selected>25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="300">100</option>
      </select>
    </div>
    <div class="form-group col-xs-6 col-md-3">
      <label>Urutkan</label>
      <select id="orderby" name="orderby" class="form-control filterinput">
        <option value="id_pesanan DESC" selected>Terbaru</option>
        <option value="id_pesanan ASC">Terlama</option>
      </select>
    </div>
    <!-- Date range -->
    <div class="form-group col-xs-6 col-md-3">
      <label>Tanggal</label>

      <div class="input-group">
        <div class="input-group-addon">
          <i class="fa fa-calendar"></i>
        </div>
        <input type="hidden" class="form-control pull-right" id="inputfiltertanggal">
        <input type="text" class="form-control pull-right" id="filtertanggal">
      </div>
      <!-- /.input group -->
    </div>
    <div class="form-group col-xs-6 col-md-3">
      <label>Nomor Pesanan</label>
      <input type="text" class="form-control filterinput" name="no_pesanan" id="no_pesanan">
    </div>

    <div class="form-group col-xs-12">
      <button id="filterbutton" onclick="fitersubmit();" class="btn btn-primary pull-right"> Terapkan </button>
    </div>
  </div>
  <div class="box-table">
    <table id="tabeltransaksi" class="tableapproval table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <?php
        foreach ($model->kolomkecil() as $key => $value) {
          if ($key!='id_pesanan' && $key!='diskon1_rp' && $key!='uang_muka') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      <?php if ($this->keamanan->allowedaction['ubah']) { ?>
      <tr style="display:none"><td><input type="hidden" name="id_userutama" class="databerubah" value="<?=$_SESSION['id_user']?>"></td><td><input type="hidden" name="auth_keyutama" class="databerubah" value="<?=$_SESSION['auth_key']?>">
      <input type="hidden" name="_csrf" class="databerubah" value="<?=$this->keamanan->generatecsrf()?>"></td></tr>
    <?php } ?>
      </thead>
      <tbody>
        <tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>
      </tbody>
      <tfoot>
      <tr>
        <th>No.</th>
        <?php
        foreach ($model->kolomkecil() as $key => $value) {
          if ($key!='id_pesanan' && $key!='diskon1_rp' && $key!='uang_muka') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      </tfoot>
    </table>
  </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-left">
    </ul>

    <ul class="pagination pagination-sm no-margin pull-right">
      <li><button id="btnprev" class="btn btn-default" onclick="changepage(false);">« Prev</button></li>
      <li><button id="btnnext" class="btn btn-default" onclick="changepage();">Next »</button></li>
    </ul>
  </div>
</div>
<!-- /.box -->

<div class="modal fade" id="modalcicilanindex">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Detail Cicilan</h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('transaksi/_simulasicicilan'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<?php if ($this->keamanan->allowedaction['ubah']) { ?>
  <div id="boxubah" class="box" style="display:none">
    <div class="box-header">
      <h3 class="box-title">Detail Transaksi</h3><button type="button" class="close pull-right" onclick="$('#boxubah').hide();$('#boxindex').show();dataajax();" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <?php $this->load->view('approval/_form'); ?>
      <div class="col-xs-12">
        <button id="btnapprove" type="button" class="btn btn-primary" onclick="approvedataajax('Approved')">Approve</button>
        <button id="btnreject" type="button" class="btn btn-danger" onclick="approvedataajax('Reject')">Reject</button>
      </div>
  </div>
</div>


<?php }
if ($this->keamanan->allowedaction['ubah']) { ?>
<div class="modal fade" id="modaldetaillengkapunit">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Detail Unit</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modaldetaillengkapcustomer">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Detail Customer</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modaldetaillengkapmarketing">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Detail Marketing</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>
<script type="text/javascript">
var perpage = 25;
var page = 1;
<?php /* var listperusahaanku = <?=json_encode($perusahaanku)?>;
var listunitku = <?=json_encode($unitku)?>;
var listcustomerku = <?=json_encode($customerku)?>; */?>
var listtransaksiku=[];
var listkolom = <?=json_encode($model->kolom())?>;
var liststatus = <?=json_encode($model->liststatus())?>;
var listjenisbayar = [{'jenis_bayar':'Cash'},{'jenis_bayar':'InHouse'},{'jenis_bayar':'KPR'}];
var hargajual = 0;

function dataajax() {
  perpage=$("#perpage").val();
  $( "#tabeltransaksi>tbody" ).html('<tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>');
  var request = $.ajax({
    url: "<?=base_url()?>ajax/datatransaksi",
    method: "POST",
    data: {
      'status': $('#status').val(),
      'no_pesanan': $('#no_pesanan').val(),
      'rangetanggal': $('#inputfiltertanggal').val(),
      'lengkap': 1,
      'page': page,
      'perpage': perpage,
      'posisi': '<?=$_SESSION['posisi']?>',
      'orderby': $("#orderby").val(),
      'id_userutama': <?=$_SESSION['id_user']?>,
      'auth_keyutama': '<?=$_SESSION['auth_key']?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    var hasil = '';
    console.log(datahasil);
    $.each(datahasil, function( index, transaksi ) {
      listtransaksiku[transaksi["id_pesanan"]]=transaksi;
      hasil +='<tr onclick="bukaformubah('+transaksi["id_pesanan"]+')"><?php
      $hapus='';
      echo '<td>\'+(((page-1)*perpage)+index+1)+\'</td>';
      foreach ($model->kolomkecil() as $key => $value) {
        if ($key!='id_pesanan' && $key!='diskon1_rp' && $key!='uang_muka') {
          if ($key=='id_unit') {
            echo '<td>\'+transaksi["nomor"]+\' Tipe \'+transaksi["nama_tipe"]+\'</td>';
          } else if ($key=='no_pesanan') {
            echo '<td><button class="btn-link" onclick="datatransaksidetail(\'+transaksi["id_pesanan"]+\')">\'+transaksi["'.$key.'"]+\'</button></td>';
          } else if ($key=='harga_jual'||$key=='cashback'||$key=='harga_net'||$key=='uang_tanda_jadi'||$key=='uang_muka_rp') {
            echo '\'; transaksi["'.$key.'"]=Math.trunc(transaksi["'.$key.'"]);';
            echo ' hasil +=\'<td>Rp. \'+transaksi["'.$key.'"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+\'</td>';
          } else if ($key=='diskon1') {
            echo '\'; transaksi["'.$key.'"]=Math.trunc(transaksi["'.$key.'"]);';
            echo ' hasil +=\'<td>\'+transaksi["'.$key.'"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+\'%</td>';
          } else {
            if ($key=='id_marketing') $key='nama_marketing';
            if ($key=='id_customer') $key='nama_customer';
            echo '<td>\'+transaksi["'.$key.'"]+\'</td>';
          }
        }
      }
      // echo '<td>\'+formdiindex(\'hidden\',transaksi["id_pesanan"],\'id_pesanan\',(((page-1)*perpage)+index+1))+\'</td>';
      // echo '<td>\'+formdiindex(\'text\',transaksi["id_pesanan"],\'no_pesanan\',transaksi["no_pesanan"])+\'</td>';
      // echo '<td>\'+formdiindex(\'pilihanpopup\',transaksi["id_pesanan"],\'id_unit\',transaksi["id_unit"])+\'</td>';
      // echo '<td>\'+formdiindex(\'pilihanpopup\',transaksi["id_pesanan"],\'id_customer\',transaksi["id_customer"])+\'</td>';
      // echo '<td>\'+formdiindex(\'date\',transaksi["id_pesanan"],\'tanggal\',transaksi["tanggal"])+\'</td>';
      // echo '<td>\'+formdiindex(\'text\',transaksi["id_pesanan"],\'alamat_surat_menyurat\',transaksi["alamat_surat_menyurat"])+\'</td>';
      // echo '<td>\'+formdiindex(\'select\',transaksi["id_pesanan"],\'jenis_bayar\',transaksi["jenis_bayar"],listjenisbayar,\'jenis_bayar\')+\'</td>';
      // echo '<td>\'+formdiindex(\'number\',transaksi["id_pesanan"],\'lama_cicilan\',transaksi["lama_cicilan"])+\'</td>';
      // echo '<td>\'+formdiindex(\'number\',transaksi["id_pesanan"],\'harga_jual\',transaksi["harga_jual"])+\'</td>';
      // echo '<td>\'+formdiindex(\'select\',transaksi["id_pesanan"],\'aktif\',transaksi["aktif"],liststatus,\'id\')+\'</td>';
/*
      'id_pesanan'=> ['label'=>'ID transaksi','default'=>0,'type'=>'text'],
      'no_pesanan'=> ['label'=>'No. Pesanan','default'=>'','type'=>'text'],
      'id_unit'=> ['label'=>'Unit','default'=>'','type'=>'pilihanpopup'],
      'id_customer'=> ['label'=>'Customer','default'=>0,'type'=>'select'],
      'tanggal'=> ['label'=>'Tanggal','default'=>date('Y-m-d'),'type'=>'date'],
      'alamat_surat_menyurat'=> ['label'=>'Alamat Surat Menyurat','default'=>'','type'=>'textarea'],
      'cara_bayar'=> ['label'=>'Cara Bayar','default'=>'','type'=>'text'],
      'lama_cicilan'=> ['label'=>'Lama Cicilan','default'=>0,'type'=>'number'],
      'harga_jual'=> ['label'=>'Harga Jual','default'=>0,'type'=>'number'],
      'nama_diskon'=> ['label'=>'Nama Diskon','default'=>'','type'=>'text'],
      'diskon1'=> ['label'=>'Diskon 1','default'=>0,'type'=>'number'],
      'diskon2'=> ['label'=>'Diskon 2','default'=>0,'type'=>'number'],
      'diskon3'=> ['label'=>'Diskon 3','default'=>0,'type'=>'number'],
      'diskon1_rp'=> ['label'=>'Diskon Rupiah 1','default'=>0,'type'=>'number'],
      'diskon2_rp'=> ['label'=>'Diskon Rupiah 1','default'=>0,'type'=>'number'],
      'diskon3_rp'=> ['label'=>'Diskon Rupiah 1','default'=>0,'type'=>'number'],
      'cashback'=> ['label'=>'Cashback','default'=>0,'type'=>'number'],
      'harga_net'=> ['label'=>'Harga NET','default'=>0,'type'=>'number'],
      'uang_tanda_jadi'=> ['label'=>'Uang Tanda Jadi','default'=>0,'type'=>'number'],
      'uang_muka'=> ['label'=>'Uang Muka','default'=>0,'type'=>'number'],
      'uang_muka_rp'=> ['label'=>'Uang Muka','default'=>0,'type'=>'number'],
      'catatan'=> ['label'=>'Catatan','default'=>'','type'=>'text'],
      'id_marketing'=> ['label'=>'Nama Marketing','default'=>'','type'=>'select'],
      'tanggal_approve'=> ['label'=>'Tanggal Approve','default'=>date('Y-m-d'),'type'=>'date'],
      'id_user_approve'=> ['label'=>'Approver','default'=>0,'type'=>'select'],
      'status'=> ['label'=>'Status','default'=>'Pending','type'=>'text']
    /*  echo '<td>';
      if ($this->keamanan->allowedaction['ubah']) {
        echo '<a tabindex="-1" href="'.base_url().'transaksi/edit/\'+transaksi["id_pesanan"]+\'"><i class="fa fa-pencil"></i></a> ';
      }
      if ($this->keamanan->allowedaction['hapus']) {
          echo '<a tabindex="-1" href="'.base_url().'transaksi/hapus/\'+transaksi["id_pesanan"]+\'"><i class="fa fa-trash"></i></a> ';
      }
      echo '</td>'; */
      ?></tr>';
    });
    $( "#tabeltransaksi>tbody" ).html(hasil);
    if (datahasil.length<perpage) $('#btnnext').attr("disabled", true); else $('#btnnext').attr("disabled", false);
    if (page<=1) $('#btnprev').attr("disabled", true); else $('#btnprev').attr("disabled", false);
    ubahdata(true);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
function datatransaksidetail(id_pesanan) {
  var request = $.ajax({
    url: "<?=base_url()?>ajax/datatransaksidetail",
    method: "POST",
    data: {
      'id_pesanan': id_pesanan,
      'posisi': '<?=$_SESSION['posisi']?>',
      'id_userutama': <?=$_SESSION['id_user']?>,
      'auth_keyutama': '<?=$_SESSION['auth_key']?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    var hasil = '';
    $( "#modalcicilanindex" ).modal();
    console.log(datahasil);
    $.each(datahasil, function( index, transaksi ) {
      transaksi["cicilan"]=Math.trunc(transaksi["cicilan"]);
      hasil +='<tr><td>'+transaksi["keterangan"]+'</td><td>Rp. '+transaksi["cicilan"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'</td><td>'+transaksi["jatuh_tempo"]+'</td></tr>';
    });
    $( "#tabelsimulasicicilan>tbody" ).html(hasil);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
function viewlengkapunit(id_unit) {
  var request = $.ajax({
    url: "<?=base_url()?>ajax/viewlengkapunit",
    method: "POST",
    data: {
      'id_unit': id_unit,
      'posisi': '<?=$_SESSION['posisi']?>',
      'id_user': <?=$_SESSION['id_user']?>,
      'auth_key': '<?=$_SESSION['auth_key']?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "html"
  });

  request.done(function( datahasil ) {
    console.log(datahasil);
    $('#modaldetaillengkapunit').modal();
    $("#modaldetaillengkapunit .modal-body").html(datahasil);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
function viewlengkapcustomer(id_customer) {
  var request = $.ajax({
    url: "<?=base_url()?>ajax/viewlengkapcustomer",
    method: "POST",
    data: {
      'id_customer': id_customer,
      'posisi': '<?=$_SESSION['posisi']?>',
      'id_user': <?=$_SESSION['id_user']?>,
      'auth_key': '<?=$_SESSION['auth_key']?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "html"
  });

  request.done(function( datahasil ) {
    console.log(datahasil);
    $('#modaldetaillengkapcustomer').modal();
    $("#modaldetaillengkapcustomer .modal-body").html(datahasil);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
function viewlengkapmarketing(id_marketing) {
  var request = $.ajax({
    url: "<?=base_url()?>ajax/viewlengkapmarketing",
    method: "POST",
    data: {
      'id_marketing': id_marketing,
      'posisi': '<?=$_SESSION['posisi']?>',
      'id_user': <?=$_SESSION['id_user']?>,
      'auth_key': '<?=$_SESSION['auth_key']?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "html"
  });

  request.done(function( datahasil ) {
    console.log(datahasil);
    $('#modaldetaillengkapmarketing').modal();
    $("#modaldetaillengkapmarketing .modal-body").html(datahasil);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
<?php if ($this->keamanan->allowedaction['ubah']) { ?>
  function updatedataajax() {
    var request = $.ajax({
      url: "<?=base_url()?>ajax/updatedatatransaksi",
      method: "POST",
      data: $("#tabeltransaksi .databerubah").serialize(),
      dataType: "json"
    });

    request.done(function( datahasil ) {
      console.log(datahasil);
      dataajax();
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      dataajax();
    });
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['ubah']) { ?>
  function resetubah(datas) {
    var nilai='';
    $.each(datas, function( index, data ) {
      nilai = data;
      if (index!='id_pesanan' && index!='diskon1_rp') {
        if (index=='id_unit') {
          $('#inputtext_unit').val(datas['nomor']+' Tipe '+datas['nama_tipe']+' di '+datas['nama_perumahan']);
        } else if (index=='harga_jual'||index=='cashback'||index=='harga_net'||index=='uang_tanda_jadi'||index=='uang_muka_rp') {
          data=Math.trunc(data);
          nilai='Rp. '+data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        } else if (index=='diskon1') {
          nilai=parseFloat(data).toFixed(2)+'%';
        } else if (index=='uang_muka') {
          if (data==0) {
            nilai='Rp. '+datas['uang_muka_rp'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
          } else {
            nilai=parseFloat(data).toFixed(2)+'%';
          }
        }
      }
      $('#input'+index).val(nilai);
    });
    if (datas['status']!='Pending') {
      $('#boxubah .btn').hide();
      $('#boxubah .statustransaksi').html('<h4 style="margin:2px">'+datas['status']+'</h4>');
    } else {
      $('#boxubah .btn').show();
      $('#boxubah .statustransaksi').html('');
    }
    $('#inputid_customer').val(datas['nama_customer']);
    $('#hidid_customer').val(datas['id_customer']);
    $('#inputid_marketing').val(datas['nama_marketing']);
    $('#hidid_marketing').val(datas['id_marketing']);
  }
  function bukaformubah(id_pesanan){
    if (listtransaksiku[id_pesanan]!=null) {
      $('#boxubah .form-control').prop('readonly', true);
      $('#boxubah select').prop('disabled', true);
      resetubah(listtransaksiku[id_pesanan]);
      $('#boxindex').hide();$('#boxubah').show();$('.boxubah input.form-control').first().focus();
      hitungharganet();
    }
  }
  function setformubah() {
    $('.formdiskon1').hide();
    $('.formdiskon2').hide();
    $('.formdiskon3').hide();
    $('.formdiskon1_rp').hide();
    $('.formdiskon2_rp').hide();
    $('.formdiskon3_rp').hide();
    $('.formtanggal_approve').hide();
    $('.formid_user_approve').hide();
    $('.formstatus').hide();
    $('#inputharga_jual').attr({'min':'0','onkeyup':'hitungharganet()','onchange': 'cekhargajual()'});
    // $('.formdiskon1 .input-group .input-group-addon').html('<input type="checkbox" onclick="$(\'#inputdiskon1,#inputdiskon1_rp\').prop(\'disabled\', !$(this).prop(\'checked\'));">');
    $('#inputdiskon1,#inputdiskon1_rp,#inputharga_net,#inputnama_diskon').prop('disabled', true);
    $('#inputdiskon1,#inputdiskon1_rp').attr({'min':'0','max':'100','onkeyup':'hitungharganet()','onchange': 'hitungharganet()'});
    //$('.formdiskon1 .input-group').append('<div class="input-group-addon"><select name="typediskon1" id="inputtypediskon1"><option value="1">%</option><option value="2">Rp</option></select></div>');
    $('.formnama_diskon .input-group .input-group-addon i').attr('class','fa fa-book');
    $('.formnama_diskon').append($('.formdiskon1 .input-group'));
    $('#inputnama_diskon').attr('placeholder','Nama Diskon');

    //$('.formdiskon2 .input-group .input-group-addon').html('<input type="checkbox" onclick="$(\'#inputdiskon2,#inputdiskon2_rp\').prop(\'disabled\', !$(this).prop(\'checked\'));">');
    //$('#inputdiskon2,#inputdiskon2_rp').prop('disabled', true);
    //$('#inputdiskon2,#inputdiskon2_rp').attr('onkeyup', 'hitungharganet()');
    //$('.formdiskon2 .input-group').append('<div class="input-group-addon"><select name="typediskon2" id="inputtypediskon2"><option value="1">%</option><option value="2">Rp</option></select></div>');
    //$('.formnama_diskon').append($('.formdiskon2 .input-group'));

    //$('.formdiskon3 .input-group .input-group-addon').html('<input type="checkbox" onclick="$(\'#inputdiskon3,#inputdiskon3_rp\').prop(\'disabled\', !$(this).prop(\'checked\'));">');
    //$('#inputdiskon3,#inputdiskon3_rp').prop('disabled', true);
    //$('#inputdiskon3,#inputdiskon3_rp').attr('onkeyup', 'hitungharganet()');
    //$('.formdiskon3 .input-group').append('<div class="input-group-addon"><select name="typediskon3" id="inputtypediskon3"><option value="1">%</option><option value="2">Rp</option></select></div>');
    //$('.formnama_diskon').append($('.formdiskon3 .input-group'));

    $('.formuang_muka_rp').hide();
    $('#inputid_customer').attr('name','text_customer');
    $('.formid_customer .input-group').append('<input type="hidden" name="id_customer" class="dataubah" id="hidid_customer">');
    $('#inputid_unit').attr({'name':'text_unit','id':'inputtext_unit'});
    $('.formid_unit .input-group').append('<input type="hidden" name="id_unit" class="dataubah" id="inputid_unit">');
    $('#inputid_marketing').attr('name','text_marketing');
    $('.formid_marketing .input-group').append('<input type="hidden" name="id_marketing" class="dataubah" id="hidid_marketing">');
    $('.formharga_net>label').after('<label class="pull-right"><sub>Diskon Rp. <b>0</b></sub></label>');
    $('.formuang_muka>label').after('<label class="pull-right"><sub><b>0</b></sub></label>');

    $('#boxubah .box-body>div:first').prepend('<span class="statustransaksi pull-right label label-primary"><h4 style="margin:2px"></h4></span>');

    $('.formlama_cicilan .input-group').append('<div class="input-group-addon" onclick="openmodalcicilanindex()" style="cursor:pointer"><i class="fa fa-calculator"></i></div>');
  }
  function cekhargajual(){
    var hj=$('#inputharga_jual').val();
    if (hj<hargajual) {
      $('#inputharga_jual').val(hargajual);
    }
    hitungharganet();
  }
  function hitungharganet() {
    var id_pesanan = $('#inputid_pesanan').val();
    var hj=listtransaksiku[id_pesanan]['harga_jual'];
    var dics1=listtransaksiku[id_pesanan]['diskon1'];
    //var dics2=$('#inputdiskon2').val();
    //var dics3=$('#inputdiskon3').val();
    var hnet = 0;
    var jmldis1=(hj*dics1/100);
    hnet=(hj-jmldis1);
    //hnet=(hnet-(hnet*dics2/100));
    //hnet=(hnet-(hnet*dics3/100));
    $('.formharga_net .pull-right>sub>b').html(jmldis1.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
    $('#inputharga_net').val('Rp. '+hnet.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
    listtransaksiku[id_pesanan]['harga_net'] = hnet;
    hitunguangmuka();
  }
function hitunguangmuka() {
  var id_pesanan = $('#inputid_pesanan').val();

  var um = listtransaksiku[id_pesanan]['uang_muka'];
  var umrp = listtransaksiku[id_pesanan]['uang_muka_rp'];
  var hnet = listtransaksiku[id_pesanan]['harga_net'];
  var hasilum = '';
  if (umrp==0) {hasilum=um*hnet/100;hasilum='Rp. '+hasilum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") } else hasilum=(umrp*100/hnet).toFixed(2)+'%';
  $('.formuang_muka .pull-right>sub>b').html(hasilum);
}
function approvedataajax(status='approved') {
  var request = $.ajax({
    url: "<?=base_url()?>ajax/approvetransaksi",
    method: "POST",
    data: {
      'id_pesanan': $('#inputid_pesanan').val(),
      'status': status,
      '_csrf': '<?=$this->keamanan->generatecsrf()?>',
      'posisi': '<?=$_SESSION['posisi']?>',
      'id_user': <?=$_SESSION['id_user']?>,
      'auth_key': '<?=$_SESSION['auth_key']?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    console.log(datahasil);
    $('#boxubah').hide();$('#boxindex').show();
    dataajax();
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( 'Mohon Cek Kembali Data Anda');
  });
}
window.addEventListener('DOMContentLoaded', (event) => {
    setformubah();
});
function openmodalcicilanindex() {
  datatransaksidetail($('#inputid_pesanan').val());
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['ubah']) { ?>
function openmodalid_unit(){
  viewlengkapunit($('#inputid_unit').val());
}
function openmodalid_customer(){
  viewlengkapcustomer($('#hidid_customer').val());
}
function openmodalid_marketing(){
  viewlengkapmarketing($('#hidid_marketing').val());
}

// function pilihunit(id){
//   $('#inputid_unit').val(id);
//   $('#modaldetaillengkapunit').modal('hide');
//   hargajual=$('.id_unit'+id+' td:nth-child(5)').html();
//   $('#inputharga_jual').val(hargajual);
//   $('#inputharga_jual').attr('min',hargajual);
//   hitungharganet();
// }
function pilihcustomer(id,nama){
  $('#inputid_customer').val(nama);
  $('#hidid_customer').val(id);
  $('#modaldetaillengkapcustomer').modal('hide');
}
function pilihmarketing(id,nama){
  $('#inputid_marketing').val(nama);
  $('#hidid_marketing').val(id);
  $('#modalinputid_marketing').modal('hide');
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['hapus']) { ?>
  function hapusdataajax(id_pesanan) {
    if (confirm('Apakah Anda yakin ingin menghapus data transaksi ini ?')) {
      var request = $.ajax({
        url: "<?=base_url()?>ajax/hapusdatatransaksi",
        method: "POST",
        data: {
          'id_userutama': <?=$_SESSION['id_user']?>,
          'auth_keyutama': '<?=$_SESSION['auth_key']?>',
          '_csrf': '<?=$this->keamanan->generatecsrf()?>',
          'id_pesanan': id_pesanan
        },
        dataType: "json"
      });

      request.done(function( datahasil ) {
        console.log(datahasil);
        dataajax();
      });

      request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
        dataajax();
      });
    }
}
<?php } ?>
window.addEventListener('DOMContentLoaded', (event) => {
$('#filtertanggal').daterangepicker(
  {
    ranges   : {
      'Hari Ini'       : [moment(), moment()],
      'Kemarin'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      '7 Hari Terakhir' : [moment().subtract(6, 'days'), moment()],
      '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
      'Bulan Ini'  : [moment().startOf('month'), moment().endOf('month')],
      'Bulan Lalu'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    startDate: moment().subtract(29, 'days'),
    endDate  : moment()
  },
  function (start, end) {
    $('#inputfiltertanggal').val('\''+start.format('YYYY-MM-DD') + '\' Hingga \'' + end.format('YYYY-MM-DD')+'\'')
  }
);
$('#inputfiltertanggal').val('\''+moment().subtract(29, 'days').format('YYYY-MM-DD') + '\' Hingga \'' + moment().format('YYYY-MM-DD')+'\'');

});
</script>
