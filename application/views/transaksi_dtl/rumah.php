<?php $perusahaanku=$this->m_perusahaan->perusahaanku();
//$marketingku=$this->m_marketing->marketingku(['id_userutama'=>$_SESSION['id_user'],'lengkap'=>true,'posisiutama'=>$_SESSION['posisi'],'auth_keyutama'=>$_SESSION['auth_key']]);
//$unitku=$this->m_unit->unitku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
//$customerku=$this->m_customer->customerku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
?><div id="boxindex" class="box">
  <div class="box-header">
    <h3 class="box-title">List Transaksi</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <?=($this->keamanan->allowedaction['tambah'])?'<button class="btn btn-primary" onclick="bukaformtambah()">Tambah</button>':''; ?>
    <button onclick="$('#filter').toggle(300);" class="btn btn-primary pull-right"><i class="fa fa-gear"></i></button>
    <div id="filter" class="row" style="display:none">
    <h4 class="col-xs-12">Filer</h4>
    <div class="form-group col-xs-3">
      <label>Status</label>
      <select id="aktif" name="aktif" class="form-control filterinput">
        <option value="2" selected>Semua</option>
        <option value="1">Aktif</option>
        <option value="0">Tidak Aktif</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Tampilkan</label>
      <select id="perpage" name="perpage" class="form-control filterinput">
        <option value="25" selected>25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="300">100</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Urutkan</label>
      <select id="orderby" name="orderby" class="form-control filterinput">
        <option value="id_pesanan DESC" selected>Terbaru</option>
        <option value="id_pesanan ASC">Terlama</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Nama transaksi</label>
      <input type="text" class="form-control filterinput" name="nama_transaksi" id="nama_transaksi">
    </div>
    <div class="form-group col-xs-12">
      <button id="filterbutton" onclick="$('#filter').toggle(300);fitersubmit();" class="btn btn-primary pull-right"> Terapkan </button>
    </div>
  </div>
  <div class="box-table">
    <table id="tabeltransaksi" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <?php
        foreach ($model->kolomkecil() as $key => $value) {
          if ($key!='id_pesanan' && $key!='diskon1_rp' && $key!='uang_muka') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      <?php if ($this->keamanan->allowedaction['ubah']) { ?>
      <tr style="display:none"><td><input type="hidden" name="id_userutama" class="databerubah" value="<?=$_SESSION['id_user']?>"></td><td><input type="hidden" name="auth_keyutama" class="databerubah" value="<?=$_SESSION['auth_key']?>">
      <input type="hidden" name="_csrf" class="databerubah" value="<?=$this->keamanan->generatecsrf()?>"></td></tr>
    <?php } ?>
      </thead>
      <tbody>
        <tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>
      </tbody>
      <tfoot>
      <tr>
        <th>No.</th>
        <?php
        foreach ($model->kolomkecil() as $key => $value) {
          if ($key!='id_pesanan' && $key!='diskon1_rp' && $key!='uang_muka') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      </tfoot>
    </table>
  </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-left">
      <li><?=($this->keamanan->allowedaction['ubah'])?'<button id="btnubah" class="btn btn-success" onclick="ubahdata(false,true);">Ubah</button>':''; ?></li>
    </ul>

    <ul class="pagination pagination-sm no-margin pull-right">
      <li><button id="btnprev" class="btn btn-default" onclick="changepage(false);">« Prev</button></li>
      <li><button id="btnnext" class="btn btn-default" onclick="changepage();">Next »</button></li>
    </ul>
  </div>
</div>
<!-- /.box -->
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
  <div id="boxtambah" class="box" style="display:none">
    <div class="box-header">
      <h3 class="box-title">Tambah Transaksi</h3><button type="button" class="close pull-right" onclick="$('#boxtambah').hide();$('#boxindex').show();dataajax();" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <?php $this->load->view('transaksi/_form'); ?>
      <button id="btntambah" type="button" class="btn btn-primary" onclick="tambahdataajax()">Simpan</button>
  </div>
</div>

<div class="modal fade" id="modalsimulasicicilan">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Simulasi Cicilan</h4>
      </div>
      <div class="modal-body">
        <p>Jumlah yang Harus Dibayar <b id="simulasijmldibayar">0</b></p>
        <div style="clear:both"></div>
        <?php $this->load->view('transaksi/_simulasicicilan'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php }
if ($this->keamanan->allowedaction['tambah'] || $this->keamanan->allowedaction['ubah']) { ?>
<div class="modal fade" id="modalinputid_unit">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Unit List <?=$_SESSION['nama_perusahaan']?></h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('unit/_listlengkap'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modalinputid_customer">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Customer List <?=$_SESSION['nama_perusahaan']?></h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('customer/_listlengkap'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modalinputid_marketing">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Marketing List <?=$_SESSION['nama_perusahaan']?></h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('marketing/_listlengkap'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>
<script type="text/javascript">
var perpage = 25;
var page = 1;
<?php /* var listperusahaanku = <?=json_encode($perusahaanku)?>;
var listunitku = <?=json_encode($unitku)?>;
var listcustomerku = <?=json_encode($customerku)?>; */?>
var listkolom = <?=json_encode($model->kolom())?>;
var liststatus = [{'aktif':1,'id':'Aktif'},{'aktif':0,'id':'Tidak Aktif'}];
var listjenisbayar = [{'jenis_bayar':'Cash'},{'jenis_bayar':'InHouse'},{'jenis_bayar':'KPR'}];
var hargajual = 0;

function dataajax() {
  perpage=$("#perpage").val();
  $( "#tabeltransaksi>tbody" ).html('<tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>');
  var request = $.ajax({
    url: "<?=base_url()?>ajax/datatransaksi",
    method: "POST",
    data: {
      'aktif': $('#aktif').val(),
      'kota': $('#kota').val(),
      'lengkap': 1,
      'page': page,
      'perpage': perpage,
      'posisi': '<?=$_SESSION['posisi']?>',
      'orderby': $("#orderby").val(),
      'id_userutama': <?=$_SESSION['id_user']?>,
      'auth_keyutama': '<?=$_SESSION['auth_key']?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    var hasil = '';
    console.log(datahasil);
    $.each(datahasil, function( index, transaksi ) {
      hasil +='<tr><?php
      $hapus='';
      echo '<td>\'+(((page-1)*perpage)+index+1)+\'</td>';
      foreach ($model->kolomkecil() as $key => $value) {
        if ($key!='id_pesanan' && $key!='diskon1_rp' && $key!='uang_muka') {
          if ($key=='id_unit') {
            echo '<td>\'+transaksi["nomor"]+\' Tipe \'+transaksi["nama_tipe"]+\'</td>';
          } else {
            if ($key=='id_marketing') $key='nama_marketing';
            if ($key=='id_customer') $key='nama_customer';
            echo '<td>\'+transaksi["'.$key.'"]+\'</td>';
          }
        }
      }
      // echo '<td>\'+formdiindex(\'hidden\',transaksi["id_pesanan"],\'id_pesanan\',(((page-1)*perpage)+index+1))+\'</td>';
      // echo '<td>\'+formdiindex(\'text\',transaksi["id_pesanan"],\'no_pesanan\',transaksi["no_pesanan"])+\'</td>';
      // echo '<td>\'+formdiindex(\'pilihanpopup\',transaksi["id_pesanan"],\'id_unit\',transaksi["id_unit"])+\'</td>';
      // echo '<td>\'+formdiindex(\'pilihanpopup\',transaksi["id_pesanan"],\'id_customer\',transaksi["id_customer"])+\'</td>';
      // echo '<td>\'+formdiindex(\'date\',transaksi["id_pesanan"],\'tanggal\',transaksi["tanggal"])+\'</td>';
      // echo '<td>\'+formdiindex(\'text\',transaksi["id_pesanan"],\'alamat_surat_menyurat\',transaksi["alamat_surat_menyurat"])+\'</td>';
      // echo '<td>\'+formdiindex(\'select\',transaksi["id_pesanan"],\'jenis_bayar\',transaksi["jenis_bayar"],listjenisbayar,\'jenis_bayar\')+\'</td>';
      // echo '<td>\'+formdiindex(\'number\',transaksi["id_pesanan"],\'lama_cicilan\',transaksi["lama_cicilan"])+\'</td>';
      // echo '<td>\'+formdiindex(\'number\',transaksi["id_pesanan"],\'harga_jual\',transaksi["harga_jual"])+\'</td>';
      // echo '<td>\'+formdiindex(\'select\',transaksi["id_pesanan"],\'aktif\',transaksi["aktif"],liststatus,\'id\')+\'</td>';
/*
      'id_pesanan'=> ['label'=>'ID transaksi','default'=>0,'type'=>'text'],
      'no_pesanan'=> ['label'=>'No. Pesanan','default'=>'','type'=>'text'],
      'id_unit'=> ['label'=>'Unit','default'=>'','type'=>'pilihanpopup'],
      'id_customer'=> ['label'=>'Customer','default'=>0,'type'=>'select'],
      'tanggal'=> ['label'=>'Tanggal','default'=>date('Y-m-d'),'type'=>'date'],
      'alamat_surat_menyurat'=> ['label'=>'Alamat Surat Menyurat','default'=>'','type'=>'textarea'],
      'cara_bayar'=> ['label'=>'Cara Bayar','default'=>'','type'=>'text'],
      'lama_cicilan'=> ['label'=>'Lama Cicilan','default'=>0,'type'=>'number'],
      'harga_jual'=> ['label'=>'Harga Jual','default'=>0,'type'=>'number'],
      'nama_diskon'=> ['label'=>'Nama Diskon','default'=>'','type'=>'text'],
      'diskon1'=> ['label'=>'Diskon 1','default'=>0,'type'=>'number'],
      'diskon2'=> ['label'=>'Diskon 2','default'=>0,'type'=>'number'],
      'diskon3'=> ['label'=>'Diskon 3','default'=>0,'type'=>'number'],
      'diskon1_rp'=> ['label'=>'Diskon Rupiah 1','default'=>0,'type'=>'number'],
      'diskon2_rp'=> ['label'=>'Diskon Rupiah 1','default'=>0,'type'=>'number'],
      'diskon3_rp'=> ['label'=>'Diskon Rupiah 1','default'=>0,'type'=>'number'],
      'cashback'=> ['label'=>'Cashback','default'=>0,'type'=>'number'],
      'harga_net'=> ['label'=>'Harga NET','default'=>0,'type'=>'number'],
      'uang_tanda_jadi'=> ['label'=>'Uang Tanda Jadi','default'=>0,'type'=>'number'],
      'uang_muka'=> ['label'=>'Uang Muka','default'=>0,'type'=>'number'],
      'uang_muka_rp'=> ['label'=>'Uang Muka','default'=>0,'type'=>'number'],
      'catatan'=> ['label'=>'Catatan','default'=>'','type'=>'text'],
      'id_marketing'=> ['label'=>'Nama Marketing','default'=>'','type'=>'select'],
      'tanggal_approve'=> ['label'=>'Tanggal Approve','default'=>date('Y-m-d'),'type'=>'date'],
      'id_user_approve'=> ['label'=>'Approver','default'=>0,'type'=>'select'],
      'status'=> ['label'=>'Status','default'=>'Pending','type'=>'text']
    /*  echo '<td>';
      if ($this->keamanan->allowedaction['ubah']) {
        echo '<a tabindex="-1" href="'.base_url().'transaksi/edit/\'+transaksi["id_pesanan"]+\'"><i class="fa fa-pencil"></i></a> ';
      }
      if ($this->keamanan->allowedaction['hapus']) {
          echo '<a tabindex="-1" href="'.base_url().'transaksi/hapus/\'+transaksi["id_pesanan"]+\'"><i class="fa fa-trash"></i></a> ';
      }
      echo '</td>'; */
      ?></tr>';
    });
    $( "#tabeltransaksi>tbody" ).html(hasil);
    if (datahasil.length<perpage) $('#btnnext').attr("disabled", true); else $('#btnnext').attr("disabled", false);
    if (page<=1) $('#btnprev').attr("disabled", true); else $('#btnprev').attr("disabled", false);
    ubahdata(true);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
<?php if ($this->keamanan->allowedaction['ubah']) { ?>
  function updatedataajax() {
    var request = $.ajax({
      url: "<?=base_url()?>ajax/updatedatatransaksi",
      method: "POST",
      data: $("#tabeltransaksi .databerubah").serialize(),
      dataType: "json"
    });

    request.done(function( datahasil ) {
      console.log(datahasil);
      dataajax();
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      dataajax();
    });
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
  function resettambah(datas=listkolom) {
    $.each(datas, function( index, data ) {
      $('#input'+index).val(data["default"]);
    });
  }
  function bukaformtambah(){
    resettambah();
    $('#boxindex').hide();$('#boxtambah').show();$('.boxtambah input.form-control').first().focus();
  }
  function setformtambah() {
    $('.formdiskon1').hide();
    $('.formdiskon2').hide();
    $('.formdiskon3').hide();
    $('.formdiskon1_rp').hide();
    $('.formdiskon2_rp').hide();
    $('.formdiskon3_rp').hide();
    $('.formtanggal_approve').hide();
    $('.formid_user_approve').hide();
    $('.formstatus').hide();
    $('#inputharga_jual').attr({'min':'0','onkeyup':'hitungharganet()','onchange': 'cekhargajual()'});
    // $('.formdiskon1 .input-group .input-group-addon').html('<input type="checkbox" onclick="$(\'#inputdiskon1,#inputdiskon1_rp\').prop(\'disabled\', !$(this).prop(\'checked\'));">');
    $('#inputdiskon1,#inputdiskon1_rp,#inputharga_net,#inputnama_diskon').prop('disabled', true);
    $('#inputdiskon1,#inputdiskon1_rp').attr({'min':'0','max':'100','onkeyup':'hitungharganet()','onchange': 'hitungharganet()'});
    $('#inputuang_muka,#typeuang_muka').attr({'onkeyup':'hitunguangmuka()','onchange': 'hitunguangmuka()'});
    $('#inputlama_cicilan').attr({'onkeyup':'resetsimulasicicilan()','onchange': 'resetsimulasicicilan()'});
    //$('.formdiskon1 .input-group').append('<div class="input-group-addon"><select name="typediskon1" id="inputtypediskon1"><option value="1">%</option><option value="2">Rp</option></select></div>');
    $('.formnama_diskon .input-group .input-group-addon i').attr('class','fa fa-book');
    $('.formnama_diskon').append($('.formdiskon1 .input-group'));
    $('#inputnama_diskon').attr('placeholder','Nama Diskon');
    $('.formnama_diskon>label').html('<input type="checkbox" class="datatambah" name="checkdiskon" onclick="$(\'#inputdiskon1,#inputdiskon1_rp,#inputnama_diskon\').prop(\'disabled\', !$(this).prop(\'checked\'));hitungharganet();"> Diskon');

    //$('.formdiskon2 .input-group .input-group-addon').html('<input type="checkbox" onclick="$(\'#inputdiskon2,#inputdiskon2_rp\').prop(\'disabled\', !$(this).prop(\'checked\'));">');
    //$('#inputdiskon2,#inputdiskon2_rp').prop('disabled', true);
    //$('#inputdiskon2,#inputdiskon2_rp').attr('onkeyup', 'hitungharganet()');
    //$('.formdiskon2 .input-group').append('<div class="input-group-addon"><select name="typediskon2" id="inputtypediskon2"><option value="1">%</option><option value="2">Rp</option></select></div>');
    //$('.formnama_diskon').append($('.formdiskon2 .input-group'));

    //$('.formdiskon3 .input-group .input-group-addon').html('<input type="checkbox" onclick="$(\'#inputdiskon3,#inputdiskon3_rp\').prop(\'disabled\', !$(this).prop(\'checked\'));">');
    //$('#inputdiskon3,#inputdiskon3_rp').prop('disabled', true);
    //$('#inputdiskon3,#inputdiskon3_rp').attr('onkeyup', 'hitungharganet()');
    //$('.formdiskon3 .input-group').append('<div class="input-group-addon"><select name="typediskon3" id="inputtypediskon3"><option value="1">%</option><option value="2">Rp</option></select></div>');
    //$('.formnama_diskon').append($('.formdiskon3 .input-group'));

    $('.formuang_muka .input-group').append('<div class="input-group-addon"><select name="typeuang_muka" class="datatambah" id="typeuang_muka"><option value="1">%</option><option value="2">Rp</option></select></div>');
    $('.formuang_muka_rp').hide();
    $('#inputid_customer').attr('name','text_customer');
    $('.formid_customer .input-group').append('<input type="hidden" name="id_customer" class="datatambah" id="hidid_customer">');
    $('#inputid_marketing').attr('name','text_marketing');
    $('.formid_marketing .input-group').append('<input type="hidden" name="id_marketing" class="datatambah" id="hidid_marketing">');
    $('.formharga_net>label').after('<label class="pull-right"><sub>Diskon Rp. <b>0</b></sub></label>');
    $('.formuang_muka>label').after('<label class="pull-right"><sub><b>0</b></sub></label>');


    $('.formlama_cicilan .input-group').append('<div class="input-group-addon" onclick="openmodalsimulasicicilan()" style="cursor:pointer"><i class="fa fa-calculator"></i></div>');
  }
  function cekhargajual(){
    var hj=$('#inputharga_jual').val();
    if (hj<hargajual) {
      $('#inputharga_jual').val(hargajual);
    }
    hitungharganet();
  }
  function hitungharganet() {
    var hj=$('#inputharga_jual').val();
    var dics1=$('#inputdiskon1').val();
    //var dics2=$('#inputdiskon2').val();
    //var dics3=$('#inputdiskon3').val();
    if (dics1<0) $('#inputdiskon1').val(0);
    if (dics1>100) $('#inputdiskon1').val(100);
    dics1=$('#inputdiskon1').val();
    if ($('#inputdiskon1').prop('disabled')) {dics1=0;}
    var hnet = 0;
    var jmldis1=(hj*dics1/100);
    hnet=(hj-jmldis1);
    //hnet=(hnet-(hnet*dics2/100));
    //hnet=(hnet-(hnet*dics3/100));
    $('.formharga_net .pull-right>sub>b').html(jmldis1.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
    $('#inputharga_net').val(hnet);
    hitunguangmuka();
    resetsimulasicicilan();
  }
function hitunguangmuka() {
  var um = $('#inputuang_muka').val();
  var hnet = $('#inputharga_net').val();
  var typeum = $('#typeuang_muka').val();
  var hasilum = '';
  if (typeum==1) {hasilum=um*hnet/100;hasilum='Rp. '+hasilum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") } else hasilum=(um*100/hnet).toFixed(2)+'%';
  $('.formuang_muka .pull-right>sub>b').html(hasilum);
}
function tambahdataajax() {
  $('#modal-tambah').modal('toggle');
  var request = $.ajax({
    url: "<?=base_url()?>ajax/tambahdatatransaksi",
    method: "POST",
    data: $("#boxtambah .datatambah, #tabelsimulasicicilan .datatambah").serialize(),
    dataType: "json"
  });

  request.done(function( datahasil ) {
    console.log(datahasil);
    $('#boxtambah').hide();$('#boxindex').show();
    dataajax();
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( 'Mohon Cek Kembali Data Anda');
  });
}
window.addEventListener('DOMContentLoaded', (event) => {
    setformtambah();
});
function resetsimulasicicilan(){
  var jmlcicilan = $('#inputlama_cicilan').val();
  var hnet = $('#inputharga_net').val();
  var uangtjadi = $('#inputuang_tanda_jadi').val();
  $('#simulasijmldibayar').html('Rp. '+hnet.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
  var jmldibayar = hnet-uangtjadi;
  var cicilan = jmldibayar/jmlcicilan;
  cicilan=Math.ceil(Math.ceil(cicilan) / 1000) * 1000;
  var tanggal = new Date($('#inputtanggal').val());
  tanggal.setMonth(tanggal.getMonth());
  tanggal.setDate(tanggal.getDate()+14);
  var rpuangtjadi = 'Rp. '+uangtjadi.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  var texttanggal = ("0" + tanggal.getDate()).slice(-2)+'/'+("0" + (tanggal.getMonth() + 1)).slice(-2)+'/'+tanggal.getFullYear();
  var isitabelcicilan = '<tr><td>Uang Tanda Jadi</td><td>'+rpuangtjadi+'</td><td>'+texttanggal+'</td></tr>';
  var rpcicilan = '';
  rpcicilan='Rp. '+cicilan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  for (i = 1; i < jmlcicilan; i++) {
    tanggal.setMonth(tanggal.getMonth()+1);
    texttanggal = ("0" + tanggal.getDate()).slice(-2)+'/'+("0" + (tanggal.getMonth() + 1)).slice(-2)+'/'+tanggal.getFullYear();
    valtanggal = tanggal.getFullYear()+'-'+("0" + (tanggal.getMonth() + 1)).slice(-2)+'-'+("0" + tanggal.getDate()).slice(-2);
    isitabelcicilan+='<tr><td><input class="datatambah" name="keteranganke['+i+']" value="Angsuran ke '+i+'" /></td><td><input class="datatambah" name="cicilanke['+i+']" value="'+cicilan+'" /></td><td><input type="date" class="form-control datatambah datepicker" name="jatuhtempoke['+i+']" value="'+valtanggal+'"></td></tr>';
  }
  cicilan=jmldibayar-(cicilan*(jmlcicilan-1));
  rpcicilan='Rp. '+cicilan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  tanggal.setMonth(tanggal.getMonth()+1);
  texttanggal = ("0" + tanggal.getDate()).slice(-2)+'/'+("0" + (tanggal.getMonth() + 1)).slice(-2)+'/'+tanggal.getFullYear();
  valtanggal = tanggal.getFullYear()+'-'+("0" + (tanggal.getMonth() + 1)).slice(-2)+'-'+("0" + tanggal.getDate()).slice(-2);
  isitabelcicilan+='<tr><td><input class="datatambah" name="keteranganke['+jmlcicilan+']" value="Angsuran ke '+jmlcicilan+'" /></td><td><input class="datatambah" name="cicilanke['+jmlcicilan+']" value="'+cicilan+'" /></td><td><input type="date" class="form-control datatambah datepicker" name="jatuhtempoke['+jmlcicilan+']" value="'+valtanggal+'"></td></tr>';
  $('#tabelsimulasicicilan tbody').html(isitabelcicilan);
}
function openmodalsimulasicicilan() {
  $('#modalsimulasicicilan').modal();
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['tambah'] || $this->keamanan->allowedaction['ubah']) { ?>
function openmodalid_unit(){
  $('#modalinputid_unit').modal();
  if (!$.fn.DataTable.isDataTable('#tabellistunit')){
    $('#tabellistunit').DataTable();
  }
}
function openmodalid_customer(){
  $('#modalinputid_customer').modal();
  if (!$.fn.DataTable.isDataTable('#tabellistcustomer')){
    $('#tabellistcustomer').DataTable();
  }
}
function openmodalid_marketing(){
  $('#modalinputid_marketing').modal();
  if (!$.fn.DataTable.isDataTable('#tabellistmarketing')){
    $('#tabellistmarketing').DataTable();
  }
}

function pilihunit(id){
  $('#inputid_unit').val(id);
  $('#modalinputid_unit').modal('hide');
  hargajual=$('.id_unit'+id+' td:nth-child(5)').html();
  $('#inputharga_jual').val(hargajual);
  $('#inputharga_jual').attr('min',hargajual);
  hitungharganet();
}
function pilihcustomer(id,nama){
  $('#inputid_customer').val(nama);
  $('#hidid_customer').val(id);
  $('#modalinputid_customer').modal('hide');
}
function pilihmarketing(id,nama){
  $('#inputid_marketing').val(nama);
  $('#hidid_marketing').val(id);
  $('#modalinputid_marketing').modal('hide');
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['hapus']) { ?>
  function hapusdataajax(id_pesanan) {
    if (confirm('Apakah Anda yakin ingin menghapus data transaksi ini ?')) {
      var request = $.ajax({
        url: "<?=base_url()?>ajax/hapusdatatransaksi",
        method: "POST",
        data: {
          'id_userutama': <?=$_SESSION['id_user']?>,
          'auth_keyutama': '<?=$_SESSION['auth_key']?>',
          '_csrf': '<?=$this->keamanan->generatecsrf()?>',
          'id_pesanan': id_pesanan
        },
        dataType: "json"
      });

      request.done(function( datahasil ) {
        console.log(datahasil);
        dataajax();
      });

      request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
        dataajax();
      });
    }
}
<?php } ?>
</script>
