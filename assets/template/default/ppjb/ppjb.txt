
<h3 class="wysiwyg-text-align-center"><b>SURAT PERJANJIAN PENGIKATAN JUAL BELI (SPPJB)</b></h3><p><b><br></b></p><blockquote><blockquote></blockquote></blockquote>Pada hari ini {{tanggal_approve}}, kami yang bertanda tangan di bawah ini :<br><blockquote><blockquote></blockquote></blockquote>1. Nama :{{nama_customer}}<br><blockquote><blockquote><blockquote></blockquote></blockquote></blockquote> Alamat : {{alamat_customer}}<br><p> Nomor {{jenis_identitas}} : {{no_identitas}}</p><blockquote><blockquote><blockquote></blockquote></blockquote></blockquote>Dalam hal ini bertindak atas nama diri pribadi sebagai pemilik/penjual yang untuk selanjutnya disebut PIHAK PERTAMA.<br><blockquote><blockquote><blockquote></blockquote></blockquote></blockquote>2. Nama : {{nama_marketing}}<br><p> Alamat : {{alamat_marketing}}<br></p>Dalam hal ini bertindak atas nama diri pribadi sebagai pembeli yang untuk selanjutnya disebut PIHAK KEDUA.<br>Para pihak menerangkan terlebih dahulu hal-hal sebagai berikut :<div><br>1. PIHAK PERTAMA, pemilik sah atas tanah dan bangunan rumah di {{nama_perumahan}}, dan dengan ini menyatakan untuk menjual sebuah tanah, bangunan rumah beserta usaha di dalamnya kepada PIHAK KEDUA.<br>2. Bahwa PIHAK KEDUA telah menyatakan keinginannya untuk membeli dari PIHAK PERTAMA tanah, bangunan rumah dan usaha di dalamnya beserta hak-hak atas tanah tersebut.<br>Berdasarkan pertimbangan tersebut di atas, maka dengan ini kedua belah pihak telah setuju untuk menandatangani Surat Perjanjian Pengikatan Jual Beli Tanah dan Bangunan, yang selanjutnya disebut Surat Pengikatan, dengan syarat-syarat dan ketentuan-ketentuan sebagai berikut:</div><h3 class="wysiwyg-text-align-center"><b>Pasal 1</b></h3><div>1) PIHAK PERTAMA dengan ini mengikatkan diri sekarang untuk kemudian pada waktunya menjual dan menyerahkan kepada PIHAK KEDUA dan PIHAK KEDUA dengan ini mengikatkan diri sekarang untuk kemudian pada waktunya membeli dan menerima penyerahan dari PIHAK PERTAMA, tanah dan bangunan tersebut pada ayat 2.<br>2) Kedua belah pihak setuju bahwa tanah dan bangunan rumah yang menjadi obyek dari jual beli berdasarkan Surat Pengikatan ini adalah bangunan rumah:<br>Terletak di<br>{{alamat_perumahan}}<br>Kota : {{kota_perumahan}}<br>Propinsi : {{propinsi_perumahan}}<br>Berdiri di atas sebidang tanah Hak Milik atas nama {{nama_customer}}, dibuktikan dengan Sertifikat Hak Milik Nomor : 1581.BR , seluas {{luas_bangunan}} m2 dan untuk selanjutnya disebut Tanah dan Rumah.<br>3) Pihak Pertama menyatakan sanggup untuk mengurus pengambilan sertifikat tersebut jika Pihak Kedua telah menyelesaikan pembayaran terhadap objek perjanjian tersebut di atas.<h3 class="wysiwyg-text-align-center"><b>Pasal 2</b></h3>1) Kedua belah pihak setuju bahwa harga Tanah, Rumah beserta usaha yang ada di dalamnya, yang menjadi obyek perjanjian ini adalah {{harga_net}}<br>2) Pihak Kedua telah melakukan pembayaran sebagai tanda jadi sebesar {{uang_tanda_jadi}} kepada Pihak Pertama pada tanggal 1 Mei 2016.<br>3) Untuk pembayaran selanjutnya sebesar {{harga_net}}, Pihak Kedua akan mengajukan Kredit Kepemilikan Rumah kepada pihak Perbankan, dan Pihak Pertama setuju untuk membantu prosesnya.<br>4) Jika dalam waktu 3 bulan sejak ditandatanganinya Surat Pengikatan ini, Kredit Kepemilikan Rumah yang diajukan oleh Pihak Kedua belum juga mendapat persetujuan pihak Perbankan, maka perjanjian ini seketika batal, dan Pihak Pertama tidak berkewajiban untuk mengembalikan uang tanda jadi yang telah dibayarkan oleh Pihak Kedua.<h3 class="wysiwyg-text-align-center"><b>Pasal 3</b></h3>1) Apabila di kemudian hari terjadi perubahan atau penambahan atas isi dari perjanjian ini maka kedua belah pihak akan merundingkannya secara musyawarah dan hasilnya dituangkan ke dalam suatu addendum (Perjanjian Tambahan) yang akan merupakan lampiran yang tidak dapat terpisahkan dari perjanjian ini.<br>2) Apabila tidak terjadi kesepakatan dalam musyawarah tersebut di atas, maka kedua belah pihak sepakat untuk menyelesaikan dan memilih tempat kediaman yang sah dan tidak berubah di Kantor Pengadilan Negeri Tanjungkarang.<br>Demikianlah Surat Pengikatan ini dibuat pada hari dan tanggal yang disebut pada awal perjanjian ini dalam rangkap 2 (dua) yang bermaterai cukup dan mempunyai kekuatan hukum yang sama.</div><div><br>Pihak Kedua<br>{{nama_marketing}}<br>Pihak Pertama<br>{{nama_customer}}</div><div><br>SAKSI-SAKSI :</div><div><br>1. Arfianto Nugroho ( ………………………. )<br><p>
2. Teguh Priyadin ( ………………………. )</p>






























</div>