<?php
$this->load->model('m_marketing');
$id_userutama=isset($_SESSION['id_user'])?$_SESSION['id_user']:(isset($id_user)?$id_user:0);
$auth_keyutama=isset($_SESSION['auth_key'])?$_SESSION['auth_key']:(isset($auth_key)?$auth_key:'');
$posisi=isset($_SESSION['posisi'])?$_SESSION['posisi']:(isset($posisi)?$posisi:'user');
$id_perusahaan=isset($_SESSION['id_perusahaan'])?$_SESSION['id_perusahaan']:(isset($id_perusahaan)?$id_perusahaan:0);
$datas = $this->m_marketing->marketingku(['id_userutama'=>$id_userutama,'lengkap'=>true,'posisi'=>$posisi,'auth_keyutama'=>$auth_keyutama,'id_perusahaan'=>$id_perusahaan,'aktif'=>1]);
$model=$this->m_marketing;
 ?>
<table id="tabellistmarketing" class="table table-bordered table-striped">
  <thead>
  <tr>
    <?php
    foreach ($model->kolomlengkap() as $key => $value) {
      if ($key!='id_marketing') {
        echo '<th>'.$value['label'].'</th>';
      }
    }
    ?>
  </tr>
  </thead>
  <tbody>
    <?php
    foreach ($datas as $keydt => $data) {
      echo '<tr class="id_marketing'.$data->id_marketing.'" onclick="pilihmarketing(\''.$data->id_marketing.'\',\''.$data->nama_marketing.'\')">';
      foreach ($model->kolomlengkap() as $keyk => $kolom) {
        if ($keyk!='id_marketing') {
          $data->$keyk=($keyk=='jenis')?$model->listjenis(($data->$keyk-1))['label']:$data->$keyk;
          echo '<td>'.$data->$keyk.'</td>';
        }
      }
      echo '</tr>';
    }
    ?>
  </tbody>
  <tfoot>
  <tr>
    <?php
    foreach ($model->kolomlengkap() as $key => $value) {
      if ($key!='id_marketing') {
        echo '<th>'.$value['label'].'</th>';
      }
    }
    ?>
  </tr>
  </tfoot>
</table>
