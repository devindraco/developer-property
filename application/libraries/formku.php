<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class formku {
 function generateinput($type='text',$name='teks',$value='',$label='Teks',$class='col-xs-12',$classicon='fa-book',$datas=null,$namevalue='teks',$aksi='tambah')
 {
   if (strpos($name, 'diskon') !== false) $classicon='fa-percent';
   if (strpos($name, 'rp') !== false || strpos($name, 'uang') !== false || strpos($name, 'harga') !== false) $classicon='fa-money';
   if ($type=='date') $classicon='fa-calendar';
   if ($name=='id_marketing'||$name=='id_user'||$name=='id_customer') $classicon='fa-user';
   if ($name=='id_perumahan'||$name=='id_unit'||$name=='id_tipe') $classicon='fa-home';
   if ((strpos($name, 'alamat') !== false)||$name=='kota'||$name=='propinsi') $classicon='fa-map';

   $act = ($aksi=='berubah')?'ubah':$aksi;
  $form = '<div class="form-group '.$class.' form'.$name.'">
    <label>'.$label.'</label>
    <div class="input-group">';
    if ($type!='checkbox') {
      $form.='<div class="input-group-addon">
        <i class="fa '.$classicon.'"></i>
      </div>';
    }
      switch($type){
        case 'text' :
          $form.='<input id="input'.$name.'" type="text" class="form-control data'.$aksi.'" name="'.$name.'" value="'.$value.'" required onkeydown="inputenter(\''.$act.'\',event)">';
          break;
        case 'pilihanpopup' :
          $form.='<input id="input'.$name.'" type="text" class="form-control data'.$aksi.'" name="'.$name.'" value="'.$value.'" required readonly onclick="openmodal'.$name.'()" onkeydown="openmodal'.$name.'()" style="cursor:pointer">';
          break;
        case 'textarea' :
          $form.='<textarea id="input'.$name.'" rows="5" class="form-control data'.$aksi.'" name="'.$name.'" required>'.$value.'</textarea>';
          break;
        case 'select' :
        $form.='<select id="input'.$name.'" class="form-control data'.$aksi.'" name="'.$name.'">';
        foreach($datas as $key => $data) {
            $data=(array)$data;
            $dipilih=isset($value)?$value:'';
            $dipilih=($dipilih==$data[$name])?' selected="selected"':'';
            $form.='<option value="'.$data[$name].'"'.$dipilih.'>'.$data[$namevalue].'</option>';
        }
        $form.='</select>';
          break;
        case 'number' :
          $form.='<input id="input'.$name.'" type="number" class="form-control data'.$aksi.'" name="'.$name.'" value="'.$value.'" required onkeydown="inputenter(\''.$act.'\',event)">';
          break;
        case 'date' :
          $form.='<input id="input'.$name.'" type="date" class="form-control data'.$aksi.' datepicker" name="'.$name.'" value="'.$value.'" required onkeydown="inputenter(\''.$act.'\',event)">';
          break;
        case 'checkbox' :
          //$form.='<input type="number" class="form-control data'.$aksi.'" name="'.$name.'" value="'.$value.'" required onkeydown="inputenter(\'tambah\',event)">';
          $form.='<input id="input'.$name.'" type="checkbox" class="minimal data'.$aksi.'" name="'.$name.'" '.($value?'checked':'').'> '.$name;
          break;
      }
    $form.='</div>
    <!-- /.input group -->
  </div>
  <!-- /.form group -->';
  return $form;
 }

}
/* End of file Template.php */
/* Location: ./system/application/libraries/Template.php */
