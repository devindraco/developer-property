<?php $perusahaanku=$this->m_perusahaan->perusahaanku();
$userku=$this->m_user->userku(['id_userutama'=>$_SESSION['id_user'],'lengkap'=>true,'posisiutama'=>$_SESSION['posisi'],'auth_keyutama'=>$_SESSION['auth_key']]);
$transaksiku=$this->t_transaksi->transaksiku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
$marketingku=$this->m_marketing->marketingku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
$customerku=$this->m_customer->customerku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
?><div id="boxindex" class="box">
  <div class="box-header">
    <h3 class="box-title">List pelunasan_piutang</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <?=($this->keamanan->allowedaction['tambah'])?'<button class="btn btn-primary" onclick="bukaformtambah()">Tambah</button>':''; ?>
    <button onclick="$('#filter').toggle(300);" class="btn btn-primary pull-right"><i class="fa fa-gear"></i></button>
    <div id="filter" class="row" style="display:none">
    <h4 class="col-xs-12">Filer</h4>
    <div class="form-group col-xs-3">
      <label>Status</label>
      <select id="aktif" name="aktif" class="form-control filterinput">
        <option value="2" selected>Semua</option>
        <option value="1">Aktif</option>
        <option value="0">Tidak Aktif</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Tampilkan</label>
      <select id="perpage" name="perpage" class="form-control filterinput">
        <option value="25" selected>25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="300">100</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Urutkan</label>
      <select id="orderby" name="orderby" class="form-control filterinput">
        <option value="id_pelunasan_piutang DESC" selected>Terbaru</option>
        <option value="id_pelunasan_piutang ASC">Terlama</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Nama pelunasan_piutang</label>
      <input type="text" class="form-control filterinput" name="nama_pelunasan_piutang" id="nama_pelunasan_piutang">
    </div>
    <div class="form-group col-xs-12">
      <button id="filterbutton" onclick="$('#filter').toggle(300);fitersubmit();" class="btn btn-primary pull-right"> Terapkan </button>
    </div>
  </div>
  <div class="box-table">
    <table id="tabelpelunasan_piutang" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <th></th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_pelunasan_piutang') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      <?php if ($this->keamanan->allowedaction['ubah']) { ?>
      <tr style="display:none"><td><input type="hidden" name="id_userutama" class="databerubah" value="<?=$_SESSION['id_user']?>"></td><td><input type="hidden" name="auth_keyutama" class="databerubah" value="<?=$_SESSION['auth_key']?>">
      <input type="hidden" name="_csrf" class="databerubah" value="<?=$this->keamanan->generatecsrf()?>"></td></tr>
    <?php } ?>
      </thead>
      <tbody>
        <tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>
      </tbody>
      <tfoot>
      <tr>
        <th>No.</th>
        <th></th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_pelunasan_piutang') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      </tfoot>
    </table>
  </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-left">
      <li><?=($this->keamanan->allowedaction['ubah'])?'<button id="btnubah" class="btn btn-success" onclick="ubahdata(false,true);">Ubah</button>':''; ?></li>
    </ul>

    <ul class="pagination pagination-sm no-margin pull-right">
      <li><button id="btnprev" class="btn btn-default" onclick="changepage(false);">« Prev</button></li>
      <li><button id="btnnext" class="btn btn-default" onclick="changepage();">Next »</button></li>
    </ul>
  </div>
</div>
<!-- /.box -->
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
  <div id="boxtambah" class="box" style="display:none">
    <div class="box-header">
      <h3 class="box-title">Tambah pelunasan_piutang</h3><button type="button" class="close pull-right" onclick="$('#boxtambah').hide();$('#boxindex').show();dataajax();" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <?php $this->load->view('keuangan/_form', ['perusahaanku'=>$perusahaanku,'customerku'=>$customerku,'marketingku'=>$marketingku,'userku'=>$userku,'transaksiku'=>$transaksiku]); ?>
      <button id="btntambah" type="button" class="btn btn-primary" onclick="tambahdataajax()">Simpan</button>
  </div>
</div>
<div class="modal fade" id="modalinputid_unit">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Unit List <?=$_SESSION['nama_perusahaan']?></h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>
<script type="text/javascript">
var perpage = 25;
var page = 1;
var listperusahaanku = <?=json_encode($perusahaanku)?>;
var listtransaksi = <?=json_encode($transaksiku)?>;
var listmarketingku = <?=json_encode($marketingku)?>;
var listcustomerku = <?=json_encode($customerku)?>;
var liststatus = [{'aktif':1,'id':'Aktif'},{'aktif':0,'id':'Tidak Aktif'}];

function dataajax() {
  perpage=$("#perpage").val();
  $( "#tabelpelunasan_piutang>tbody" ).html('<tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>');
  var request = $.ajax({
    url: "<?=base_url()?>ajax/datapelunasan_piutang",
    method: "POST",
    data: {
      'aktif': $('#aktif').val(),
      'kota': $('#kota').val(),
      'lengkap': 1,
      'page': page,
      'perpage': perpage,
      'posisi': '<?=$_SESSION['posisi']?>',
      'orderby': $("#orderby").val(),
      'id_userutama': <?=$_SESSION['id_user']?>,
      'auth_keyutama': '<?=$_SESSION['auth_key']?>'
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    var hasil = '';
    console.log(datahasil);
    $.each(datahasil, function( index, pelunasan_piutang ) {
      var status = (pelunasan_piutang["aktif"]==1)?'Aktif':'Tidak Aktif';
      hasil +='<tr><?php
      $hapus='';
      if ($this->keamanan->allowedaction['hapus']) {
          $hapus='<button class="btn btn-link" tabindex="-1" onclick="hapusdataajax(\'+pelunasan_piutang["id_pelunasan_piutang"]+\')"><i class="fa fa-trash"></i></button> ';
      }
      echo '<td>\'+formdiindex(\'hidden\',pelunasan_piutang["id_pelunasan_piutang"],\'id_pelunasan_piutang\',(((page-1)*perpage)+index+1))+\'</td>';
      echo '<td style="min-width:0;padding:0">'.$hapus.'</td>';
      echo '<td>\'+formdiindex(\'text\',pelunasan_piutang["id_pelunasan_piutang"],\'nama_pelunasan_piutang\',pelunasan_piutang["nama_pelunasan_piutang"])+\'</td>';
      echo '<td>\'+formdiindex(\'select\',pelunasan_piutang["id_pelunasan_piutang"],\'id_perusahaan\',pelunasan_piutang["id_perusahaan"],listperusahaanku,\'nama_perusahaan\',\''.base_url().'perusahaan\')+\'</td>';
      echo '<td>\'+formdiindex(\'text\',pelunasan_piutang["id_pelunasan_piutang"],\'alamat\',pelunasan_piutang["alamat"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',pelunasan_piutang["id_pelunasan_piutang"],\'kota\',pelunasan_piutang["kota"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',pelunasan_piutang["id_pelunasan_piutang"],\'propinsi\',pelunasan_piutang["propinsi"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',pelunasan_piutang["id_pelunasan_piutang"],\'telp\',pelunasan_piutang["telp"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',pelunasan_piutang["id_pelunasan_piutang"],\'hp\',pelunasan_piutang["hp"])+\'</td>';
      echo '<td>\'+formdiindex(\'select\',pelunasan_piutang["id_pelunasan_piutang"],\'jenis\',pelunasan_piutang["aktif"],listjenis,\'label\')+\'</td>';
      echo '<td>\'+formdiindex(\'select\',pelunasan_piutang["id_pelunasan_piutang"],\'id_user\',pelunasan_piutang["id_user"],listuserku,\'username\',\''.base_url().'user\')+\'</td>';
      echo '<td>\'+formdiindex(\'select\',pelunasan_piutang["id_pelunasan_piutang"],\'aktif\',pelunasan_piutang["aktif"],liststatus,\'id\')+\'</td>';
    /*  echo '<td>';
      if ($this->keamanan->allowedaction['ubah']) {
        echo '<a tabindex="-1" href="'.base_url().'pelunasan_piutang/edit/\'+pelunasan_piutang["id_pelunasan_piutang"]+\'"><i class="fa fa-pencil"></i></a> ';
      }
      if ($this->keamanan->allowedaction['hapus']) {
          echo '<a tabindex="-1" href="'.base_url().'pelunasan_piutang/hapus/\'+pelunasan_piutang["id_pelunasan_piutang"]+\'"><i class="fa fa-trash"></i></a> ';
      }
      echo '</td>'; */
      ?></tr>';
    });
    $( "#tabelpelunasan_piutang>tbody" ).html(hasil);
    if (datahasil.length<perpage) $('#btnnext').attr("disabled", true); else $('#btnnext').attr("disabled", false);
    if (page<=1) $('#btnprev').attr("disabled", true); else $('#btnprev').attr("disabled", false);
    ubahdata(true);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
<?php if ($this->keamanan->allowedaction['ubah']) { ?>
  function updatedataajax() {
    var request = $.ajax({
      url: "<?=base_url()?>ajax/updatedatapelunasan_piutang",
      method: "POST",
      data: $("#tabelpelunasan_piutang .databerubah").serialize(),
      dataType: "json"
    });

    request.done(function( datahasil ) {
      console.log(datahasil);
      dataajax();
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      dataajax();
    });
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
  function bukaformtambah(){
    resetformtambah();
    $('#boxindex').hide();$('#boxtambah').show();$('.boxtambah input.form-control').first().focus();
  }
  function resetformtambah() {
    $('.formdiskon1').hide();
    $('.formdiskon2').hide();
    $('.formdiskon3').hide();
    $('.formdiskon1_rp').hide();
    $('.formdiskon2_rp').hide();
    $('.formdiskon3_rp').hide();
  }
function tambahdataajax() {
  $('#modal-tambah').modal('toggle');
  var request = $.ajax({
    url: "<?=base_url()?>ajax/tambahdatapelunasan_piutang",
    method: "POST",
    data: $("#modal-tambah .datatambah").serialize(),
    dataType: "json"
  });

  request.done(function( datahasil ) {
    console.log(datahasil);
    dataajax();
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
    dataajax();
  });
}
window.addEventListener('DOMContentLoaded', (event) => {
    $('#tabellistunit').DataTable({"scrollX": true});
});
function pilihunit(id){
  $('#inputid_unit').val(id);
  $('#modalinputid_unit').modal('hide');
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['hapus']) { ?>
  function hapusdataajax(id_pelunasan_piutang) {
    if (confirm('Apakah Anda yakin ingin menghapus data pelunasan_piutang ini ?')) {
      var request = $.ajax({
        url: "<?=base_url()?>ajax/hapusdatapelunasan_piutang",
        method: "POST",
        data: {
          'id_userutama': <?=$_SESSION['id_user']?>,
          'auth_keyutama': '<?=$_SESSION['auth_key']?>',
          '_csrf': '<?=$this->keamanan->generatecsrf()?>',
          'id_pelunasan_piutang': id_pelunasan_piutang
        },
        dataType: "json"
      });

      request.done(function( datahasil ) {
        console.log(datahasil);
        dataajax();
      });

      request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
        dataajax();
      });
    }
}
<?php } ?>
</script>
