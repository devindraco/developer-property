<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_cluster extends CI_Model {

  function kolom($id='') {
    $listkolom = array(
      'id_cluster'=> ['label'=>'ID Cluster','default'=>0,'type'=>'text'],
      'nama_cluster'=> ['label'=>'Nama Cluster','default'=>'','type'=>'text'],
      'id_perumahan'=> ['label'=>'Perumahan','default'=>1,'type'=>'select'],
      'aktif'=> ['label'=>'Status','default'=>1,'type'=>'checkbox']
    );
    if (isset($listkolom[$id])) {
      return $listkolom[$id];
    } else {
      return $listkolom;
    }
  }

  function dataupdate($data) {
    $sql = null;
    if (isset($data['id_cluster'])) {
      $sql = array();
      $query = '';
      foreach ($data['id_cluster'] as $key => $value) {
        $sql[$key]='EXEC sp_m_cluster @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\',';
        foreach ($this->kolom() as $id => $label) {
          if (isset($data[$id])) {
            $sql[$key].=isset($data[$id][$key])?' @'.$id.'=\''.$data[$id][$key].'\',':'';
          }
        }
        $sql[$key]=substr($sql[$key], 0, -1);
        $sql[$key].=';';
        $query.=$sql[$key].' ';
      }
      if ($query!=''){
        $q=$this->db->query($query);
        //$sql=$q->result_array();
      }
    }
    return '';
  }

  function datainsert($data) {
    $query='EXEC sp_m_cluster @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\',';
    foreach ($this->kolom() as $id => $label) {
      if (isset($data[$id])) {
        $data[$id]=($id=='aktif')?(($data[$id]=='on')?1:0):$data[$id];
        $query.=' @'.$id.'=\''.$data[$id].'\',';
      }
    }
    $query=substr($query, 0, -1);
    $query.=';';
    if ($query!=''){
      $q=$this->db->query($query);
      //$sql=$q->result_array();
    }
    return '';
  }

  function datahapus($data) {
    $data['id_user']=isset($data['id_user'])?$data['id_user']:0;
    $data['auth_key']=isset($data['auth_key'])?$data['auth_key']:'-';
    $data['id_cluster']=isset($data['id_cluster'])?$data['id_cluster']:0;
    $query='EXEC sp_m_cluster @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\', @id_cluster=\''.$data['id_cluster'].'\', @aksi=\'hapus\';';
    $q=$this->db->query($query);
    return '';
  }

  function clusterku($datainput){
  $q = $this->load->model('m_user');
	$response = null;
    $data = array(
      'id_cluster'=>isset($datainput['id_cluster'])?$datainput['id_cluster']:0,
      'id_perumahan'=>isset($datainput['id_perumahan'])?$datainput['id_perumahan']:0,
      'lengkap'=>isset($datainput['lengkap'])?$datainput['lengkap']:false,
      'aktif'=>isset($datainput['aktif'])?$datainput['aktif']:2,
      'page'=>isset($datainput['page'])?$datainput['page']:1,
      'perpage'=>isset($datainput['perpage'])?$datainput['perpage']:25,
      'nama_cluster'=>isset($datainput['nama_cluster'])?$datainput['nama_cluster']:'',
      'orderby'=>isset($datainput['orderby'])?$datainput['orderby']:'id_cluster DESC',
      'alldata'=>isset($datainput['alldata'])?$datainput['alldata']:false,
      'posisi'=>isset($datainput['posisi'])?$datainput['posisi']:'user',
      'id_user'=>isset($datainput['id_user'])?$datainput['id_user']:0,
      'auth_key'=>isset($datainput['auth_key'])?$datainput['auth_key']:'-',
      'dropdown'=>isset($datainput['dropdown'])?$datainput['dropdown']:false,
    );
		$response = array();
		$id_user = htmlspecialchars($data['id_user']);
    $auth_key = htmlspecialchars($data['auth_key']);
		$id_perumahan = htmlspecialchars($data['id_perumahan']);
    $id_cluster = htmlspecialchars($data['id_cluster']);
    $nama_cluster = htmlspecialchars($data['nama_cluster']);
    $aktif = (int)$data['aktif'];
    $page = ((int)$data['page']>=1)?(int)$data['page']:1;
    $perpage = ((int)$data['perpage']>=1)?(int)$data['perpage']:25;
    $orderby = $data['orderby'];
		$checkid = ($id_cluster!=0)?' AND id_cluster='.$id_cluster:'';
    $select = ($data['lengkap'])?'id_cluster, aktif, nama_cluster, id_perumahan, nama_perumahan, alamat_perumahan, kota_perumahan, propinsi_perumahan, telp_perumahan, website_perumahan, max_cicilan_cash, max_cicilan_kpr, max_cicilan_inhouse, max_persen_dp, default_uang_tanda_jadi, aktif_perumahan, id_perusahaan, nama_perusahaan, id_pemilik, alamat_perusahaan, kota_perusahaan, propinsi_perusahaan, telp_perusahaan, website_perusahaan, npwp_perusahaan, id_user, auth_key, nama_depan, nama_belakang, jabatan, aktif_user':'id_cluster,aktif,id_perumahan,id_perusahaan,nama_cluster';
    if ($data['dropdown']) {
      $select=str_replace('nama_cluster','nama_cluster+\' - \'+nama_perumahan as nama_cluster', $select);
    }
    $aktif = ($aktif==1||$aktif==0)?' AND aktif='.$aktif:'';
    $tambahan = ($nama_cluster!='')?' AND nama_cluster LIKE \'%'.$nama_cluster.'%\'':'';
    $tambahan = ' AND id_user='.$id_user.' AND auth_key=\''.$auth_key.'\'';
    $limit=($data['alldata'])?'':' OFFSET '.(($data['page']-1)*$data['perpage']).' ROWS FETCH NEXT '.$data['perpage'].' ROWS ONLY';
    $q = $this->db->query('SELECT '.$select.' FROM v_clusterku WHERE id_user='.$id_user.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit);
		// if ($data['posisi']=='owner') {
		// 	$q = $this->db->query('SELECT '.$select.' FROM v_cluster_owner WHERE id_user='.$id_user.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit);
		// } else {
		// 	$q = $this->db->query('SELECT '.$select.' FROM v_cluster_owner WHERE id_user='.$id_user.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit);
		// }
    //$q = $this->db->query('exec sp_data_m_perumahan @id_usr ='.$id_user);
		$response = ($id_perumahan==0)?$q->result():$q->row();
    //$response = 'SELECT '.$select.' FROM v_perumahan_owner WHERE id_user='.$id_user.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit;
    return $response;
  }


}
