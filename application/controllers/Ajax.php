<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct() {
		 parent::__construct();
	 }


 // PERUSAHAAN -------------------------------------------------------------------
 public function tambahdataperusahaan() {
  $data='';
  if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
 	 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
 		 $data = $this->m_perusahaan->datainsert($_POST);
 	 }
  }
  echo json_encode($data);
 }
 public function updatedataperusahaan() {
  $data='';
  if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
 	if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
 		$data = $this->m_perusahaan->datainsert($_POST);
 	}
  }
  echo json_encode($data);
 }

 public function hapusdataperusahaan() {
  $data='';
  if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])&&isset($_POST['id_perusahaan'])){
	 	if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 		$data = $this->m_perusahaan->datahapus($_POST);
	 	}
  }
  echo json_encode($data);
 }


 // PERUMAHAN -------------------------------------------------------------------
 public function dataperumahan()
{
		$this->load->model('m_perumahan');
		$perumahanku=$this->m_perumahan->perumahanku($_POST);
		echo json_encode($perumahanku);
}
public function updatedataperumahan() {
 $data='';
 $this->load->model('m_perumahan');
 if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
	 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		 $data = $this->m_perumahan->dataupdate($_POST);
	 }
 }
 echo json_encode($data);
}
public function tambahdataperumahan() {
$data='';
$this->load->model('m_perumahan');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
	if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		$data = $this->m_perumahan->datainsert($_POST);
	}
}
echo json_encode($data);
}
public function hapusdataperumahan() {
$data='';
$this->load->model('m_perumahan');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])&&isset($_POST['id_perumahan'])){
	if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		$data = $this->m_perumahan->datahapus($_POST);
	}
}
echo json_encode($data);
}

// CLUSTER -------------------------------------------------------------------
public function datacluster()
{
	 $this->load->model('m_cluster');
	 $clusterku=$this->m_cluster->clusterku($_POST);
	 echo json_encode($clusterku);
}
public function updatedatacluster() {
$data='';
$this->load->model('m_cluster');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
	if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		$data = $this->m_cluster->dataupdate($_POST);
	}
}
echo json_encode($data);
}
public function tambahdatacluster() {
$data='';
$this->load->model('m_cluster');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->m_cluster->datainsert($_POST);
 }
}
echo json_encode($data);
}
public function hapusdatacluster() {
$data='';
$this->load->model('m_cluster');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])&&isset($_POST['id_cluster'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->m_cluster->datahapus($_POST);
 }
}
echo json_encode($data);
}

// TIPE -------------------------------------------------------------------
public function datatipe()
{
	 $this->load->model('m_tipe');
	 $tipeku=$this->m_tipe->tipeku($_POST);
	 echo json_encode($tipeku);
}
public function updatedatatipe() {
$data='';
$this->load->model('m_tipe');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
	if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		$data = $this->m_tipe->dataupdate($_POST);
	}
}
echo json_encode($data);
}
public function tambahdatatipe() {
$data='';
$this->load->model('m_tipe');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->m_tipe->datainsert($_POST);
 }
}
echo json_encode($data);
}
public function hapusdatatipe() {
$data='';
$this->load->model('m_tipe');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])&&isset($_POST['id_tipe'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->m_tipe->datahapus($_POST);
 }
}
echo json_encode($data);
}

// UNIT -------------------------------------------------------------------
public function dataunit()
{
	 $this->load->model('m_unit');
	 $unitku=$this->m_unit->unitku($_POST);
	 echo json_encode($unitku);
}
public function viewlengkapunit()
{
	 $this->load->model('m_unit');
	 $this->load->view('unit/_datalengkap',$_POST);
}
public function updatedataunit() {
$data='';
$this->load->model('m_unit');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
	if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		$data = $this->m_unit->dataupdate($_POST);
	}
}
echo json_encode($data);
}
public function tambahdataunit() {
$data='';
$this->load->model('m_unit');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->m_unit->datainsert($_POST);
 }
}
echo json_encode($data);
}
public function hapusdataunit() {
$data='';
$this->load->model('m_unit');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])&&isset($_POST['id_unit'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->m_unit->datahapus($_POST);
 }
}
echo json_encode($data);
}


// MARKETING -------------------------------------------------------------------
public function datamarketing()
{
	 $this->load->model('m_marketing');
	 $marketingku=$this->m_marketing->marketingku($_POST);
	 echo json_encode($marketingku);
}
public function viewlengkapmarketing()
{
	 $this->load->model('m_marketing');
	 $this->load->view('marketing/_datalengkap',$_POST);
}
public function updatedatamarketing() {
$data='';
$this->load->model('m_marketing');
if (isset($_POST['id_userutama'])&&isset($_POST['auth_keyutama'])&&isset($_POST['_csrf'])){
	if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		$data = $this->m_marketing->dataupdate($_POST);
	}
}
echo json_encode($data);
}
public function tambahdatamarketing() {
$data='';
$this->load->model('m_marketing');
if (isset($_POST['id_userutama'])&&isset($_POST['auth_keyutama'])&&isset($_POST['_csrf'])) {
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->m_marketing->datainsert($_POST);
 }
}
echo json_encode($data);
}
public function hapusdatamarketing() {
$data='';
$this->load->model('m_marketing');
if (isset($_POST['id_userutama'])&&isset($_POST['auth_keyutama'])&&isset($_POST['_csrf'])&&isset($_POST['id_marketing'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->m_marketing->datahapus($_POST);
 }
}
echo json_encode($data);
}




// USER -------------------------------------------------------------------
public function datauser()
{
	 $this->load->model('m_user');
	 $userku=$this->m_user->userku($_POST);
	 echo json_encode($userku);
}
public function updatedatauser() {
$data='';
$this->load->model('m_user');
if (isset($_POST['id_userutama'])&&isset($_POST['auth_keyutama'])&&isset($_POST['_csrf'])){
	if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		$data = $this->m_user->dataupdate($_POST);
	}
}
echo json_encode($data);
}
public function tambahdatauser() {
$data='';
$this->load->model('m_user');
if (isset($_POST['id_userutama'])&&isset($_POST['auth_keyutama'])&&isset($_POST['_csrf'])&&isset($_POST['perusahaan'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->m_user->datainsert($_POST);
 }
}
echo json_encode($data);
}
public function hapusdatauser() {
$data='';
$this->load->model('m_user');
if (isset($_POST['id_userutama'])&&isset($_POST['auth_keyutama'])&&isset($_POST['_csrf'])&&isset($_POST['id_user'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->m_user->datahapus($_POST);
 }
}
echo json_encode($data);
}
public function datalistmenu() {
$data='';
$this->load->model('m_user_menu');
if (isset($_POST['id_userutama'])&&isset($_POST['auth_keyutama'])&&isset($_POST['id_user'])&&isset($_POST['_csrf'])){
	if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		$data = $this->m_user_menu->listmenuku($_POST);
	}
}
echo json_encode($data);
}


// CUSTOMER -------------------------------------------------------------------
public function listdatacustomer()
{
	 $this->load->model('m_customer');
	 if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
		 $customerku=$this->m_customer->listcustomerku($_POST);
	 }
	 echo json_encode($customerku);
}
public function datacustomer()
{
	 $this->load->model('m_customer');
	 $customerku=$this->m_customer->customerku($_POST);
	 echo json_encode($customerku);
}
public function viewlengkapcustomer()
{
	 $this->load->model('m_customer');
	 $this->load->view('customer/_datalengkap',$_POST);
}
public function updatedatacustomer() {
$data='';
$this->load->model('m_customer');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
	if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		$data = $this->m_customer->dataupdate($_POST);
	}
}
echo json_encode($data);
}
public function updatedatacustomerdokumen() {
$data='';
$this->load->model('m_customer');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
	if ($_POST['_csrf']==$this->keamanan->generatecsrf() && $data = $this->m_customer->customerku($_POST)){
		$data = $this->m_customer->dataupdatedokumen($_POST,$_FILES);
	}
}
echo json_encode($data);
}
public function tambahdatacustomer() {
$data='';
$this->load->model('m_customer');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->m_customer->datainsert($_POST,$_FILES);
 }
}
echo json_encode($data);
}
public function hapusdatacustomer() {
$data='';
$this->load->model('m_customer');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->m_customer->datahapus($_POST);
 }
}
echo json_encode($data);
}


// TRANSAKSI -------------------------------------------------------------------
public function datatransaksi()
{
	 $this->load->model('t_transaksi');
	 $transaksiku=$this->t_transaksi->transaksiku($_POST);
	 echo json_encode($transaksiku);
}
public function datatransaksidetail()
{
	$data='';
	 $this->load->model('t_transaksi');
	 if (isset($_POST['id_userutama'])&&isset($_POST['auth_keyutama'])&&isset($_POST['id_pesanan'])){
		 $data=$this->t_transaksi->transaksidetailku($_POST);
	 }
	 echo json_encode($data);
}
public function updatedatatransaksi() {
$data='';
$this->load->model('t_transaksi');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
	if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		$data = $this->t_transaksi->dataupdate($_POST);
	}
}
echo json_encode($data);
}
public function tambahdatatransaksi() {
$data='';
$this->load->model('t_transaksi');
if (isset($_POST['id_userutama'])&&isset($_POST['auth_keyutama'])&&isset($_POST['id_unit'])&&isset($_POST['id_customer'])&&isset($_POST['id_marketing'])&&isset($_POST['_csrf'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->t_transaksi->datainsert($_POST);
 }
}
echo json_encode($data);
}
public function hapusdatatransaksi() {
$data='';
$this->load->model('t_transaksi');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->t_transaksi->datahapus($_POST);
 }
}
echo json_encode($data);
}
public function detailcicilan()
{
	 $this->load->model('t_transaksi_dtl');
	 $transaksiku=$this->t_transaksi_dtl->transaksiku($_POST);
	 echo json_encode($transaksiku);
}

// KWITANSI -------------------------------------------------------------------
public function datakwitansi() {
$data='';
$this->load->model('t_kwitansi');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->t_kwitansi->kwitansiku($_POST);
 }
}
echo json_encode($data);
}

public function tambahdatakwitansi() {
$data='--';
$this->load->model('t_kwitansi');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])&&isset($_FILES['inputimageFile'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->t_kwitansi->datasimpan($_POST,$_FILES['inputimageFile']);
 }
}
echo json_encode($data);
}

public function updatedatakwitansi() {
$data='--';
$this->load->model('t_kwitansi');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->t_kwitansi->datasimpan($_POST,$_FILES['inputimageFile']);
 }
}
echo json_encode($data);
}

// PENGELUARAN -------------------------------------------------------------------
public function datapengeluaran()
{
	$data='';
	 $this->load->model('t_pengeluaran');
	 if (isset($_POST['id_user'])&&isset($_POST['auth_key'])){
		 $data=$this->t_pengeluaran->pengeluaranku($_POST);
	 }
	 echo json_encode($data);
}
public function tambahdatapengeluaran() {
 $data='';
 $this->load->model('t_kwitansi');
 $this->load->model('t_pengeluaran');
 if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['id_kwitansi'])&&isset($_POST['_csrf'])){
	if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		if ($_POST['id_kwitansi']==0) {
				$data = $this->t_kwitansi->datasimpan($_POST,$_FILES['inputimageFile']);
				$_POST['id_kwitansi'] = isset($data['id_kwitansi'])?$data['id_kwitansi']:$_POST['id_kwitansi'];
    }
		$data = $this->t_pengeluaran->datainsert($_POST);
	}
 }
 echo json_encode($data);
}
public function updatedatapengeluaran() {
 $data='';
 $this->load->model('t_pengeluaran');
 if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])&&isset($_POST['id_pengeluaran'])){
	 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		 $data = $this->t_pengeluaran->datainsert($_POST);
	 }
 }
 echo json_encode($data);
}

public function hapusdatapengeluaran() {
 $data='';
 $this->load->model('t_pengeluaran');
 if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])&&isset($_POST['id_pengeluaran'])){
	 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		 $data = $this->t_pengeluaran->datahapus($_POST);
	 }
 }
 echo json_encode($data);
}


// TEMPLATE -------------------------------------------------------------------
public function simpantemplatedokumen()
{
	$transaksiku='-';
	 $this->load->model('m_template_dokumen');
	 if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['jenis_template'])&&isset($_POST['template'])&&isset($_POST['id_template_dokumen'])&&isset($_POST['id_perusahaan'])&&isset($_POST['_csrf'])){
		 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
			 $transaksiku=$this->m_template_dokumen->simpan_template_dokumenku($_POST);
		 }
	 }
	 echo json_encode($transaksiku);
}

public function datatemplatedokumen()
{
	$transaksiku='-';
	 $this->load->model('m_template_dokumen');
	 if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['jenis_template'])&&isset($_POST['id_perusahaan'])&&isset($_POST['_csrf'])){
		 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
			 $transaksiku=$this->m_template_dokumen->template_dokumenku($_POST);
		 }
	 }
	 echo json_encode($transaksiku);
}

// PELUSASAN PIUTANG -------------------------------------------------------------------
public function datapelunasan_piutang()
{
	 $this->load->model('pelunasan_piutang');
	 $pelunasan_piutangku=$this->m_pelunasan_piutang->pelunasan_piutangku($_POST);
	 echo json_encode($pelunasan_piutangku);
}
public function updatedatapelunasan_piutang() {
$data='';
$this->load->model('pelunasan_piutang');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
	if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		$data = $this->m_pelunasan_piutang->dataupdate($_POST);
	}
}
echo json_encode($data);
}
public function tambahdatapelunasan_piutang() {
$data='';
$this->load->model('pelunasan_piutang');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->m_pelunasan_piutang->datainsert($_POST);
 }
}
echo json_encode($data);
}
public function hapusdatapelunasan_piutang() {
$data='';
$this->load->model('pelunasan_piutang');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->m_pelunasan_piutang->datahapus($_POST);
 }
}
echo json_encode($data);
}



// APPROVAL -------------------------------------------------------------------
public function approvetransaksi()
{
	$data='';
	$this->load->model('t_transaksi');
	if (isset($_POST['id_pesanan'])&&isset($_POST['status'])&&isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
	 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		 $data = $this->t_transaksi->approve($_POST);
	 }
	}
	echo json_encode($data);
}

public function approvepengeluaran()
{
	$data='';
	$this->load->model('t_pengeluaran');
	if (isset($_POST['id_pengeluaran'])&&isset($_POST['status'])&&isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
	 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		 $data = $this->t_pengeluaran->approve($_POST);
	 }
	}
	echo json_encode($data);
}



// NUP -------------------------------------------------------------------
public function datanup()
{
	 $this->load->model('t_nup');
	 $nup=$this->t_nup->nupku($_POST);
	 echo json_encode($nup);
}
public function updatedatanup() {
$data='';
$this->load->model('t_nup');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
	if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		$data = $this->t_nup->dataupdate($_POST);
	}
}
echo json_encode($data);
}
public function tambahdatanup() {
$data='';
$this->load->model('t_nup');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->t_nup->datainsert($_POST);
 }
}
echo json_encode($data);
}
public function hapusdatanup() {
$data='';
$this->load->model('t_nup');
if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
	 $data = $this->t_nup->datahapus($_POST);
 }
}
echo json_encode($data);
}

public function simpandatanupkoordinat() {
	$data='';
	$this->load->model('t_nup');
	if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])){
	 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		 $data = $this->t_nup->simpankoordinat($_POST);
	 }
	}
	echo json_encode($data);
}

public function simpandatanuppeta() {
	$data='--';
	$this->load->model('t_nup');
	if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])&&isset($_FILES['inputimageFile'])){
	 if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
		 $data = $this->t_nup->simpanpeta($_POST,$_FILES['inputimageFile']);
	 }
	}
	echo json_encode($data);
}

public function listpemilihnup() {
	$data='';
	$this->load->model('t_nup');
	if (isset($_POST['id_user'])&&isset($_POST['auth_key'])&&isset($_POST['_csrf'])&&isset($_POST['id_nup'])) {
		if ($_POST['_csrf']==$this->keamanan->generatecsrf()){
			$data = $this->t_nup->listunitku($_POST);
		}
	}
	echo json_encode($data);
}


// END OF FILE ----------------------------------------
}
