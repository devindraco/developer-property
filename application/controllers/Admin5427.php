<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin5427 extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct() {
		 parent::__construct();
	 }
	 public function index($page=1)
	 {
		 $this->load->model('m_user');
		 $this->load->model('m_menu');
		 $aktif=isset($_GET['aktif'])?$_GET['aktif']:2;
		 $perpage=isset($_GET['perpage'])?$_GET['perpage']:25;
		 $page=(int)$page;
		 $this->setlayouts('utama','admin/utama',['model'=>$this->m_user,'menudipilih'=>'Master Data']);
	 }

	 public function login()
	 {
	 	$error='';
	 	if (isset($_POST['login'])&&isset($_POST['username'])&&isset($_POST['password'])&&isset($_POST['_csrf'])) {
	 		if ($_POST['_csrf']==$this->keamanan->generatecsrf('login')){
	 			if ($_POST['username']=='admsidutama'&&$_POST['password']=='adm845k') {
	 				$newdata = array(
	 								'id_user'  => 0,
	 								'nama'     =>  'Admin',
	 								'auth_key'     =>  $this->keamanan->generateauth_key(523),
	 								'posisi' => 'owner',
	 								'id_perusahaan' => -32,
	 								'nama_perusahaan' => ''
	 				);
	 				$this->session->set_userdata($newdata);
	 				redirect('admin5427/', 'refresh');
	 			} else {
	 				$error='Username/Password Salah. Silahkan Coba Lagi';
	 			}
	 		} else {
	 			$error='Sesi telah habis, silahkan login kembali.';
	 		}
	 		$this->setlayouts('login','blank',['error'=>$error]);
	 	} else {
	 		$this->setlayouts('login','blank');
	 	}
	 }



// END OF FILE ----------------------------------------
}
