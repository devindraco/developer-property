<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_nup extends CI_Model {

  function kolom($id='') {
    $listkolom = array(
      'id_nup'=> ['label'=>'ID nup','default'=>0,'type'=>'text'],
      'kode_nup'=> ['label'=>'Kode NUP','default'=>'','type'=>'text'],
      'nama_nup'=> ['label'=>'Nama NUP','default'=>'','type'=>'text'],
      'id_perumahan'=> ['label'=>'Perumahan','default'=>'a','type'=>'select'],
      'tanggal'=> ['label'=>'Tanggal','default'=>date('Y-m-d'),'type'=>'date'],
      'status'=> ['label'=>'Status','default'=>1,'type'=>'select']
    );
    if (isset($listkolom[$id])) {
      return $listkolom[$id];
    } else {
      return $listkolom;
    }
  }

  function liststatus($id='') {
    $liststatus = array(
      ['status'=>1,'label'=> 'Tersedia'],
      ['status'=>2,'label'=> 'Sudah Di Pesan'],
      ['status'=>'n','label'=> 'Not Ready'],
      ['status'=>'s','label'=> 'Sold'],
    );
    if (isset($liststatus[$id])) {
      return $liststatus[$id];
    } else {
      return $liststatus;
    }
  }

  function dataupdate($data) {
    $sql = null;
    if (isset($data['id_nup'])) {
      $sql = array();
      $query = '';
      foreach ($data['id_nup'] as $key => $value) {
        $sql[$key]='EXEC sp_t_nup @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\', @id_perusahaan=\''.$data['id_perusahaan'].'\',@aksi=\'ubah\',';
        foreach ($this->kolom() as $id => $label) {
          if (isset($data[$id])) {
            $sql[$key].=isset($data[$id][$key])?' @'.$id.'=\''.$data[$id][$key].'\',':'';
          }
        }
        $sql[$key]=substr($sql[$key], 0, -1);
        $sql[$key].=';';
        $query.=$sql[$key].' ';
      }
      if ($query!=''){
        $q=$this->db->query($query);
        //$sql=$q->result_array();
      }
    }
    return '';
  }

  function simpanpeta($data,$file) {
    $nupku = $this->nupku($data);
    $target_path='';
    if ($nupku) {
      if ($file!=null) {
          //$path = str_replace('inputimageFile','',$key);
          $target_path = BASEPATH.'../assets/images/nup/'.$nupku->id_nup.'/';
          if(!file_exists($target_path)){ mkdir($target_path, '0777', true);}
          $target_path = $target_path.basename('peta.jpg');

          move_uploaded_file($file['tmp_name'], $target_path);
          $target_path = $file['tmp_name'];
      }
    }
    return $target_path;
  }

  function datainsert($data) {
    $query='EXEC sp_t_nup @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\', @id_perusahaan=\''.$data['id_perusahaan'].'\',@aksi=\'tambah\',';
    foreach ($this->kolom() as $id => $label) {
      if (isset($data[$id])) {
        $query.=' @'.$id.'=\''.$data[$id].'\',';
      }
    }
    $query=substr($query, 0, -1);
    $query.=';';
    if ($query!=''){
      $q=$this->db->query($query);
      //$sql=$q->result_array();
    }
    return '';
  }

  function simpankoordinat($data) {
    $tambahan = isset($data['coords'])?' @coords=\''.$data['coords'].'\',':'';
    $tambahan = isset($data['nokupon'])?$tambahan.' @nokupon='.$data['nokupon'].',':$tambahan;
    $query='EXEC sp_t_nup_unit @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\', @id_nup='.$data['id_nup'].', @id_unit='.$data['id_unit'].','.$tambahan.' @status=\''.$data['status'].'\';';
    if ($query!=''){
      $q=$this->db->query($query);
      $sql=$q->row_array();
    }
    return $sql;
  }

  function datahapus($data) {
    $data['id_user']=isset($data['id_user'])?$data['id_user']:0;
    $data['auth_key']=isset($data['auth_key'])?$data['auth_key']:'-';
    $data['id_nup']=isset($data['id_nup'])?$data['id_nup']:0;
    $query='EXEC sp_t_nup @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\', @id_nup=\''.$data['id_nup'].'\', @aksi=\'hapus\';';
    $q=$this->db->query($query);
    return '';
  }

  function nupku($datainput){
  $q = $this->load->model('m_user');
	$response = null;
    $data = array(
      'id_nup'=>isset($datainput['id_nup'])?$datainput['id_nup']:0,
      'kode_nup'=>isset($datainput['kode_nup'])?$datainput['kode_nup']:0,
      'lengkap'=>isset($datainput['lengkap'])?$datainput['lengkap']:false,
      'status'=>isset($datainput['status'])?$datainput['status']:'a',
      'page'=>isset($datainput['page'])?$datainput['page']:1,
      'perpage'=>isset($datainput['perpage'])?$datainput['perpage']:25,
      'nama_nup'=>isset($datainput['nama_nup'])?$datainput['nama_nup']:'',
      'orderby'=>isset($datainput['orderby'])?$datainput['orderby']:'id_nup DESC',
      'alldata'=>isset($datainput['alldata'])?$datainput['alldata']:false,
      'posisi'=>isset($datainput['posisi'])?$datainput['posisi']:'user',
      'id_perusahaan'=>isset($datainput['id_perusahaan'])?$datainput['id_perusahaan']:0,
      'id_perumahan'=>isset($datainput['id_perumahan'])?$datainput['id_perumahan']:0,
      'id_user'=>isset($datainput['id_user'])?$datainput['id_user']:1,
      'auth_key'=>isset($datainput['auth_key'])?$datainput['auth_key']:'-',
    );
		$response = array();
		$id_user = htmlspecialchars($data['id_user']);
    $auth_key = htmlspecialchars($data['auth_key']);
		$kode_nup = htmlspecialchars($data['kode_nup']);
    $id_nup = htmlspecialchars($data['id_nup']);
    $status = htmlspecialchars($data['status']);
    $id_perusahaan = ((int)$data['id_perusahaan']>=1)?(int)$data['id_perusahaan']:0;
    $id_perumahan = ((int)$data['id_perumahan']>=1)?(int)$data['id_perumahan']:0;
    $page = ((int)$data['page']>=1)?(int)$data['page']:1;
    $perpage = ((int)$data['perpage']>=1)?(int)$data['perpage']:25;
    $orderby = $data['orderby'];
    $checkid = ($id_nup!=0)?' AND id_nup='.$id_nup:'';
    $select = ($data['lengkap'])?'*':'id_nup,status,nama_nup,id_perusahaan';
    $status = ($status!='a')?' AND status=\''.$status.'\'':'';
    $tambahan = ' AND id_user='.$id_user.' AND auth_key=\''.$auth_key.'\'';
    $tambahan .= ($id_perusahaan!=0)?' AND id_perusahaan='.$id_perusahaan:'';
    $tambahan .= ($id_perumahan!=0)?' AND id_perumahan='.$id_perumahan:'';
    $limit=($data['alldata'])?'':' OFFSET '.(($data['page']-1)*$data['perpage']).' ROWS FETCH NEXT '.$data['perpage'].' ROWS ONLY';
    $q = $this->db->query('SELECT '.$select.' FROM v_nupku WHERE 1=1'.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit);
    //$q = $this->db->query('SELECT '.$select.' FROM t_nup WHERE id_user='.$id_user.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit);
		// if ($data['posisi']=='owner') {
		// 	$q = $this->db->query('SELECT '.$select.' FROM v_nup_owner WHERE id_user='.$id_user.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit);
		// } else {
		// 	$q = $this->db->query('SELECT '.$select.' FROM v_nup_owner WHERE id_user='.$id_user.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit);
		// }
    //$q = $this->db->query('exec sp_data_m_tipe @id_usr ='.$id_user);
		$response = ($id_nup==0)?$q->result():$q->row();
    return $response;
  }

  // Data Unit yang terdaftar di NUP
  function listunitku($datainput){
	$response = null;
    $data = array(
      'id_nup'=>isset($datainput['id_nup'])?$datainput['id_nup']:0,
      'id_unit'=>isset($datainput['id_unit'])?$datainput['id_unit']:0,
      'kode_nup'=>isset($datainput['kode_nup'])?$datainput['kode_nup']:0,
      'lengkap'=>isset($datainput['lengkap'])?$datainput['lengkap']:false,
      'status'=>isset($datainput['status'])?$datainput['status']:'0',
      'page'=>isset($datainput['page'])?$datainput['page']:1,
      'perpage'=>isset($datainput['perpage'])?$datainput['perpage']:25,
      'nama_nup'=>isset($datainput['nama_nup'])?$datainput['nama_nup']:'',
      'orderby'=>isset($datainput['orderby'])?$datainput['orderby']:'id_nup DESC',
      'alldata'=>isset($datainput['alldata'])?$datainput['alldata']:false,
      'posisi'=>isset($datainput['posisi'])?$datainput['posisi']:'user',
      'id_perusahaan'=>isset($datainput['id_perusahaan'])?$datainput['id_perusahaan']:0,
      'id_perumahan'=>isset($datainput['id_perumahan'])?$datainput['id_perumahan']:0,
      'id_user'=>isset($datainput['id_user'])?$datainput['id_user']:1,
      'auth_key'=>isset($datainput['auth_key'])?$datainput['auth_key']:'-',
    );
		$response = array();
    $id_user = htmlspecialchars($data['id_user']);
    $auth_key = htmlspecialchars($data['auth_key']);
    $id_nup = htmlspecialchars($data['id_nup']);
    $id_unit = htmlspecialchars($data['id_unit']);
    $checkid = ($id_unit!=0)?' AND id_unit='.$id_unit:'';
    $id_perusahaan = ((int)$data['id_perusahaan']>=1)?(int)$data['id_perusahaan']:0;
    $id_perumahan = ((int)$data['id_perumahan']>=1)?(int)$data['id_perumahan']:0;
    $orderby = $data['orderby'];
    //$select = ($data['lengkap'])?'*':'id_nup,status,nama_nup,id_perusahaan';
    $select = '*';
    $tambahan = ($id_nup!=0)?' AND (id_nup='.$id_nup.' OR id_nup IS NULL)':'';
    $tambahan .= (!$data['lengkap'])?' AND id_nup='.$id_nup:'';
    $tambahan .= ' AND id_user='.$id_user.' AND auth_key=\''.$auth_key.'\'';
    $tambahan .= ($id_perusahaan!=0)?' AND id_perusahaan='.$id_perusahaan:'';
    $tambahan .= ($id_perumahan!=0)?' AND id_perumahan='.$id_perumahan:'';
    $limit=($data['alldata'])?'':' OFFSET '.(($data['page']-1)*$data['perpage']).' ROWS FETCH NEXT '.$data['perpage'].' ROWS ONLY';
    $q = $this->db->query('SELECT '.$select.' FROM v_nup_unit WHERE id_user='.$id_user.$checkid.$tambahan.' ORDER BY '.$orderby.$limit);
    //$q = $this->db->query('SELECT '.$select.' FROM t_nup WHERE id_user='.$id_user.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit);
		// if ($data['posisi']=='owner') {
		// 	$q = $this->db->query('SELECT '.$select.' FROM v_nup_owner WHERE id_user='.$id_user.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit);
		// } else {
		// 	$q = $this->db->query('SELECT '.$select.' FROM v_nup_owner WHERE id_user='.$id_user.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit);
		// }
    //$q = $this->db->query('exec sp_data_m_tipe @id_usr ='.$id_user);
		$response = ($id_unit==0)?$q->result():$q->row();
    // $response = 'SELECT '.$select.' FROM v_nup_owner WHERE id_user='.$id_user.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit;
    return $response;
  }

}
