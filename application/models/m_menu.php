<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_menu extends CI_Model {

  function menuicon($id_menu) {
    $arricon =array(
      1 => 'files-o',
      2 => 'money',
      3 => 'money',
      4 => 'money',
      5 => 'money',
      6 => 'money',
      25 => 'check',
      53 =>'th',
    );
    return isset($arricon[$id_menu])?$arricon[$id_menu]:'circle-o';
  }

  function listmenu($datainput){
    $data = array(
      'id_menu'=>isset($datainput['id_menu'])?$datainput['id_menu']:0,
      'posisi'=>isset($datainput['posisi'])?$datainput['posisi']:'user',
    );
		$response = array();
		$id_menu = htmlspecialchars($data['id_menu']);
    $checkid = ($id_menu!=0)?' WHERE id_menu='.$id_menu:'';
		if ($data['posisi']=='owner') {
			$q = $this->db->query('SELECT id_menu,nama_menu,slug,id_parent,urutan FROM m_menu'.$checkid.' ORDER BY id_parent, urutan');
		} else {
			$q = $this->db->query('SELECT id_menu,nama_menu,slug,id_parent,urutan FROM m_menu'.$checkid.' ORDER BY id_parent, urutan');
		}
    //$q = $this->db->query('exec sp_data_m_tipe @id_usr ='.$id_user);
		$response = ($id_menu==0)?$q->result_array():$q->row();
    // $response = 'SELECT '.$select.' FROM v_unit_owner WHERE id_user='.$id_user.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit;
    return $response;
  }

  function getMenuanak($induk,$data,$type="array",$menudipilih='dashboard',$spasi=10){
    $datahasil = array(); $html='';
    foreach ($data as $key => $value) {
      if ($value['id_parent']==$induk) {
        $anak = $this->getMenuanak($value['id_menu'],$data,$type,$menudipilih,$spasi+20);
          if ($anak!=null) {
            $menudashboard = '';
            $active = ($menudipilih==$value['nama_menu'])?' active':'';
            $html.='<tr><td style="padding-left:'.$spasi.'px">
              <label>
                <i class="fa fa-'.$this->menuicon($value['id_menu']).'"></i>
                <span>'.$value['nama_menu'].'</span>
              </label></td>
              <td><input class="inputhakview inputhak  inputhakview'.$value['id_menu'].'" onchange="cekaktifhakakses('.$value['id_menu'].')" type="checkbox" name="view['.$value['id_menu'].']"></td>
              <td></td>
              <td></td>
              <td></td>
              </tr>
              '.$anak;
          } else {
            $html.='<tr><td style="padding-left:'.$spasi.'px"><label><i class="fa fa-'.$this->menuicon($value['id_menu']).'"></i> <span>'.$value['nama_menu'].'</span></label></td>
            <td><input class="inputhakview inputhak inputhakview'.$value['id_menu'].'" onchange="cekaktifhakakses('.$value['id_menu'].')" type="checkbox" name="view['.$value['id_menu'].']"></td>
            <td><input class="inputhaktambah inputhak inputhaktambah'.$value['id_menu'].'" type="checkbox" name="tambah['.$value['id_menu'].']" disabled></td>
            <td><input class="inputhakubah inputhak inputhakubah'.$value['id_menu'].'" type="checkbox" name="ubah['.$value['id_menu'].']" disabled></td>
            <td><input class="inputhakhapus inputhak inputhakhapus'.$value['id_menu'].'" type="checkbox" name="hapus['.$value['id_menu'].']" disabled></td></tr>';
          }
          $datahasil[]=$value;
      }
    }
    return ($type=='array')?$datahasil:$html;
  }
  function getMenu($menudipilih='dashboard'){
    return '<table class="table table-bordered table-striped tabelhakakses" style="margin-top:0px"><tr><th>Menu</th><th><b>Akses</b></th><th>Tambah</th><th>Ubah</th><th>Hapus</th></tr>
    <tr style="color:white;background:black"><td>Semua Akses</td>
    <td><input id="inputhakview" type="checkbox" name="view" onclick="$(\'.inputhakview:enabled\').prop(\'checked\', $(\'#inputhakview\').prop(\'checked\')).trigger(\'change\');"></td>
    <td><input id="inputhaktambah" type="checkbox" name="tambah" onclick="$(\'.inputhaktambah:enabled\').prop(\'checked\', $(\'#inputhaktambah\').prop(\'checked\')).trigger(\'change\');"></td>
    <td><input id="inputhakubah" type="checkbox" name="ubah" onclick="$(\'.inputhakubah:enabled\').prop(\'checked\', $(\'#inputhakubah\').prop(\'checked\')).trigger(\'change\');"></td>
    <td><input id="inputhakhapus" type="checkbox" name="hapus" onclick="$(\'.inputhakhapus:enabled\').prop(\'checked\', $(\'#inputhakhapus\').prop(\'checked\')).trigger(\'change\');"></td></tr>
    '.$this->getMenuanak(NULL,$this->listmenu(['posisi'=>'owner']),'html',$menudipilih).'</table>';
  }


}
