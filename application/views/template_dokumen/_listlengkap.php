<?php
$this->load->model('m_template_dokumen');
$default=isset($default)?$default:'';
$jenis_template=isset($jenis_template)?$jenis_template:'Semua';
if ($default=='') {
$id_user=isset($_SESSION['id_user'])?$_SESSION['id_user']:(isset($id_user)?$id_user:0);
$auth_key=isset($_SESSION['auth_key'])?$_SESSION['auth_key']:(isset($auth_key)?$auth_key:'');
$posisi=isset($_SESSION['posisi'])?$_SESSION['posisi']:(isset($posisi)?$posisi:'user');
$id_perusahaan=isset($_SESSION['id_perusahaan'])?$_SESSION['id_perusahaan']:(isset($id_perusahaan)?$id_perusahaan:0);
$datas = $this->m_template_dokumen->template_dokumenku(['id_user'=>$id_user,'lengkap'=>true,'posisi'=>$posisi,'auth_key'=>$auth_key,'id_perusahaan'=>$id_perusahaan,'jenis_template'=>$jenis_template]);
$model=$this->m_template_dokumen;
} else {
  $files = scandir(BASEPATH.'../assets/template/default/'.$default);
  //$files= glob(BASEPATH.'../assets/template/default/'.$default.'/*.{txt}', GLOB_BRACE);
  foreach ($files as $key => $value) {
    $ext = pathinfo($value, PATHINFO_EXTENSION);
    if ($ext=='txt') {
      $value=str_replace('.txt','',$value);
      $datas[]=array(
        'nama_template'=>ucfirst($value),
        'jenis_template'=>ucfirst($default),
        'id_template_dokumen'=>$value,
        'id_perusahaan'=>'default/'.$default,
      );
    }
  }
  $datas = isset($datas)?$datas:[];
}
$datatextarea='';
foreach ($datas as $keydt => $data) {
  $template = file_get_contents(BASEPATH.'../assets/template/'.$data["id_perusahaan"].'/'.$data["id_template_dokumen"].'.txt');
  echo '<div class="col-lg-4 col-xs-6" id="templatedokumen'.$data["id_template_dokumen"].'" onclick="pilihtemplate_dokumen(\''.$data["id_template_dokumen"].'\')" style="cursor:pointer">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>'.$data["nama_template"].'</h3>

              <p>'.$data["jenis_template"].'</p>
            </div>
            <div class="icon">
              <i class="ion ion-document"></i>
            </div>
          </div>
        </div>';
        $datatextarea .= 'datatemplate'.$jenis_template.'[\''.$data["id_template_dokumen"].'\']='.json_encode($template).';';
}
?><div style="clear:both"></div>
<script type="text/javascript">
if (typeof datatemplate<?=$jenis_template?> == "undefined") { datatemplate<?=$jenis_template?>=new Array();}
<?=$datatextarea?>
</script>
