<?php
$this->load->model('m_cluster');
$id_user=isset($_SESSION['id_user'])?$_SESSION['id_user']:(isset($id_user)?$id_user:0);
$auth_key=isset($_SESSION['auth_key'])?$_SESSION['auth_key']:(isset($auth_key)?$auth_key:'');
$posisi=isset($_SESSION['posisi'])?$_SESSION['posisi']:(isset($posisi)?$posisi:'user');
$id_perusahaan=isset($_SESSION['id_perusahaan'])?$_SESSION['id_perusahaan']:(isset($id_perusahaan)?$id_perusahaan:0);
$datas = $this->m_cluster->clusterku(['id_user'=>$id_user,'lengkap'=>true,'posisi'=>$posisi,'auth_key'=>$auth_key,'id_perusahaan'=>$id_perusahaan]);
$model=$this->m_cluster;
 ?>
<table id="tabellistcluster" class="table table-bordered table-striped">
  <thead>
  <tr>
    <?php
    foreach ($model->kolom() as $key => $value) {
      if ($key!='id_cluster'){
        echo '<th>'.$value['label'].'</th>';
      }
    }
    ?>
  </tr>
  </thead>
  <tbody>
    <?php
    foreach ($datas as $keydt => $data) {
      echo '<tr class="id_cluster'.$data->id_cluster.'" onclick="pilihcluster(\''.$data->id_cluster.'\',\''.$data->id_perumahan.'\')">';
      foreach ($model->kolom() as $keyk => $kolom) {
        if ($keyk!='id_cluster'){
          $data->$keyk=($keyk=='id_perumahan')?$data->nama_perumahan:$data->$keyk;
          $data->$keyk=($keyk=='aktif')?(($data->$keyk==1)?'Aktif':'Tidak Aktif'):$data->$keyk;
          echo '<td>'.$data->$keyk.'</td>';
        }
      }
      echo '</tr>';
    }
    ?>
  </tbody>
  <tfoot>
  <tr>
    <?php
    foreach ($model->kolom() as $key => $value) {
      if ($key!='id_cluster'){
        echo '<th>'.$value['label'].'</th>';
      }
    }
    ?>
  </tr>
  </tfoot>
</table>
