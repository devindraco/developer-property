<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Check_session
{
        public function __construct()
        {

        }

        public function __get($property)
        {
                if ( ! property_exists(get_instance(), $property))
                {
                        show_error('property: ' . strong($property) . ' not exist.');
                }
                return get_instance()->$property;
        }
        public function validate()
        {
               if (in_array($this->router->class, array("IndexController","admin5427"))&&in_array($this->router->method, array("login")))//login is a sample login controller
               // if (in_array($this->router->class, array("IndexController"))&&(in_array($this->router->method, array("login"))||in_array($this->router->method, array("landingpage"))))
                {
                  if(!$this->session->has_userdata('id_user')||!$this->session->has_userdata('nama')||!$this->session->has_userdata('auth_key')) return; else redirect('', 'refresh');
                }
                if (in_array($this->router->class, array("ajax"))) {
                  return;
                }
               if(!$this->session->has_userdata('id_user')||!$this->session->has_userdata('nama')||!$this->session->has_userdata('id_perusahaan'))
               {
                 // if (in_array($this->router->class, array("IndexController"))&&(in_array($this->router->method, array("login"))||in_array($this->router->method, array("landingpage"))))
                 //    return;
                 // else redirect('login', 'landingpage');
                 redirect('login', 'refresh');
               } else if($this->session->id_perusahaan==0&&!(in_array($this->router->class, array("perusahaan")))) {
                 redirect('perusahaan/listperusahaan', 'refresh');
               } else {
                 $this->keamanan->usermenu = $this->m_user_menu->getMenudata();
                 //print_r($_SESSION); echo '---';
                 //if (!$this->keamanan->usermenu) session_destroy();
                 if (!$this->keamanan->checkrule(uri_string())) redirect('notallowed', 'refresh');

               }
        }
}
