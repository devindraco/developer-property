var socket = require( 'socket.io' );
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = socket.listen( server );
var port = process.env.PORT || 3000;
var pilihanunit = [];
server.listen(port, function () {
  console.log('Server listening at port %d', port);
});
io.on('connection', function (socket) {
  socket.on( 'new_message', function( data ) {
    if (data.message=='bookingunit') {
      if (typeof data.id_unit != 'undefined' && typeof data.iddevice != 'undefined' && typeof data.nokupon != 'undefined') {
        if (typeof pilihanunit[data.nokupon] != 'undefined')
          data.message='kuponused';
        else if (pilihanunit.includes(data.id_unit))
          data.message='booked';
        else
          pilihanunit[data.nokupon]=data.id_unit;
      } else {
        data.iddevice=0; data.id_unit=0; data.nokupon=0;
      }
    }
    io.sockets.emit( 'new_message', {
      iddevice: data.iddevice,
      id_unit: data.id_unit,
      nokupon: data.nokupon,
      message: data.message
    });
  });
});
