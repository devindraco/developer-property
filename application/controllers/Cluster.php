<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cluster extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct() {
		 parent::__construct();
	 }
	 public function index($page=1)
 	{
		$this->load->model('m_cluster');
		$this->load->model('m_perumahan');
		$aktif=isset($_GET['aktif'])?$_GET['aktif']:2;
		$perpage=isset($_GET['perpage'])?$_GET['perpage']:25;
		$page=(int)$page;
		$this->setlayouts('utama','cluster/utama',['model'=>$this->m_cluster,'menudipilih'=>'Master Data']);
 	}
	public function tambah()
 {
	 	$this->load->model('m_cluster');
		$this->setlayouts('utama','cluster/_form',['action'=>'tambah','perusahaanku'=>$this->m_perusahaan->perusahaanku(0,false,1),'menudipilih'=>'Master Data']);
 }
 public function edit($id=-1)
{
	$this->load->model('m_cluster');
	if ($datacluster=$this->m_cluster->clusterku($id,true)) {
		$_POST=(array)$datacluster;
		$this->setlayouts('utama','cluster/_form',['action'=>'edit','perusahaanku'=>$this->m_perusahaan->perusahaanku(0,false,1),'menudipilih'=>'Master Data']);
	}
}
	public function listcluster()
	{
			$this->setlayouts('listcluster','utama',['clusterku'=>$this->m_cluster->clusterku(0,false,1),'menudipilih'=>'dashboard']);
	}
	public function ganticluster($id=1)
	{
			if ($this->m_cluster->clusterku($id)!=null) {
				$_SESSION['id_cluster'] = $id;
				redirect('', 'refresh');
			}
	}
}
