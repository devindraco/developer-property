<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct() {
		 parent::__construct();
	 }
	public function pengeluaran($page=1)
 {
	 $this->load->model('t_pengeluaran');
	 $this->load->model('m_cluster');
	$this->load->model('m_perumahan');
	$this->load->model('t_kwitansi');
	 $aktif=isset($_GET['aktif'])?$_GET['aktif']:2;
	 $perpage=isset($_GET['perpage'])?$_GET['perpage']:25;
	 $page=(int)$page;
	 $this->setlayouts('utama','pengeluaran/utama',['model'=>$this->t_pengeluaran,'id_cluster'=>0,'menudipilih'=>'Transaksi']);
 }
 public function kwitansi($page=1)
{
	$this->load->model('t_kwitansi');
	$aktif=isset($_GET['aktif'])?$_GET['aktif']:2;
	$perpage=isset($_GET['perpage'])?$_GET['perpage']:25;
	$page=(int)$page;
	$this->setlayouts('utama','kwitansi/utama',['model'=>$this->t_kwitansi,'menudipilih'=>'Transaksi']);
}
	public function rumah()
 {
	 $this->load->model('t_transaksi');
	 $this->load->model('m_marketing');
	 $this->load->model('m_unit');
	 $this->load->model('m_customer');
	 $this->load->model('m_template_dokumen');
	 $aktif=isset($_GET['aktif'])?$_GET['aktif']:2;
	 $page=isset($_GET['page'])?$_GET['page']:1;
	 $perpage=isset($_GET['perpage'])?$_GET['perpage']:25;
	 $id_unit=isset($_GET['id_unit'])?$_GET['id_unit']:0;
	 $page=(int)$page;
	 $this->setlayouts('utama','transaksi/rumah',['model'=>$this->t_transaksi,'id_unit'=>$id_unit,'menudipilih'=>'Transaksi']);
 }

 public function rumah_banyak()
 {
	$this->load->model('t_transaksi');
	$this->load->model('m_marketing');
	$this->load->model('m_unit');
	$this->load->model('m_customer');
	$this->load->model('m_template_dokumen');
	$id_unit=isset($_GET['id_unit'])?$_GET['id_unit']:0;
	$this->setlayouts('utama','transaksi/rumah_banyak',['model'=>$this->t_transaksi,'id_unit'=>$id_unit,'menudipilih'=>'Transaksi']);
 }

 public function ppjb($page=1)
 {
	$this->load->model('t_transaksi');
	$this->load->model('m_marketing');
	$this->load->model('m_unit');
	$this->load->model('m_customer');
	$this->load->model('m_template_dokumen');
	$aktif=isset($_GET['aktif'])?$_GET['aktif']:2;
	$perpage=isset($_GET['perpage'])?$_GET['perpage']:25;
	$page=(int)$page;
	$this->setlayouts('utama','transaksi/ppjb',['model'=>$this->t_transaksi,'menudipilih'=>'Transaksi']);
 }
	public function listtransaksi()
	{
			$this->setlayouts('listtransaksi','utama',['transaksiku'=>$this->t_transaksi->transaksiku(0,false,1),'menudipilih'=>'dashboard']);
	}
}
