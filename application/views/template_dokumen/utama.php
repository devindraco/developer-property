<?php $perusahaanku=$this->m_perusahaan->perusahaanku();
//$marketingku=$this->m_marketing->marketingku(['id_userutama'=>$_SESSION['id_user'],'lengkap'=>true,'posisiutama'=>$_SESSION['posisi'],'auth_keyutama'=>$_SESSION['auth_key']]);
//$unitku=$this->m_unit->unitku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
//$customerku=$this->m_customer->customerku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
?>
<script type="text/javascript">
var textareasemua=null;
var datatemplateSemua=[];
</script>
<div id="boxindex" class="box">
  <div class="box-header">
    <h3 class="box-title">List Template Dokumen</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <?=($this->keamanan->allowedaction['tambah'])?'<button class="btn btn-primary" onclick="bukaformtambah()">Tambah</button>':''; ?>
    <button onclick="$('#filter').toggle(300);" class="btn btn-primary pull-right"><i class="fa fa-gear"></i></button>
    <div id="filter" class="row" style="display:none">
    <h4 class="col-xs-12">Filer</h4>
    <div class="form-group col-xs-3">
      <label>Jenis</label>
      <select id="jenis_template" name="jenis_template" class="form-control filterinput">
        <option value="Semua" selected>Semua</option>
        <?php foreach ($model->listjenis_template() as $key => $status) {
          echo '<option value="'.$status['jenis_template'].'">'.$status['label'].'</option>';
        } ?>
      </select>
    </div>
    <div class="form-group col-xs-6 col-md-3">
      <label>Jumlah</label>
      <select id="perpage" name="perpage" class="form-control filterinput">
        <option value="25" selected>25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="300">100</option>
      </select>
    </div>
    <div class="form-group col-xs-6 col-md-3">
      <label>Urutkan</label>
      <select id="orderby" name="orderby" class="form-control filterinput">
        <option value="id_template_dokumen DESC" selected>Terbaru</option>
        <option value="id_template_dokumen ASC">Terlama</option>
      </select>
    </div>

    <div class="form-group col-xs-6 col-md-3">
      <label>Nama Template</label>
      <input type="text" class="form-control filterinput" name="nama_template" id="nama_template">
    </div>

    <div class="form-group col-xs-12">
      <button id="filterbutton" onclick="fitersubmit();" class="btn btn-primary pull-right"> Terapkan </button>
    </div>
  </div>
  <div class="box-table">
    <table id="tabeltemplate" class="tabeltemplate table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_template_dokumen') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      <?php if ($this->keamanan->allowedaction['ubah']) { ?>
      <tr style="display:none"><td><input type="hidden" name="id_userutama" class="databerubah" value="<?=$_SESSION['id_user']?>"></td><td><input type="hidden" name="auth_keyutama" class="databerubah" value="<?=$_SESSION['auth_key']?>">
      <input type="hidden" name="_csrf" class="databerubah" value="<?=$this->keamanan->generatecsrf()?>"></td></tr>
    <?php } ?>
      </thead>
      <tbody>
        <tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>
      </tbody>
      <tfoot>
      <tr>
        <th>No.</th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_template_dokumen') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      </tfoot>
    </table>
  </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-left">
    </ul>

    <ul class="pagination pagination-sm no-margin pull-right">
      <li><button id="btnprev" class="btn btn-default" onclick="changepage(false);">« Prev</button></li>
      <li><button id="btnnext" class="btn btn-default" onclick="changepage();">Next »</button></li>
    </ul>
  </div>
</div>
<!-- /.box -->

<div id="boxdetail" class="box" style="display:none">
  <div class="box-header">
    <h3 class="box-title">Detail Template</h3><button type="button" class="close pull-right" onclick="bukalistdata()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="form-group col-xs-6 formnama_template">
      <label>Nama Template</label>
      <div class="input-group"><div class="input-group-addon">
          <i class="fa fa-book"></i>
        </div>
        <input id="inputnama_template" type="text" class="form-control datatambah" name="nama_template" value="" required="" onkeydown="inputenter('tambah',event)">
      </div>
    </div>
    <div class="form-group col-xs-6 formnama_template">
      <label>Jenis Template</label>
      <div class="input-group"><div class="input-group-addon">
          <i class="fa fa-book"></i>
        </div>
        <select id="inputjenis_template" name="jenis_template" class="form-control filterinput">
          <?php foreach ($model->listjenis_template() as $key => $status) {
            echo '<option value="'.$status['jenis_template'].'">'.$status['label'].'</option>';
          } ?>
        </select>
      </div>
    </div>

    <div class="form-group col-xs-12 formtemplate">
      <label>Isi Template</label>
      <div class="input-group" style="width: 100%;">
        <textarea id="textareasemua" rows="10" placeholder="Tulis Template Anda di Sini" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
      </div>
    </div>
    <div style="clear:both"></div>
  </div>
  <div class="box-footer">
    <button type="button" class="btn btn-primary pull-right" onclick="simpantemplatedokumen()">Simpan</button>
  </div>

</div>

<div class="modal fade" id="modalpilihdefaulttemplate">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah Marketing</h4>
      </div>
      <div class="modal-body" style="padding:0">
        <div class="nav-tabs-custom" style="margin:0">
        <?php echo '<ul class="nav nav-tabs">';
        foreach ($model->listjenis_template() as $key => $jenis) {
          $active=($key==0)?' class="active"':'';
          echo '<li data-jenis="'.$jenis["jenis_template"].'"'.$active.'><a href="#jenisdefault'.$jenis["jenis_template"].'" data-toggle="tab">'.$jenis["label"].'</a></li>';
        }
        echo '</ul><div class="tab-content">';
        foreach ($model->listjenis_template() as $key => $jenis) {
          $active=($key==0)?' active':'';
          echo '<div class="tab-pane'.$active.'" id="jenisdefault'.$jenis["jenis_template"].'">';
          $this->load->view('template_dokumen/_listlengkap',['default'=>$jenis["jenis_template"]]);
          echo '</div>';
        }
        echo '</div>';
        ?>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script type="text/javascript">
var perpage = 25;
var page = 1;
var id_template_dokumendipilih = 0;
<?php /* var listperusahaanku = <?=json_encode($perusahaanku)?>;
var listunitku = <?=json_encode($unitku)?>;
var listcustomerku = <?=json_encode($customerku)?>; */?>
var listkolom = <?=json_encode($model->kolom())?>;
var hargajual = 0;

function dataajax() {
  perpage=$("#perpage").val();
  $( "#tabeltemplate>tbody" ).html('<tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>');
  var request = $.ajax({
    url: "<?=base_url()?>ajax/datatemplatedokumen",
    method: "POST",
    data: {
      'jenis_template': $('#jenis_template').val(),
      'nama_template': $('#nama_template').val(),
      'lengkap': 1,
      'page': page,
      'perpage': perpage,
      'posisi': '<?=$_SESSION['posisi']?>',
      'orderby': $("#orderby").val(),
      '_csrf': '<?=$this->keamanan->generatecsrf()?>',
      'id_user': <?=$_SESSION['id_user']?>,
      'auth_key': '<?=$_SESSION['auth_key']?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    var hasil = '';
    console.log(datahasil);
    $.each(datahasil, function( index, template ) {
      hasil +='<tr class="baristemplate'+template["id_template_dokumen"]+'" onclick="dataajaxtemplate('+template["id_template_dokumen"]+')"><?php
      $hapus='';
      echo '<td>\'+(((page-1)*perpage)+index+1)+\'</td>';
      foreach ($model->kolom() as $key => $value) {
        if ($key!='id_template_dokumen') {
          echo '<td>\'+template["'.$key.'"]+\'</td>';
        }
      }
      ?></tr>';
    });
    $( "#tabeltemplate>tbody" ).html(hasil);
    if (datahasil.length<perpage) $('#btnnext').attr("disabled", true); else $('#btnnext').attr("disabled", false);
    if (page<=1) $('#btnprev').attr("disabled", true); else $('#btnprev').attr("disabled", false);
    ubahdata(true);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
function dataajaxtemplate(id_template_dokumen) {
  var request = $.ajax({
    url: "<?=base_url()?>ajax/datatemplatedokumen",
    method: "POST",
    data: {
      'id_template_dokumen' : id_template_dokumen,
      'jenis_template': $('#jenis_template').val(),
      'lengkap': 1,
      'posisi': '<?=$_SESSION['posisi']?>',
      '_csrf': '<?=$this->keamanan->generatecsrf()?>',
      'id_user': <?=$_SESSION['id_user']?>,
      'auth_key': '<?=$_SESSION['auth_key']?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    var hasil = '';
    console.log(datahasil);
    id_template_dokumendipilih=id_template_dokumen;
    $('#inputnama_template').val($('.baristemplate'+id_template_dokumendipilih+' td:nth-child(2)').html());
    $('#inputjenis_template').val($('.baristemplate'+id_template_dokumendipilih+' td:nth-child(3)').html());
    $('iframe').contents().find('.wysihtml5-editor').html(datahasil);
    $('#boxdetail').show();$('#boxindex').hide();
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
function bukalistdata() {
  id_template_dokumendipilih=0;
  $('#inputnama_template').val('');
  $('#inputjenis_template').val('semua');
  $('iframe').contents().find('.wysihtml5-editor').html('');
  $('#boxindex').show();
  $('#boxdetail').hide();
  dataajax();
}
function bukaformtambah() {
  id_template_dokumendipilih=0;
  $('#inputnama_template').val('');
  $('#inputjenis_template').val('semua');
  $('iframe').contents().find('.wysihtml5-editor').html('');
  $('#modalpilihdefaulttemplate').modal();
}
function pilihtemplate_dokumen(template) {
  $('#inputnama_template').val(template);
  $('#inputjenis_template').val($('#modalpilihdefaulttemplate .nav.nav-tabs>li.active').attr('data-jenis'));
  $('iframe').contents().find('.wysihtml5-editor').html(datatemplateSemua[template]);
  $('#modalpilihdefaulttemplate').modal('hide');
  $('#boxindex').hide();
  $('#boxdetail').show();
}
function simpantemplatedokumen() {
  var request = $.ajax({
    url: "<?=base_url()?>ajax/simpantemplatedokumen",
    method: "POST",
    data: {
      'id_template_dokumen': id_template_dokumendipilih,
      'jenis_template': $('#inputjenis_template').val(),
      'nama_template': $('#inputnama_template').val(),
      'template' : $('iframe').contents().find('.wysihtml5-editor').html(),
      'posisi': '<?=$_SESSION['posisi']?>',
      'id_user': <?=$_SESSION['id_user']?>,
      'auth_key': '<?=$_SESSION['auth_key']?>',
      '_csrf': '<?=$this->keamanan->generatecsrf()?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "html"
  });

  request.done(function( datahasil ) {
    console.log(datahasil);
    $('iframe').contents().find('.wysihtml5-editor').html('');
    $('#boxdetail').hide();$('#boxindex').show();dataajax();
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
window.addEventListener('DOMContentLoaded', (event) => {
var templatealignment = {
      textAlign: function(context) {
        return "<li><div class='btn-group'>" +
            "<a class='btn btn-default' data-wysihtml5-command='justifyLeft' data-wysihtml5-command-value='&justifyLeft;' title= 'Align text left'>" +
            "<span class='glyphicon glyphicon-align-left'></span></a>" +
            "<a class='btn btn-default' data-wysihtml5-command='justifyCenter' data-wysihtml5-command-value='&justifyCenter;' title= 'Align text center'>" +
            "<span class='glyphicon glyphicon-align-center'></span></a>" +
            "<a class='btn btn-default' data-wysihtml5-command='justifyRight' data-wysihtml5-command-value='&justifyRight;' title= 'Align text right'>" +
            "<span class='glyphicon glyphicon-align-right'></span></a>" +
            "<a class='btn btn-default' data-wysihtml5-command='JustifyFull' data-wysihtml5-command-value='&JustifyFull;' title= 'Align text Justify'>" +
            "<span class='glyphicon glyphicon-align-justify'></span></a>" +
            "</li>";
      },
      pilihShortcode: function(context) {
        return "<li>" +
        "<button class='btn btn-primary' onclick=\"$('#modalpilihShortcode').modal();\" >Shortcode</button>" +
        "<div class=\"modal fade\" id=\"modalpilihShortcode\">" +
          "<div class=\"modal-dialog modal-md\">" +
            "<div class=\"modal-content\">" +
              "<div class=\"modal-header\">" +
                  "<a class=\"close\" data-dismiss=\"modal\">&times;</a>" +
                "<h3>Tambah Shortcode</h3>" +
              "</div>" +
              "<div class=\"modal-body\">" +
              "<?php $listshortcode = $this->t_transaksi->listkolomdokumen();
              //data-wysihtml5-command='insertHTML' data-wysihtml5-command-value='&hellip;'
              foreach ($listshortcode as $key => $value) {
                if (substr($key, 0, 3)=='id_') {
                  if ($key!='id_pesanan') echo '</div>';
                  $label = str_replace('id_','',$key);
                  echo "<div class='col-xs-4'><h4>".ucfirst($label)."</h4>";
                  if ($key=='id_pesanan') {
                      echo "<a data-wysihtml5-command='insertHTML' class='btn btn-link' data-wysihtml5-command-value='&#123;&#123;list_cicilan&#125;&#125;' data-dismiss='modal'>list_cicilan</a>";
                  }
                } else {
                  echo "<a data-wysihtml5-command='insertHTML' class='btn btn-link' data-wysihtml5-command-value='&#123;&#123;".$key."&#125;&#125;' data-dismiss='modal'>".$value."</a>";
                }
              }
              ?></div><div style=\"clear:both\"></div>"+
              "</div>" +
              "<div class=\"modal-footer\">" +
                "<button type=\"button\" class=\"btn btn-default pull-left\" data-dismiss=\"modal\">Tutup</button>" +
              "</div>" +
            "</div>" +
          "</div>" +
        "</div>" +
        "</li>";
      }
    };
$('#textareasemua').wysihtml5({
  classes: {
    "wysiwyg-text-align-center": 1,
    "wysiwyg-text-align-justify": 1,
    "wysiwyg-text-align-left": 1,
    "wysiwyg-text-align-right": 1
  },
  "toolbar": {
    "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
    "emphasis": true, //Italics, bold, etc. Default true
    "lists": true, //(Un)ordered lists, e.g. Bullet"s, Numbers. Default true
    "html": false, //Button which allows you to edit the generated HTML. Default false
    "link": true, //Button to insert a link. Default true
    "image": true, //Button to insert an image. Default true,
    "color": true, //Button to change color of font
    "blockquote": false,
    'textAlign': true, // custom defined buttons to align text see myCustomTemplates variable above
    'pilihShortcode': true
  },
  stylesheets: ["<?=base_url()?>assets/css/main.css"],
  customTemplates: templatealignment
});
});
</script>
