<?php $clusterku=$this->m_cluster->clusterku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key'],'dropdown'=>true]);

?>
<div class="box">
  <div class="box-header">
    <h3 class="box-title">List tipe</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <?=($this->keamanan->allowedaction['tambah'])?'<button class="btn btn-primary" onclick="$(\'#modal-tambah\').modal();$(\'.modal input.form-control\').first().focus()">Tambah</button>':''; ?>
    <button onclick="$('#filter').toggle(300);" class="btn btn-primary pull-right"><i class="fa fa-gear"></i></button>
    <div id="filter" class="row" style="display:none">
    <h4 class="col-xs-12">Filer</h4>
    <div class="form-group col-xs-3">
      <label>Status</label>
      <select id="aktif" name="aktif" class="form-control filterinput">
        <option value="2" selected>Semua</option>
        <option value="1">Aktif</option>
        <option value="0">Tidak Aktif</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Tampilkan</label>
      <select id="perpage" name="perpage" class="form-control filterinput">
        <option value="25" selected>25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="300">100</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Urutkan</label>
      <select id="orderby" name="orderby" class="form-control filterinput">
        <option value="id_tipe DESC" selected>Terbaru</option>
        <option value="id_tipe ASC">Terlama</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Nama tipe</label>
      <input type="text" class="form-control filterinput" name="nama_tipe" id="nama_tipe">
    </div>
    <div class="form-group col-xs-12">
      <button id="filterbutton" onclick="$('#filter').toggle(300);fitersubmit();" class="btn btn-primary pull-right"> Terapkan </button>
    </div>
  </div>
  <div class="box-table">
    <table id="tabeltipe" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <th></th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_tipe') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      <?php if ($this->keamanan->allowedaction['ubah']) { ?>
      <tr style="display:none"><td><input type="hidden" name="id_user" class="databerubah" value="<?=$_SESSION['id_user']?>"></td><td><input type="hidden" name="auth_key" class="databerubah" value="<?=$_SESSION['auth_key']?>">
      <input type="hidden" name="_csrf" class="databerubah" value="<?=$this->keamanan->generatecsrf()?>"></td></tr>
    <?php } ?>
      </thead>
      <tbody>
        <tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>
      </tbody>
      <tfoot>
      <tr>
        <th>No.</th>
        <th></th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_tipe') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      </tfoot>
    </table>
  </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-left">
      <li><?=($this->keamanan->allowedaction['ubah'])?'<button id="btnubah" class="btn btn-success" onclick="ubahdata(false,true);">Ubah</button>':''; ?></li>
    </ul>

    <ul class="pagination pagination-sm no-margin pull-right">
      <li><button id="btnprev" class="btn btn-default" onclick="changepage(false);">« Prev</button></li>
      <li><button id="btnnext" class="btn btn-default" onclick="changepage();">Next »</button></li>
    </ul>
  </div>
</div>
<!-- /.box -->
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
<div class="modal fade" id="modal-tambah">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah tipe</h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('tipe/_form', ['clusterku'=>$clusterku]); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button id="btntambah" type="button" class="btn btn-primary" onclick="tambahdataajax()">Simpan</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>
<script type="text/javascript">
var perpage = 25;
var page = 1;
var listclusterku = <?=json_encode($clusterku)?>;
var liststatus = [{'aktif':1,'id':'Aktif'},{'aktif':0,'id':'Tidak Aktif'}];

function dataajax() {
  perpage=$("#perpage").val();
  $( "#tabeltipe>tbody" ).html('<tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>');
  var request = $.ajax({
    url: "<?=base_url()?>ajax/datatipe",
    method: "POST",
    data: {
      'aktif': $('#aktif').val(),
      'kota': $('#kota').val(),
      'lengkap': 1,
      'page': page,
      'perpage': perpage,
      'posisi': '<?=$_SESSION['posisi']?>',
      'orderby': $("#orderby").val(),
      'id_user': <?=$_SESSION['id_user']?>,
      'auth_key': '<?=$_SESSION['auth_key']?>'
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    var hasil = '';
    console.log(datahasil);
    $.each(datahasil, function( index, tipe ) {
      var status = (tipe["aktif"]==1)?'Aktif':'Tidak Aktif';
      hasil +='<tr><?php
      $hapus='';
      if ($this->keamanan->allowedaction['hapus']) {
          $hapus='<button class="btn btn-link" tabindex="-1" onclick="hapusdataajax(\'+tipe["id_tipe"]+\')"><i class="fa fa-trash"></i></button> ';
      }
      echo '<td>\'+formdiindex(\'hidden\',tipe["id_tipe"],\'id_tipe\',(((page-1)*perpage)+index+1))+\'</td>';
      echo '<td style="min-width:0;padding:0">'.$hapus.'</td>';
      echo '<td>\'+formdiindex(\'text\',tipe["id_tipe"],\'nama_tipe\',tipe["nama_tipe"])+\'</td>';
      echo '<td>\'+formdiindex(\'select\',tipe["id_tipe"],\'id_cluster\',tipe["id_cluster"],listclusterku,\'nama_cluster\',\''.base_url().'cluster\')+\'</td>';
      echo '<td>\'+formdiindex(\'number\',tipe["id_tipe"],\'harga\',tipe["harga"])+\'</td>';
      echo '<td>\'+formdiindex(\'number\',tipe["id_tipe"],\'lebar\',tipe["lebar"])+\'</td>';
      echo '<td>\'+formdiindex(\'number\',tipe["id_tipe"],\'panjang\',tipe["panjang"])+\'</td>';
      echo '<td>\'+formdiindex(\'number\',tipe["id_tipe"],\'luas_bangunan\',tipe["luas_bangunan"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',tipe["id_tipe"],\'catatan\',tipe["catatan"])+\'</td>';
      echo '<td>\'+formdiindex(\'select\',tipe["id_tipe"],\'aktif\',tipe["aktif"],liststatus,\'id\')+\'</td>';
    /*  echo '<td>';
      if ($this->keamanan->allowedaction['ubah']) {
        echo '<a tabindex="-1" href="'.base_url().'tipe/edit/\'+tipe["id_tipe"]+\'"><i class="fa fa-pencil"></i></a> ';
      }
      if ($this->keamanan->allowedaction['hapus']) {
          echo '<a tabindex="-1" href="'.base_url().'tipe/hapus/\'+tipe["id_tipe"]+\'"><i class="fa fa-trash"></i></a> ';
      }
      echo '</td>'; */
      ?></tr>';
    });
    $( "#tabeltipe>tbody" ).html(hasil);
    if (datahasil.length<perpage) $('#btnnext').attr("disabled", true); else $('#btnnext').attr("disabled", false);
    if (page<=1) $('#btnprev').attr("disabled", true); else $('#btnprev').attr("disabled", false);
    ubahdata(true);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
<?php if ($this->keamanan->allowedaction['ubah']) { ?>
  function updatedataajax() {
    var request = $.ajax({
      url: "<?=base_url()?>ajax/updatedatatipe",
      method: "POST",
      data: $("#tabeltipe .databerubah").serialize(),
      dataType: "json"
    });

    request.done(function( datahasil ) {
      console.log(datahasil);
      dataajax();
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      dataajax();
    });
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
  function tambahdataajax() {
    $('#modal-tambah').modal('toggle');;
    var request = $.ajax({
      url: "<?=base_url()?>ajax/tambahdatatipe",
      method: "POST",
      data: $("#modal-tambah .datatambah").serialize(),
      dataType: "json"
    });

    request.done(function( datahasil ) {
      console.log(datahasil);
      dataajax();
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      dataajax();
    });
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['hapus']) { ?>
  function hapusdataajax(id_tipe) {
    if (confirm('Apakah Anda yakin ingin menghapus data Tipe ini ?')) {
      var request = $.ajax({
        url: "<?=base_url()?>ajax/hapusdatatipe",
        method: "POST",
        data: {
          'id_user': <?=$_SESSION['id_user']?>,
          'auth_key': '<?=$_SESSION['auth_key']?>',
          '_csrf': '<?=$this->keamanan->generatecsrf()?>',
          'id_tipe': id_tipe
        },
        dataType: "json"
      });

      request.done(function( datahasil ) {
        console.log(datahasil);
        dataajax();
      });

      request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
        dataajax();
      });
    }
}
<?php } ?>
</script>
