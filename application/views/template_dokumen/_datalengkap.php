<?php
$this->load->model('m_customer');
$id_customer=isset($id_customer)?$id_customer:0;
$id_user=isset($_SESSION['id_user'])?$_SESSION['id_user']:(isset($id_user)?$id_user:0);
$auth_key=isset($_SESSION['auth_key'])?$_SESSION['auth_key']:(isset($auth_key)?$auth_key:'');
$posisi=isset($_SESSION['posisi'])?$_SESSION['posisi']:(isset($posisi)?$posisi:'user');
$id_perusahaan=isset($_SESSION['id_perusahaan'])?$_SESSION['id_perusahaan']:(isset($id_perusahaan)?$id_perusahaan:0);
$data = $this->m_customer->customerku(['id_user'=>$id_user,'lengkap'=>true,'posisi'=>$posisi,'auth_key'=>$auth_key,'id_perusahaan'=>$id_perusahaan,'id_customer'=>$id_customer]);
$model=$this->m_customer;
if ($data) {
  echo '<h3 style="margin-top:0">'.$data->nama_customer.'</h3>
<p>Alamat : '.$data->alamat.'</p>
<p>Telepon : <b>'.(($data->telp==0)?'-':'<a href="call:'.$data->telp.'" target="_blank">'.$data->telp.'</a>').'</b></p>
<p>HP : <b>'.(($data->hp1==0)?'-':'<a href="call:'.$data->hp1.'" target="_blank">'.$data->hp1.'</a>').''.(($data->hp2==0)?'':', <a href="call:'.$data->hp2.'" target="_blank">'.$data->hp2.'</a>').'</b></p>
<p>Identitas : <b>'.$data->jenis_identitas.' - '.$data->no_identitas.'</b></p>
<p>Email : <b>'.(($data->email=='')?'-':'<a href="mailto:'.$data->email.'" target="_blank">'.$data->email.'</a>').'</b></p>
<p>NPWP : <b>'.$data->npwp.'</b></p>';
}
 ?>
