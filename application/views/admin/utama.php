<?php $_GET['lengkap'] = true;
$useradmin = $model->useradmin($_GET);
$this->keamanan->allowedaction = array('tambah'=>true,'ubah'=>true,'hapus'=>true);
 ?>
<div class="box">
  <div class="box-header">
    <h3 class="box-title">List User</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <?=($this->keamanan->allowedaction['tambah'])?'<button class="btn btn-primary" onclick="$(\'#modal-tambah\').modal();$(\'.modal input.form-control\').first().focus()">Tambah</button>':''; ?>
    <button onclick="$('#filter').toggle(300);" class="btn btn-primary pull-right"><i class="fa fa-gear"></i></button>
    <form id="filter" class="row" method="get" action="" style="display:none">
    <h4 class="col-xs-12">Filer</h4>
    <div class="form-group col-xs-3">
      <label>Status</label>
      <select id="aktif" name="aktif" class="form-control filterinput">
        <option value="2" selected>Semua</option>
        <option value="1">Aktif</option>
        <option value="0">Tidak Aktif</option>
      </select>
    </div>
    <input type="hidden" class="form-control filterinput" name="page" id="page" value="<?=isset($_GET['page'])?$_GET['page']:1?>">
    <div class="form-group col-xs-3">
      <label>Tampilkan</label>
      <select id="perpage" name="perpage" class="form-control filterinput">
        <option value="25" selected>25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="300">100</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Urutkan</label>
      <select id="orderby" name="orderby" class="form-control filterinput">
        <option value="id_user DESC" selected>Terbaru</option>
        <option value="id_user ASC">Terlama</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Nama user</label>
      <input type="text" class="form-control filterinput" name="nama_user" id="nama_user">
    </div>
    <div class="form-group col-xs-12">
      <button id="filterbutton" type="submit" class="btn btn-primary pull-right"> Terapkan </button>
    </div>
  </form>
  <div class="box-table">
    <table id="tabeluser" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <th></th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_user'&&$key!='password') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      <?php if ($this->keamanan->allowedaction['ubah']) { ?>
      <tr style="display:none"><td><input type="hidden" name="id_userutama" class="databerubah" value="<?=$_SESSION['id_user']?>"></td><td><input type="hidden" name="auth_keyutama" class="databerubah" value="<?=$_SESSION['auth_key']?>">
      <input type="hidden" name="_csrf" class="databerubah" value="<?=$this->keamanan->generatecsrf()?>"></td></tr>
    <?php } ?>
      </thead>
      <tbody>
        <tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>
      </tbody>
      <tfoot>
      <tr>
        <th>No.</th>
        <th></th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_user'&&$key!='password') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      </tfoot>
    </table>
  </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-left">
      <li><?=($this->keamanan->allowedaction['ubah'])?'<button id="btnubah" class="btn btn-success" onclick="ubahdata(false,true);">Ubah</button>':''; ?></li>
    </ul>

    <ul class="pagination pagination-sm no-margin pull-right">
      <li><button id="btnprev" class="btn btn-default" onclick="changepage(false);">« Prev</button></li>
      <li><button id="btnnext" class="btn btn-default" onclick="changepage();">Next »</button></li>
    </ul>
  </div>
</div>
<!-- /.box -->
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
<div class="modal fade" id="modal-tambah">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah user</h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('admin/_form', ['perusahaanku'=>$perusahaanku]); ?>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>
<script type="text/javascript">
var perpage=25;
var page = 1;
var liststatus = [{'aktif':1,'id':'Aktif'},{'aktif':0,'id':'Tidak Aktif'}];
var listuser = <?=json_encode($useradmin)?>

function dataajax() {
  perpage=$("#perpage").val();
  page = $("#page").val();
    var hasil='';
    $.each(listuser, function( index, user ) {
      var status = (user["aktif"]==1)?'Aktif':'Tidak Aktif';
      hasil +='<tr><?php
      $hapus='';
      if ($this->keamanan->allowedaction['hapus']) {
          $hapus='<button class="btn btn-link" tabindex="-1" onclick="hapusdataajax(\'+user["id_user"]+\')"><i class="fa fa-trash"></i></button> ';
      }
      echo '<td>\'+formdiindex(\'hidden\',user["id_user"],\'id_user\',(((page-1)*perpage)+index+1))+\'</td>';
      echo '<td style="min-width:0;padding:0">'.$hapus.'</td>';
      echo '<td><p>\'+user["username"]+\'</p></td>';
      //echo '<td>\'+formdiindex(\'text\',user["id_user"],\'username\',user["username"])+\'</td>';
      echo '<td style="display:none">\'+formdiindex(\'hiddenval\',user["id_user"],\'password\',user["password_hash"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',user["id_user"],\'nama_depan\',user["nama_depan"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',user["id_user"],\'nama_belakang\',user["nama_belakang"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',user["id_user"],\'jabatan\',user["jabatan"])+\'</td>';
      echo '<td>\'+formdiindex(\'select\',user["id_user"],\'aktif\',user["aktif"],liststatus,\'id\')+\'</td>';
    /*  echo '<td>';
      if ($this->keamanan->allowedaction['ubah']) {
        echo '<a tabindex="-1" href="'.base_url().'user/edit/\'+user["id_user"]+\'"><i class="fa fa-pencil"></i></a> ';
      }
      if ($this->keamanan->allowedaction['hapus']) {
          echo '<a tabindex="-1" href="'.base_url().'user/hapus/\'+user["id_user"]+\'"><i class="fa fa-trash"></i></a> ';
      }
      echo '</td>'; */
      ?></tr>';
    });
    $( "#tabeluser>tbody" ).html(hasil);
    if (listuser.length<perpage) $('#btnnext').attr("disabled", true); else $('#btnnext').attr("disabled", false);
    if (page<=1) $('#btnprev').attr("disabled", true); else $('#btnprev').attr("disabled", false);
    ubahdata(true);

}
<?php if ($this->keamanan->allowedaction['ubah']) { ?>
  function updatedataajax() {
    var request = $.ajax({
      url: "<?=base_url()?>ajax/updatedatauser",
      method: "POST",
      data: $("#tabeluser .databerubah").serialize(),
      dataType: "json"
    });

    request.done(function( datahasil ) {
      console.log(datahasil);
      dataajax();
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      dataajax();
    });
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
  function tambahdataajax() {
    if ($('#modal-tambah #tabeldataperusahaan input.inputperusahaan:checked').length>0) {
    $('#modal-tambah').modal('toggle');;
    var request = $.ajax({
      url: "<?=base_url()?>ajax/tambahdatauser",
      method: "POST",
      data: $("#modal-tambah .datatambah, #modal-tambah .inputperusahaan").serialize(),
      dataType: "json"
    });

    request.done(function( datahasil ) {
      console.log(datahasil);
      dataajax();
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      dataajax();
    });
  } else {
    alert('Mohon Pilih Paling Tidak 1 Perusahaan');
  }
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['hapus']) { ?>
  function hapusdataajax(id_user) {
    if (confirm('Apakah Anda yakin ingin menghapus data user ini ?')) {
      var request = $.ajax({
        url: "<?=base_url()?>ajax/hapusdatauser",
        method: "POST",
        data: {
          'id_user': <?=$_SESSION['id_user']?>,
          'auth_key': '<?=$_SESSION['auth_key']?>',
          '_csrf': '<?=$this->keamanan->generatecsrf()?>',
          'id_user': id_user
        },
        dataType: "json"
      });

      request.done(function( datahasil ) {
        console.log(datahasil);
        dataajax();
      });

      request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
        dataajax();
      });
    }
}
<?php } ?>
</script>
