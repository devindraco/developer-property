<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$error=isset($error)?$error:'';
$perusahaanku=isset($perusahaanku)?$perusahaanku:$this->m_perusahaan->perusahaanku(0,false,1);
$namaperusahaan=isset($_SESSION['nama_perusahaan'])?$_SESSION['nama_perusahaan']:'PT INDRACO';
$title=isset($title)?$title:$namaperusahaan;
$menu = $this->m_user_menu->getMenu($menudipilih);
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?=$title?></title>

    <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/bower_components/Ionicons/css/ionicons.min.css">
	<!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/dist/css/skins/_all-skins.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/jquery.fancybox.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?=base_url()?>assets/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/main.css">
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background-color:#222d32">
<header class="main-header">
    <!-- Logo -->
    <a href="/sid" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><?=$title?></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><?=$title?></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-building"></i> Perusahaan
            </a>
            <ul class="dropdown-menu">
              <li class="header">List Perusahaan</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
				<?php
				foreach($perusahaanku as $key => $perusahaan) {
					echo '<li>
                    <a href="'.base_url().'perusahaan/gantiperusahaan/'.$perusahaan->id_perusahaan.'">
                      <div class="pull-left">
                        <i class="fa fa-building"></i>
                      </div>
                      <h4>
                        '.$perusahaan->nama_perusahaan.'
                      </h4>
                    </a>
                  </li>';
				} ?>
                </ul>
              </li>
              <li class="footer"><a href="<?=base_url()?>perusahaan/listperusahaan">Lihat Semua Perusahaan</a></li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-user"></i>
              <span class="hidden-xs"><?=$_SESSION['nama']?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- Menu Body -->
              <li class="user-body">
			  <p><?=ucwords($_SESSION['posisi'])?></p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?=base_url()?>profil" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?=base_url()?>logout" class="btn btn-default btn-flat">Logout</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
		<div class="pull-left image">
          <i class="fa fa-user fa-big" style="color:white"></i>
        </div>
        <div class="pull-left info">
          <p><?= $_SESSION['nama']; ?></p>
        </div>
      </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
				<?=$menu?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?=ucwords($menudipilih)?></h1>
    </section>

    <!-- Main content -->
    <section class="content">
		<?= $contents ?>
	</section>
	<p class="footer" style="padding:5px 15px;margin:0">&copy; <?=date('Y')?> PT INDRACO All Right Reserved.</p>
</div>

<!-- jQuery 3 -->
<script src="<?=base_url()?>assets/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?=base_url()?>assets/adminlte/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?=base_url()?>assets/adminlte/bower_components/raphael/raphael.min.js"></script>
<?=($menudipilih=='dashboard')?'<script src="'.base_url().'assets/adminlte/bower_components/morris.js/morris.min.js"></script>':'';?>
<!-- Sparkline -->
<script src="<?=base_url()?>assets/adminlte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?=base_url()?>assets/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?=base_url()?>assets/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?=base_url()?>assets/adminlte/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?=base_url()?>assets/adminlte/bower_components/moment/min/moment.min.js"></script>
<script src="<?=base_url()?>assets/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?=base_url()?>assets/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?=base_url()?>assets/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?=base_url()?>assets/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- DataTables -->
<script src="<?=base_url()?>assets/adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?=base_url()?>assets/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/adminlte/dist/js/adminlte.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.fancybox.min.js"></script>
<?=($this->keamanan->allowedaction['ubah']||$this->keamanan->allowedaction['tambah'])?'<script src="'.base_url().'assets/js/form.js"></script>':'';
?><script src="<?=base_url()?>assets/js/main.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url()?>assets/adminlte/dist/js/demo.js"></script>
</body>
</html>
