<?php $perusahaanku=$this->m_perusahaan->perusahaanku(0,false,1);?>
<div class="box">
  <div class="box-header">
    <h3 class="box-title">List perumahan</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <?=($this->keamanan->allowedaction['tambah'])?'<button class="btn btn-primary" onclick="$(\'#modal-tambah\').modal();$(\'.modal input.form-control\').first().focus()">Tambah</button>':''; ?>
    <button onclick="$('#filter').toggle(300);" class="btn btn-primary pull-right"><i class="fa fa-gear"></i></button>
    <div id="filter" class="row" style="display:none">
    <h4 class="col-xs-12">Filer</h4>
    <div class="form-group col-xs-3">
      <label>Status</label>
      <select id="aktif" name="aktif" class="form-control filterinput">
        <option value="2" selected>Semua</option>
        <option value="1">Aktif</option>
        <option value="0">Tidak Aktif</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Tampilkan</label>
      <select id="perpage" name="perpage" class="form-control filterinput">
        <option value="25" selected>25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="300">100</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Urutkan</label>
      <select id="orderby" name="orderby" class="form-control filterinput">
        <option value="id_perumahan DESC" selected>Terbaru</option>
        <option value="id_perumahan ASC">Terlama</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Kota</label>
      <input type="text" class="form-control filterinput" name="kota" id="kota">
    </div>
    <div class="form-group col-xs-12">
      <button id="filterbutton" onclick="$('#filter').toggle(300);fitersubmit();" class="btn btn-primary pull-right"> Terapkan </button>
    </div>
  </div>
  <div class="box-table">
    <table id="tabelperumahan" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <th></th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_perumahan') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      <?php if ($this->keamanan->allowedaction['ubah']) { ?>
      <tr style="display:none"><td><input type="hidden" name="id_user" class="databerubah" value="<?=$_SESSION['id_user']?>"></td><td><input type="hidden" name="auth_key" class="databerubah" value="<?=$_SESSION['auth_key']?>">
      <input type="hidden" name="_csrf" class="databerubah" value="<?=$this->keamanan->generatecsrf()?>"></td></tr>
    <?php } ?>
      </thead>
      <tbody>
        <tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>
      </tbody>
      <tfoot>
      <tr>
        <th>No.</th>
        <th></th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_perumahan') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      </tfoot>
    </table>
  </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-left">
      <li><?=($this->keamanan->allowedaction['ubah'])?'<button id="btnubah" class="btn btn-success" onclick="ubahdata(false,true);">Ubah</button>':''; ?></li>
    </ul>

    <ul class="pagination pagination-sm no-margin pull-right">
      <li><button id="btnprev" class="btn btn-default" onclick="changepage(false);">« Prev</button></li>
      <li><button id="btnnext" class="btn btn-default" onclick="changepage();">Next »</button></li>
    </ul>
  </div>
</div>
<!-- /.box -->
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
<div class="modal fade" id="modal-tambah">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah Perumahan</h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('perumahan/_form', ['perusahaanku'=>$perusahaanku]); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button id="btntambah" type="button" class="btn btn-primary" onclick="tambahdataajax()">Simpan</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>
<script type="text/javascript">
var perpage = 25;
var page = 1;
var listperusahaanku = <?=json_encode($perusahaanku)?>;
var liststatus = [{'aktif':1,'id':'Aktif'},{'aktif':0,'id':'Tidak Aktif'}];

function dataajax() {
  perpage=$("#perpage").val();
  $( "#tabelperumahan>tbody" ).html('<tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>');
  var request = $.ajax({
    url: "<?=base_url()?>ajax/dataperumahan",
    method: "POST",
    data: {
      'aktif': $('#aktif').val(),
      'kota': $('#kota').val(),
      'lengkap': 1,
      'page': page,
      'perpage': perpage,
      'posisi': '<?=$_SESSION['posisi']?>',
      'orderby': $("#orderby").val(),
      'id_user': <?=$_SESSION['id_user']?>,
      'auth_key': '<?=$_SESSION['auth_key']?>'
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    var hasil = '';
    console.log(datahasil);
    $.each(datahasil, function( index, perumahan ) {
      var status = (perumahan["aktif"]==1)?'Aktif':'Tidak Aktif';
      hasil +='<tr><?php
      $hapus='';
      if ($this->keamanan->allowedaction['hapus']) {
          $hapus='<button class="btn btn-link" tabindex="-1" onclick="hapusdataajax(\'+perumahan["id_perumahan"]+\')"><i class="fa fa-trash"></i></button> ';
      }
      echo '<td>\'+formdiindex(\'hidden\',perumahan["id_perumahan"],\'id_perumahan\',(((page-1)*perpage)+index+1))+\'</td>';
      echo '<td style="min-width:0;padding:0">'.$hapus.'</td>';
      echo '<td>\'+formdiindex(\'text\',perumahan["id_perumahan"],\'nama_perumahan\',perumahan["nama_perumahan"])+\'</td>';
      echo '<td>\'+formdiindex(\'select\',perumahan["id_perumahan"],\'id_perusahaan\',perumahan["id_perusahaan"],listperusahaanku,\'nama_perusahaan\',\''.base_url().'perusahaan/edit/\'+perumahan["id_perusahaan"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',perumahan["id_perumahan"],\'alamat\',perumahan["alamat"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',perumahan["id_perumahan"],\'kota\',perumahan["kota"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',perumahan["id_perumahan"],\'propinsi\',perumahan["propinsi"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',perumahan["id_perumahan"],\'telp\',perumahan["telp"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',perumahan["id_perumahan"],\'website\',perumahan["website"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',perumahan["id_perumahan"],\'bank\',perumahan["bank"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',perumahan["id_perumahan"],\'max_cicilan_cash\',perumahan["max_cicilan_cash"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',perumahan["id_perumahan"],\'max_cicilan_kpr\',perumahan["max_cicilan_kpr"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',perumahan["id_perumahan"],\'max_cicilan_inhouse\',perumahan["max_cicilan_inhouse"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',perumahan["id_perumahan"],\'max_persen_dp\',perumahan["max_persen_dp"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',perumahan["id_perumahan"],\'uang_tanda_jadi\',perumahan["uang_tanda_jadi"])+\'</td>';
      echo '<td>\'+formdiindex(\'select\',perumahan["id_perumahan"],\'aktif\',perumahan["aktif"],liststatus,\'id\')+\'</td>';
    /*  echo '<td>';
      if ($this->keamanan->allowedaction['ubah']) {
        echo '<a tabindex="-1" href="'.base_url().'perumahan/edit/\'+perumahan["id_perumahan"]+\'"><i class="fa fa-pencil"></i></a> ';
      }
      if ($this->keamanan->allowedaction['hapus']) {
          echo '<a tabindex="-1" href="'.base_url().'perumahan/hapus/\'+perumahan["id_perumahan"]+\'"><i class="fa fa-trash"></i></a> ';
      }
      echo '</td>'; */
      ?></tr>';
    });
    $( "#tabelperumahan>tbody" ).html(hasil);
    if (datahasil.length<perpage) $('#btnnext').attr("disabled", true); else $('#btnnext').attr("disabled", false);
    if (page<=1) $('#btnprev').attr("disabled", true); else $('#btnprev').attr("disabled", false);
    ubahdata(true);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
<?php if ($this->keamanan->allowedaction['ubah']) { ?>
  function updatedataajax() {
    var request = $.ajax({
      url: "<?=base_url()?>ajax/updatedataperumahan",
      method: "POST",
      data: $("#tabelperumahan .databerubah").serialize(),
      dataType: "json"
    });

    request.done(function( datahasil ) {
      dataajax();
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      dataajax();
    });
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
  function tambahdataajax() {
    $('#modal-tambah').modal('toggle');
    var request = $.ajax({
      url: "<?=base_url()?>ajax/tambahdataperumahan",
      method: "POST",
      data: $("#modal-tambah .datatambah").serialize(),
      dataType: "json"
    });

    request.done(function( datahasil ) {
      dataajax();
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      dataajax();
    });
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['hapus']) { ?>
  function hapusdataajax(id_perumahan) {
    if (confirm('Apakah Anda yakin ingin menghapus data perumahan ini ?')) {
      var request = $.ajax({
        url: "<?=base_url()?>ajax/hapusdataperumahan",
        method: "POST",
        data: {
          'id_user': <?=$_SESSION['id_user']?>,
          'auth_key': '<?=$_SESSION['auth_key']?>',
          '_csrf': '<?=$this->keamanan->generatecsrf()?>',
          'id_perumahan': id_perumahan
        },
        dataType: "json"
      });

      request.done(function( datahasil ) {
        console.log(datahasil);
        dataajax();
      });

      request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
        dataajax();
      });
    }
}
<?php } ?>
</script>
