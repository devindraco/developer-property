<?php
$this->load->model('m_perumahan');
$id_user=isset($_SESSION['id_user'])?$_SESSION['id_user']:(isset($id_user)?$id_user:0);
$auth_key=isset($_SESSION['auth_key'])?$_SESSION['auth_key']:(isset($auth_key)?$auth_key:'');
$posisi=isset($_SESSION['posisi'])?$_SESSION['posisi']:(isset($posisi)?$posisi:'user');
$id_perusahaan=isset($_SESSION['id_perusahaan'])?$_SESSION['id_perusahaan']:(isset($id_perusahaan)?$id_perusahaan:0);
$datas = $this->m_perumahan->perumahanku(['id_user'=>$id_user,'lengkap'=>true,'posisi'=>$posisi,'auth_key'=>$auth_key,'id_perusahaan'=>$id_perusahaan,'status'=>'r']);
$model=$this->m_perumahan;
 ?>
<table id="tabellistperumahan" class="table table-bordered table-striped">
  <thead>
  <tr>
    <?php
    foreach ($model->kolom() as $key => $value) {
        echo '<th>'.$value['label'].'</th>';
    }
    ?>
  </tr>
  </thead>
  <tbody>
    <?php
    foreach ($datas as $keydt => $data) {
      echo '<tr class="id_perumahan'.$data->id_perumahan.'" onclick="pilihperumahan(\''.$data->id_perumahan.'\',\''.$data->nama_perumahan.'\')">';
      foreach ($model->kolom() as $keyk => $kolom) {
          echo '<td>'.$data->$keyk.'</td>';
      }
      echo '</tr>';
    }
    ?>
  </tbody>
  <tfoot>
  <tr>
    <?php
    foreach ($model->kolom() as $key => $value) {
        echo '<th>'.$value['label'].'</th>';
    }
    ?>
  </tr>
  </tfoot>
</table>
