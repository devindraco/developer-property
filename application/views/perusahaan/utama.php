<div class="box">
  <div class="box-header">
    <h3 class="box-title">List Perusahaan</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <?=($this->keamanan->allowedaction['tambah'])?'<a href="'.base_url().'perusahaan/tambah" class="btn btn-primary">Tambah</a>':''; ?>
    <table id="tabelperusahaan" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <th>Nama Perusahaan</th>
        <th>Kota</th>
        <th>Telp</th>
        <th>Status</th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>
        <?php foreach($perusahaanku as $key => $perusahaan) {
          $status=($perusahaan->aktif)?'Aktif':'Tidak Aktif';
          $action=($this->keamanan->allowedaction['ubah'])?'<a href="'.base_url().'perusahaan/edit/'.$perusahaan->id_perusahaan.'"><i class="fa fa-pencil"></i></a> ':'';
          $action.=($this->keamanan->allowedaction['hapus'])?'<button class="btn btn-link" tabindex="-1" onclick="hapusdataajax(\''.$perusahaan->id_perusahaan.'\')"><i class="fa fa-trash"></i></button>':'';
          echo '<tr><td>'.($key+1).'</td><td>'.$perusahaan->nama_perusahaan.'</td><td>'.$perusahaan->kota.'</td><td>'.$perusahaan->telp.'</td><td>'.$status.'</td>
          <td>'.$action.'</td></tr>';
        } ?>
      </tbody>
      <tfoot>
      <tr>
        <th>No.</th>
        <th>Nama Perusahaan</th>
        <th>Kota</th>
        <th>Telp</th>
        <th>Status</th>
        <th>Action</th>
      </tr>
      </tfoot>
    </table>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
<script type="text/javascript">
<?php if ($this->keamanan->allowedaction['hapus']) { ?>
  function hapusdataajax(id_perusahaan) {
    if (confirm('Apakah Anda yakin ingin menghapus data perusahaan ini ?')) {
      var request = $.ajax({
        url: "<?=base_url()?>ajax/hapusdataperusahaan",
        method: "POST",
        data: {
          'id_user': <?=$_SESSION['id_user']?>,
          'auth_key': '<?=$_SESSION['auth_key']?>',
          '_csrf': '<?=$this->keamanan->generatecsrf()?>',
          'id_perusahaan': id_perusahaan
        },
        dataType: "json"
      });

      request.done(function( datahasil ) {
        if (id_perusahaan==<?=$_SESSION['id_perusahaan']?>) {
          $(location).attr('href','<?=base_url()?>perusahaan/gantiperusahaan/0');
        } else {
          $(location).attr('href','<?=base_url()?>perusahaan');
        }
      });

      request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
      });
    }
}
<?php } ?>
</script>
