<div class="box">
  <div class="box-header">
    <h3 class="box-title"><?=ucfirst($action)?> Perusahaan</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <form action="" method="post" enctype="application/x-www-form-urlencoded" class="row">
      <input type="hidden" name="_csrf" value="<?=$this->keamanan->generatecsrf()?>">
      <input type="hidden" name="id_perusahaan" value="<?=(isset($_POST['id_perusahaan'])?$_POST['id_perusahaan']:'')?>">
      <div class="form-group col-xs-12">
        <label>Nama Perusahaan</label>
        <div class="input-group">
          <div class="input-group-addon">
            <i class="fa fa-building"></i>
          </div>
          <input type="text" class="form-control" name="nama_perusahaan" value="<?=(isset($_POST['nama_perusahaan'])?$_POST['nama_perusahaan']:'')?>" required>
        </div>
        <!-- /.input group -->
      </div>
      <!-- /.form group -->

      <div class="form-group col-xs-12">
        <label>Alamat</label>
        <div class="input-group">
          <div class="input-group-addon">
            <i class="fa fa-map"></i>
          </div>
          <textarea rows="5" class="form-control" name="alamat" required><?=(isset($_POST['alamat'])?$_POST['alamat']:'')?></textarea>
        </div>
        <!-- /.input group -->
      </div>
      <!-- /.form group -->

      <div class="form-group col-md-6 col-xs-12">
        <label>Kota</label>
        <div class="input-group">
          <div class="input-group-addon">
            <i class="fa fa-map"></i>
          </div>
          <input type="text" class="form-control" name="kota" value="<?=(isset($_POST['kota'])?$_POST['kota']:'')?>" required>
        </div>
        <!-- /.input group -->
      </div>
      <!-- /.form group -->


      <div class="form-group col-md-6 col-xs-12">
        <label>Propinsi</label>
        <div class="input-group">
          <div class="input-group-addon">
            <i class="fa fa-map"></i>
          </div>
          <input type="text" class="form-control" name="propinsi" value="<?=(isset($_POST['propinsi'])?$_POST['propinsi']:'')?>" required>
        </div>
        <!-- /.input group -->
      </div>
      <!-- /.form group -->


      <div class="form-group col-md-4 col-xs-12">
        <label>No. Telp</label>
        <div class="input-group">
          <div class="input-group-addon">
            <i class="fa fa-phone"></i>
          </div>
          <input type="text" class="form-control" name="telp" value="<?=(isset($_POST['telp'])?$_POST['telp']:'')?>" required>
        </div>
        <!-- /.input group -->
      </div>
      <!-- /.form group -->

      <div class="form-group col-md-4 col-xs-12">
        <label>Website</label>
        <div class="input-group">
          <div class="input-group-addon">
            <i class="fa fa-globe"></i>
          </div>
          <input type="text" class="form-control" name="website" value="<?=(isset($_POST['website'])?$_POST['website']:'')?>" required>
        </div>
        <!-- /.input group -->
      </div>
      <!-- /.form group -->


      <div class="form-group col-md-4 col-xs-12">
        <label>NPWP</label>
        <div class="input-group">
          <div class="input-group-addon">
            <i class="fa fa-credit-card"></i>
          </div>
          <input type="text" class="form-control" name="npwp" value="<?=(isset($_POST['npwp'])?$_POST['npwp']:'')?>" required>
        </div>
        <!-- /.input group -->
      </div>
      <!-- /.form group -->

      <div class="form-group col-xs-12">
        <div class="input-group">
          <label>
          <input type="checkbox" class="minimal" name="aktif" <?=(isset($_POST['aktif'])?($_POST['aktif']?'checked':''):'checked')?>> Aktif
        </label>
          <button type="submit" class="btn btn-block btn-primary">Submit</button>
        </div>
    </div>

    </form>

  </div>
  <!-- /.box-body -->

</div>
<!-- /.box -->
