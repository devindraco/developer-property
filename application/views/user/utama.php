<?php $perusahaanku=$this->m_perusahaan->perusahaanku();
?><div class="box">
  <div class="box-header">
    <h3 class="box-title">List User</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <?=($this->keamanan->allowedaction['tambah'])?'<button class="btn btn-primary" onclick="bukaformtambah()">Tambah</button>':''; ?>
    <button onclick="$('#filter').toggle(300);" class="btn btn-primary pull-right"><i class="fa fa-gear"></i></button>
    <div id="filter" class="row" style="display:none">
    <h4 class="col-xs-12">Filer</h4>
    <div class="form-group col-xs-3">
      <label>Status</label>
      <select id="aktif" name="aktif" class="form-control filterinput">
        <option value="2" selected>Semua</option>
        <option value="1">Aktif</option>
        <option value="0">Tidak Aktif</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Tampilkan</label>
      <select id="perpage" name="perpage" class="form-control filterinput">
        <option value="25" selected>25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="300">100</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Urutkan</label>
      <select id="orderby" name="orderby" class="form-control filterinput">
        <option value="id_user DESC" selected>Terbaru</option>
        <option value="id_user ASC">Terlama</option>
      </select>
    </div>
    <div class="form-group col-xs-3">
      <label>Nama user</label>
      <input type="text" class="form-control filterinput" name="nama_user" id="nama_user">
    </div>
    <div class="form-group col-xs-12">
      <button id="filterbutton" onclick="$('#filter').toggle(300);fitersubmit();" class="btn btn-primary pull-right"> Terapkan </button>
    </div>
  </div>
  <div class="box-table">
    <table id="tabeluser" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <th></th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_user'&&$key!='password') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      <?php if ($this->keamanan->allowedaction['ubah']) { ?>
      <tr style="display:none"><td><input type="hidden" name="id_userutama" class="databerubah" value="<?=$_SESSION['id_user']?>"></td><td><input type="hidden" name="auth_keyutama" class="databerubah" value="<?=$_SESSION['auth_key']?>">
      <input type="hidden" name="_csrf" class="databerubah" value="<?=$this->keamanan->generatecsrf()?>"></td></tr>
    <?php } ?>
      </thead>
      <tbody>
        <tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>
      </tbody>
      <tfoot>
      <tr>
        <th>No.</th>
        <th></th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_user'&&$key!='password') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      </tfoot>
    </table>
  </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-left">
      <li><?=($this->keamanan->allowedaction['ubah'])?'<button id="btnubah" class="btn btn-success" onclick="ubahdata(false,true);">Ubah</button>':''; ?></li>
    </ul>

    <ul class="pagination pagination-sm no-margin pull-right">
      <li><button id="btnprev" class="btn btn-default" onclick="changepage(false);">« Prev</button></li>
      <li><button id="btnnext" class="btn btn-default" onclick="changepage();">Next »</button></li>
    </ul>
  </div>
</div>
<!-- /.box -->
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
<div class="modal fade" id="modal-tambah">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah user</h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('user/_form', ['perusahaanku'=>$perusahaanku]); ?>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>
<script type="text/javascript">
var perpage = 25;
var page = 1;
var liststatus = [{'aktif':1,'id':'Aktif'},{'aktif':0,'id':'Tidak Aktif'}];

function dataajax() {
  perpage=$("#perpage").val();
  $( "#tabeluser>tbody" ).html('<tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>');
  var request = $.ajax({
    url: "<?=base_url()?>ajax/datauser",
    method: "POST",
    data: {
      'aktif': $('#aktif').val(),
      'lengkap': 1,
      'page': page,
      'perpage': perpage,
      'posisi': '<?=$_SESSION['posisi']?>',
      'orderby': $("#orderby").val(),
      'id_userutama': <?=$_SESSION['id_user']?>,
      '_csrf': '<?=$this->keamanan->generatecsrf()?>',
      'auth_keyutama': '<?=$_SESSION['auth_key']?>'
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    var hasil = '';
    console.log(datahasil);
    $('#datahakakses .userlist').html('<option selected value="0">None</option>');
    $.each(datahasil, function( index, user ) {
      var status = (user["aktif"]==1)?'Aktif':'Tidak Aktif';
      hasil +='<tr><?php
      $aksi='';
      if ($this->keamanan->allowedaction['ubah']) {
          $aksi.='<button class="btn btn-link" tabindex="-1" onclick="bukaformubah(\'+user["id_user"]+\')"><i class="fa fa-pencil"> Ubah</i></button> ';
      }
      if ($this->keamanan->allowedaction['hapus']) {
          $aksi.='<button class="btn btn-link" tabindex="-1" onclick="hapusdataajax(\'+user["id_user"]+\')"><i class="fa fa-trash"> Hapus</i></button> ';
      }
      echo '<td>\'+formdiindex(\'hidden\',user["id_user"],\'id_user\',(((page-1)*perpage)+index+1))+\'</td>';
      echo '<td style="min-width:0;padding:0">'.$aksi.'</td>';
      echo '<td><p>\'+user["username"]+\'</p></td>';
      //echo '<td>\'+formdiindex(\'text\',user["id_user"],\'username\',user["username"])+\'</td>';
      echo '<td style="display:none">\'+formdiindex(\'hiddenval\',user["id_user"],\'password\',user["password_hash"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',user["id_user"],\'nama_depan\',user["nama_depan"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',user["id_user"],\'nama_belakang\',user["nama_belakang"])+\'</td>';
      echo '<td>\'+formdiindex(\'text\',user["id_user"],\'jabatan\',user["jabatan"])+\'</td>';
      echo '<td>\'+formdiindex(\'select\',user["id_user"],\'aktif\',user["aktif"],liststatus,\'id\')+\'</td>';
    /*  echo '<td>';
      if ($this->keamanan->allowedaction['ubah']) {
        echo '<a tabindex="-1" href="'.base_url().'user/edit/\'+user["id_user"]+\'"><i class="fa fa-pencil"></i></a> ';
      }
      if ($this->keamanan->allowedaction['hapus']) {
          echo '<a tabindex="-1" href="'.base_url().'user/hapus/\'+user["id_user"]+\'"><i class="fa fa-trash"></i></a> ';
      }
      echo '</td>'; */
      ?></tr>';
      $('#datahakakses .userlist').append('<option value="'+user["id_user"]+'">'+user["nama_depan"]+' ('+user["jabatan"]+')</option>');
    });
    $( "#tabeluser>tbody" ).html(hasil);
    if (datahasil.length<perpage) $('#btnnext').attr("disabled", true); else $('#btnnext').attr("disabled", false);
    if (page<=1) $('#btnprev').attr("disabled", true); else $('#btnprev').attr("disabled", false);
    ubahdata(true);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
function cekaktifhakakses(id) {
  if (!$('.inputhakview'+id).prop('checked')) {
    $('.inputhaktambah'+id).prop('checked',false);
    $('.inputhakubah'+id).prop('checked',false);
    $('.inputhakhapus'+id).prop('checked',false);
    $('.inputhaktambah'+id).prop('disabled',true);
    $('.inputhakubah'+id).prop('disabled',true);
    $('.inputhakhapus'+id).prop('disabled',true);
  } else {
    $('.inputhaktambah'+id).prop('disabled',false);
    $('.inputhakubah'+id).prop('disabled',false);
    $('.inputhakhapus'+id).prop('disabled',false);
  }
}
function bukaformtambah () {
  $('#modal-tambah .modal-title').html('Tambah User');
  $('#btntabmarketing').click();
  sethakaksesuser(0);
  $('#modal-tambah').modal();$('.modal input.form-control').first().focus();
}
function bukaformubah(id_user) {
  sethakaksesuser(id_user);
  $('#modal-tambah .modal-title').html('Ubah User');
  $('#btntabmarketing').click();
  $('#modal-tambah').modal();$('.modal input.form-control').first().focus();
}

function sethakaksesuser(id_user) {
  $('.tabelhakakses input[type="checkbox"]').prop('checked',false).trigger('change');
  if (id_user!=0) {
    var request = $.ajax({
      url: "<?=base_url()?>ajax/datalistmenu",
      method: "POST",
      data: {
        'id_user': id_user,
        'lengkap': 1,
        'posisi': '<?=$_SESSION['posisi']?>',
        'id_userutama': <?=$_SESSION['id_user']?>,
        '_csrf': '<?=$this->keamanan->generatecsrf()?>',
        'auth_keyutama': '<?=$_SESSION['auth_key']?>'
      },
      dataType: "json"
    });

    request.done(function( datahasil ) {
      var hasil = '';
      console.log(datahasil);
      $.each(datahasil, function( index, menu ) {
        $('.inputhakview'+menu['id_menu']).prop('checked',true).trigger('change');
        $('.inputhaktambah'+menu['id_menu']).prop('checked',(menu['act_tambah']==1));
        $('.inputhakubah'+menu['id_menu']).prop('checked',(menu['act_ubah']==1));
        $('.inputhakhapus'+menu['id_menu']).prop('checked',(menu['act_hapus']==1));
      });
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
    });
  }
}
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
  function tambahdataajax() {
    if ($('#modal-tambah #tabeldataperusahaan input.inputperusahaan:checked').length>0) {
    $('#modal-tambah').modal('toggle');;
    var request = $.ajax({
      url: "<?=base_url()?>ajax/tambahdatauser",
      method: "POST",
      data: $("#modal-tambah .datatambah, #modal-tambah .inputperusahaan, #modal-tambah .inputhak").serialize(),
      dataType: "json"
    });

    request.done(function( datahasil ) {
      console.log(datahasil);
      dataajax();
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      dataajax();
    });
  } else {
    alert('Mohon Pilih Paling Tidak 1 Perusahaan');
  }
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['ubah']) { ?>
  function ubahdataajax() {
    if ($('#modal-tambah #tabeldataperusahaan input.inputperusahaan:checked').length>0) {
    $('#modal-tambah').modal('toggle');;
    var request = $.ajax({
      url: "<?=base_url()?>ajax/tambahdatauser",
      method: "POST",
      data: $("#modal-tambah .datatambah, #modal-tambah .inputperusahaan, #modal-tambah .inputhak").serialize(),
      dataType: "json"
    });

    request.done(function( datahasil ) {
      console.log(datahasil);
      dataajax();
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      dataajax();
    });
  } else {
    alert('Mohon Pilih Paling Tidak 1 Perusahaan');
  }
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['hapus']) { ?>
  function hapusdataajax(id_user) {
    if (confirm('Apakah Anda yakin ingin menghapus data user ini ?')) {
      var request = $.ajax({
        url: "<?=base_url()?>ajax/hapusdatauser",
        method: "POST",
        data: {
          'id_userutama': <?=$_SESSION['id_user']?>,
          'auth_keyutama': '<?=$_SESSION['auth_key']?>',
          '_csrf': '<?=$this->keamanan->generatecsrf()?>',
          'id_user': id_user
        },
        dataType: "json"
      });

      request.done(function( datahasil ) {
        console.log(datahasil);
        dataajax();
      });

      request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
        dataajax();
      });
    }
}
<?php } ?>
</script>
