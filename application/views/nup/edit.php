<?php $id_nup=isset($id_nup)?$id_nup:0;
$id_perumahan=isset($id_perumahan)?$id_perumahan:0;?>
<script type="text/javascript">
var coordunitdipilih= '';
var idunitdipilih= 0;
window.addEventListener('DOMContentLoaded', (event) => {
  $(document).keyup(function(e) {
    if (e.key === "Escape") {
       var str = $('#koordinatunit').val();
       var koor = str.substr(0,str.lastIndexOf(','));
       $('#koordinatunit').val(koor.substr(0,koor.lastIndexOf(','))).trigger('change');
    }
  });
  $('#tabellistunit').DataTable({"info": false,"order": [] });
});
function gantikoordinatunit(posisix,posisiy) {
  if (idunitdipilih!=0) {
    var koor = ($('#koordinatunit').val()=='')?'':$('#koordinatunit').val()+',';
    $('#koordinatunit').val(koor+''+(Math.round(posisix * 100) / 100)+','+(Math.round(posisiy * 100) / 100)).trigger('change');
  }
}
function pilihunit(id_unit,teksunit,coord) {
  idunitdipilih=id_unit;
  coordunitdipilih=coord;
  $('.id_unit').html(teksunit);
  $('#koordinatunit').val(coord).trigger('change');
}
function koordinatberubah() {
  if (idunitdipilih!=0) {
    myLeave();
    clearPoly(coordunitdipilih);
    drawPoly($('#koordinatunit').val(),'green');
  }
}
function simpandatanupkoordinat(){
  var request = $.ajax({
    url: "<?=base_url()?>ajax/simpandatanupkoordinat",
    method: "POST",
    data: {
      'id_user':<?=$_SESSION['id_user']?>,
      'auth_key':'<?=$_SESSION['auth_key']?>',
      '_csrf':'<?=$this->keamanan->generatecsrf()?>',
      'id_nup':<?=$id_nup?>,
      'id_unit':idunitdipilih,
      'coords':$('#koordinatunit').val(),
      'status':1
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    location.reload();
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}

function simpandatanuppeta() {
  var formData = new FormData(document.getElementById('formimagepeta'));
  var request = $.ajax({
    url: "<?=base_url()?>ajax/simpandatanuppeta",
    method: "POST",
    data: formData,
    processData: false,
    contentType: false,
    dataType: "json"
  });

  request.done(function( datahasil ) {
    if (datahasil!='') {
        location.reload();
    }
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
</script>
<div class="col-xs-3">
  <div class="row" style="overflow:auto">
    Unit Dipilih : <span class="id_unit"></span>
    <input type="text" class="form-control" name="koordinatunit" id="koordinatunit" onchange="koordinatberubah()" value="" placeholder="Koordinat Unit" >
    <button type="button" onclick="simpandatanupkoordinat()" class="btn btn-primary">Simpan</button>
  </div>
  <div id="unitnup" class="row" style="overflow:auto;max-height:500px">
    <?php $this->load->view('nup/_listunit', ['id_nup'=>$id_nup,'id_perumahan'=>$id_perumahan]); ?>
  </div>
</div>
<div class="col-xs-9">
<?php $this->load->view('nup/tampilanpeta',['edit'=>true,'live'=>false,'jsdrawend'=>'koordinatberubah();']); ?>
<form id="formimagepeta" method="post" enctype="multipart/form-data"  action="">
    <input type="hidden" name="id_user" value="<?=$_SESSION['id_user']?>">
    <input type="hidden" name="auth_key" value="<?=$_SESSION['auth_key']?>">
      <input type="hidden" name="_csrf" value="<?=$this->keamanan->generatecsrf()?>">
      <input type="hidden" name="id_nup" value="<?=$id_nup?>">
  Ganti Peta : <input type="file" class="inputimage form-control" name="inputimageFile" data-name="peta" accept="image/gif, image/jpeg, image/png" /> <button type="button" class="btn btn-primary" onclick="simpandatanuppeta()"> Simpan</button>
</form>
</div>

<div class="modal fade" id="modalinputid_unit">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Unit List <?=$_SESSION['nama_perusahaan']?></h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('unit/_listlengkap'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
