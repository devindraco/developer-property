<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipe extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct() {
		 parent::__construct();
	 }
	 public function index($page=1)
 	{
		$this->load->model('m_tipe');
		$this->load->model('m_cluster');
		$aktif=isset($_GET['aktif'])?$_GET['aktif']:2;
		$perpage=isset($_GET['perpage'])?$_GET['perpage']:25;
		$page=(int)$page;
		$this->setlayouts('utama','tipe/utama',['model'=>$this->m_tipe,'menudipilih'=>'Master Data']);
 	}
	public function listtipe()
	{
			$this->setlayouts('listtipe','utama',['tipeku'=>$this->m_tipe->tipeku(0,false,1),'menudipilih'=>'dashboard']);
	}
}
