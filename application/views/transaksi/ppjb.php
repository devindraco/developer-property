<?php $perusahaanku=$this->m_perusahaan->perusahaanku();
//$marketingku=$this->m_marketing->marketingku(['id_userutama'=>$_SESSION['id_user'],'lengkap'=>true,'posisiutama'=>$_SESSION['posisi'],'auth_keyutama'=>$_SESSION['auth_key']]);
//$unitku=$this->m_unit->unitku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
//$customerku=$this->m_customer->customerku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
?>
<script type="text/javascript">
var textareappjb=null;
var datatemplateppjb=[];
var id_template_dokumen=0;
</script>
<div id="boxindex" class="box">
  <div class="box-header">
    <h3 class="box-title">List Transaksi (PPJB)</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div id="filter" class="row">
    <h4 class="col-xs-12">Filer</h4>
    <div class="form-group col-xs-6 col-md-3">
      <label>Jumlah</label>
      <select id="perpage" name="perpage" class="form-control filterinput">
        <option value="25" selected>25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="300">100</option>
      </select>
    </div>
    <div class="form-group col-xs-6 col-md-3">
      <label>Urutkan</label>
      <select id="orderby" name="orderby" class="form-control filterinput">
        <option value="id_pesanan DESC" selected>Terbaru</option>
        <option value="id_pesanan ASC">Terlama</option>
      </select>
    </div>
    <!-- Date range -->
    <div class="form-group col-xs-6 col-md-3">
      <label>Tanggal</label>

      <div class="input-group">
        <div class="input-group-addon">
          <i class="fa fa-calendar"></i>
        </div>
        <input type="hidden" class="form-control pull-right" id="inputfiltertanggal">
        <input type="text" class="form-control pull-right" id="filtertanggal">
      </div>
      <!-- /.input group -->
    </div>
    <div class="form-group col-xs-6 col-md-3">
      <label>Nomor Pesanan</label>
      <input type="text" class="form-control filterinput" name="no_pesanan" id="no_pesanan">
    </div>

    <div class="form-group col-xs-12">
      <button id="filterbutton" onclick="fitersubmit();" class="btn btn-primary pull-right"> Terapkan </button>
    </div>
  </div>
  <div class="box-table">
    <table id="tabeltransaksi" class="tableapproval table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <?php
        foreach ($model->kolomkecil() as $key => $value) {
          if ($key!='id_pesanan' && $key!='diskon1_rp' && $key!='uang_muka') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      <?php if ($this->keamanan->allowedaction['ubah']) { ?>
      <tr style="display:none"><td><input type="hidden" name="id_userutama" class="databerubah" value="<?=$_SESSION['id_user']?>"></td><td><input type="hidden" name="auth_keyutama" class="databerubah" value="<?=$_SESSION['auth_key']?>">
      <input type="hidden" name="_csrf" class="databerubah" value="<?=$this->keamanan->generatecsrf()?>"></td></tr>
    <?php } ?>
      </thead>
      <tbody>
        <tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>
      </tbody>
      <tfoot>
      <tr>
        <th>No.</th>
        <?php
        foreach ($model->kolomkecil() as $key => $value) {
          if ($key!='id_pesanan' && $key!='diskon1_rp' && $key!='uang_muka') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      </tfoot>
    </table>
  </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-left">
    </ul>

    <ul class="pagination pagination-sm no-margin pull-right">
      <li><button id="btnprev" class="btn btn-default" onclick="changepage(false);">« Prev</button></li>
      <li><button id="btnnext" class="btn btn-default" onclick="changepage();">Next »</button></li>
    </ul>
  </div>
</div>
<!-- /.box -->

<div class="modal fade" id="modalcicilanindex">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Detail Cicilan</h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('transaksi/_simulasicicilan'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modaltemplatedokumen">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Pilih Template</h4>
      </div>
      <div class="modal-body">
          <?php $this->load->view('template_dokumen/_listlengkap',['jenis_template'=>'ppjb']); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<div id="boxdetail" class="box" style="display:none">
  <div class="box-header">
    <h3 class="box-title">Laporan PPJB</h3><button type="button" class="close pull-right" onclick="$('#boxdetail').hide();$('#boxindex').show();dataajax();" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <textarea id="textareappjb" rows="10" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
  </div>
  <div class="box-footer">
    <button type="button" class="btn btn-primary pull-right" onclick="cetakdokumen()">Cetak</button>
  </div>

</div>

<!-- /.modal -->
<?php if ($this->keamanan->allowedaction['ubah']) { ?>
<div class="modal fade" id="modaldetaillengkapunit">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Detail Unit</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modaldetaillengkapcustomer">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Detail Customer</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modaldetaillengkapmarketing">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Detail Marketing</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php } ?>
<script type="text/javascript">
var perpage = 25;
var page = 1;
var id_pesanandipilih = 1;
<?php /* var listperusahaanku = <?=json_encode($perusahaanku)?>;
var listunitku = <?=json_encode($unitku)?>;
var listcustomerku = <?=json_encode($customerku)?>; */?>
var listtransaksiku=[];
var listkolom = <?=json_encode($model->kolom())?>;
var liststatus = <?=json_encode($model->liststatus())?>;
var listjenisbayar = [{'jenis_bayar':'Cash'},{'jenis_bayar':'InHouse'},{'jenis_bayar':'KPR'}];
var hargajual = 0;

function dataajax() {
  perpage=$("#perpage").val();
  $( "#tabeltransaksi>tbody" ).html('<tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>');
  var request = $.ajax({
    url: "<?=base_url()?>ajax/datatransaksi",
    method: "POST",
    data: {
      'status': 'Lunas',
      'alldataselect': true,
      'no_pesanan': $('#no_pesanan').val(),
      'rangetanggal': $('#inputfiltertanggal').val(),
      'lengkap': 1,
      'page': page,
      'perpage': perpage,
      'posisi': '<?=$_SESSION['posisi']?>',
      'orderby': $("#orderby").val(),
      'id_userutama': <?=$_SESSION['id_user']?>,
      'auth_keyutama': '<?=$_SESSION['auth_key']?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    var hasil = '';
    console.log(datahasil);
    $.each(datahasil, function( index, transaksi ) {

      hasil +='<tr onclick="bukapilihantemplate('+transaksi["id_pesanan"]+')"><?php
      $hapus='';
      echo '<td>\'+(((page-1)*perpage)+index+1)+\'</td>';
      foreach ($model->kolomkecil() as $key => $value) {
        if ($key!='id_pesanan' && $key!='diskon1_rp' && $key!='uang_muka') {
          if ($key=='id_unit') {
            echo '\'; transaksi["unit"]=transaksi["nomor"]+\' Tipe \'+transaksi["nama_tipe"]; ';
            echo 'hasil+=\'<td>\'+transaksi["unit"]+\'</td>';
          } else if ($key=='harga_jual'||$key=='cashback'||$key=='harga_net'||$key=='uang_tanda_jadi'||$key=='uang_muka_rp') {
            echo '\'; transaksi["'.$key.'"]=Math.trunc(transaksi["'.$key.'"]); transaksi["'.$key.'"]=\'Rp. \'+transaksi["'.$key.'"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");';
            echo ' hasil +=\'<td>\'+transaksi["'.$key.'"]+\'</td>';
          } else if ($key=='diskon1') {
            echo '\'; transaksi["'.$key.'"]=Math.trunc(transaksi["'.$key.'"]); transaksi["'.$key.'"]=transaksi["'.$key.'"].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");';
            echo ' hasil +=\'<td>\'+transaksi["'.$key.'"]+\'%</td>';
          } else {
            if ($key=='id_marketing') $key='nama_marketing';
            if ($key=='id_customer') $key='nama_customer';
            echo '<td>\'+transaksi["'.$key.'"]+\'</td>';
          }
        }
      }
      ?></tr>';
      listtransaksiku[transaksi["id_pesanan"]]=transaksi;
    });
    $( "#tabeltransaksi>tbody" ).html(hasil);
    if (datahasil.length<perpage) $('#btnnext').attr("disabled", true); else $('#btnnext').attr("disabled", false);
    if (page<=1) $('#btnprev').attr("disabled", true); else $('#btnprev').attr("disabled", false);
    ubahdata(true);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
function bukapilihantemplate(id_pesanan) {
  id_pesanandipilih = id_pesanan;
  $('#modaltemplatedokumen').modal();
}
function pilihtemplate_dokumen(id_template=0) {
  $('#modaltemplatedokumen').modal('hide');
  id_template_dokumen=id_template;
  var html = datatemplateppjb[id_template];
  html=replacevariabeldokumen(html,listtransaksiku[id_pesanandipilih]);
  $('iframe').contents().find('.wysihtml5-editor').html(html);
  $('#boxdetail').show();$('#boxindex').hide();
}
function cetakdokumen() {
  var html = $('iframe').contents().find('html').html();
  var w = window.open();
  w.document.write(html);
  w.print();
  w.close();
}
function viewlengkapunit(id_unit) {
  var request = $.ajax({
    url: "<?=base_url()?>ajax/viewlengkapunit",
    method: "POST",
    data: {
      'id_unit': id_unit,
      'posisi': '<?=$_SESSION['posisi']?>',
      'id_user': <?=$_SESSION['id_user']?>,
      'auth_key': '<?=$_SESSION['auth_key']?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "html"
  });

  request.done(function( datahasil ) {
    console.log(datahasil);
    $('#modaldetaillengkapunit').modal();
    $("#modaldetaillengkapunit .modal-body").html(datahasil);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
function viewlengkapcustomer(id_customer) {
  var request = $.ajax({
    url: "<?=base_url()?>ajax/viewlengkapcustomer",
    method: "POST",
    data: {
      'id_customer': id_customer,
      'posisi': '<?=$_SESSION['posisi']?>',
      'id_user': <?=$_SESSION['id_user']?>,
      'auth_key': '<?=$_SESSION['auth_key']?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "html"
  });

  request.done(function( datahasil ) {
    console.log(datahasil);
    $('#modaldetaillengkapcustomer').modal();
    $("#modaldetaillengkapcustomer .modal-body").html(datahasil);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}

function viewlengkapmarketing(id_marketing) {
  var request = $.ajax({
    url: "<?=base_url()?>ajax/viewlengkapmarketing",
    method: "POST",
    data: {
      'id_marketing': id_marketing,
      'posisi': '<?=$_SESSION['posisi']?>',
      'id_user': <?=$_SESSION['id_user']?>,
      'auth_key': '<?=$_SESSION['auth_key']?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "html"
  });

  request.done(function( datahasil ) {
    console.log(datahasil);
    $('#modaldetaillengkapmarketing').modal();
    $("#modaldetaillengkapmarketing .modal-body").html(datahasil);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}
window.addEventListener('DOMContentLoaded', (event) => {
$('#filtertanggal').daterangepicker(
  {
    ranges   : {
      'Hari Ini'       : [moment(), moment()],
      'Kemarin'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      '7 Hari Terakhir' : [moment().subtract(6, 'days'), moment()],
      '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
      'Bulan Ini'  : [moment().startOf('month'), moment().endOf('month')],
      'Bulan Lalu'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    startDate: moment().subtract(29, 'days'),
    endDate  : moment()
  },
  function (start, end) {
    $('#inputfiltertanggal').val('\''+start.format('YYYY-MM-DD') + '\' Hingga \'' + end.format('YYYY-MM-DD')+'\'')
  }
);
$('#inputfiltertanggal').val('\''+moment().subtract(29, 'days').format('YYYY-MM-DD') + '\' Hingga \'' + moment().format('YYYY-MM-DD')+'\'');
var templatealignment = {
      textAlign: function(context) {
        return "<li><div class='btn-group'>" +
            "<a class='btn btn-default' data-wysihtml5-command='justifyLeft' data-wysihtml5-command-value='&justifyLeft;' title= 'Align text left'>" +
            "<span class='glyphicon glyphicon-align-left'></span></a>" +
            "<a class='btn btn-default' data-wysihtml5-command='justifyCenter' data-wysihtml5-command-value='&justifyCenter;' title= 'Align text center'>" +
            "<span class='glyphicon glyphicon-align-center'></span></a>" +
            "<a class='btn btn-default' data-wysihtml5-command='justifyRight' data-wysihtml5-command-value='&justifyRight;' title= 'Align text right'>" +
            "<span class='glyphicon glyphicon-align-right'></span></a>" +
            "</li>";
      }
    };
$('#textareappjb').wysihtml5({
  classes: {
    "wysiwyg-text-align-center": 1,
    "wysiwyg-text-align-justify": 1,
    "wysiwyg-text-align-left": 1,
    "wysiwyg-text-align-right": 1
  },
  "toolbar": {
    "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
    "emphasis": true, //Italics, bold, etc. Default true
    "lists": true, //(Un)ordered lists, e.g. Bullet"s, Numbers. Default true
    "html": false, //Button which allows you to edit the generated HTML. Default false
    "link": true, //Button to insert a link. Default true
    "image": true, //Button to insert an image. Default true,
    "color": true, //Button to change color of font
    "blockquote": false,
    'textAlign': true // custom defined buttons to align text see myCustomTemplates variable above
  },
  stylesheets: ["<?=base_url()?>assets/css/main.css"],
  customTemplates: templatealignment
});
});
</script>
