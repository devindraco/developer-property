<?php /*<div class="box">
  <div class="box-header">
    <h3 class="box-title"><?=ucfirst($action)?> marketing</h3>
  </div>
  <!-- /.box-header -->

  <div class="box-body">
    <form action="" method="post" enctype="application/x-www-form-urlencoded" class="row">
    */
    ?>
    <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#datamarketing" id="btntabmarketing" data-toggle="tab">Data User</a></li>
              <li><a href="#dataperusahaan" id="btntabperusahaan" data-toggle="tab">Data Perusahaan</a></li>
              <li><a href="#datahakakses" id="btntabhakakses" data-toggle="tab">Hak Akses</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="datamarketing">
                <input class="datatambah" type="hidden" name="id_userutama" class="databerubah" value="<?=$_SESSION['id_user']?>">
                <input class="datatambah" type="hidden" name="auth_keyutama" class="databerubah" value="<?=$_SESSION['auth_key']?>">
                  <input class="datatambah" type="hidden" name="_csrf" value="<?=$this->keamanan->generatecsrf()?>">
                  <input class="datatambah" type="hidden" name="id_user" value="0">
                <?php
                foreach ($this->m_user->kolom() as $key => $value) {
                  if ($key!='id_user'){
                    $datas=($key=='aktif')?[['aktif'=>1,'id'=>'Aktif'],['aktif'=>0,'id'=>'Tidak Aktif']]:null;
                    $namevalue=($key=='aktif')?'id':'';
                    echo $this->formku->generateinput($value['type'],$key,(isset($_POST[$key])?$_POST[$key]:$value['default']),$value['label'],$class='col-xs-6','fa-book',$datas,$namevalue);
                  }
                }
                ?>
                <div style="clear:both"></div>
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <button id="btntambah" type="button" class="btn btn-success pull-right" onclick="$('#btntabperusahaan').click()">Next »</button>
                <div style="clear:both"></div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="dataperusahaan">
                <div style="clear:both"></div>
                <table id="tabeldataperusahaan" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Perusahahan</th>
                  </tr>
                </thead>
                <tbody>
                <?php $htmlperusahaan = '';$htmlperusahaan = '';
                $tempidpr=0;
                $htmlperusahaan .= '<div class="form-group divpr" style="margin:0"><div class="input-group checkbox"><label><input type="checkbox" onclick="$(\'.inputpr\').prop(\'checked\', $(\'#inputpr\').prop(\'checked\'));" id="inputpr">Pilih Semuanya</label></div></div><hr style="margin:0;border-color:#ddd">';
                foreach ($perusahaanku as $key => $perusahaan) {
                  if ($perusahaan->id_perusahaan!=$tempidpr){
                    $htmlperusahaan .= '<div class="form-group divpr divpr'.$perusahaan->id_perusahaan.'"><div class="input-group checkbox"><label><input type="checkbox" class="inputpr'.$perusahaan->id_perusahaan.' inputpr inputperusahaan" name="perusahaan['.$perusahaan->id_perusahaan.']"><b> '.$perusahaan->nama_perusahaan.'</b></label></div></div>';
                    $tempidpr=$perusahaan->id_perusahaan;
                  }
                }
                echo '<tr><td>'.$htmlperusahaan.'</td></tr>';
                ?>
                </tbody>
                </table>
                <button id="btntambah" type="button" class="btn btn-success" onclick="$('#btntabmarketing').click()">« Prev</button>
                <button id="btntambah" type="button" class="btn btn-success pull-right" onclick="$('#btntabhakakses').click()">Next »</button>
                <div style="clear:both"></div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="datahakakses">
                <div><span class="pull-left">Copy Akses dari </span><select onchange="sethakaksesuser(this.value)" class="userlist pull-left" style="height:25px;padding:2px;margin-left:10px;"><option value="0">None</option></select></div>
                <?php echo $this->m_menu->getMenu(''); ?>
                <div style="clear:both"></div>
                <button id="btntambah" type="button" class="btn btn-success" onclick="$('#btntabperusahaan').click()">« Prev</button>
                <button id="btntambah" type="button" class="btn btn-primary pull-right" onclick="tambahdataajax()">Simpan</button>
                <div style="clear:both"></div>
              </div>
              <!-- /.tab-pane -->

            </div>
            <!-- /.tab-content -->
          </div>
<?php /*
    </form>

  </div>
  <!-- /.box-body -->

</div>
<!-- /.box -->
*/ ?>
<script type="text/javascript">
function pilihperusahaan(id){
    $('.divheadpr').removeClass('mmarketingdipilih');
    $('.divheadpr'+id).addClass('mmarketingdipilih');
    $('.divpr').hide();$('.divpr'+id).show();
}
</script>
