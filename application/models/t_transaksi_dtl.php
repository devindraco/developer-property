<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class t_transaksi_dtl extends CI_Model {

  function kolom($id='') {
    $listkolom = array(
      'id_pesanan'=> ['label'=>'ID transaksi','default'=>0,'type'=>'text'],
      'cicilan'=> ['label'=>'ID transaksi','default'=>0,'type'=>'number'],
      'keterangan'=> ['label'=>'ID transaksi','default'=>'','type'=>'text'],
      'urutan'=> ['label'=>'ID transaksi','default'=>0,'type'=>'number'],
      'jatuh_tempo'=> ['label'=>'ID transaksi','default'=>date('Y-m-d'),'type'=>'date']
    );
    if (isset($listkolom[$id])) {
      return $listkolom[$id];
    } else {
      return $listkolom;
    }
  }


  function dataupdate($data) {
    $sql = null;
    if (isset($data['id_pesanan'])) {
      $sql = array();
      $query = '';
      foreach ($data['id_pesanan'] as $key => $value) {
        $sql[$key]='EXEC sp_t_pesanan @id_userutama='.$data['id_userutama'].', @auth_keyutama=\''.$data['auth_keyutama'].'\',';
        foreach ($this->kolom() as $id => $label) {
          if (isset($data[$id])) {
            $sql[$key].=isset($data[$id][$key])?' @'.$id.'=\''.$data[$id][$key].'\',':'';
          }
        }
        $sql[$key]=substr($sql[$key], 0, -1);
        $sql[$key].=';';
        $query.=$sql[$key].' ';
      }
      if ($query!=''){
        $q=$this->db->query($query);
        //$sql=$q->result_array();
      }
    }
    return '';
  }

  function datainsert($data) {
    $query='EXEC sp_t_pesanan @id_userutama='.$data['id_userutama'].', @id_user='.$data['id_userutama'].', @auth_keyutama=\''.$data['auth_keyutama'].'\',';
    $data['id_pesanan']=0;
    $pakaidiskon = false;$uangmukarp = '';
    if (isset($data['checkdiskon'])) { if ($data['checkdiskon']=='on') { $pakaidiskon = true;}}
    if (isset($data['typeuang_muka'])) { if ($data['typeuang_muka']==2 && isset($data['uang_muka'])) { $uangmukarp = $data['uang_muka'];}}
    foreach ($this->kolom() as $id => $kolom) {
      if (isset($data[$id])) {
        $data[$id]=($id=='aktif')?(($data[$id]=='on')?1:0):$data[$id];
        $data[$id]=($id=='id_user_approve')?0:$data[$id];
        $data[$id]=($id=='tanggal_approve')?null:$data[$id];
        if (!$pakaidiskon) { $data[$id]=($id=='nama_diskon')?'':$data[$id];
          $data[$id]=($id=='diskon1')?0:$data[$id];$data[$id]=($id=='diskon2')?0:$data[$id];$data[$id]=($id=='diskon3')?0:$data[$id];
          $data[$id]=($id=='diskon1rp')?0:$data[$id];$data[$id]=($id=='diskon2rp')?0:$data[$id];$data[$id]=($id=='diskon3rp')?0:$data[$id];
        }
        $data[$id]=($id=='uang_muka'&&$uangmukarp!='')?0:$data[$id];
        $data[$id]=($id=='uang_muka_rp'&&$uangmukarp!='')?$uangmukarp:$data[$id];
        $query.=' @'.$id.'=\''.$data[$id].'\',';
      } else {
        $query.=' @'.$id.'=\''.$kolom['default'].'\',';
      }
    }
    $query=substr($query, 0, -1);
    $query.=';';
    if ($query!=''){
      $q=$this->db->query($query);
      $hasil=$q->row_array();
    }
    $query='';
    if (isset($data['cicilanke'])&&isset($data['jatuhtempoke'])&&isset($data['keteranganke'])&& isset($hasil['id_pesanan'])) {
      $query='INSERT INTO t_pesanan_dtl VALUES ('.$hasil["id_pesanan"].',\''.$data["uang_tanda_jadi"].'\',\'Uang Tanda Jadi\',0,\''.$data["tanggal"].'\'),';
      foreach ($data['cicilanke'] as $idcicilan => $cicilanke) {
        $query.=' ('.$hasil["id_pesanan"].',\''.$data["cicilanke"][$idcicilan].'\',\''.$data["keteranganke"][$idcicilan].'\','.$idcicilan.',\''.$data["jatuhtempoke"][$idcicilan].'\'),';
      }
      $query=substr($query, 0, -1);
      $query.=';';
    }
    if ($query!=''){
      $q=$this->db->query($query);
      //$sql=$this->db->error()['message'];
    }
    //print_r($sql);
    return $query;
  }

  function datahapus($data) {
    $data['id_userutama']=isset($data['id_userutama'])?$data['id_userutama']:0;
    $data['auth_keyutama']=isset($data['auth_keyutama'])?$data['auth_keyutama']:'-';
    $data['id_pesanan']=isset($data['id_pesanan'])?$data['id_pesanan']:0;
    $query='EXEC sp_m_pesanan @id_userutama='.$data['id_userutama'].', @auth_keyutama=\''.$data['auth_keyutama'].'\', @id_pesanan=\''.$data['id_pesanan'].'\', @aksi=\'hapus\';';
    $q=$this->db->query($query);
    return '';
  }

  function transaksiku($datainput){
  	$response = null;
      $data = array(
        'id_pesanan'=>isset($datainput['id_pesanan'])?$datainput['id_pesanan']:0,
        'lengkap'=>isset($datainput['lengkap'])?$datainput['lengkap']:false,
        'status'=>isset($datainput['status'])?$datainput['status']:'',
        'page'=>isset($datainput['page'])?$datainput['page']:1,
        'perpage'=>isset($datainput['perpage'])?$datainput['perpage']:25,
        'no_pesanan'=>isset($datainput['no_pesanan'])?$datainput['no_pesanan']:'',
        'orderby'=>isset($datainput['orderby'])?$datainput['orderby']:'id_pesanan DESC',
        'alldata'=>isset($datainput['alldata'])?$datainput['alldata']:false,
        'posisi'=>isset($datainput['posisi'])?$datainput['posisi']:'user',
        'id_perusahaan'=>isset($datainput['id_perusahaan'])?$datainput['id_perusahaan']:0,
        'id_userutama'=>isset($datainput['id_userutama'])?$datainput['id_userutama']:0,
        'auth_keyutama'=>isset($datainput['auth_keyutama'])?$datainput['auth_keyutama']:'-',
      );
  		$response = array();
  		$id_userutama = htmlspecialchars($data['id_userutama']);
      $auth_keyutama = htmlspecialchars($data['auth_keyutama']);
      $id_pesanan= htmlspecialchars($data['id_pesanan']);
      $no_pesanan = htmlspecialchars($data['no_pesanan']);
      $status = htmlspecialchars($data['status']);
      $page = ((int)$data['page']>=1)?(int)$data['page']:1;
      $perpage = ((int)$data['perpage']>=1)?(int)$data['perpage']:25;
      $id_perusahaan = ((int)$data['id_perusahaan']>=1)?(int)$data['id_perusahaan']:0;
      $orderby = $data['orderby'];
  		$checkid = ($id_pesanan!=0)?' AND id_pesanan='.$id_pesanan:'';
      $select = ($data['lengkap'])?'*':'id_pesanan,status,id_perusahaan,no_pesanan';
      $status = ($status!='')?' AND status=\''.$status.'\'':'';
      $tambahan = ($no_pesanan!='')?' AND no_pesanan LIKE \'%'.$no_pesanan.'%\'':'';
      $tambahan .= ' AND id_userutama='.$id_userutama.' AND auth_keyutama=\''.$auth_keyutama.'\'';
      $tambahan .= ($id_perusahaan!=0)?' AND id_perusahaan='.$id_perusahaan:'';
      $limit=($data['alldata'])?'':' OFFSET '.(($data['page']-1)*$data['perpage']).' ROWS FETCH NEXT '.$data['perpage'].' ROWS ONLY';
  		if ($data['posisi']=='owner') {
  			$q = $this->db->query('SELECT '.$select.' FROM v_pesanan_owner WHERE 1=1'.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit);
  		} else {
  			$q = $this->db->query('SELECT '.$select.' FROM v_pesanan_owner WHERE 1=1'.$checkid.$status.$tambahan.' ORDER BY '.$orderby.$limit);
  		}
      //$q = $this->db->query('exec sp_data_t_transaksi @id_usr ='.$id_user);
  		$response = ($id_pesanan==0)?$q->result():$q->row();
      //$response = 'SELECT '.$select.' FROM v_transaksi_owner WHERE id_user='.$id_user.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit;
      return $response;
    }


}
