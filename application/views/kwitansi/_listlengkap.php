<?php
$this->load->model('t_kwitansi');
$id_user=isset($_SESSION['id_user'])?$_SESSION['id_user']:(isset($id_user)?$id_user:0);
$auth_key=isset($_SESSION['auth_key'])?$_SESSION['auth_key']:(isset($auth_key)?$auth_key:'');
$posisi=isset($_SESSION['posisi'])?$_SESSION['posisi']:(isset($posisi)?$posisi:'user');
$id_perusahaan=isset($_SESSION['id_perusahaan'])?$_SESSION['id_perusahaan']:(isset($id_perusahaan)?$id_perusahaan:0);
$datas = $this->t_kwitansi->kwitansiku(['id_user'=>$id_user,'lengkap'=>true,'posisi'=>$posisi,'auth_key'=>$auth_key,'id_perusahaan'=>$id_perusahaan]);
$model=$this->t_kwitansi;
 ?>
<table id="tabellistkwitansi" class="table table-bordered table-striped">
  <thead>
  <tr>
    <th>Foto</th>
    <?php
    foreach ($model->kolom() as $key => $value) {
      if ($key!='id_kwitansi' && $key!='slug'){
        echo '<th>'.$value['label'].'</th>';
      }
    }
    ?>
  </tr>
  </thead>
  <tbody>
    <?php
    foreach ($datas as $keydt => $data) {
      echo '<tr class="id_kwitansi'.$data["id_kwitansi"].'" onclick="pilihkwitansi(\''.$data["id_kwitansi"].'\',\''.$data["no_kwitansi"].'\',\''.$data["slug"].'\')">';
      echo '<td><img class="img-responsive" style="width:100px;" src="'.base_url().'assets/images/kwitansi/'.$data["id_perusahaan"].'/'.$data["slug"].'" /></td>';
      foreach ($model->kolom() as $keyk => $kolom) {
        if ($keyk!='id_kwitansi' && $keyk!='slug'){
          echo '<td>'.$data[$keyk].'</td>';
        }
      }
      echo '</tr>';
    }
    ?>
  </tbody>
  <tfoot>
  <tr>
    <th>Foto</th>
    <?php
    foreach ($model->kolom() as $key => $value) {
      if ($key!='id_kwitansi' && $key!='slug'){
        echo '<th>'.$value['label'].'</th>';
      }
    }
    ?>
  </tr>
  </tfoot>
</table>
