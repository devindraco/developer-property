<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IndexController extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct() {
		 parent::__construct();
	 }
	public function index()
	{
		$this->load->model('m_unit');
			$this->setlayouts('utama','utama',['menudipilih'=>'dashboard']);
	}
	public function login()
	{
		$this->load->model('m_user');
		$error='';
		if (isset($_POST['login'])&&isset($_POST['username'])&&isset($_POST['password'])&&isset($_POST['_csrf'])) {
			if ($_POST['_csrf']==$this->keamanan->generatecsrf('login')){
				if ($datauser=$this->m_user->login($_POST['username'],$_POST['password'])) {
					$newdata = array(
					        'id_user'  => $datauser->id_user,
					        'nama'     =>  $datauser->nama_depan,
									'auth_key'     =>  $datauser->auth_key,
					        'posisi' => ($datauser->jabatan!=null)?$datauser->jabatan:'user',
									'id_perusahaan' => 0,
									'nama_perusahaan' => ''
					);
					$this->session->set_userdata($newdata);
					redirect('perusahaan/listperusahaan', 'refresh');
				} else {
					$error='Username/Password Salah. Silahkan Coba Lagi';
				}
			} else {
				$error='Sesi telah habis, silahkan login kembali.';
			}
			$this->setlayouts('login','blank',['error'=>$error]);
		} else {
			$this->setlayouts('login','blank');
		}
	}
	public function logout()
	{
		session_destroy();
		redirect('', 'refresh');
	}
	public function notfound()
	{
		$this->load->view('notfound');
	}
	public function notallowed()
	{
		$this->load->view('notallowed');
	}
}
