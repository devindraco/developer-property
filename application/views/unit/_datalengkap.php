<?php
$this->load->model('m_unit');
$id_unit=isset($id_unit)?$id_unit:0;
$id_user=isset($_SESSION['id_user'])?$_SESSION['id_user']:(isset($id_user)?$id_user:0);
$auth_key=isset($_SESSION['auth_key'])?$_SESSION['auth_key']:(isset($auth_key)?$auth_key:'');
$posisi=isset($_SESSION['posisi'])?$_SESSION['posisi']:(isset($posisi)?$posisi:'user');
$id_perusahaan=isset($_SESSION['id_perusahaan'])?$_SESSION['id_perusahaan']:(isset($id_perusahaan)?$id_perusahaan:0);
$data = $this->m_unit->unitku(['id_user'=>$id_user,'lengkap'=>true,'posisi'=>$posisi,'auth_key'=>$auth_key,'id_perusahaan'=>$id_perusahaan,'id_unit'=>$id_unit]);
$model=$this->m_unit;
if ($data) {
  echo '<h2 style="margin-top:0">'.$data->nama_perumahan.'</h2><h3 style="margin-top:0">Rp. '.number_format($data->harga, 0, ',', '.' ).'</h3>
<p>Nomor <b>'.$data->nomor.'</b></p>
<p>Tipe <b>'.$data->nama_tipe.'</b></p>
<p>Cluster <b>'.$data->nama_cluster.'</b></p>
<p>Luas <b>'.$data->luas_bangunan.'m²</b></p>
<p>Ukuran <b>'.$data->panjang.'m</b> x <b>'.$data->lebar.'m</b></p>';
}
 ?>
