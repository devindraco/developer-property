<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_user extends CI_Model {

  function kolom($id='') {
    $listkolom = array(
      'id_user'=> ['label'=>'ID user','default'=>0,'type'=>'text'],
      'username'=> ['label'=>'Username','default'=>'','type'=>'text'],
      'password'=> ['label'=>'Password','default'=>'','type'=>'text'],
      'nama_depan'=> ['label'=>'Nama Depan','default'=>'','type'=>'text'],
      'nama_belakang'=> ['label'=>'Nama Belakang','default'=>'','type'=>'text'],
      'jabatan'=> ['label'=>'Jabatan','default'=>'','type'=>'text'],
      'aktif'=> ['label'=>'Status','default'=>1,'type'=>'checkbox']
    );
    if (isset($listkolom[$id])) {
      return $listkolom[$id];
    } else {
      return $listkolom;
    }
  }
  function dataupdate($data) {
    $sql = null;
    if (isset($data['id_user'])) {
      $sql = array();
      $query = '';
      foreach ($data['id_user'] as $key => $value) {
        $sql[$key]='EXEC sp_m_user @id_userutama='.$data['id_userutama'].', @auth_keyutama=\''.$data['auth_keyutama'].'\',';
        foreach ($this->kolom() as $id => $label) {
          if (isset($data[$id])) {
            $sql[$key].=isset($data[$id][$key])?' @'.$id.'=\''.$data[$id][$key].'\',':'';
          }
        }
        $sql[$key]=substr($sql[$key], 0, -1);
        $sql[$key].=';';
        $query.=$sql[$key].' ';
      }
      if ($query!=''){
        $q=$this->db->query($query);
        $sql=$q->result_array();
      }
    }
    return '';
  }

  function datainsert($data) {
    $query='EXEC sp_m_user @id_userutama='.$data['id_userutama'].', @auth_keyutama=\''.$data['auth_keyutama'].'\',';
    foreach ($this->kolom() as $id => $label) {
      if (isset($data[$id])) {
        $data[$id]=($id=='aktif')?(($data[$id]=='on')?1:0):$data[$id];
        $data[$id]=($id=='password')?$this->keamanan->generatepassword($data[$id]):$data[$id];
        $query.=' @'.$id.'=\''.$data[$id].'\',';
      }
    }
    $query=substr($query, 0, -1);
    $query.=';';
    $q=$this->db->query($query);
    if ($sql=$q->row_array()){
      $id_user = $sql["id_user"];
      $query='';
      foreach ($data['perusahaan'] as $key => $perusahaan) {
        $query.=' EXEC sp_m_user_perusahaan @id_userutama='.$data['id_userutama'].', @auth_keyutama=\''.$data['auth_keyutama'].'\', @id_perusahaan='.$key.', @id_user='.$id_user.'; ';
      }
      if ($query!=''){
        $q=$this->db->query($query);
        if ($sql=$q->row_array()){
          $query='';
          foreach ($data['view'] as $key => $view) {
            $tambah=isset($data['tambah'])?(isset($data['tambah'][$key])?1:0):0;
            $ubah=isset($data['ubah'])?(isset($data['ubah'][$key])?1:0):0;
            $hapus=isset($data['hapus'])?(isset($data['hapus'][$key])?1:0):0;
            $query.=' EXEC sp_m_user_menu @id_userutama='.$data['id_userutama'].', @auth_keyutama=\''.$data['auth_keyutama'].'\', @id_user='.$id_user.', @act_tambah='.$tambah.',@act_ubah='.$ubah.',@act_hapus='.$hapus.', @id_menu='.$key.'; ';
          }
          if ($query!=''){
            $q=$this->db->query($query);
          }
        }
      }
    }
    return '';
  }

  function datahapus($data) {
    $data['id_user']=isset($data['id_user'])?$data['id_user']:0;
    $data['id_userutama']=isset($data['id_userutama'])?$data['id_userutama']:0;
    $data['auth_keyutama']=isset($data['auth_keyutama'])?$data['auth_keyutama']:'-';
    $query='EXEC sp_m_user @id_userutama='.$data['id_userutama'].', @auth_keyutama=\''.$data['auth_keyutama'].'\', @id_user=\''.$data['id_user'].'\', @aksi=\'hapus\';';
    $q=$this->db->query($query);
    return '';
  }

  function userhakakses($datainput) {
    $data = array(
      'id_user'=>isset($datainput['id_user'])?$datainput['id_user']:0,
      'lengkap'=>isset($datainput['lengkap'])?$datainput['lengkap']:false,
      'aktif'=>isset($datainput['aktif'])?$datainput['aktif']:2,
      'page'=>isset($datainput['page'])?$datainput['page']:1,
      'perpage'=>isset($datainput['perpage'])?$datainput['perpage']:25,
      'nama'=>isset($datainput['nama'])?$datainput['nama']:'',
      'orderby'=>isset($datainput['orderby'])?$datainput['orderby']:'id_user DESC',
      'alldata'=>isset($datainput['alldata'])?$datainput['alldata']:false,
      'posisi'=>isset($datainput['posisi'])?$datainput['posisi']:'user',
      'id_userutama'=>isset($datainput['id_userutama'])?$datainput['id_userutama']:0,
      'auth_keyutama'=>isset($datainput['auth_keyutama'])?$datainput['auth_keyutama']:'-',
    );
    $response = array();
		$id_userutama = (int)($data['id_userutama']);
    $auth_keyutama = htmlspecialchars($data['auth_keyutama']);
    $nama = htmlspecialchars($data['nama']);
    $id_user = (int)$data['id_user'];
    $aktif = (int)$data['aktif'];
    $page = ((int)$data['page']>=1)?(int)$data['page']:1;
    $perpage = ((int)$data['perpage']>=1)?(int)$data['perpage']:25;
    $orderby = $data['orderby'];
		$checkid = ($id_user!=0)?' AND id_user='.$id_user:'';
    $select = ($data['lengkap'])?'*':'id_user,aktif,username,nama_depan,nama_belakang,jabatan';
    $aktif = ($aktif==1||$aktif==0)?' AND aktif='.$aktif:'';
    $tambahan = ($nama!='')?' AND (nama_depan LIKE \'%'.$nama.'%\') OR (nama_belakang LIKE \'%'.$nama.'%\')':'';
    $limit=($data['alldata'])?'':' OFFSET '.(($data['page']-1)*$data['perpage']).' ROWS FETCH NEXT '.$data['perpage'].' ROWS ONLY';
		if ($data['posisi']=='owner') {
			$q = $this->db->query('SELECT '.$select.' FROM m_user u LEFT OUTER JOIN m_user_menu um ON u.id_user=um.id_user WHERE 1 '.$tambahan.$checkid.$aktif.' ORDER BY '.$orderby.$limit);
		} else {
			$q = $this->db->query('SELECT '.$select.' FROM m_user u LEFT OUTER JOIN m_user_menu um ON u.id_user=um.id_user WHERE 1 '.$tambahan.$checkid.$aktif.' ORDER BY '.$orderby.$limit);
		}
    //$q = $this->db->query('exec sp_data_m_tipe @id_usr ='.$id_user);
		$response = ($id_user==0)?$q->result():$q->row();
    $response = 'SELECT m_user u LEFT OUTER JOIN m_user_menu um ON u.id_user=um.id_user WHERE '.$tambahan.$checkid.$aktif.' ORDER BY '.$orderby.$limit;

    return $response;
  }

  function userku($datainput){
  $q = $this->load->model('m_user');
	$response = null;
    $data = array(
      'id_user'=>isset($datainput['id_user'])?$datainput['id_user']:0,
      'lengkap'=>isset($datainput['lengkap'])?$datainput['lengkap']:false,
      'aktif'=>isset($datainput['aktif'])?$datainput['aktif']:2,
      'page'=>isset($datainput['page'])?$datainput['page']:1,
      'perpage'=>isset($datainput['perpage'])?$datainput['perpage']:25,
      'nama'=>isset($datainput['nama'])?$datainput['nama']:'',
      'orderby'=>isset($datainput['orderby'])?$datainput['orderby']:'id_user DESC',
      'alldata'=>isset($datainput['alldata'])?$datainput['alldata']:false,
      'posisi'=>isset($datainput['posisi'])?$datainput['posisi']:'user',
      'id_perusahaan'=>isset($datainput['id_perusahaan'])?$datainput['id_perusahaan']:0,
      'id_userutama'=>isset($datainput['id_userutama'])?$datainput['id_userutama']:0,
      'auth_keyutama'=>isset($datainput['auth_keyutama'])?$datainput['auth_keyutama']:'-',
    );
		$response = array();
		$id_userutama = (int)($data['id_userutama']);
    $auth_keyutama = htmlspecialchars($data['auth_keyutama']);
    $nama = htmlspecialchars($data['nama']);
    $id_user = (int)$data['id_user'];
    $id_perusahaan = (int)$data['id_perusahaan'];
    $aktif = (int)$data['aktif'];
    $page = ((int)$data['page']>=1)?(int)$data['page']:1;
    $perpage = ((int)$data['perpage']>=1)?(int)$data['perpage']:25;
    $orderby = $data['orderby'];
		$checkid = ($id_user!=0)?' AND id_user='.$id_user:'';
    $select = ($data['lengkap'])?'DISTINCT id_user,id_user AS id_user_approve, username, password_hash, password_reset_token, auth_key, aktif, nama_depan, nama_belakang, jabatan, id_userutama, auth_keyutama,jabatanutama':'id_user,aktif,username,nama_depan,nama_belakang,jabatan';
    $aktif = ($aktif==1||$aktif==0)?' AND aktif='.$aktif:'';
    $tambahan = 'id_userutama='.$id_userutama.' AND auth_keyutama=\''.$auth_keyutama.'\'';
    $tambahan .= ($nama!='')?' AND (nama_depan LIKE \'%'.$nama.'%\') OR (nama_belakang LIKE \'%'.$nama.'%\')':'';
    $tambahan .= ($id_perusahaan!=0)?' AND id_perusahaan='.$id_perusahaan:'';
    $limit=($data['alldata'])?'':' OFFSET '.(($data['page']-1)*$data['perpage']).' ROWS FETCH NEXT '.$data['perpage'].' ROWS ONLY';
    $q = $this->db->query('SELECT '.$select.' FROM v_userlistku WHERE '.$tambahan.$checkid.$aktif.' ORDER BY '.$orderby.$limit);
		// if ($data['posisi']=='owner') {
		// 	$q = $this->db->query('SELECT '.$select.' FROM v_userlist_owner WHERE '.$tambahan.$checkid.$aktif.' ORDER BY '.$orderby.$limit);
		// } else {
		// 	$q = $this->db->query('SELECT '.$select.' FROM v_userlist_owner WHERE '.$tambahan.$checkid.$aktif.' ORDER BY '.$orderby.$limit);
		// }
    //$q = $this->db->query('exec sp_data_m_tipe @id_usr ='.$id_user);
		$response = ($id_user==0)?$q->result():$q->row();
    //$response = 'SELECT '.$select.' FROM v_userlist_owner WHERE '.$tambahan.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit;
    return $response;
  }

  function getUser($data){
    $select = isset($data['select'])?$data['select']:'u.id_user,u.auth_key,u.nama_depan,u.jabatan,u.nama_belakang,pr.id_perusahaan';
    $id_user = isset($data['id_user'])?$data['id_user']:0;
    $auth_key = isset($data['auth_key'])?$data['auth_key']:'-';
    $response = array();
    $q = $this->db->query('SELECT '.$select.' FROM m_user u LEFT OUTER JOIN m_perusahaan pr  ON u.id_user=pr.id_pemilik WHERE u.id_user=\''.$id_user.'\' AND u.auth_key=\''.$auth_key.'\' AND u.aktif=1');
    $response = $q->row();
    if ($response){
      if ($response->id_perusahaan==null) {
        $q = $this->db->query('SELECT '.$select.' FROM m_user u LEFT OUTER JOIN m_user_perusahaan up ON u.id_user=up.id_user LEFT OUTER JOIN m_perusahaan pr ON up.id_perusahaan=pr.id_perusahaan WHERE u.username=\''.$username.'\' AND u.password_hash=\''.$password.'\' AND u.aktif=1');
        $response = $q->row();
      }
    }
    return $response;
  }

  function login($username='',$password=''){

    $response = array();
	  $username = htmlspecialchars($username);
	  $password = $this->keamanan->generatepassword(htmlspecialchars($password));
    $q = $this->db->query('SELECT u.id_user,u.auth_key,u.nama_depan,u.jabatan,u.nama_belakang,pr.id_perusahaan FROM m_user u LEFT OUTER JOIN m_perusahaan pr ON u.id_user=pr.id_pemilik WHERE u.username=\''.$username.'\' AND u.password_hash=\''.$password.'\' AND u.aktif=1');
    $response = $q->row();
    if ($response){
      if ($response->id_perusahaan==null) {
        $q = $this->db->query('SELECT u.id_user,u.auth_key,u.nama_depan,u.jabatan,u.nama_belakang,pr.id_perusahaan FROM m_user u LEFT OUTER JOIN m_user_perusahaan up ON u.id_user=up.id_user LEFT OUTER JOIN m_perusahaan pr ON up.id_perusahaan=pr.id_perusahaan WHERE u.username=\''.$username.'\' AND u.password_hash=\''.$password.'\' AND u.aktif=1');
        $response = $q->row();
      }
      $newauth_key = $this->keamanan->generateauth_key($response->id_user);
      $q = $this->db->query('UPDATE m_user SET auth_key =\''.$newauth_key.'\' WHERE id_user='.$response->id_user);
      $response->auth_key=$newauth_key;
    }
    return $response;
  }

  // Fungsi Khusus Admin5427
  function useradmin($datainput){
  $q = $this->load->model('m_user');
	$response = null;
    $data = array(
      'id_user'=>isset($datainput['id_user'])?$datainput['id_user']:0,
      'lengkap'=>isset($datainput['lengkap'])?$datainput['lengkap']:false,
      'aktif'=>isset($datainput['aktif'])?$datainput['aktif']:2,
      'page'=>isset($datainput['page'])?$datainput['page']:1,
      'perpage'=>isset($datainput['perpage'])?$datainput['perpage']:25,
      'nama'=>isset($datainput['nama'])?$datainput['nama']:'',
      'orderby'=>isset($datainput['orderby'])?$datainput['orderby']:'id_user DESC',
      'alldata'=>isset($datainput['alldata'])?$datainput['alldata']:false,
      'id_perusahaan'=>isset($datainput['id_perusahaan'])?$datainput['id_perusahaan']:0,
    );
		$response = array();
    $nama = htmlspecialchars($data['nama']);
    $id_user = (int)$data['id_user'];
    $id_perusahaan = (int)$data['id_perusahaan'];
    $aktif = (int)$data['aktif'];
    $page = ((int)$data['page']>=1)?(int)$data['page']:1;
    $perpage = ((int)$data['perpage']>=1)?(int)$data['perpage']:25;
    $orderby = $data['orderby'];
		$checkid = ($id_user!=0)?' AND id_user='.$id_user:'';
    $select = ($data['lengkap'])?'DISTINCT id_user,id_user AS id_user_approve, username, password_hash, password_reset_token, auth_key, aktif, nama_depan, nama_belakang, jabatan':'id_user,aktif,username,nama_depan,nama_belakang,jabatan';
    $aktif = ($aktif==1||$aktif==0)?' AND aktif='.$aktif:'';
    $tambahan = ($nama!='')?' AND (nama_depan LIKE \'%'.$nama.'%\') OR (nama_belakang LIKE \'%'.$nama.'%\')':'';
    $tambahan .= ($id_perusahaan!=0)?' AND id_perusahaan='.$id_perusahaan:'';
    $limit=($data['alldata'])?'':' OFFSET '.(($data['page']-1)*$data['perpage']).' ROWS FETCH NEXT '.$data['perpage'].' ROWS ONLY';
    $q = $this->db->query('SELECT '.$select.' FROM v_userlistku WHERE 1=1'.$tambahan.$checkid.$aktif.' ORDER BY '.$orderby.$limit);
		// if ($data['posisi']=='owner') {
		// 	$q = $this->db->query('SELECT '.$select.' FROM v_userlist_owner WHERE '.$tambahan.$checkid.$aktif.' ORDER BY '.$orderby.$limit);
		// } else {
		// 	$q = $this->db->query('SELECT '.$select.' FROM v_userlist_owner WHERE '.$tambahan.$checkid.$aktif.' ORDER BY '.$orderby.$limit);
		// }
    //$q = $this->db->query('exec sp_data_m_tipe @id_usr ='.$id_user);
		$response = ($id_user==0)?$q->result():$q->row();
    //$response = 'SELECT '.$select.' FROM v_userlist_owner WHERE '.$tambahan.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit;
    return $response;
  }

}
