<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_customer extends CI_Model {

  function getPemilik(){

    $response = array();

    // Select record
    $this->db->select('*');
    $q = $this->db->get('m_pemilik');
    $response = $q->result_array();

    return $response;
  }

  function kolom($id='') {
    $listkolom = array(
      'id_customer'=> ['label'=>'ID Customer','default'=>0,'type'=>'text'],
      'nama_customer'=> ['label'=>'Nama Customer','default'=>'','type'=>'text'],
      'id_perusahaan'=> ['label'=>'Perusahaan','default'=>1,'type'=>'select'],
      'email'=> ['label'=>'Email','default'=>'','type'=>'text'],
      'telp'=> ['label'=>'Telepon','default'=>0,'type'=>'text'],
      'alamat'=> ['label'=>'Alamat','default'=>'','type'=>'textarea'],
      'hp1'=> ['label'=>'HP 1','default'=>0,'type'=>'text'],
      'hp2'=> ['label'=>'HP 2','default'=>0,'type'=>'text'],
      'jenis_identitas'=> ['label'=>'Jenis Identitas','default'=>'KTP','type'=>'text'],
      'no_identitas'=> ['label'=>'No Identitas','default'=>'','type'=>'text'],
      'npwp'=> ['label'=>'NPWP','default'=>'','type'=>'text'],
      'aktif'=> ['label'=>'Status','default'=>1,'type'=>'checkbox']
    );
    if (isset($listkolom[$id])) {
      return $listkolom[$id];
    } else {
      return $listkolom;
    }
  }

  function dataupdate($data) {
    $sql = null;
    if (isset($data['id_customer'])) {
      $sql = array();
      $query = '';
      foreach ($data['id_customer'] as $key => $value) {
        $data['id_user']=isset($data['id_user'])?$data['id_user']:0;
        $data['auth_key']=isset($data['auth_key'])?$data['auth_key']:'-';
        $sql[$key]='EXEC sp_m_customer @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\',';
        foreach ($this->kolom() as $id => $label) {
          if (isset($data[$id])) {
            $sql[$key].=isset($data[$id][$key])?' @'.$id.'=\''.$data[$id][$key].'\',':'';
          }
        }
        $sql[$key]=substr($sql[$key], 0, -1);
        $sql[$key].=';';
        $query.=$sql[$key].' ';
      }
      if ($query!=''){
        $q=$this->db->query($query);
        //$sql=$q->result_array();
      }
    }
    return '';
  }

  function dataupdatedokumen($data,$files) {
    if ($files!=null) {
      foreach ($files as $key => $file) {
        $path = str_replace('inputimageFile','',$key);
        $target_path = BASEPATH.'../assets/images/customer/'.$data['id_perusahaan'].'/';
        if(!file_exists($target_path)){ mkdir($target_path, '0777', true);}
        $target_path = $target_path.basename($path.$data['id_customer'].'.jpg');

        move_uploaded_file($file['tmp_name'], $target_path);
        $target_path = $file['tmp_name'];
      }
    }
    return $target_path;
  }

  function datainsert($data,$files=null) {
    $data['id_user']=isset($data['id_user'])?$data['id_user']:0;
    $data['auth_key']=isset($data['auth_key'])?$data['auth_key']:'-';
    $query='EXEC sp_m_customer @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\',';
    foreach ($this->kolom() as $id => $label) {
      if (isset($data[$id])) {
        $data[$id]=($id=='aktif')?(($data[$id]=='on')?1:0):$data[$id];
        $query.=' @'.$id.'=\''.$data[$id].'\',';
      }
    }
    $query=substr($query, 0, -1);
    $query.=';';
    if ($query!=''){
      $q=$this->db->query($query);
      $sql=$q->result_array();
      if ($sql=$q->row_array()){
      //if (true){
        if ($files!=null) {
          foreach ($files as $key => $file) {
            $path = str_replace('inputimageFile','',$key);
            $target_path = BASEPATH.'../assets/images/customer/'.$data['id_perusahaan'].'/';
            if(!file_exists($target_path)){ mkdir($target_path, '0777', true);}
            $target_path = $target_path.basename($path.$sql['id_customer'].'.jpg');
            move_uploaded_file($file['tmp_name'], $target_path);
          }
        }
      }
    }
    return '';
  }

  function datahapus($data) {
    $data['id_user']=isset($data['id_user'])?$data['id_user']:0;
    $data['auth_key']=isset($data['auth_key'])?$data['auth_key']:'-';
    $data['id_customer']=isset($data['id_customer'])?$data['id_customer']:0;
    $query='EXEC sp_m_customer @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\', @id_customer=\''.$data['id_customer'].'\', @aksi=\'hapus\';';
    //$q=$this->db->query($query);
    return $query;
  }

  function customerku($datainput){
  $q = $this->load->model('m_user');
	$response = null;
    $data = array(
      'id_customer'=>isset($datainput['id_customer'])?$datainput['id_customer']:0,
      'lengkap'=>isset($datainput['lengkap'])?$datainput['lengkap']:false,
      'aktif'=>isset($datainput['aktif'])?$datainput['aktif']:2,
      'page'=>isset($datainput['page'])?$datainput['page']:1,
      'perpage'=>isset($datainput['perpage'])?$datainput['perpage']:25,
      'orderby'=>isset($datainput['orderby'])?$datainput['orderby']:'id_customer DESC',
      'posisi'=>isset($datainput['posisi'])?$datainput['posisi']:'user',
      'id_user'=>isset($datainput['id_user'])?$datainput['id_user']:0,
      'id_perusahaan'=>isset($datainput['id_perusahaan'])?$datainput['id_perusahaan']:0,
      'auth_key'=>isset($datainput['auth_key'])?$datainput['auth_key']:'-',
    );
		$response = array();
		$id_user = htmlspecialchars($data['id_user']);
    $auth_key = htmlspecialchars($data['auth_key']);
		$id_customer = htmlspecialchars($data['id_customer']);
    $id_perusahaan = ((int)$data['id_perusahaan']>=1)?(int)$data['id_perusahaan']:0;
    $aktif = (int)$data['aktif'];
    $page = ((int)$data['page']>=1)?(int)$data['page']:1;
    $perpage = ((int)$data['perpage']>=1)?(int)$data['perpage']:25;
    $orderby = $data['orderby'];
		$checkid = ($id_customer!=0)?' AND id_customer='.$id_customer:'';
    $select = ($data['lengkap'])?'*':'id_customer, id_perusahaan, nama_customer, alamat, hp1, telp, hp2, jenis_identitas, no_identitas, email, aktif, npwp, nama_perusahaan';
    $aktif = ($aktif==1||$aktif==0)?' AND aktif='.$aktif:'';
    $tambahan = ' AND id_user='.$id_user.' AND auth_key=\''.$auth_key.'\'';
    $tambahan .= ($id_perusahaan!=0)?' AND id_perusahaan ='.$id_perusahaan:'';
    $limit=($data['lengkap'])?'':' OFFSET '.(($data['page']-1)*$data['perpage']).' ROWS FETCH NEXT '.$data['perpage'].' ROWS ONLY';
    $q = $this->db->query('SELECT '.$select.' FROM v_customerku WHERE id_user='.$id_user.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit);
		// if ($data['posisi']=='owner') {
		// 	$q = $this->db->query('SELECT '.$select.' FROM v_customer_owner WHERE id_user='.$id_user.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit);
		// } else {
		// 	$q = $this->db->query('SELECT '.$select.' FROM v_customer_user WHERE id_user='.$id_user.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit);
		// }
    //$q = $this->db->query('exec sp_data_m_customer @id_usr ='.$id_user);
		$response = ($id_customer==0)?$q->result():$q->row();
    return $response;
  }


}
