<?php $perusahaanku=$this->m_perusahaan->perusahaanku();
//$marketingku=$this->m_marketing->marketingku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisiutama'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
//$clusterku=$this->m_cluster->clusterku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
//$perumahanku=$this->m_perumahan->perumahanku(['id_user'=>$_SESSION['id_user'],'lengkap'=>true,'posisi'=>$_SESSION['posisi'],'auth_key'=>$_SESSION['auth_key']]);
?><div id="boxindex" class="box">
  <div class="box-header">
    <h3 class="box-title">List Kwitansi</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <?=($this->keamanan->allowedaction['tambah'])?'<button class="btn btn-primary" onclick="bukaformtambah()">Tambah</button>':''; ?>
    <button onclick="$('#filter').toggle(300);" class="btn btn-primary pull-right"><i class="fa fa-gear"></i></button>
    <div id="filter" class="row" style="display:none">
    <h4 class="col-xs-12">Filer</h4>
    <div class="form-group col-xs-6 col-md-3">
      <label>Jumlah</label>
      <select id="perpage" name="perpage" class="form-control filterinput">
        <option value="25" selected>25</option>
        <option value="50">50</option>
        <option value="100">100</option>
        <option value="300">100</option>
      </select>
    </div>
    <div class="form-group col-xs-6 col-md-3">
      <label>Urutkan</label>
      <select id="orderby" name="orderby" class="form-control filterinput">
        <option value="tanggal DESC" selected>Terbaru</option>
        <option value="tanggal ASC">Terlama</option>
      </select>
    </div>
    <!-- Date range -->
    <div class="form-group col-xs-6 col-md-3">
      <label>Tanggal</label>

      <div class="input-group">
        <div class="input-group-addon">
          <i class="fa fa-calendar"></i>
        </div>
        <input type="hidden" class="form-control pull-right" id="inputfiltertanggal">
        <input type="text" class="form-control pull-right" id="filtertanggal">
      </div>
      <!-- /.input group -->
    </div>
    <div class="form-group col-xs-6 col-md-3">
      <label>Nomor kwitansi</label>
      <input type="text" class="form-control filterinput" name="no_kwitansi" id="no_kwitansi">
    </div>

    <div class="form-group col-xs-12">
      <button id="filterbutton" onclick="$('#filter').toggle(300);fitersubmit();" class="btn btn-primary pull-right"> Terapkan </button>
    </div>
  </div>
  <div class="box-table">
    <table id="tabelkwitansi" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <th></th>
        <th>Foto</th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_kwitansi' && $key!='slug') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      <?php if ($this->keamanan->allowedaction['ubah']) { ?>
      <tr style="display:none"><td><input type="hidden" name="id_user" class="databerubah" value="<?=$_SESSION['id_user']?>"></td><td><input type="hidden" name="auth_key" class="databerubah" value="<?=$_SESSION['auth_key']?>">
      <input type="hidden" name="_csrf" class="databerubah" value="<?=$this->keamanan->generatecsrf()?>"></td></tr>
    <?php } ?>
      </thead>
      <tbody>
        <tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>
      </tbody>
      <tfoot>
      <tr>
        <th>No.</th>
        <th></th>
        <th>Foto</th>
        <?php
        foreach ($model->kolom() as $key => $value) {
          if ($key!='id_kwitansi' && $key!='slug') {
            echo '<th>'.$value['label'].'</th>';
          }
        }
        ?>
      </tr>
      </tfoot>
    </table>
  </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-left">
      <?php /*<li><?=($this->keamanan->allowedaction['ubah'])?'<button id="btnubah" class="btn btn-success" onclick="ubahdata(false,true);">Ubah</button>':''; ?></li> */ ?>
    </ul>

    <ul class="pagination pagination-sm no-margin pull-right">
      <li><button id="btnprev" class="btn btn-default" onclick="changepage(false);">« Prev</button></li>
      <li><button id="btnnext" class="btn btn-default" onclick="changepage();">Next »</button></li>
    </ul>
  </div>
</div>
<!-- /.box -->

<?php if ($this->keamanan->allowedaction['tambah']) { ?>
  <div id="boxtambah" class="box" style="display:none">
    <div class="box-header">
      <h3 class="box-title">Tambah kwitansi</h3><button type="button" class="close pull-right" onclick="$('#boxtambah').hide();$('#boxindex').show();dataajax();" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <?php $this->load->view('kwitansi/_form'); ?>
      <button id="btntambah" type="button" class="btn btn-primary" onclick="simpandataajax()">Simpan</button>
  </div>
</div>

<?php } ?>
<script type="text/javascript">
var perpage = 25;
var page = 1;
<?php /* var listperusahaanku = <?=json_encode($perusahaanku)?>;
var listclusterku = <?=json_encode($clusterku)?>;
var listperumahanku = <?=json_encode($perumahanku)?>; */?>
var listkolom = <?=json_encode($model->kolom())?>;
var listkwitansi = [];
var hargajual = 0;
var angkaum=0;

function dataajax() {
  perpage=$("#perpage").val();
  $( "#tabelkwitansi>tbody" ).html('<tr><td colspan="6" class="overlay" style="height:100px"><i class="fa fa-refresh fa-spin"></i></td></tr>');
  var request = $.ajax({
    url: "<?=base_url()?>ajax/datakwitansi",
    method: "POST",
    data: {
      'status': $('#status').val(),
      'no_kwitansi': $('#no_kwitansi').val(),
      'rangetanggal': $('#inputfiltertanggal').val(),
      'lengkap': 1,
      'page': page,
      'perpage': perpage,
      'posisi': '<?=$_SESSION['posisi']?>',
      'orderby': $("#orderby").val(),
      '_csrf': '<?=$this->keamanan->generatecsrf()?>',
      'id_user': <?=$_SESSION['id_user']?>,
      'auth_key': '<?=$_SESSION['auth_key']?>',
      'id_perusahaan': '<?=$_SESSION['id_perusahaan']?>'
    },
    dataType: "json"
  });

  request.done(function( datahasil ) {
    var hasil = '';
    console.log(datahasil);
    $.each(datahasil, function( index, kwitansi ) {
      listkwitansi[kwitansi['id_kwitansi']]=kwitansi;
      hasil +='<tr><?php
      $hapus='';
      echo '<td>\'+(((page-1)*perpage)+index+1)+\'</td>';
      echo '<td style="min-width:0;padding:0"><button class="btn btn-link" type="button" onclick="bukaformubah(\'+kwitansi["id_kwitansi"]+\')"><i class="fa fa-pencil"></i></button></td>';
      echo '<td><a id="popupsingle_image" data-fancybox="kwitansi" href="'.base_url().'assets/images/kwitansi/\'+kwitansi["id_perusahaan"]+\'/\'+kwitansi["slug"]+\'"><img class="img-responsive" style="width:100px;" src="'.base_url().'assets/images/kwitansi/\'+kwitansi["id_perusahaan"]+\'/\'+kwitansi["slug"]+\'" /></a></td>';
      foreach ($model->kolom() as $key => $value) {
        if ($key!='id_kwitansi' && $key!='slug') {
            echo '<td>\'+kwitansi["'.$key.'"]+\'</td>';
        }
      }
      ?></tr>';
    });
    $( "#tabelkwitansi>tbody" ).html(hasil);
    if (datahasil.length<perpage) $('#btnnext').attr("disabled", true); else $('#btnnext').attr("disabled", false);
    if (page<=1) $('#btnprev').attr("disabled", true); else $('#btnprev').attr("disabled", false);
    ubahdata(true);
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( "Request failed: " + textStatus );
  });
}

<?php if ($this->keamanan->allowedaction['ubah']) { ?>
  function updatedataajax() {
    var request = $.ajax({
      url: "<?=base_url()?>ajax/updatedatakwitansi",
      method: "POST",
      data: $("#tabelkwitansi .databerubah").serialize(),
      dataType: "json"
    });

    request.done(function( datahasil ) {
      console.log(datahasil);
      dataajax();
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
      dataajax();
    });
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['tambah']) { ?>
  function resettambah(datas=listkolom) {
    $('#inputimageFile').val('');
    $.each(listkolom, function( index, data ) {
      var nilai=(typeof datas[index]['default']!=='undefined')?data['default']:datas[index];
      $('#input'+index).val(nilai);
      if (index=='slug') {
        if (nilai!=''){
          $('.previewimageimageFile').attr('src','<?=base_url().'assets/images/kwitansi/'.$_SESSION['id_perusahaan'].'/'?>'+nilai);
        } else {
          $('.previewimageimageFile').attr('src','<?=base_url().'assets/images/default/kwitansi.jpg'?>');
        }
      }
    });
  }
  function bukaformtambah(){
    resettambah();
    $('#boxindex').hide();$('#boxtambah').show();$('.boxtambah input.form-control').first().focus();
  }
  function bukaformubah(id_kwitansi){
    resettambah(listkwitansi[id_kwitansi]);
    $('#boxindex').hide();$('#boxtambah').show();$('.boxtambah input.form-control').first().focus();
  }

function simpandataajax() {
  var formData = new FormData(document.getElementById('formkwitansi'));
  var url = ($('#inputid_kwitansi').val()==0)?"<?=base_url()?>ajax/tambahdatakwitansi":"<?=base_url()?>ajax/updatedatakwitansi";
  $('#modal-tambah').modal('toggle');
  var request = $.ajax({
    url: url,
    method: "POST",
    data: formData,
    processData: false,
    contentType: false,
    dataType: "json"
  });

  request.done(function( datahasil ) {
    console.log(datahasil);
    $('#boxtambah').hide();$('#boxindex').show();
    dataajax();
  });

  request.fail(function( jqXHR, textStatus ) {
    alert( 'Mohon Cek Kembali Data Anda');
  });
}
window.addEventListener('DOMContentLoaded', (event) => {
  resettambah();
});

<?php } ?>
function hitunghargatotal() {
  $('#inputtotal').val(parseInt($('#inputjumlah').val())*parseInt($('#inputharga_satuan').val()));
}
<?php if ($this->keamanan->allowedaction['tambah'] || $this->keamanan->allowedaction['ubah']) { ?>
function openmodalid_cluster(){
  $('#modalinputid_cluster').modal();
  if (!$.fn.DataTable.isDataTable('#tabellistcluster')){
    $('#tabellistcluster').DataTable();
  }
}
function openmodalid_perumahan(){
  $('#modalinputid_perumahan').modal();
  if (!$.fn.DataTable.isDataTable('#tabellistperumahan')){
    $('#tabellistperumahan').DataTable();
  }
}

function pilihcluster(id,id_perumahan){
  $('#inputid_cluster').val(id);
  $('#inputtext_cluster').val($('.id_cluster'+id+' td:nth-child(1)').html());
  $('#inputid_perumahan').val($('.id_cluster'+id+' td:nth-child(2)').html());
  $('#hidid_perumahan').val(id_perumahan);
  $('#modalinputid_cluster').modal('hide');
}
function pilihperumahan(id,nama){
  $('#inputid_perumahan').val(nama);
  $('#hidid_perumahan').val(id);
  $('#inputid_cluster').val('0');
  $('#inputtext_cluster').val('');
  $('#modalinputid_perumahan').modal('hide');
}
<?php } ?>
<?php if ($this->keamanan->allowedaction['hapus']) { ?>
  function hapusdataajax(id_kwitansi) {
    if (confirm('Apakah Anda yakin ingin menghapus data kwitansi ini ?')) {
      var request = $.ajax({
        url: "<?=base_url()?>ajax/hapusdatakwitansi",
        method: "POST",
        data: {
          'id_user': <?=$_SESSION['id_user']?>,
          'auth_key': '<?=$_SESSION['auth_key']?>',
          '_csrf': '<?=$this->keamanan->generatecsrf()?>',
          'id_kwitansi': id_kwitansi
        },
        dataType: "json"
      });

      request.done(function( datahasil ) {
        console.log(datahasil);
        dataajax();
      });

      request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
        dataajax();
      });
    }
}
<?php } ?>
window.addEventListener('DOMContentLoaded', (event) => {
  $('#boxtambah #formkwitansi').prepend('<div class="form-group col-xs-6 formimageFile">'+htmlinputimage('imageFile','')+'</div>');
$('#filtertanggal').daterangepicker(
  {
    ranges   : {
      'Hari Ini'       : [moment(), moment()],
      'Kemarin'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      '7 Hari Terakhir' : [moment().subtract(6, 'days'), moment()],
      '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
      'Bulan Ini'  : [moment().startOf('month'), moment().endOf('month')],
      'Bulan Lalu'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    startDate: moment().subtract(29, 'days'),
    endDate  : moment()
  },
  function (start, end) {
    $('#inputfiltertanggal').val('\''+start.format('YYYY-MM-DD') + '\' Hingga \'' + end.format('YYYY-MM-DD')+'\'')
  }
);
$('#inputfiltertanggal').val('\''+moment().subtract(29, 'days').format('YYYY-MM-DD') + '\' Hingga \'' + moment().format('YYYY-MM-DD')+'\'');
$("a.popupsingle_image").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	600,
		'speedOut'		:	200,
		'overlayShow'	:	false
	});

});
</script>
