<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_perumahan extends CI_Model {

  function getPemilik(){

    $response = array();

    // Select record
    $this->db->select('*');
    $q = $this->db->get('m_pemilik');
    $response = $q->result_array();

    return $response;
  }

  function kolom($id='') {
    $listkolom = array(
      'id_perumahan'=> ['label'=>'ID Perumahan','default'=>0,'type'=>'text'],
      'nama_perumahan'=> ['label'=>'Nama Perumahan','default'=>'','type'=>'text'],
      'id_perusahaan'=> ['label'=>'Perusahaan','default'=>1,'type'=>'select'],
      'alamat'=> ['label'=>'Alamat','default'=>'','type'=>'textarea'],
      'kota'=> ['label'=>'Kota','default'=>'Surabaya','type'=>'text'],
      'propinsi'=> ['label'=>'Propinsi','default'=>'Jawa Timur','type'=>'text'],
      'telp'=> ['label'=>'Telepon','default'=>0,'type'=>'text'],
      'website'=> ['label'=>'Website','default'=>'','type'=>'text'],
      'bank'=> ['label'=>'Bank Account','default'=>'','type'=>'text'],
      'max_cicilan_cash'=> ['label'=>'Max Cicilan Cash','default'=>1,'type'=>'number'],
      'max_cicilan_kpr'=> ['label'=>'Max Cicilan KPR','default'=>12,'type'=>'number'],
      'max_cicilan_inhouse'=> ['label'=>'Max Cicilan Inhouse','default'=>12,'type'=>'number'],
      'max_persen_dp'=> ['label'=>'Max Persen DP','default'=>20,'type'=>'number'],
      'uang_tanda_jadi'=> ['label'=>'Uang Tanda Jadi','default'=>50000000,'type'=>'number'],
      'aktif'=> ['label'=>'Status','default'=>1,'type'=>'checkbox']
    );
    if (isset($listkolom[$id])) {
      return $listkolom[$id];
    } else {
      return $listkolom;
    }
  }

  function dataupdate($data) {
    $sql = null;
    if (isset($data['id_perumahan'])) {
      $sql = array();
      $query = '';
      foreach ($data['id_perumahan'] as $key => $value) {
        $data['id_user']=isset($data['id_user'])?$data['id_user']:0;
        $data['auth_key']=isset($data['auth_key'])?$data['auth_key']:'-';
        $sql[$key]='EXEC sp_m_perumahan @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\',';
        foreach ($this->kolom() as $id => $label) {
          if (isset($data[$id])) {
            $sql[$key].=isset($data[$id][$key])?' @'.$id.'=\''.$data[$id][$key].'\',':'';
          }
        }
        $sql[$key]=substr($sql[$key], 0, -1);
        $sql[$key].=';';
        $query.=$sql[$key].' ';
      }
      if ($query!=''){
        $q=$this->db->query($query);
        //$sql=$q->result_array();
      }
    }
    return '';
  }

  function datainsert($data) {
    $data['id_user']=isset($data['id_user'])?$data['id_user']:0;
    $data['auth_key']=isset($data['auth_key'])?$data['auth_key']:'-';
    $query='EXEC sp_m_perumahan @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\',';
    foreach ($this->kolom() as $id => $label) {
      if (isset($data[$id])) {
        $data[$id]=($id=='aktif')?(($data[$id]=='on')?1:0):$data[$id];
        $query.=' @'.$id.'=\''.$data[$id].'\',';
      }
    }
    $query=substr($query, 0, -1);
    $query.=';';
    if ($query!=''){
      $q=$this->db->query($query);
      //$sql=$q->result_array();
    }
    return '';
  }

  function datahapus($data) {
    $data['id_user']=isset($data['id_user'])?$data['id_user']:0;
    $data['auth_key']=isset($data['auth_key'])?$data['auth_key']:'-';
    $data['id_perumahan']=isset($data['id_perumahan'])?$data['id_perumahan']:0;
    $query='EXEC sp_m_perumahan @id_user='.$data['id_user'].', @auth_key=\''.$data['auth_key'].'\', @id_perumahan=\''.$data['id_perumahan'].'\', @aksi=\'hapus\';';
    $q=$this->db->query($query);
    return '';
  }

  function perumahanku($datainput){
  $q = $this->load->model('m_user');
	$response = null;
    $data = array(
      'id_perumahan'=>isset($datainput['id_perumahan'])?$datainput['id_perumahan']:0,
      'id_perusahaan'=>isset($datainput['id_perusahaan'])?$datainput['id_perusahaan']:0,
      'lengkap'=>isset($datainput['lengkap'])?$datainput['lengkap']:false,
      'aktif'=>isset($datainput['aktif'])?$datainput['aktif']:2,
      'page'=>isset($datainput['page'])?$datainput['page']:1,
      'perpage'=>isset($datainput['perpage'])?$datainput['perpage']:25,
      'orderby'=>isset($datainput['orderby'])?$datainput['orderby']:'id_perumahan DESC',
      'posisi'=>isset($datainput['posisi'])?$datainput['posisi']:'user',
      'id_user'=>isset($datainput['id_user'])?$datainput['id_user']:0,
      'auth_key'=>isset($datainput['auth_key'])?$datainput['auth_key']:'-',
    );
		$response = array();
		$id_user = htmlspecialchars($data['id_user']);
    $auth_key = htmlspecialchars($data['auth_key']);
		$id_perumahan = htmlspecialchars($data['id_perumahan']);
    $id_perusahaan = htmlspecialchars($data['id_perusahaan']);
    $aktif = (int)$data['aktif'];
    $page = ((int)$data['page']>=1)?(int)$data['page']:1;
    $perpage = ((int)$data['perpage']>=1)?(int)$data['perpage']:25;
    $orderby = $data['orderby'];
		$checkid = ($id_perumahan!=0)?' AND id_perumahan='.$id_perumahan:'';
    $select = ($data['lengkap'])?'*':'id_perumahan,aktif,alamat,kota,nama_perumahan,id_perusahaan,nama_perusahaan';
    $aktif = ($aktif==1||$aktif==0)?' AND aktif='.$aktif:'';
    $tambahan = ' AND id_user='.$id_user.' AND auth_key=\''.$auth_key.'\'';
    $tambahan .= ($id_perusahaan!=0)?' AND id_perusahaan='.$id_perusahaan:'';
    $limit=($data['lengkap'])?'':' OFFSET '.(($data['page']-1)*$data['perpage']).' ROWS FETCH NEXT '.$data['perpage'].' ROWS ONLY';
    $q = $this->db->query('SELECT '.$select.' FROM v_perumahanku WHERE id_user='.$id_user.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit);

		// if ($data['posisi']=='owner') {
		// 	$q = $this->db->query('SELECT '.$select.' FROM v_perumahan_owner WHERE id_user='.$id_user.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit);
		// } else {
		// 	$q = $this->db->query('SELECT '.$select.' FROM v_perumahan_user WHERE id_user='.$id_user.$checkid.$aktif.$tambahan.' ORDER BY '.$orderby.$limit);
		// }
    //$q = $this->db->query('exec sp_data_m_perumahan @id_usr ='.$id_user);
		$response = ($id_perumahan==0)?$q->result():$q->row();
    return $response;
  }


}
