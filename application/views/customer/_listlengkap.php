<?php
$this->load->model('m_customer');
$id_user=isset($_SESSION['id_user'])?$_SESSION['id_user']:(isset($id_user)?$id_user:0);
$auth_key=isset($_SESSION['auth_key'])?$_SESSION['auth_key']:(isset($auth_key)?$auth_key:'');
$posisi=isset($_SESSION['posisi'])?$_SESSION['posisi']:(isset($posisi)?$posisi:'user');
$id_perusahaan=isset($_SESSION['id_perusahaan'])?$_SESSION['id_perusahaan']:(isset($id_perusahaan)?$id_perusahaan:0);
$datas = $this->m_customer->customerku(['id_user'=>$id_user,'lengkap'=>true,'posisi'=>$posisi,'auth_key'=>$auth_key,'id_perusahaan'=>$id_perusahaan,'status'=>'r']);
$model=$this->m_customer;
 ?>
<table id="tabellistcustomer" class="table table-bordered table-striped">
  <thead>
  <tr>
    <?php
    foreach ($model->kolom() as $key => $value) {
      if ($key!='id_customer'&&$key!='id_perusahaan'&&$key!='aktif'){
        echo '<th>'.$value['label'].'</th>';
      }
    }
    ?>
  </tr>
  </thead>
  <tbody>
    <?php
    foreach ($datas as $keydt => $data) {
      echo '<tr class="id_customer'.$data->id_customer.'" onclick="pilihcustomer(\''.$data->id_customer.'\',\''.$data->nama_customer.'\',\''.$data->alamat.'\')">';
      foreach ($model->kolom() as $keyk => $kolom) {
        if ($keyk!='id_customer'&&$keyk!='id_perusahaan'&&$keyk!='aktif'){
          echo '<td>'.$data->$keyk.'</td>';
        }
      }
      echo '</tr>';
    }
    ?>
  </tbody>
  <tfoot>
  <tr>
    <?php
    foreach ($model->kolom() as $key => $value) {
      if ($key!='id_customer'&&$key!='id_perusahaan'&&$key!='aktif'){
        echo '<th>'.$value['label'].'</th>';
      }
    }
    ?>
  </tr>
  </tfoot>
</table>
